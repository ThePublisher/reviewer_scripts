# Changelog

This file documents all notable changes to the GreaseMonkey scripts in this repository.

## GCR Bookmark Buttons v2 ([gc_BookmarkButtons.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gc_BookmarkButtons.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.03 | 2017-04-17 | Changes to accomodate for new bookmark list page. |
|  v02.02 | 2017-04-01 | Minor code cleanup. |
|  v02.01 | 2017-03-19 | Fixed logic for declaring status variable. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v01.82 | 2014-03-13 | Fix to accommodate site change. |
|  v01.81 | 2011-11-09 | Fix to accommodate site change. |
|  v01.80 | 2011-07-29 | Added Refresh link that works even after a bulk delete. |
|  v01.70 | 2011-07-25 | Update for site changes. |
|  v01.60 | 2010-08-09 | Update for site changes. Added update notification. |
|  v01.50 | 2010-06-22 | Duplicate paging controls to top of list. |
|  v01.40 | 2010-01-19 | Update for site changes. |
|  v01.30 | 2007-10-11 | Updated to accomodate site changes to bookmarks. |
|  v01.20 | ????-??-?? | Removed blue-bar code, since it has been incorporated in the site. |
|  v01.10 | ????-??-?? | Added "blue-bar" display to bookmark table to increase readability. |
|  v01.00 | ????&#8209;??&#8209;??&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |

## GCR Log Maximizer v2 ([gc_Log_Maximizer.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gc_Log_Maximizer.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.02 | 2017-08-23 | Fix SignedInAs. |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v04.00 | 2016-02-02 | Switch to Markdown formatting. |
|  v03.01 | 2015-10-31 | Fixed bug which occurred when switching to Update coordinate type log. |
|  v03.00 | 2015-04-20 | Added Displayed Font size controls. |
|  v02.99 | 2015-03-31 | Fix for site change. |
|  v02.98 | 2015-03-25 | Fix for site change. Converted to new version checker. |
|  v02.97 | 2013-06-04 | Fix for site change. |
|  v02.96 | 2011-05-17 | Added work-around for FF4 bug. |
|  v02.95 | 2011-05-01 | Fix for site change. |
|  v02.94 | 2011-02-01 | Fix to accommodate the site's multi-language support. |
|  v02.93 | 2011-01-27 | Added store/retrieve of last note text submitted, so log text is not lost due to site failures. Saved text is retrieved via User Script Menu option. |
|  v02.92 | 2010-11-10 | Fix for site change. Added duplicate Submit button, based on user request. Reduced granularity of text box adjustment to 4 pixels, for greater control. |
|  v02.91 | 2010-10-16 | Fix to accommodate the site's multi-language support. |
|  v02.90 | 2010-08-05 | Added Auto-Visit function for trackables. |
|  v02.82 | 2010-07-30 | Fix for site change. Added option to always open with smilie bar displayed. |
|  v02.81 | 2010-05-18 | Minor fix to make sure it works with other scripts. |
|  v02.80 | 2010-05-06 | Fix for work with trackable logs. |
|  v02.70 | 2010-02-24 | Fix for site change. Added update notification. |
|  v02.06 | 2010-02-13 | Restored ability to hide/show inventory list. |
|  v02.05 | 2010-02-13 | Update for site changes. Removed left/right arrows, as the text box now auto-resizes to screen width. |
|  v02.04 | 2009-08-03 | Removed 2 lines of text explaining encryption function. |
|  v02.03 | 2009-02-04 | Minor change to support the PDA Field Notes script. |
|  v02.02 | 2008-11-10 | Added menu option to select font for text area. Choose the font from the font list, and select "Use currently selected font for text area" from the Greasemonkey menu. Added "Smiley Bar" to replace the pop-up window. |
|  v02.01 | 2008-07-27 | Added coordinate paste-box. |
|  v02.00 | 2008-07-24 | Added edit controls for Bold, Italics, Strikethrough, Fonts, Text Color, and Hyperlinks. Added Date Detection, to notify when selected date is not the current date. Added ability to rename the months to that of other languages. Added control to change selected date to current date. Added menu option to control if inventory is initially hidden or shown. Fix to accommodate website changes. |
|  v01.05 | 2007-10-09 | Minor fix to accommodate website changes. |
|  v01.04 | ????-??-?? | Minor fix to accommodate website changes. |
|  v01.03 | ????-??-?? | Cosmetic update. Replaced text-based controls with icon images from the famfamfam.com "Silk" icon collection. Pretty! |
|  v01.02 | ????-??-?? | Text quoting added. Copy text to text box and click ">". Line breaks will be added at the approxomate line width, and each line prepended with a ">". |
|  v01.01 | ????-??-?? | Bug inventory is hidden, and inventory count added next to submit button if inventory not empty. Click on inventory count to show/hide the inventory selector box.  Remaining Characters display added. Count now included in validating submission. Turn on/off by clicking the # symbol. Automatically turns on when count is below 100. |
|  v01.00 | ????&#8209;??&#8209;??&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |

## GCR Quick Hint v2 ([gc_Quick_Hint.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gc_Quick_Hint.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v01.02 | 2010-01-22 | Update for site changes.  |
|  v01.01 | 2008-07-26 | Updated for current site configuration. |
|  v01.00 | 2005&#8209;12&#8209;19&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |

## GCR Bookmark Processor v2 ([gcr_Bookmark_Processor.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Bookmark_Processor.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.02 | 2017-04-17 | Changes to accomodate for new bookmark list page. |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v02.00 | 2016-05-05 | Added Queue note display. Added browser positioning and publishing for all open Review Pages. Added Watch and unwatch, options. Simplified interface. Added option to copy all displayed cache waypoint IDs to clipboard. This provides a way to split or combine bookmark lists. Duplicates the paging control to the top of the page.  |
|  v01.20 | 2014-03-13 | Fix for site update. |
|  v01.10 | 2011-12-03 | Added Lock, Unlock, and Post Log options. |
|  v01.01 | 2011-11-11 | Fix for site update. |
|  v01.00 | 2011&#8209;08&#8209;27&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |

## GCR Challenge Checker v2 ([gcr_Challenge_Checker.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Challenge_Checker.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v01.01 | 2016-05-30 | Added check for cache & checker tag waypoint ID match. |
|  v01.00 | ????&#8209;??&#8209;??&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |

## GCR Bookmark Scheduled Archives v2 ([gcr_Bookmark_SBA_Caches.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Bookmark_SBA_Caches.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v02.33 | 2016-01-25 | Update for site changes. |
|  v02.32 | 2015-03-30 | Update for site changes. |
|  v02.31 | 2014-03-18 | Update for site changes. |
|  v02.30 | 2011-05-07 | Update for site changes. Added Auto-Update routine. |
|  v02.20 | 2010-01-28 | Update for site changes. |
|  v02.10 | 2008-07-28 | Update for site changes. Custom settings are now stored in GM cookies. |
|  v02.00 | 2006-10-05 | Added custom second link. |
|  v01.20 | ????-??-?? | Fix to accomodate gc.com page changes. |
|  v01.10 | ????-??-?? | Update for Greasemonkey 0.6.4. |
|  v01.00 | ????&#8209;??&#8209;??&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |

## GCR GMail Review Links v2 ([gcr_Gmail_Review_Links.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Gmail_Review_Links.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v02.21 | 2015-10-25 | Fix for Greasemonkey change. |
|  v02.20 | 2012-09-01 | Fix for internal Gmail changes. |
|  v02.10 | 2010-08-16 | Performance improvement. |
|  v02.00 | 2010-08-16 | Works with latest version of Gmail. Added options to allow user to set parameters for links. Added option to auto-open the associated cache page. |
|  v01.91 | 2010-07-12 | Fix for change of cache page URL. Added auto-update. |
|  v01.90 | 2009-10-25 | Fix link formatting. |
|  v01.80 | 2009-09-12 | Fix for API getConvRhsElement no longer working. |
|  v01.70 | 2008-12-01 | Bug fix. Script was not always firing. |
|  v01.60 | 2008-09-14 | Fix for Firefox 3. |
|  v01.50 | 2008-05-28 | User-editable subdomain (www/obey0). Defaults to www. |
|  v01.40 | 2007-12-06 | Accommodate GMail v2 interface, and low-speed html interface. Beta release. |
|  v01.30 | 2007-11-01 | Changed to open links in obey environment. |
|  v01.20 | 2006-08-24 | Changed number of logs displayed on cache page to 10. |
|  v01.10 | ????-??-?? | Added Bookmark names. |
|  v01.00 | ????&#8209;??&#8209;??&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |

## GCR Hint Killer v2 ([gcr_Hint_Killer.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Hint_Killer.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v01.31 | 2016-07-26 | Added link to activate and unwatch with 1 click. |
|  v01.30 | 2016-03-10 | Added coordinate paste-in box to Edit Listing Page. Fix for site change. |
|  v01.20 | 2007-11-07 | Added 'decrypt' option. |
|  v01.10 | 2007-10-31 | Change to work with multiple subdomains. |
|  v01.00 | ????&#8209;??&#8209;??&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |

## GCR Kill Zone v2 ([gcr_Kill_Zone.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Kill_Zone.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v01.97 | 2016-02-02 | Fix for Markdown. |
|  v01.96 | 2015-12-15 | Bug fix. |
|  v01.95 | 2014-03-15 | Fix for site update. |
|  v01.94 | 2013-06-22 | Fix for site update. |
|  v01.93 | 2011-12-12 | Fix for site update. Added log tagging, to identify script-generated logs. |
|  v01.92 | 2011-05-06 | Fix for site update. |
|  v01.91 | 2011-03-07 | Fix for site update. Added auto-tab closing to links. Make changes so that Kill Zone and Owner Action Needed scripts can co-exist better when both active at the same time. |
|  v01.90 | 2010-12-08 | Added interface to set log texts, bookmark setting, and warning duration. |
|  v01.81 | 2010-11-18 | Fix to accomodate site change. Added auto update notification. |
|  v01.80 | 2010-07-18 | Terminate if not signed on as reviewer. |
|  v01.70 | 2008-06-03 | Fix to accomodate change to log type id. |
|  v01.60 | 2008-01-07 | Fixed problem with submit button being disabled (conflict with another script). |
|  v01.50 | 2007-11-03 | Bug fix. |
|  v01.40 | 2007-10-31 | Change to work with multiple subdomains. |
|  v01.30 | ????-??-?? | Fix to accomodate page change. Added date stamp to bookmark entry name. |
|  v01.20 | ????-??-?? | Added separate links for Kill Zone operations. Updated language for both notes. |
|  v01.10 | ????-??-?? | Update for Greasemonkey 0.6.4. |
|  v01.00 | ????&#8209;??&#8209;??&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |

## GCR Kill Zone/Owner Action for Review Page v2 ([gcr_Kill_ZoneOwner_Action_for_Review_Page.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Kill_ZoneOwner_Action_for_Review_Page.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v01.00 | ????&#8209;??&#8209;??&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |

## GCR Kill Zone/Owner Action for Log Book v2 ([gcr_KZOA_Logbook.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_KZOA_Logbook.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v01.00 | ????&#8209;??&#8209;??&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |

## GCR Log Entry v2 ([gcr_Log_Entry.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Log_Entry.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.02 | 2017-08-17 | Fix SignedInAs. |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v02.35 | 2015-11-18 | Bug fix. |
|  v02.34 | 2015-09-13 | Fixed log deletion, when requested from review page. |
|  v02.32 | 2013-06-17 | Added icon for 'restore previous log', and removed that options from the GM menu. Log text now gets saved whenever a log page unloads, regardless of whether the Submit button was clicked. This means you can recall your text if you accidently close a tab, close the browser, click a link or refresh the page. |
|  v02.31 | 2013-06-04 | Fixed focus problem with Update Coordinates logs, so the submit button is activated. Added new auto-log type, for Traditional caches that are too close to a multi or puzzle stage. Added %dlat% and %dlon% replacement variables, which are the decimal-degree coordinates of the cache being posted to. This is to allow the creation of "all nearby caches" links within the logs. Added "Copy to Other Tabs" link, which copies the current text to any other open log entry pages. Will no longer replace any existing log text when changing log type to Publish or Retract. Removed obsolete options for No Home Coordinates.Added export/import option to make it easier to set up all templates at once for new reviewers. |
|  v02.30 | 2011-09-28 | Added %wptno% log replacement value, for the numeric waypoint id. |
|  v02.20 | 2011-02-15 | Added ability to delete logs and close tabs. |
|  v02.11 | 2010-11-10 | Update for site changes. |
|  v02.10 | 2010-05-18 | Added multiple language/version support. Enclose various language versions in &lt;lang id="xxx"&gt;&lt;/lang&gt; tags, and user can select the language from an array of buttons next to the log type selector. Direction to nearby cache now returns an empty string if the distance is 0 feet, instead of "Here"; |
|  v02.01 | 2010-04-05 | Bug fix (text for Retract Listing). Added auto update notification. |
|  v02.00 | 2010-01-26 | Added option to always set Cannot Delete for Review Notes. |
|  v01.91 | 2010-01-13 | Update for site changes. |
|  v01.90 | 2009-12-10 | Added Owner and Proximity Direction replacement variables. |
|  v01.80 | 2009-11-03 | Added template system for Proximity, Vacation, Home Coordinate, Handicap, Update Coordinates logs. Must be using the Review Page script v6.6 or later to use. |
|  v01.70 | 2009-06-26 | Added ability to pass default text in a parameter. |
|  v01.60 | 2008-07-24 | Fix to accommodate site changes. |
|  v01.50 | 2008-06-06 | Change for new Review Note type. |
|  v01.40 | 2007-10-31 | Change to work with multiple subdomains. |
|  v01.30 | ????-??-?? | Reworked auto-submit code. Added code for auto-submitting Publish logs. |
|  v01.20 | ????-??-?? | Update for Greasemonkey 0.6.4. |
|  v01.10 | ????-??-?? | Handicap notification log now automatically submits itself. Changed text of the Handicap notification log to reflect new PQ attributes filter. Cleaned up URL parameter parsing. |
|  v01.00 | ????&#8209;??&#8209;??&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |

## GCR Log Review Queue v2 ([gcr_log_review_queue.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_log_review_queue.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v01.64 | 2016-06-30 | Fix for site change. |
|  v01.63 | 2016-06-22 | Accomodate bad coordinates change data. |
|  v01.62 | 2015-11-05 | Fix for protocol security change. |
|  v01.61 | 2015-01-20 | Fix to remove distance from 1st column. |
|  v01.60 | 2013-08-29 | Worked caches that are not removed from the list are now tagged so they can easily be reworked with a single click. |
|  v01.51 | 2012-12-17 | Fix to compensate for site change. |
|  v01.50 | 2012-02-27 | Hide country/state lists until hovered. Added a 3/4 second delay before hover is activated. Moved the permalink to far right. Added ability to show/hide the log text. Added option to multi-open the logbook page instead of the cache page. This works with the new Logbook UA/Kill Zone script. |
|  v01.41 | 2012-05-11 | Fix to compensate for site change. |
|  v01.40 | 2011-09-21 | Added links to auto-check NA logs for caches that are duplicates of existing logs. Tags caches you've opened, so they can all be set to 'worked' at once. Checks for multiple Update Coordinate logs to the same cache, and flags them. Added option to auto-run the litmus test when opening review pages. Added activity log, per user request. Added ability to turn on/off the "compressed header" mode. |
|  v01.30 | 2011-07-13 | Added links to auto-check NA logs for caches that are either disabled or archived. |
|  v01.20 | 2011-07-11 | Added highlighting for move distances beyond a specified distance. Distance is configurable, in feet or meters. |
|  v01.10 | 2011-07-07 | Added Settings interface. Added log text to the Needs Archive logs. Added All Check/Uncheck box for the Worked check box. |
|  v01.00 | 2011&#8209;06&#8209;29&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |

## GCR Mem 2 Bookmark v2 ([gcr_Mem_2_Bookmark.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Mem_2_Bookmark.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.02 | 2017-04-17 | Changes to accomodate for new bookmark list page. |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v01.11 | 2014-11-14 | Fix to accommodate non-English page displays. |
|  v01.10 | 2012-12-26 | Added a text-entry box to allow the pasting of a list of caches that can be added to memory (and then added to a bookmark). |
|  v01.00 | ????&#8209;??&#8209;??&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |

## Geocaching Owner Action Needed v2 ([gcr_Owner_Action_Needed.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Owner_Action_Needed.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v01.46 | 2016-02-02 | Fix for Markdown. |
|  v01.45 | 2014-03-15 | Fix for site update. |
|  v01.44 | 2013-06-22 | Fix for site update. |
|  v01.43 | 2012-12-12 | Fix for site update. Added log tagging, to identify script-generated logs. |
|  v01.42 | 2011-05-06 | Fix for site update. |
|  v01.41 | 2011-03-07 | Fix for site update. Added auto-tab closing to links. Make changes so that Kill Zone and Owner Action Needed scripts can co-exist better when both active at the same time. |
|  v01.40 | 2010-12-08 | Added interface to set log texts, bookmark setting, and warning duration. |
|  v01.31 | 2010-11-18 | Update for site changes. Added auto update notification. |
|  v01.30 | 2010-07-18 | Terminate if not signed on as reviewer. |
|  v01.20 | 2010-02-01 | Update for site changes. |
|  v01.10 | 2008-01-07 | Fixed problem with submit button being disabled (conflict with another script). |
|  v01.00 | 2007&#8209;11&#8209;05&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |

## Geocaching Profile Page Enhancement v2 ([gcr_Profile_Page.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Profile_Page.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v01.21 | 2013-01-29 | Update for Greasemonkey change. |
|  v01.20 | 2010-10-21 | Removed Owner Notes (obsolete). Added icon links to show all hides and finds. |
|  v01.10 | 2005-12-06 | Updated for Greasemonkey 0.6.4 (Firefox 1.5). |
|  v01.00 | ????&#8209;??&#8209;??&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |

## Geocaching Review Queue Highlighter v2 ([gcr_Queue_Loc_Hilite.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Queue_Loc_Hilite.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v01.52 | 2015-11-01 | Added ability to define rectangular areas. Fix for site change. |
|  v01.51 | 2012-02-15 | Bug fix. |
|  v01.50 | 2012-01-19 | Added ability to hide non-highlighted caches. Plus/Minus icons near the top toggle the feature off/on. Hover over the cache count in the upper left to see how many are displayed, and how many are hidden. Fix to accommodate change in map URL. |
|  v01.40 | 2011-05-06 | dded code to detect when the Review Queue script is loaded before performing the highlighting. It is no longer necessary to consider which script runs first. |
|  v01.30 | 2011-05-05 | Update to accommodate site change. Added Auto-updater. |
|  v01.20 | 2008-07-23 | Update to accommodate site change. |
|  v01.10 | 2008-04-01 | Update to accommodate site change. |
|  v01.00 | 2005&#8209;11&#8209;05&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |

## GCR Review v2 ([gcr_Review_Page.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Review_Page.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.03 | 2017-05-12 | Removed temp workaround needed during the introduction of the new "Log Date Set By Player" column. |
|  v02.02 | 2017-04-01 | Minor code cleanup. |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v07.98 | 2016-06-29 | Fix for site change. |
|  v07.97 | 2016-05-29 | Added Challenge Checker auto-open, and setting for use of an alternate profile, if the reviewer has finds on their profile. Added waypoint ID to data passed to challenge checker script. Added option to change the copy icon's function to copy a Markdown link. Fixed message typo. |
|  v07.96 | 2016-05-17 | Bug fix. |
|  v07.95 | 2016-05-17 | Bug fix. |
|  v07.94 | 2016-05-17 | Option to hide ZoneCatcher and Litmus bars if no messages. Fix map auto-zoom to compensate for site changes. Added lat + lon option to clipboard copy function. Added alert for missing coordinate type entry. Added alert for possible DDMMSS usage. If event date more than 90 days in the future, date hover-text displays the date the event can be published. |
|  v07.93 | 2015-04-07 | Fix for site not providing litmus data on archived caches. |
|  v07.92 | 2015-04-06 | Added copy-to-clipboard for litmus entries. Works with or without the old litmus link. |
|  v07.91 | 2016-05-17 | Added multiple outputs for the copy-to-clipboard icon. Added multiple text options for the "auto clean up" icon. Added fix for removal of beta litmus test. |
|  v07.90 | 2014-09-17 | Fix for resizing images. Owner link now opens in new tab. Added display of Placed Date day of the week to the date hover text. Changed Litmus Test Viewer to use the new Beta Litmus Test. Added icon to copy formatted cache name & ID to the clipboard. Added SnapBack code to settings interface. Removed large amount of obsolete code. |
|  v07.89 | 2013-11-11 | Bug fix. |
|  v07.88 | 2013-11-04 | Bug fix. |
|  v07.87 | 2013-11-03 | Bug fix. |
|  v07.86 | 2013-10-29 | Added support for browser positioning and publishing from the bookmark page and review queue. Fixed log note counter. Positioning to logs is now done by using the anchor 'log_table', which is designed to also work with the Review Kill Zone Links script. Removed no longer needed code that fixed popup log profile links. Update to code that hides empty Cache Review Notes, due to site changes. Added button to insert timestamp and signature into the Cache Review Notes. |
|  v07.85 | 2012-12-12 | Added 'No Final Waypoint' check for Wherigo caches. Added 'Cache Page is Locked' message to the notification area. |
|  v07.84 | 2012-04-12 | Changed wording on User Note link, and added a link to directly open a Post User Log window. |
|  v07.83 | 2013-11-03 | Cleaned up how cache notes are displayed. Cache Note auto-refreshes when the entry pop-up window is closed. Changed Javascript warning to use the General Warning area. Added additional Final Waypoint warnings, for more than 2 miles from posted coordinates, and multiple entries. Fixed minor bug caused by site change. |
|  v07.82 | 2011-11-10 | Bug fix. |
|  v07.81 | 2011-11-10 | Changed closing reviewer note timing, which will stop the constant box resizing. |
|  v07.80 | 2011-11-08 | Fixed bug in the auto-lock/unlock routine. Fix to compensate for Google map change. Fix to litmus test icon, to accomodate site change. Moved reviewer notes to the top, and put them into collapsed mode if empty. |
|  v07.79 | 2011-10-05 | Added PM cache icon. Changed date calculations to work regardless of date format perference. |
|  v07.78 | 2011-09-23 | Added warning for puzzles & multis without a final waypoint. |
|  v07.77 | 2011-09-15 | Added ability to edit the log text for the database cleanup function. Added optional automatic resizing of short/long description images over 600px wide. |
|  v07.76 | 2011-08-18 | Fix to accomodate FF6. Fixed bug for auto-close on vacation logs. |
|  v07.75 | 2011-08-09 | Added options to set pref for auto submit & close tab for proximity and vacation logs. |
|  v07.74 | 2011-08-08 | Added URL parameter to force running the litmus test ('AutoLitmus=On') - not case sensitive - to interface with the Log Queue. Moved most settings to a single interface page. Cleaned up no-longer-used code. |
|  v07.73 | 2011-05-23 | Removed some unnecessary whitespace. Added control to add cache to local storage memory (for automatically adding to bookmarks, etc.). Changed first 2 cells of cache info table to fixed length, so icons in table with stay in the same position on all pages. Added temporary fix for missing favorite icon. |
|  v07.72 | 2011-04-06 | Added ability to lock and unlock via a passed parameter. Updated code for creating quick action buttons, to be compatable with FF3 & FF4. |
|  v07.71 | 2011-03-10 | Change links in the navigation boxes to open new tabs. |
|  v07.70 | 2011-03-08 | Added one-click log deletion. Changed handicap message to auto-close after posting. Fix to accommodate changes to Google Map (restored Map Type and Zoom customization). Using green button to publish caches now auto-closes the tab. Fixed small bug involving identifying earthcaches, caused by site update. |
|  v07.62 | 2011-02-01 | Change to accommodate site update. |
|  v07.61 | 2011-01-19 | Changed geolocation to use JSON, added more descriptive error messages when unable to retrieve data, and changed call routine to only call when cache is not published. |
|  v07.60 | 2010-12-10 | Added ability to set default map type. Added non-review note alert next to Publish button. Remove background image. |
|  v07.50 | 2010-10-09 | Fix thing broken by the new site release. Added ability to automatically change the map zoom setting, when viewing a cache with no additional waypoints. Value entered is the number of 'clicks' from the system's default setting. |
|  v07.43 | 2010-09-20 | Added breadcrumb links for maps and logs. Fixed proximity template for disabled nearby caches. Removed duplicate "archived logs" icon from the list of nearby caches. |
|  v07.42 | 2010-07-29 | Detects proximity of nearby caches by checking cell class, instead of interpreting distance value. Added code to accept distances in meters or km. |
|  v07.41 | 2010-07-28 | Beta fix for site change. Fix for cache names with single quotes in the name. Fix for when geo-location site is down. |
|  v07.40 | 2010-06-14 | Redesign of menu box. Added unique icons for each function. Moved code for Edit Waypoint link from the waypoint script to this script. Added error/warn count to display next to the litmus link. |
|  v07.31 | 2010-06-05 | Fix to allow script to run for other languages. |
|  v07.30 | 2010-06-04 | Site change updates, and improvements: Fixed yellow-flag showing when it should be red (no waypoints for nearby cache); Fixed Litmus results when using non-English setting; Fixed display problems in Litmus viewer; Fixed terra-server map links; Added display of URL for Related Websites, so it is no longer necessary to hover over the link to see the URL. |
|  v07.22 | 2010-04-27 | Additional update to accomodate site changes (duplicate links). |
|  v07.21 | 2010-04-01 | Update to accomodate site changes. |
|  v07.10 | 2010-03-23 | Added highlighting to Related Web Page, when there is an actual URL attached to it. Also changed the hover text to be the URL, so reviewer has an idea of what it is before clicking on it. If there is no Related Web Page, text converts to '(none)'. |
|  v07.00 | 2010-03-10 | Added auto update notification. Added coordinate/place checking via geolocation. Fixed 14-day date checking to include CITO events. Fixed problem with opening the litmus viewer before the litmus data has loaded. Changed log quotes to stand out (similar to that on cache pages). Script namespace updated to current script host. This necessitates that the old version be uninstalled before installing this version. |
|  v06.90 | 2010-02-03 | Fixed background color for cache note. |
|  v06.80 | 2010-01-13 | Update for site changes. |
|  v06.70 | 2009-12-10 | Added direction from nearby cache as a replacement variable. Added Cache Owner as a replacement variable for all auto-log types. Added Proximity Direction as a replacement variable for the proximity auto-log type. Added alert for events over 90 days in advance. Added hover-text to date to always show the number of days away. Added code to accommodate Review Queue Note. |
|  v06.60 | 2009-10-29 | UAdded automated Proximity, Vacation, and Home Coordinate log links, using customizable templates. Also change the Handicap log and Update Coordinates log to use the same template system. For unpublished Events, calculates days to event date, and shows a warning if less than 14 days from the current date. Must be using Log Entry script v1.8 to make use of the templates. |
|  v06.50 | 2009-10-08 | Fix to accommodate system change of how disabled and unpublished cache names are displayed in the nearby caches list. |
|  v06.40 | 2009-06-26 | Changed Update Coordinates link to only appear on Published caches. Added "Clean Up" link, for disabled, unpublished caches. Needs latest version (v01.70 of the Log Entry script. Fixed "School" link. Fixed problem with litmus auto-run options. Added link for displaying Government location on Google maps. |
|  v06.30 | 2008-11-13 | Added "Litmus failed to complete" notice, rather than the never-ending hourglass icon. Fixed problem with the litmus auto-run options duplicating when you open/close the litmus viewer multiple times. |
|  v06.20 | 2008-11-10 | Added 'action' parm to allow watch, unwatch, hold, unhold, and publish caches from other pages. Fix to accommodate fewer map links on some non-US review pages. |
|  v06.10 | 2008-10-21 | Change to how nearby schools are displayed, to accommodate Google Maps change. |
|  v06.00 | 2008-08-05 | Added separate link to open Change Coordinates log in a new window. Removed redundant labeling from Google map link. Added highlight for undefined cache sizes. |
|  v05.90 | 2008-07-24 | Removed extraneous text from bottom of Litmus viewport. Fixed bug with nearby cache's map link. |
|  v05.80 | 2008-07-23 | Minor bug fix to Litmus viewport open/close. |
|  v05.70 | 2008-07-23 | Fix to accommodate site changes. Changed litmus viewer to only show litmus data, and not the page navigation. Rewrote additional map code. Moved Driving Direction Map to the bottom of the map list. |
|  v05.60 | 2008-06-10 | Change to litmus viewer map reviewer, to Accommodate changes to site. |
|  v05.50 | 2008-06-06 | Bug fix. Opening Litmus Viewer was reseting the tabs favicon. |
|  v05.40 | 2008-06-06 | Litmus page viewport can show in errors-only mode (default). This can be turned off from the menu. Added litmus results indicator to show in page tab. This can be turned off from the menu. Added link to show cache page in all-logs mode. Changed GPX link to graphic. Add link to view cache images in Gallery mode. Fix to accommodate new Reviewer Note (Published) log. Added script detection for short/long descriptions. Includes temporary fix to change litmus targets from "blank" to "_blank". |
|  v05.30 | 2008-03-19 | Improved Background Image removal - anchor in the page URL will no longer cause it to fail. Added options to choose when to auto-run litmus test, based on current cache page status (watched, disabled, held). |
|  v05.20 | 2008-02-20 | Minor fix to cache/owner notes, to accommodate cache/owner names that includes non-alphanumeric characters. A small subset of characters would cause the pop-up window to not display. |
|  v05.10 | 2008-02-08 | Litmus viewer links now open in a new tab. User can select if Litmus viewer cache page links open Reviewer pages instead. Moved most-used switches to options that can be set from the Tools/Greasemonkey menu. Background Image block removed if no background image specified. Added quick-access links for drop-down options watch/unwatch, hold/unhold, and publish. Change profile links to open in current domain. Fixed broken Yahoo maps. |
|  v05.00 | 2007-11-01 | Added ability to customize radius size and display from Tools/Greasemonkey menu, for both regular Google and RR maps. Added map link to display nearby schools. Change to work with multiple sub domains. |
|  v04.91 | 2007-09-15 | Minor fix to accommodate change of cache name. |
|  v04.90 | ????-??-?? | Added hourglass wait icon to nearby additional waypoints flags. Added support code Additional Waypoints link on archived logs page. |
|  v04.80 | ????-??-?? | Added link to download GPX files, with additional waypoints included. |
|  v04.70 | ????-??-?? | Added Litmus Test preview. Preview only occurs automatically on unpublished review pages, but you can click the icon to force a preview (or refresh an existing one). A script switch is available for those that want to turn this option off (dial-up users, perhaps). |
|  v04.60 | ????-??-?? | Changed proximity highlighting to ignore virtuals and webcams. Changed nearest-caches MAP link to show that cache, and the primary cache in the LocusPrime Google Map. Title box changes to red for archived caches. |
|  v04.50 | ????-??-?? | Minor fix to accommodate website change. |
|  v04.40 | ????-??-?? | Minor fix to accommodate website change. |
|  v04.30 | ????-??-?? | Removed Lost Outdoors map link. Cache name block background turns gray if cache is in disabled status. |
|  v04.20 | ????-??-?? | Update for Greasemonkey 0.6.4. |
|  v04.10 | ????-??-?? | Added Proximity warning to list of nearby caches. Changes background color if cache is closer than 0.1 mile. If there is at least one cache closer than 0.1 mile, the background of the cache-type icon will also change color. Does not apply if current cache is Event or CITO. Handicap Accessible link is now turned off if cache is an Event or CITO type. Added archived logs link to list of nearby caches. Added map link LocusPrime Google Map. Added 1-Click Driving Directions from your home coordinates. (Optional, leave HomeLabel blank if you don't want to use it.) |
|  v04.00 | ????-??-?? | Owner Notes display is now created locally, rather than rendered through an iFrame from a remote site. Only the actual note text is fetched from another site. Added Refresh link for Cache Notes. You no longer need to reload the entire page to see your note changes. Added links to launch page showing all nearby caches, and all nearby caches of the same type (useful for checking for nearby events on the same day). Changed 'Google Maps' switch to 'More Maps'. Replaced Google map with link that shows waypoint in the info bubble. Added options for satellite and hybrid map versions. Added links for GC Terraserver, and Lost Outdoors. Added a "Large" version of the Geocaching.com Maps display. Code change should prevent "phantom" empty cache notes from appearing on a page. Bug fix - not all DOM elements were being loaded, based on the switch settings. |
|  v03.20 | ????-??-?? | Replaced characters Greasemonkey 0.5.0 can't handle. |
|  v03.10 | ????-??-?? | Changed owner/user notes to new server. |
|  v03.00 | ????-??-?? | Cache Notes. Similar to Owner notes, but attached to a specific cache. Handicap Accessible note is now a link. Opens up tab for a Reviewer Note, pre-filled with appropriate note. (Requires use of "GC Log Entry" script.) |
|  v02.00 | ????-??-?? | Displays Owner Notes in the owner stats area. Adds link to Archive/Unarchive a cache, and for Reviewer Notes. Put cache and/or owner name on a separate line, to make copying to clipboard easier. (Thanks to huggy\_d1 for the idea!) Highlights links if owner's hides and/or finds are at or below a specified value. (Thanks to huggy_d1 for the idea!) |
|  v01.10 | ????-??-?? | Changes size and color of the two elements you most often need to copy to the clipboard - the cache name and waypoint ID. Adds a new link that will open the cache in its normal page format in a new window/tab. Cache owner shown in larger size and in blue. However, if the listed name is different from their actual name, it is shown in red. Shows a 'handicap accessible?' notification if Terrain is rated 1, and the Wheelchair accessible attribute icon is not shown. Shows owner distance from cache in red, if greater than specified limit. BUG FIX: Now converts distance properly if being displayed if feet instead of miles. Adds links to redisplay the reviewer page with 5, 10, 15, or 20 nearby caches listed. Adds Google Maps and Google Satellite to the available map links. Map will display a push-pin	at the cache's coordinates. NOTE: The black dot on the push-pin indicates the location, not the point of the push-pin. |
|  v01.00 | ????&#8209;??&#8209;??&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |

## GCR Review Queue v2 ([gcr_Review_Queue.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Review_Queue.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v03.51 | 2016-06-29 | Added icon to launch High Submissions Analyser. Shift-click to set value. |
|  v03.50 | 2016-06-29 | Fix for site change. |
|  v03.49 | 2015-01-28 | Added submitted date to queue. Restored watched icon. |
|  v03.48 | 2015-01-22 | Bug fix. |
|  v03.47 | 2015-01-20 | Fix for site change. |
|  v03.46 | 2014-09-17 | Removed code for controlling the review queue note in the review page. This is now handled completely in the review page script. Clean-up of other obsolete code. |
|  v03.45 | 2014-03-11 | Added pending time publishing display, and link to timed publishing bookmark. Added ability to open review pages with archived nearby caches filtered out. Added ability to reposition all open review pages, and to publish all open review pages. |
|  v03.44 | 2012-09-25 | Added support for displaying queue notes in bookmarks (using the bookmark processing script). Filtering bugs fixed. Fix for Firefox 12 changes. |
|  v03.43 | 2012-02-14 | Add reporting of high submission users. Fixed bug with mass-hold title text not being updated. Added cache type totals to the type selector drop-down text. Added 1/4 second delay to the region/state hover effects. For held caches, |
|  v03.42 | 2011-08-02 | Fix to accommodate site changes. |
|  v03.41 | 2011-06-28 | Bolded Placed Date to make it easier to find. Places lists are now hidden, and appear when you hover over the list's selector. List of places selected now appears in the browser's title. |
|  v03.40 | 2011-06-28 | Preliminary fix for Site Change. May contain bugs. |
|  v03.37 | 2011-05-25 | Fix for Firefox 4. Restored paging control to center of the page, instead of the far right. |
|  v03.36 | 2011-05-06 | Added controls to make micro-adjustments to the queue table size. |
|  v03.35 | 2011-05-05 | Bug fix for EarthCaches. |
|  v03.34 | 2011-04-26 | Code to remove left-hand navigation strip no longer needed with latest site release. Fixed filter background color, which the new release breaks. Restores full-size font to the paging controls. |
|  v03.33 | 2011-03-25 | Added checkbox to select whether filtered out EarthCaches are displayed or hidden. |
|  v03.32 | 2011-02-22 | Fix corrupted file. |
|  v03.31 | 2011-02-02 | Filtered earthcache rows are now collapsed, so they no longer appear in the list. Added checkbox option to turn on/off the hover tracking. |
|  v03.30 | 2011-01-27 | Added hover tracking to queue list, via CSS. Added ability to save a list of caches from the queue, and user it to create a bookmark list (via separate script, using local storage to allow data to be shared between scripts. |
|  v03.20 | 2010-02-03 | Fix to Hold Mode detection. |
|  v03.10 | 2010-01-13 | Update for site changes. |
|  v03.00 | 2009-12-06 | Added filtering by cache or owner name. Added color selection for filtering. Removed blue-bar. Added filtering for earthcaches (temporary, pending GC site change). Added individual cache filtering. Added queue notes, which can be added or edited from the queue or the review page. |
|  v02.20 | 2008-11-04 | Added option to hold/unhold from list. Changed the Publish link to an icon, and changed the method it uses. |
|  v02.10 | 2008-07-23 | Fix to accommodate site change. |
|  v02.00 | 2007-10-31 | Change to work with multiple sub-domains. |
|  v01.90 | 2007-04-30 | Added selector hover text to show URL code options. |
|  v01.80 | ????-??-?? | Change to prevent "\[map] \[litmus] [GPX]" from breaking to another line. |
|  v01.70 | ????-??-?? | Added "Publish" link, visible only when viewing caches on Hold. |
|  v01.60 | ????-??-?? | Moved "Review Queue" title to line by itself. Removed some unnecessary white space. Added routine to shorten lengthy country names. |
|  v01.50 | ????-??-?? | Added blue bar display to the queue table. |
|  v01.40 | ????-??-?? | Cosmetic change to keep drop-down selector and its label together if the line wraps within the browser. Fixed hover text for the selector. |
|  v01.30 | ????-??-?? | Added drop-down selector to set number of nearby caches to show. |
|  v01.20 | ????-??-?? | Added ability to change open-order on the fly (no need to reload the page). Setting is persistent between sessions. Options links are on the bottom of the page. |
|  v01.10 | ????-??-?? | Cleaned up code. No longer uses embedded function. Always opens in tabs now, even if not running a tab manager (GM 0.6.4+ only). Removed never-used links to remove from watchlist. |
|  v01.00 | ????&#8209;??&#8209;??&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |

## GCR Reviewer Forums Fix v2 ([gcr_Reviewer_Forums_Fix.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Reviewer_Forums_Fix.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v01.01 | ????-??-?? | Restores avatar to correct proportions. Adds separator bar between pinned and unpinned topics. Changes all links in posts to open in a new tab. Adds icons to open forums, threads, and threads (new posts) in new tabs. Adds alert function for main forums, when signed on under your reviewer profile. |
|  v01.00 | ????&#8209;??&#8209;??&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |

## GCR Seek Cache v2 ([gcr_Seek_Cache.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Seek_Cache.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v01.01 | 2011-08-30 | Turns GC codes into links to the Reviewer Page for that cache. |
|  v01.00 | 2011&#8209;08&#8209;30&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |

## GCR Timed Publishing 1 Review Page v2 ([gcr_Timed_Publishing_1_Review.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Timed_Publishing_1_Review.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v01.15 | 2016-06-30 | Fix for site change. |
|  v01.14 | 2016-06-29 | Code to convert coordinates that are scientific notation. Cleaned up parameter code that calls out to time zone API. Fix for site change. |
|  v01.13 | 2015-03-31 | Fix for site change. |
|  v01.12 | 2015-02-01 | Fix for Firefox 35 security change. |
|  v01.11 | 2014-11-09 | Bug fix. |
|  v01.10 | 2014-11-08 | Option to automatically create a Review Queue Note when cache is successfully added to bookmark. Added Localization for Time Zone Name. Added storage dump to clipboard option. Fixed time default cleanup. Added optional controls to allow changing the width of the drop-down boxes (for non-windows machines). Fixed settings screen so the browser page will snap back to its prior scroll position when settings are closed. Fixed bug where day of the week selector was not recalculating in a specific condition. Fix for removal of the cache note from the review page. |
|  v01.00 | 2014&#8209;04&#8209;04&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |

## GCR Timed Publishing 2 Create Bookmark Page v2 ([gcr_Timed_Publishing_2_Create_Bookmark.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Timed_Publishing_2_Create_Bookmark.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v01.15 | 2016-06-30 | Fix for site change. |
|  v01.14 | 2016-06-29 | Fix for site change. |
|  v01.13 | 2015-03-31 | Fix for site change. |
|  v01.12 | 2015-02-01 | Fix for Firefox 35 security change. |
|  v01.11 | 2014-11-09 | Bug fix. |
|  v01.10 | 2014-11-08 | Added optional Review Queue Note generation when a cache is successfully added to the bookmark. |
|  v01.00 | 2014&#8209;04&#8209;04&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |

## GCR Timed Publishing 3 Create Bookmark Page v2 ([gcr_Timed_Publishing_3_Bookmark_Page.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Timed_Publishing_3_Bookmark_Page.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.02 | 2017-08-22 | Fix SignedInAs. |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v01.15 | 2016-06-30 | Fix for site change. |
|  v01.14 | 2016-06-29 | Addition of Recall Settings button, for manual submission. Fix for site change. |
|  v01.13 | 2015-03-31 | Fix for site change. |
|  v01.12 | 2015-02-01 | Fix for Firefox 35 security change. |
|  v01.11 | 2014-11-09 | Bug fix. |
|  v01.10 | 2014-11-08 | Update release number. |
|  v01.00 | 2014&#8209;04&#8209;04&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |

## GCR Timed Publishing 4 Results Page v2 ([gcr_Timed_Publishing_4_Results_Page.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Timed_Publishing_4_Results_Page.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.02 | 2017-08-22 | Fix SignedInAs. |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v01.15 | 2016-06-30 | Fix for site change. |
|  v01.14 | 2016-06-29 | Addition of Recall Settings button, for manual submission. Fix for site change. |
|  v01.13 | 2015-03-31 | Fix for site change. |
|  v01.12 | 2015-02-01 | Fix for Firefox 35 security change. |
|  v01.11 | 2014-11-09 | Bug fix. |
|  v01.10 | 2014-11-08 | Update release number. |
|  v01.00 | 2014&#8209;04&#8209;04&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |

## GCR Geocaching Waypoint Links v2 v2 ([gcr_Waypoint_links.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Waypoint_links.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v02.91 | 2012-05-03 | Fix for site changes and change to Yahoo map link. |
|  v02.90 | 2010-08-19 | Final fix for site changes. Removed controls now duplicated by the site. Added link to show a waypoint in Google Maps, so the waypoint pushpin can be seen in Street View. |
|  v02.81 | 2010-06-29 | Preliminary fix for site changes. |
|  v02.80 | 2010-06-14 | Removed "add waypoint" link. Moved to main Review page script. |
|  v02.70 | 2010-06-02 | Update for site changes. |
|  v02.60 | 2010-01-25 | Removed wrapping in coordinate cell. Improved formatting when waypoints include notes. |
|  v02.50 | 2010-01-13 | Update for site changes. |
|  v02.40 | 2008-07-23 | Fix to accomodate site change. |
|  v02.30 | 2008-05-20 | Bug fix. |
|  v02.20 | 2008-05-19 | Added color alert for waypoints 3 or more miles from the posted coordinates. Changed topo map link to use Admin Terraserver maps. Fix site error in map link. |
|  v02.10 | 2007-12-17 | Fix for cache names and waypoint text containing characters above ascii 127. These characters are converted to entity codes which the browser will convert back. |
|  v02.00 | 2007-10-19 | Added links to show all additional waypoints and optionally the posted coordinates, with a selectable radius, on Google maps. Can individual turn off waypoints for this function. Can customize selectable radius size and color. Change to work with multiple subdomains. |
|  v01.60 | 2007-04-04 | Added additional map links to directly open the geocaching Google map and the Topozone map, with a pushpin marking the waypoint location. |
|  v01.50 | ????-??-?? | Another fix to accomodate website change. Moved Additional Waypoint link to the Navagation box. |
|  v01.40 | ????-??-?? | Minor fix to accomodate website change. |
|  v01.30 | ????-??-?? | Added display of distance to posted coordinates. Makes it easy to identify puzzles that are too far away, and as a sanity check for waypoints. |
|  v01.20 | ????-??-?? | Check for existing 'wp' parm in URL before adding one. This is for compatibility with Review script v4.3. |
|  v01.10 | ????-??-?? | Added link for Additional Waypoints. |
|  v01.00 | ????&#8209;??&#8209;??&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |

## GCR Word Highlighter v2 ([gcr_Word_Highlighter.user.js](https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Word_Highlighter.user.js))

| Version | Date       | Modifications |
| -------:| ---------- | ------------- |
|  v02.01 | 2017-04-01 | Minor code cleanup. |
|  v02.00 | 2017-02-08 | Version reset to 2.0 and name change to add v2. |
|  v01.11 | 2017-04-01 | Fix for site change. |
|  v01.10 | 2017-04-01 | Added wildcard and expression replacement. |
|  v01.00 | 2011&#8209;08&#8209;03&nbsp;<!-- Hack to avoid wrapping --> | Initial release. |
