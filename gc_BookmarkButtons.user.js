﻿// ==UserScript==
// @name           GC Bookmark Buttons v2
// @description    Adds buttons to select all archived/disabled
// @namespace      http://www.geocaching.com/admin
// @version        02.03
// @icon           http://i.imgur.com/kIEMexQ.png
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gc_BookmarkButtons.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gc_BookmarkButtons.user.js
// @include        http*://*.geocaching.com/bookmarks/view.aspx?*
// @include        http*://*.geocaching.com/bookmarks/bulk.aspx?*
// @grant          GM_addStyle
// ==/UserScript==
/*
Function:
 Adds 'Check All Active' 'Check All Archived' and 'Check All
 Disabled' buttons next to the existing 'Check All' button.
 Like the 'Check All' button, these toggle between Check and
 Uncheck functions when they are clicked.

 Adds a duplicate of the paging controls to the top of the
 page, when bookmarks span multipe pages, so you don't have to
 scroll to the bottom just to change the page.

 Also, adds the bookmark title as the link title text, as the
 strike-through font can sometimes make it difficult to read.

Usage:
 Just click, to check or uncheck the appropriate type of entry.

*/

	// Globals.
	var xButtonID;
	var checkVal = new Array(false, false, false);
	// Rewrite CSS for page to take up less space.
	GM_addStyle("body { line-height: normal !important; }");
	GM_addStyle("#table { line-height: 1.75em !important; }");
	GM_addStyle("th, td { padding-top: 0.2em !important; padding-right: 0.5em !important; padding-bottom: 0.2em !important; padding-left: 0.5em !important; }	" );

	// Get ListID and guid.
	var lidSpan = document.getElementById('ctl00_ContentBody_lbHeading');
	var lidLink = lidSpan.getElementsByTagName('a')[0];
	// If there is an edit ID (not a bulk delete confirmation screen).
	if (lidLink) {
		// Create Refresh link.
		var spanRefresh = document.createElement('span');
		spanRefresh.id = 'spanRefresh';
		spanRefresh.style.marginLeft = '10px';
		var lnkRefresh = document.createElement('a');
		lnkRefresh.appendChild(document.createTextNode('Refresh'));
		spanRefresh.appendChild(lnkRefresh);
		insertAfter(spanRefresh, lidSpan);

		// Get list id and guids or codes for this page.
		var ListID = UrlParm('ListID', true, lidLink.href);
		var guid = UrlParm('guid', true);
		var code = UrlParm('code', true);

		// If both present, then make Refresh link url same as page url and store the
		// pair in session storage.
		if (ListID && guid) {
			lnkRefresh.href = location.href;
			window.sessionStorage.setItem('ListIDguid', ListID + ',' + guid);
		}
		else if (ListID && code) {
			lnkRefresh.href = location.href;
			window.sessionStorage.setItem('ListIDguid', ListID + ',' + code);
		}
		// Otherwise, if there is a listid, then get data from session storage. If the
		// listids match, create Refresh url using the stored guid (or code for the new bookmark page).
		else {
			if (ListID) {
				var ssListIDguid = window.sessionStorage.getItem('ListIDguid');
				var aSplit = ssListIDguid.split(',');
				var ssListID = aSplit[0];
				var ssguid = aSplit[1];
				if (ssListID == ListID) {
					lnkRefresh.href = curDomain() + '/bookmarks/view.aspx?guid=' + ssguid;
				}
			}
		}
	}

	// Get handle to 'Bulk Delete' button.
	e_BulkDelete = document.getElementById("ctl00_ContentBody_ListInfo_btnDelete");

	// Generate list of checkboxes.
	var CheckBoxList = document.evaluate(
		"//input[@type='checkbox']",
		document,
		null,
		XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
		null);

	// Create spacer 1.
	e_space_span1 = document.createElement("span");
	e_space_span1.id = 'space_span1';
	e_space_span1.innerHTML = '&nbsp;';
	e_BulkDelete.parentNode.insertBefore(e_space_span1, e_BulkDelete.nextSibling);

	// Create 'Active' button.
	e_btn_AllActive = document.createElement("input");
	e_btn_AllActive.id = 'btn_AllActive';
	e_btn_AllActive.type = 'button';
	e_btn_AllActive.name = 'btn_AllActive';
	e_btn_AllActive.value = 'Check Active';
	e_space_span1.parentNode.insertBefore(e_btn_AllActive, e_space_span1.nextSibling);

	// Create spacer 2.
	e_space_span2 = document.createElement("span");
	e_space_span2.id = 'space_span2';
	e_space_span2.innerHTML = '&nbsp;';
	e_btn_AllActive.parentNode.insertBefore(e_space_span2, e_btn_AllActive.nextSibling);

	// Create 'Disabled' button.
	e_btn_AllDisabled = document.createElement("input");
	e_btn_AllDisabled.id = 'btn_AllDisabled';
	e_btn_AllDisabled.type = 'button';
	e_btn_AllDisabled.name = 'btn_AllDisabled';
	e_btn_AllDisabled.value = 'Check Disabled';
	e_space_span2.parentNode.insertBefore(e_btn_AllDisabled, e_space_span2.nextSibling);

	// Create spacer 3.
	e_space_span3 = document.createElement("span");
	e_space_span3.id = 'space_span3';
	e_space_span3.innerHTML = '&nbsp;';
	e_btn_AllDisabled.parentNode.insertBefore(e_space_span3, e_btn_AllDisabled.nextSibling);

	// Create 'All Archived' button.
	e_btn_AllArchived = document.createElement("input");
	e_btn_AllArchived.id = 'btn_AllArchived';
	e_btn_AllArchived.type = 'button';
	e_btn_AllArchived.name = 'btn_AllArchived';
	e_btn_AllArchived.value = 'Check Archived';
	e_space_span3.parentNode.insertBefore(e_btn_AllArchived, e_space_span3.nextSibling);

	// Add event listeners to buttons.
	e_btn_AllActive.addEventListener('click', AllActiveClicked, false);
	e_btn_AllArchived.addEventListener('click', AllArchivedClicked, false);
	e_btn_AllDisabled.addEventListener('click', AllDisabledClicked, false);

	// Get collection of links.
	var AllLinks = document.evaluate(
		"//a[@href]",
		document,
		null,
		XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
		null);
	maxR = AllLinks.snapshotLength;
	// Look for cache links.
	for (var i = 0; i < maxR; i++) {
		var xLink = AllLinks.snapshotItem(i);

		// If a cache link.
		if (xLink.href.search(/\/seek\/cache_details\.aspx\?/,"g") >= 0) {
			var zText = xLink.lastChild;
			while ((zText.nodeName != '#text') && (zText)) {
				zText = zText.firstChild
			}
			xLink.title = zText.data;
		}
	}

//****************************************************************************************//
//                                                                                        //
//                         Functions                                                      //
//                                                                                        //
//****************************************************************************************//

	// Event listener for All Active button.
	function AllActiveClicked() {
		xButtonID = e_btn_AllActive.id;
		AllOfType(0);
	}

	// Event listener for All Disabled button.
	function AllDisabledClicked() {
		xButtonID = e_btn_AllDisabled.id;
		AllOfType(1);
	}

	// Event listener for All Archived button.
	function AllArchivedClicked() {
		xButtonID = e_btn_AllArchived.id;
		AllOfType(2);
	}

	// 0 = All Active
	// 1 = All Disabled		Class = Strike
	// 2 = All Archived		Class = OldWarning Strike
	function AllOfType(aot) {
		// Toggle action value.
		checkVal[aot] = !checkVal[aot];

		// Process checkbox list.
		var maxR = CheckBoxList.snapshotLength;
		if (maxR > 0) {
			for (var i = 0; i < maxR; i++) {
				var aCheckBox = CheckBoxList.snapshotItem(i);
				// var cbRow = document.getElementById('r' + aCheckBox.id);
				var cbRow = aCheckBox.parentNode;
				if (cbRow.nodeName != 'TR') {
					cbRow = cbRow.parentNode;
				}
				if (cbRow.nodeName == 'TR') {
					var stsArc = (cbRow.getElementsByClassName('OldWarning Strike').length > 0);
					var stsDis = (cbRow.getElementsByClassName('Strike').length > 0);
					var status = 0;
					if (stsArc) { status = 2; }
					else if (stsDis) { status = 1; }
					if (aot == status) { aCheckBox.checked = checkVal[aot]; }
				}
			}
		}

		// Toggle button label.
		var btnLabel = document.getElementById(xButtonID).value;
		if (checkVal[aot]) {
			btnLabel = btnLabel.replace(/Check/, 'Uncheck');
		} else {
			btnLabel = btnLabel.replace(/Uncheck/, 'Check');
		}
		document.getElementById(xButtonID).value = btnLabel;
	}

	// Move up the DOM tree until a specific DOM type is reached.
	function fUpGenToType(gNode, gType) {
		var gNode;
		var gType = gType.toUpperCase();
		while (gNode.nodeName.toUpperCase() != gType) {
			gNode = gNode.parentNode;
			if (gNode.nodeName == 'undefined') {
				gNode = null;
				break;
			}
		}
		return gNode;
	}

	// Returns a URL parameter.
	//  ParmName - Parameter name to look for.
	//  IgnoreCase - (optional) *false, true. Ignore parmeter name case.
	//  UrlString - (optional) String to search. If omitted, document URL is used.
	function UrlParm(ParmName, IgnoreCase, UrlString) {
		var RegRslt, sc = '', RtnVal = '';
		if (IgnoreCase) {sc = 'i'}
		if(UrlString) {
			var PageUrl = UrlString;
		} else {
			PageUrl = document.location.href + '';
		}
		var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
		var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
		RegRslt = RegEx1.exec(ParmString);
		if (RegRslt) {RtnVal = RegRslt[2]}
		return RtnVal;
	}

	// Returns the current domain, including http or https protocol, without a trailing '/';
	function curDomain() {
		return location.protocol + '//' + document.domain;
	}

	// Insert element after an existing element.
	function insertAfter(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement.nextSibling);
	}

	// Insert element aheadd of an existing element.
	function insertAheadOf(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement);
	}
