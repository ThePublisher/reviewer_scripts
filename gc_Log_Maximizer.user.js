﻿// ==UserScript==
// @name           GCR Log Maximizer v2
// @description    Log Maximizer
// @namespace      http://www.geocaching.com/admin
// @version        02.02
// @icon           http://i.imgur.com/KIaIMnz.png
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gc_Log_Maximizer.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gc_Log_Maximizer.user.js
// @include        http*://*.geocaching.com/seek/log.aspx*
// @grant          GM_addStyle
// @grant          GM_getValue
// @grant          GM_log
// @grant          GM_registerMenuCommand
// @grant          GM_setValue
// ==/UserScript==

/*
Function:
 Allows user to resize text entry box on log pages. Also keeps
 user from trying to submit a log when a log type has not been
 selected, or no log text has been entered.

 Tracks number of characters entered into log. Shows number of
 remaining characters before hitting maximum of 4,000.

Usage:
 Use the arrow icons (next to the smilie button) to adjust the size
 of the text entry box. The Submit Button Validation function is
 controlled by the asterisk icon. Click the icon to turn the validation
 off. The icon will change to a X-ed circle. Click again to turn the
 validation back on.

Features:
 Separate settings are maintained for each member profile that uses the PC.

*/

	var imgQuote =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAgCAYAAAABtRhCAAAA" +
		"BGdBTUEAALGPC%2FxhBQAAAAlwSFlzAAAOwAAADsABataJCQAAABh0RVh0U29mdHdhcmUA" +
		"cGFpbnQubmV0IDQuMC45bDN%2BTgAAAaxJREFUSEu9lDtLA0EUhfMDLCwtLC0sLCwsLCz8" +
		"CRYWFhb%2BgCCCCCKKCGIQCwsRQUQLQW3EyojvZwi%2BwQUDkVgkQQlIjM9AErnO2TWwu3" +
		"NJApvrgY8z9%2Bxld2dnZn1E9K%2BwoSRsKAkbSsKGkrChJGxYitpgL7Ucj1PX1TwN3q%2" +
		"Bbjhq5u5eDDTnqtvtpOrZL2UJOlbqQzzzuU8PeEEr2HoAN3bSdTdJHPquG5ZX%2FKVDn5R" +
		"yG7L3Y0A73sNtMnFaT5xSIBk1HbVeph2qBm8j7kzJLxluS2kNTGGp9WMdwOqaGll5zXzCt" +
		"Twvs%2FK2HqedshrCOSmwvqNn0my9VVOvJBMzR4yjcdF8vKLO0FA%2FB2D472FhF9RlrMM" +
		"d1R%2BGm8WBEmaWHzxRhBkpsL%2FA8Q5D4TiuzdPoSpfqdAQy1vqqsIei5WVTmlOguXU6E" +
		"lVUuT%2BfQf7eirDJ5%2FtPgwONti8IYnw%2FnsOlw1JxF1f6lOG84dxA2zXBko%2BwZrB" +
		"Q2xG7bShnUcTGrSv26F7Sg%2BWis7Dp4gQ0lYUNJ2FASNpSEDSVhQ0nYUBI2lIQNJWFDOc" +
		"j3C6pTwmL7WJ0SAAAAAElFTkSuQmCC";

	// Get currently signed-on geocaching.com profile.
	var SignedInX = document.getElementsByClassName('user-name')[0];
	if (SignedInX) {
		var SignedInAs =  SignedInX.firstChild.data.trim();
		SignedInAs = SignedInAs.replace(/<\&amp;>/g, '&');
	} else { return; }

	// Left/right trim whitespace functions.
	String.prototype.ltrim=function() {
		return this.replace(/^\s*/g,'');
	}
	String.prototype.rtrim=function() {
		return this.replace(/\s*$/g,'');
	}

	GM_registerMenuCommand('Set Log Maximizer II Options', fSetOptions);

	var bShowInventory = (GM_getValue('HideInventory_' + SignedInAs, 'On') == 'On');
	var bShowSmileyBar = (GM_getValue('HideSmileyBar_' + SignedInAs, 'Off') == 'On');

	// Exit if not in edit mode.
	var btnQuote = document.getElementById('mdd_quote');
	if (!btnQuote) { return; }  // Not in edit mode. Exit script.

	// Set default quote template value.
	var dftQuoteTemplate = '#_**quote:**_#\n\n----\n\n%%text%%\n\n----\n\n'

	// Maximize screen space.
	if (GM_getValue('MaximizeSpace' + '_' + SignedInAs, true)) {
		// Remove page title.
		var pagTitle = document.getElementById('ctl00_ContentBody_lbHeading');
		if (pagTitle) {
			var h2PagTitle = pagTitle.parentNode;
			h2PagTitle.parentNode.removeChild(h2PagTitle);
		}

		// Remove Navigation bar.
		var navBar = document.getElementById('Navigation');
		if (navBar) {
			navBar.parentNode.removeChild(navBar);
		}
	}

	// Move submit button to the right.
	sbmButton = document.getElementById('ctl00_ContentBody_LogBookPanel1_btnSubmitLog');
	sbmButton.parentNode.style.textAlign = 'right';

	// Create duplicate button on top of page.
	LogButtonTop = sbmButton.cloneNode(false);
	LogButtonTop.style.marginLeft = '20px';
	var tmpDD = document.getElementById("uxDateVisited");
	if (tmpDD) {
		tmpDD = tmpDD.parentNode;
	}
	tmpDD.appendChild(LogButtonTop);

	// Get text area handle.
	var e_LogTextArea = document.getElementsByClassName('mdd_editor')[0];
	if (!e_LogTextArea) {
		GM_log('Unable to locate text area. Class name changed.');
		return;
	}

	// Reset layout (text box size & screen scroll position) to user's default.
	var NewLayout = GM_getValue('DefaultLayout_' + SignedInAs, '250px,0');
	var aNewLayout = NewLayout.split(',');
	e_LogTextArea.style.height = aNewLayout[0];
	document.documentElement.scrollTop = aNewLayout[1];

	// Add script links to toolbar.
	var divHow2Format = document.getElementsByClassName('mdd_links')[0];

	// Create divider bar.
	divDivider1 = document.createElement('div');
	divDivider1.classList.add('mdd_links');
	divDivider1.style.color = 'DimGray';
	divDivider1.style.marginRight = '3px';
	divDivider1.style.marginLeft = '3px';
	divDivider1.appendChild(document.createTextNode('|'));

	// Link to open script settings.
	var divOpenOptions = document.createElement('div');
	divOpenOptions.classList.add('mdd_links');
	var lnkOpenOptions = document.createElement('a');
	lnkOpenOptions.appendChild(document.createTextNode('Options'));
	lnkOpenOptions.classList.add('mdd_help');
	lnkOpenOptions.tabindex = '-1';
	lnkOpenOptions.href = 'javascript:void(0)';
	lnkOpenOptions.title = 'Open Options for\nLog Maximizer Script';
	lnkOpenOptions.addEventListener('click', fSetOptions, true);
	divOpenOptions.appendChild(lnkOpenOptions);
	insertAfter(divOpenOptions, divHow2Format);
	insertAfter(divDivider1, divHow2Format);

	// Link to save textbox size.
	var divSaveLayout = document.createElement('div');
	divSaveLayout.classList.add('mdd_links');
	var lnkSaveLayout = document.createElement('a');
	lnkSaveLayout.appendChild(document.createTextNode('Save Layout'));
	lnkSaveLayout.classList.add('mdd_help');
	lnkSaveLayout.tabindex = '-1';
	lnkSaveLayout.href = 'javascript:void(0)';
	lnkSaveLayout.title = 'Save textbox size and\nscroll position as default.';
	lnkSaveLayout.addEventListener('click', fSaveLayout, true);
	divSaveLayout.appendChild(lnkSaveLayout);
	divDivider2 = divDivider1.cloneNode(true);
	insertAheadOf(divSaveLayout, divOpenOptions);
	insertAfter(divDivider2, divSaveLayout);

	// Add smiley bar.
	var litDescrCharCount = document.getElementById('litDescrCharCount');
	var lnkSmileys = document.createElement('a');
	lnkSmileys.appendChild(document.createTextNode('Insert Smiley'));
	lnkSmileys.href = 'javascript:void(0)';
	lnkSmileys.addEventListener("click", fSmileyBarOnOff, true);
	lnkSmileys.title = 'Open/Close the Smiley Bar';
	litDescrCharCount.parentNode.appendChild(lnkSmileys);

	// Create smiley bar.
	var sbTable = document.createElement('table');
	var sbRow = document.createElement('tr');
	var sbCol = document.createElement('td');
	sbTable.appendChild(sbRow);
	sbRow.appendChild(sbCol);
	sbCol.id = 'sbCol';

	if (bShowSmileyBar) {
		lnkSmileys.setAttribute('status', true);
		sbTable.style.visibility = 'visible';
	} else {
		lnkSmileys.setAttribute('status', false);
		sbTable.style.visibility = 'collapse';
	}

	sbTable.style.width = '114px';
	sbTable.style.border = 'solid 1px rgb(68, 142, 53)';
	sbTable.style.marginTop = '10px';
	sbTable.style.MozBorderRadius = '5px';
	sbTable.style.backgroundColor = 'rgb(239, 239, 239)';

	var arSbImgs = ['icon_smile.gif', 'icon_smile_big.gif', 'icon_smile_cool.gif',
			'icon_smile_blush.gif', 'icon_smile_tongue.gif', 'icon_smile_evil.gif',
			'icon_smile_wink.gif', 'icon_smile_clown.gif', 'icon_smile_blackeye.gif',
			'icon_smile_8ball.gif', 'icon_smile_sad.gif', 'icon_smile_shy.gif',
			'icon_smile_shock.gif', 'icon_smile_angry.gif', 'icon_smile_dead.gif',
			'icon_smile_sleepy.gif', 'icon_smile_kisses.gif', 'icon_smile_approve.gif',
			'icon_smile_dissapprove.gif', 'icon_smile_question.gif'];
	var arSbTitles = ['smile', 'big smile', 'cool', 'blush', 'tongue', 'evil', 'wink', 'clown',
			'black eye', 'eightball', 'frown', 'shy', 'shocked', 'angry', 'dead', 'sleepy',
			'kisses', 'approve', 'disapprove', 'question'];
	var arSbCodes = ['[:)]', '[:D]', '[8D]', '[:I]', '[:P]', '[}:)]', '[;)]', '[:o)]', '[B)]', '[8]',
			'[:(]', '[8)]', '[:O]', '[:(!]', '[xx(]', '[|)]', '[:X]', '[^]', '[V]', '[?]'];
	for (var c = 0; c < arSbImgs.length; c++) {
		var sbLink = document.createElement("a");
		var sbImg = document.createElement("img");
		sbImg.src = '../images/icons/' + arSbImgs[c];
		sbImg.border = 0;
		sbLink.style.marginLeft = '5px';
		sbLink.style.marginRight = '5px';
		sbLink.appendChild(sbImg);
		sbLink.title = arSbTitles[c];
		sbLink.setAttribute('code', arSbCodes[c]);
		sbLink.href = 'javascript:void(0)';
		sbLink.addEventListener("click", fSmileyClicked, true);
		sbCol.appendChild(sbLink);
	}
	var ctBreak = document.createElement('br');
	litDescrCharCount.parentNode.appendChild(sbTable);

	// Add custom quote icon.
	var liBaseQuote = document.getElementById('mdd_quote').parentNode;
	var liCustomQuote = document.createElement('li');
	liCustomQuote.id = 'liCustomQuote';
	liCustomQuote.style.display = 'none';
	var aCustomQuote = document.createElement('a');
	var imgCustomQuote = document.createElement('img');
	imgCustomQuote.src = imgQuote;
	liCustomQuote.appendChild(aCustomQuote);
	aCustomQuote.appendChild(imgCustomQuote);
	aCustomQuote.href = 'javascript:void(0)';
	aCustomQuote.style.marginLeft = '3px';
	aCustomQuote.style.marginRight = '3px';
	aCustomQuote.style.backgroundPosition = '30px 6px';
	aCustomQuote.classList.add('mdd_button');
	aCustomQuote.title = 'Apply Custom\nQuote template';
	insertAfter(liCustomQuote, liBaseQuote)
	aCustomQuote.addEventListener('click', fQuoteText, true);
	fSetCustomQuoteVisibility();

	// Add coordinate paste-in box, if coordinate entry is present.
	var ns = document.getElementById('ctl00_ContentBody_LogBookPanel1_LatlongEditor1:_selectNorthSouth');
	if (ns) {
		// Locate table holding coordinate entry fields.
		coorTab = ns.parentNode;
		while (coorTab.nodeName != 'TABLE') {
			coorTab = coorTab.parentNode;
		}

		var LatlongEditor1 = document.getElementsByName("ctl00$ContentBody$LogBookPanel1$LatlongEditor1");
		var WptEntryType = LatlongEditor1[0].value;

		// Create text area, and add to cell.
		coorBox = document.createElement("textarea");
		coorBox.rows = 2;
		coorBox.cols = 23;
		coorBox.id = 'coorBox';
		coorBox.title = 'Enter free-form coordinates, and click Add Coordinates.'
		coorBox.style.marginTop = '8px';
		coorTab.rows[4].cells[0].appendChild(coorBox);
		coorTab.rows[4].cells[0].appendChild(document.createElement("br"));

		// Get hemisphere defaults.
		var nsDefault = GM_getValue("nsSelect_" + SignedInAs, 1);
		var weDefault = GM_getValue("weSelect_" + SignedInAs, -1);

		// Add hemisphere selectors.
		var nsSelect = document.createElement("select");
		nsSelect.id = "nsSelect_" + SignedInAs;
		var nsOption = document.createElement("option");
		nsOption.value = 1;
		nsOption.appendChild(document.createTextNode("N"));
		nsSelect.appendChild(nsOption);
		nsOption.selected = (nsOption.value == nsDefault);
		var nsOption = document.createElement("option");
		nsOption.value = -1;
		nsOption.appendChild(document.createTextNode("S"));
		nsSelect.appendChild(nsOption);
		nsOption.selected = (nsOption.value == nsDefault);
		coorTab.rows[4].cells[0].appendChild(nsSelect);
		nsSelect.addEventListener("change", nsweChange, true);

		var weSelect = document.createElement("select");
		weSelect.id = "weSelect_" + SignedInAs;
		var weOption = document.createElement("option");
		weOption.value = -1;
		weOption.appendChild(document.createTextNode("W"));
		weSelect.appendChild(weOption);
		weOption.selected = (weOption.value == weDefault);
		var weOption = document.createElement("option");
		weOption.value = 1;
		weOption.appendChild(document.createTextNode("E"));
		weSelect.appendChild(weOption);
		weOption.selected = (weOption.value == weDefault);
		coorTab.rows[4].cells[0].appendChild(weSelect);
		weSelect.addEventListener("change", nsweChange, true);

		// Add button.
		ctButton = document.createElement("input");
		ctButton.id = 'ctButton';
		ctButton.type = 'button';
		ctButton.name = 'ctButton';
		ctButton.value = 'Add Coordinates';
		coorTab.rows[4].cells[0].appendChild(ctButton);
		ctButton.addEventListener('click', ctButtonClicked, false);
	}
	// If travel bugs, hide, and add link to show.
	var e_TBPanel = document.getElementById("tblTravelBugs").parentNode;
	if (e_TBPanel) {
		if (!bShowInventory) {
			e_TBPanel.style.display = 'none';
		}
		e_tblTravelBugs = document.getElementById("tblTravelBugs");
		if (e_tblTravelBugs) {
			s1 = '';
			var BugCount = (e_tblTravelBugs.rows.length - 2);
			if (BugCount != 1) { s1 = 's'; }
			var e_BugCountSpan = document.createElement("span");
			e_BugCountSpan.style.color = 'rgb(255, 0, 0)';
			e_BugCountSpan.style.fontWeight = 'bold';
			e_BugCountSpan.style.paddingRight = '40px';
			e_BugCountLink = document.createElement("a");
			e_BugCountLink.href = 'javascript:void(0)';
			e_BugCountLink.title = 'Click to show/hide inventory';
			e_BugCountLink.addEventListener("click", ToggleInventory, true);
			e_BugCountLink.appendChild(document.createTextNode(BugCount + ' item' + s1 +
				' in inventory'));
			e_BugCountSpan.appendChild(e_BugCountLink);
			// e_TBPanel.insertBefore(e_BugCountSpan,
					// e_TBPanel.firstChild);
			insertAheadOf(e_BugCountSpan, e_TBPanel)
		}
	}

//****************************************************************************************//
//                                                                                        //
//                         Functions                                                      //
//                                                                                        //
//****************************************************************************************//

	// Set custom quote icon visibility based on setting value.
	function fSetCustomQuoteVisibility() {
		var liCustomQuote = document.getElementById('liCustomQuote');
		switch(GM_getValue('UseCustomQuote' + '_' + SignedInAs, true)) {
			case true:
				liCustomQuote.style.display = null;
				break;
			case false:
				liCustomQuote.style.display = 'none';
				break;
		}
	}

	// Store default dropdown values.
	function nsweChange() {
		GM_setValue(this.id, this.value)
	}

	// Coordinate button clicked.
	function ctButtonClicked() {
		// Split input value into number groups.
		var cs = coorBox.value;
		cs = cs.replace(/(\D)+/g,' ').trim();
		var nums = cs.split(' ');
		var selectNorthSouth = document.getElementById("ctl00_ContentBody_LogBookPanel1_LatlongEditor1:_selectNorthSouth");
		var selectEastWest = document.getElementById("ctl00_ContentBody_LogBookPanel1_LatlongEditor1:_selectEastWest");
		var LatDegs = document.getElementById("ctl00_ContentBody_LogBookPanel1_LatlongEditor1__inputLatDegs");
		var LonDegs = document.getElementById("ctl00_ContentBody_LogBookPanel1_LatlongEditor1__inputLongDegs");

		// Save original coordinates.
		var prvNS = selectNorthSouth.value - 0;
		var prvEW = selectEastWest.value - 0;
		var prvLat = 0;
		var prvLon = 0;

		selectNorthSouth.value = nsSelect.value;
		selectEastWest.value = weSelect.value;

		// Type 0: Decimal Degrees (DegDec)
		if (WptEntryType == 0) {
			prvLat = LatDegs.value * prvNS;
			prvLon = LonDegs.value * prvEW;
			if (nums.length < 4) {
				alert('Not enough numeric data elements.');
			} else {
				LatDegs.value = nums[0] + '.' + nums[1];
				LonDegs.value = nums[2] + '.' + nums[3];
			}
		// Type 1: Degrees and minutes (MinDec)
		} else if (WptEntryType == 1) {
			if (nums.length < 6) {
				alert('Not enough numeric data elements.');
			} else {
				var LatMins = document.getElementById("ctl00_ContentBody_LogBookPanel1_LatlongEditor1__inputLatMins");
				var LonMins = document.getElementById("ctl00_ContentBody_LogBookPanel1_LatlongEditor1__inputLongMins");
				LatDegs.value = nums[0];
				LatMins.value = nums[1] + '.' + nums[2];
				LonDegs.value = nums[3];
				LonMins.value = nums[4] + '.' + nums[5];
			}
		// Type 2: Degrees, minutes, seconds (DMS)
		} else if (WptEntryType == 2) {
			if (nums.length < 8) {
				alert('Not enough numeric data elements.');
			} else {
				var LatMins = document.getElementById("ctl00_ContentBody_LogBookPanel1_LatlongEditor1__inputLatMins");
				var LatSecs = document.getElementById("ctl00_ContentBody_LogBookPanel1_LatlongEditor1__inputLatSecs");
				var LonMins = document.getElementById("ctl00_ContentBody_LogBookPanel1_LatlongEditor1__inputLongMins");
				var LonSecs = document.getElementById("ctl00_ContentBody_LogBookPanel1_LatlongEditor1__inputLongSecs");
				LatDegs.value = nums[0];
				LatMins.value = nums[1];
				LatSecs.value = nums[2] + '.' + nums[3];
				LonDegs.value = nums[4];
				LonMins.value = nums[5];
				LonSecs.value = nums[6] + '.' + nums[7];
			}
		}
	}

	// Show/hide inventory.
	function ToggleInventory() {
		bShowInventory = (!bShowInventory);
		var e_TBPanel = document.getElementById("tblTravelBugs").parentNode
		if (bShowInventory) {
			e_TBPanel.style.display = '';
		} else {
			e_TBPanel.style.display = 'none';
		}
	}

	// Toggle smiley bar on/off.
	function fSmileyBarOnOff() {
		var sbStatusOn = eval(this.getAttribute('status'));
		sbStatusOn = eval(!sbStatusOn);
		this.setAttribute('status', sbStatusOn);
		if (sbStatusOn) {
			sbTable.style.visibility = 'visible';
		} else {
			sbTable.style.visibility = 'collapse';
		}
		e_LogTextArea.focus();
	}

	function fSmileyClicked() {
		var sCode = this.getAttribute('code');

		// Autotrim selection;
		fAutoTrim();

		// Reset work fields.
		var nTxt = '';

		// Save original text value and scroll position.
		var oTxt = e_LogTextArea.value;
		var txtTop = e_LogTextArea.scrollTop;

		// If portion of the text selected, save that text.
		var selStart = e_LogTextArea.selectionStart;
		var selEnd = e_LogTextArea.selectionEnd;

		// Get text on each side of the selection/carat;
		var txt1 = oTxt.substr(0, selStart);
		var txt2 = oTxt.substr(selEnd);

		// Create new text and insert into text area.
		nTxt = txt1 + sCode + txt2;
		e_LogTextArea.value = nTxt;

		// Position carot, based on if any text was selected.
		var newCaretPos = selStart + sCode.length;
		e_LogTextArea.focus();
		e_LogTextArea.selectionStart = newCaretPos;
		e_LogTextArea.selectionEnd = newCaretPos;
		e_LogTextArea.scrollTop = txtTop;
	}

	// Apply Auto Trim to selected text, if option is turned on.
	function fAutoTrim() {
		if (GM_getValue('AutoTrim_' + SignedInAs, 'On') == 'On') {
			var atStart = e_LogTextArea.selectionStart;
			var atEnd = e_LogTextArea.selectionEnd;
			if (atEnd > atStart) {
				var atTxt = e_LogTextArea.value;
				var atLastChar = atTxt.substr(atEnd-1, 1);
				while (atLastChar.match(/\s/)) {
					atEnd--;
					var atLastChar = atTxt.substr(atEnd-1, 1);
				}
				e_LogTextArea.selectionEnd = atEnd;
			}
		}
	}

	function fSaveLayout() {
		var resp = confirm('Save current text box size and screen position as defaults?');
		if (resp) {
			var CurHeight = getComputedStyle(e_LogTextArea)['height'];
			GM_setValue('DefaultLayout_' + SignedInAs, CurHeight + ',' +
					document.documentElement.scrollTop);
			alert('Layout has been saved.');
		} else {
			alert('Operation canceled');
		}
	}

	// Perform custom text quoting.
	function fQuoteText() {
		// Get quote template.
		txtQT = GM_getValue('QuoteTemplate' + '_' + SignedInAs, dftQuoteTemplate);

		// Validate template.
		if (txtQT.indexOf('%%text%%') == -1) {
			var resp = confirm('Quote Template is missing the required %%text%% replacement variable.\n\n' +
					'Do you want to edit the template settings?');
			if (resp) { fSetOptions(); }
			return;
		}

		if (txtQT.indexOf('%%text%%') != txtQT.lastIndexOf('%%text%%')) {
			resp = confirm('Quote Template contains multiple %%text%% replacement variables.\n' +
					'Only one replacement variable should be present.\n\n' +
					'Do you want to edit the template settings?');
			if (resp) { fSetOptions(); }
			return;
		}

		// Split template.
		var opnQuoteCode = txtQT.substr(0, txtQT.indexOf('%%text%%'));
		var clsQuoteCode = txtQT.substr(txtQT.indexOf('%%text%%') + 8);

		// Autotrim selection;
		fAutoTrim();

		// Reset work fields.
		var nTxt = '';
		var xTxt = '';

		// Save original text value and scroll position.
		var oTxt = e_LogTextArea.value;
		var txtTop = e_LogTextArea.scrollTop;

		// If portion of the text selected, save starting and ending points.
		var selStart = e_LogTextArea.selectionStart;
		var selEnd = e_LogTextArea.selectionEnd;

		// If block quote, use enhances mode, if turned on.
		if (selStart == selEnd) {
			e_LogTextArea.select();
			selStart = e_LogTextArea.selectionStart;
			selEnd = e_LogTextArea.selectionEnd;
		}

		// Get selected text.
		if (selStart < selEnd) {
			xTxt = oTxt.substring(selStart, selEnd);
		}

		// Get text on each side of the selection/carat;
		var txt1 = oTxt.substr(0, selStart);
		var txt2 = oTxt.substr(selEnd);

		// Italicize/Indent Quoted Text if selected.
		if (GM_getValue('ItalicizeQuotedText' + '_' + SignedInAs, false) ||
				GM_getValue('IndentQuotedText' + '_' + SignedInAs, false)) {
			var aTxt = xTxt.split('\n');
			var j = aTxt.length;
			for (var i = 0; i < j; i++) {
				aTxt[i] = aTxt[i].trim();
				if (GM_getValue('ItalicizeQuotedText' + '_' + SignedInAs, false)) {
					if (aTxt[i]) { aTxt[i] = `*${aTxt[i]}*`; }
				}
				if (GM_getValue('IndentQuotedText' + '_' + SignedInAs, false)) {
					if (aTxt[i]) { aTxt[i] = `> ${aTxt[i]}`; }
				}
			}
			xTxt =  aTxt.join('\n');
		}

		// Create new text and insert into text area.
		nTxt = txt1 + opnQuoteCode + xTxt + clsQuoteCode + txt2;
		e_LogTextArea.value = nTxt;

		// Position carot, based on if any text was selected.
		var newCaretPos = selStart + opnQuoteCode.length;
		if (xTxt.length) { newCaretPos += xTxt.length + clsQuoteCode.length; }
		e_LogTextArea.selectionStart = newCaretPos;
		e_LogTextArea.selectionEnd = newCaretPos;
		e_LogTextArea.scrollTop = txtTop;

		// Force text area to update.
		e_LogTextArea.focus();
		var changeEvent = document.createEvent ("Event");
		changeEvent.initEvent ("keyup", true, false);
		e_LogTextArea.dispatchEvent(changeEvent);
	}

	function fSetOptions() {
		if (!fCreateSettingsDiv('Log Maximizer Script Options')) { return; };
		var divSet = document.getElementById('gm_divSet');
		divSet.setAttribute('winTopPos', document.documentElement.scrollTop);  // snapback code

		// Generate one unique DOM name for all setting controls being created.
		settingName = 'sc_' + Math.floor(Math.random()*100000001).toString();

		//****************************************************************************************//
		//                                                                                        //
		//                             Start of Control Creation                                  //
		//                                                                                        //
		//  fCreateSetting(vID, vType, vLabel, vTitle, vDftVal, vSize, vSelVal, vSelTxt)          //
		//                                                                                        //
		//    vID = Identifier for control, and for saving setting data.                          //
		//    vType = checkbox (input + type.checkbox), text (input), textarea, select            //
		//    vLabel = Text for control label. Above textareas, or to the right of all others.    //
		//    vTitle = Title text for input control and label, or vLabel if not specified.        //
		//    vDftVal = May be text, true/false, or matches a vSelVal array element.              //
		//    vSize = Length of input (text) box, or height of textarea (i.e., '20px').           //
		//    vSelVal = Array of select option values.                                            //
		//    vSelTxt = Array of select option text. If omitted, SelVal values are used.          //
		//                                                                                        //
		//****************************************************************************************//

		GM_addStyle('legend.gmsettings { background: cornsilk; border: solid 2px slategray; border-radius: 8px; padding: 6px; }');
		GM_addStyle('fieldset.gmsettings { background: lightyellow; border-radius: 10px; padding: 5px; border: solid 4px green} ' );

		vDftVal = GM_getValue('MaximizeSpace' + '_' + SignedInAs, true);
		vLabel = 'Check to maximize screen space by removing unnecessary elements.';
		var cbMaximizeSpace = fCreateSetting('MaximizeSpace', 'checkbox', vLabel, '', vDftVal);

		var vDftVal = GM_getValue('HideSmileyBar' + '_' + SignedInAs, 'Off');
		var vLabel = 'Initial display of Smiley Bar.';
		var vSelVal = ['Off', 'On'];
		var selHideSmileyBar = fCreateSetting('HideSmileyBar', 'select', vLabel, '', vDftVal, '', vSelVal, '');

		var vDftVal = GM_getValue('HideInventory' + '_' + SignedInAs, 'On');
		var vLabel = 'Initial display of Inventory.';
		var vSelVal = ['Off', 'On'];
		var selHideInventory = fCreateSetting('HideInventory', 'select', vLabel, '', vDftVal, '', vSelVal, '');

		vDftVal = GM_getValue('UseCustomQuote' + '_' + SignedInAs, true);
		vLabel = 'Use Custom Quote formatting.';
		var cbUseCustomQuote = fCreateSetting('UseCustomQuote', 'checkbox', vLabel, '', vDftVal);

		vDftVal = GM_getValue('QuoteTemplate' + '_' + SignedInAs, dftQuoteTemplate);
		vLabel = 'Custom Quotation template. %%text%% will be replaced with selected text, ' +
				'or the current contents of the text box, if no text is selected.';
		var txtaraQuoteTemplate = fCreateSetting('QuoteTemplate', 'textarea', vLabel, '', vDftVal, '175px');

		// ---------------------- Custom Setting Code ---------------------- //
		// Add reset link to page.
		var ds_ResetToDefaultDiv = document.createElement('div');
		ds_ResetToDefaultDiv.id = 'ds_ResetToDefaultDiv';
		ds_ResetToDefaultDiv.style.textAlign = 'right';
		var linkResetToDefaultDiv = document.createElement('a');
		linkResetToDefaultDiv.href = 'javascript:void(0)';
		linkResetToDefaultDiv.title = 'Reset quote template to original default value.';
		linkResetToDefaultDiv.appendChild(document.createTextNode('Reset Quote Template to Default'));
		ds_ResetToDefaultDiv.appendChild(linkResetToDefaultDiv);
		pQuoteTemplate.appendChild(ds_ResetToDefaultDiv);
		linkResetToDefaultDiv.addEventListener('click', fResetToDefault, true);

		function fResetToDefault() {
			document.getElementById('txtaraQuoteTemplate').value = dftQuoteTemplate;
		}
		// ---------------------- Custom Setting Code ---------------------- //

		vDftVal = GM_getValue('IndentQuotedText' + '_' + SignedInAs, false);
		vLabel = 'Check to auto-indent customized quoted text.';
		var cbIndentQuotedText = fCreateSetting('IndentQuotedText', 'checkbox', vLabel, '', vDftVal);

		vDftVal = GM_getValue('ItalicizeQuotedText' + '_' + SignedInAs, false);
		vLabel = 'Check to auto-italicize customized quoted text.';
		var cbItalicizeQuotedText = fCreateSetting('ItalicizeQuotedText', 'checkbox', vLabel, '', vDftVal);

		fGroupSettings('Custom Text Quoting', ['UseCustomQuote', 'QuoteTemplate',
				'IndentQuotedText', 'ItalicizeQuotedText']);

		//****************************************************************************************//
		//                                                                                        //
		//                              End of Control Creation                                   //
		//                                                                                        //
		//****************************************************************************************//

		fOpenOptionsDiv();
	}

	// Cancel button clicked.
	function fCancelButtonClicked() {
		var resp = confirm('Any changes will be lost. Close Settings?');
		if (resp) {
			fCloseSettingsDiv();
		}
	}

	// Save button clicked. Store all settings.
	function fSaveButtonClicked() {
		// Get array of setting controls.
		var scs = document.getElementsByName(settingName);

		// Process each control.
		var scl = scs.length;
		for (var i = 0; i < scl; i++) {
			var sc = scs[i];
			var rawid = sc.getAttribute('rawid', '');
			var ctrltype = sc.getAttribute('ctrltype', '');
			if (ctrltype == 'checkbox') {
				GM_setValue(rawid + '_' + SignedInAs, sc.checked);
			} else {
				GM_setValue(rawid + '_' + SignedInAs, sc.value);
			}
		}
		fCloseSettingsDiv();
		fSetCustomQuoteVisibility();  // Custom modification, to update display.
	}

	// Create Div for settings GUI.
	function fCreateSettingsDiv(sTitle) {
		// If div already exists, reposition browser, and show alert.
		var divSet = document.getElementById("gm_divSet");
		if (divSet) {
			var YOffsetVal = parseInt(divSet.getAttribute('YOffsetVal', '0'));
			window.scrollTo(window.pageXOffset, YOffsetVal);
			alert('Edit Setting interface already on screen.');
			return false;
		}

		// Set styles for titles and elements.
		GM_addStyle('.SettingTitle {font-size: medium; font-style: italic; font-weight: bold; ' +
				'text-align: center; margin-bottom: 12px; !important; } ' );
		GM_addStyle('.SettingVersionTitle {font-size: small; font-style: italic; font-weight: bold; ' +
				'text-align: center; margin-bottom: 12px; !important; } ' );
		GM_addStyle('.SettingElement {text-align: left; margin-left: 6px; ' +
				'margin-right: 6px; margin-bottom: 12px; !important; } ' );
		GM_addStyle('.SettingButtons {text-align: right; margin-left: 6px; ' +
				'margin-right: 6px; margin-bottom: 6px; !important; } ' );
		GM_addStyle('.SettingLabel {font-weight: bold; margin-left: 6px !important;} ' ); +

		// Create blackout div.
		document.body.setAttribute('style', 'height:100%;');
		var divBlackout = document.createElement('div');
		divBlackout.id = 'divBlackout';
		divBlackout.setAttribute('style', 'z-index: 900; background-color: rgb(0, 0, 0); ' +
				'visibility: hidden; opacity: 0; position: fixed; left: 0px; top: 0px; '+
				'height: 100%; width: 100%; display: block;');

		// Create div.
		divSet = document.createElement('div');
		divSet.id = 'gm_divSet';
		divSet.setAttribute('style', 'position: absolute; z-index: 1000; visibility: hidden; ' +
				'padding: 5px; outline: 6px ridge blue; background: #FFFFCC;');
		popwidth = parseInt(window.innerWidth * .7);
		divSet.style.width = popwidth + 'px';

		// Create heading.
		var ds_Heading = document.createElement('div');
		ds_Heading.setAttribute('class', 'SettingTitle');
		ds_Heading.appendChild(document.createTextNode(sTitle));
		divSet.appendChild(ds_Heading);

		var ds_Version = document.createElement('div');
		ds_Version.setAttribute('class', 'SettingVersionTitle');
		ds_Version.appendChild(document.createTextNode('Script Version ' + GM_info.script.version));
		divSet.appendChild(ds_Version);

		// Add div to page.
		var toppos =  parseInt(window.pageYOffset +  60);
		var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
		divSet.style.top = toppos + 'px';
		divSet.style.left = leftpos + 'px';
		divSet.setAttribute('YOffsetVal', window.pageYOffset);

		// Add blackout and setting divs.
		document.body.appendChild(divBlackout);
		document.body.appendChild(divSet);
		window.addEventListener('resize', fSetLeftPos, true);

		return true;
	}

	/*
	vID = GetSetting ID, and type + vID is control ID.
	vType = checkbox (input + type.checkbox), text (input), textarea, select
	vLabel = Text for control label.
	vTitle = Title text for input control and label, or vLabel if not specified.
	vDftVal = May be text, true/false, or matches a vSelVal array element.
	vSize = Length of input (text) box, or height of textarea, in pixels.
	vSelVal = Array of select option values.
	vSelTxt = Array of select option text.
	*/
	// Create settings from passed data.
	function fCreateSetting(vID, vType, vLabel, vTitle, vDftVal, vSize, vSelVal, vSelTxt) {
		var divSet = document.getElementById('gm_divSet');
		var kParagraph = document.createElement('p');
		kParagraph.id = 'p' + vID;
		kParagraph.setAttribute('class', 'SettingElement');
		switch (vType) {
			case 'checkbox':
				var kElem = document.createElement('input');
				kElem.id = 'cb' + vID;
				kElem.type = 'checkbox';
				kElem. checked = vDftVal;
				break;
			case 'text':
				if (!vSize) { vSize = '150px'; }
				var kElem = document.createElement('input');
				kElem.id = 'txt' + vID;
				kElem.style.width = vSize;
				kElem.value = vDftVal;
				kElem.addEventListener('focus', fSelectAllText, true);
				break;
				case 'textarea':
				if (!vSize) { vSize = '80px'; }
				var kElem = document.createElement('textarea');
				kElem.id = 'txtara' + vID;
				kElem.style.width = '100%';
				kElem.style.height = vSize;
				kElem.value = vDftVal;
				break;
			case 'select':
				var kElem = document.createElement('select');
				kElem.id = 'sel' + vID;
				if (vSelVal) {
					if (vSelVal.constructor == Array) {
						for (var i in vSelVal) {
							var kOption = document.createElement('option');
							kOption.value = vSelVal[i];
							kOption.selected = (vSelVal[i] == vDftVal);
							if ((vSelTxt.constructor == Array) && (vSelTxt.length >= i-1)) {
								var kTxtNode = vSelTxt[i];
							} else {
								var kTxtNode = vSelVal[i];
							}
							kOption.appendChild(document.createTextNode(kTxtNode));
							kElem.appendChild(kOption);
						}
					}
				}
				break;
		}

		var kLabel = document.createElement('label');
		kLabel.setAttribute('class', 'SettingLabel');
		kLabel.setAttribute('for', kElem.id);
		kLabel.appendChild(document.createTextNode(vLabel));
		if (!vTitle) { vTitle = vLabel; }
		kElem.title = vTitle;
		kLabel.title = kElem.title;
		kElem.name = settingName;
		kElem.setAttribute('ctrltype', vType);
		kElem.setAttribute('rawid', vID);

		if (vType == 'textarea') {
			kParagraph.appendChild(kLabel);
			kParagraph.appendChild(document.createElement('br'));
			kParagraph.appendChild(kElem);
		} else {
			kParagraph.appendChild(kElem);
			kParagraph.appendChild(kLabel);
		}
		divSet.appendChild(kParagraph);

		return kElem;
	}

	// Routine for creating setting groupings.
	// Use after the settings group has been established.
	// Pass the heading text, and an array of setting IDs.
	// Example:
	// [Create SettingName1 here]
	// [Create SettingName2 here]
	// [Create SettingName3 here]
	// fGroupSettings('Group Heading Text', ['SettingName1', 'SettingName2', 'SettingName3']);
	function fGroupSettings(legendText, aID) {
		var divSet = document.getElementById('gm_divSet');
		var fldset = document.createElement('fieldset');
		fldset.classList.add('gmsettings');
		divSet.appendChild(fldset);
		var fldsetlgd = document.createElement('legend');
		fldsetlgd.classList.add('gmsettings');
		fldset.appendChild(fldsetlgd);
		fldsetlgd.appendChild(document.createTextNode(legendText));
		var ic = aID.length;
		for (var i = 0; i < ic; i++) {
			fldset.appendChild(document.getElementById('p' + aID[i]));
		}
	}

	// Resize/reposition on window resizing.
	function fSetLeftPos() {
		var divSet = document.getElementById('gm_divSet');
		if (divSet) {
			var popwidth = parseInt(window.innerWidth * .7);
			divSet.style.width = popwidth + 'px';
			var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
			divSet.style.left = leftpos + 'px';
		}
	}

	function fOpenOptionsDiv() {
		// Create buttons.
		fCreateSaveCancleButtons();

		// Add blackout and setting divs.
		var divBlackout = document.getElementById('divBlackout');
		var divSet = document.getElementById('gm_divSet');
		divSet.style.visibility = 'visible';
		divBlackout.style.visibility = 'visible';
		var op = 0;
		var si = window.setInterval(fShowBlackout, 40);

		// Function to fade-in blackout div.
		function fShowBlackout() {
			op = op + .05;
			divBlackout.style.opacity = op;
			if (op >= .75) {
				window.clearInterval(si);
			}
		}
	}

	// Create Save/Cancel Buttons.
	function fCreateSaveCancleButtons() {
		var divSet = document.getElementById('gm_divSet');
		var ds_ButtonsP = document.createElement('div');
		ds_ButtonsP.setAttribute('class', 'SettingButtons');
		var ds_SaveButton = document.createElement('button');
		ds_SaveButton = document.createElement('button');
		ds_SaveButton.appendChild(document.createTextNode("Save"));
		ds_SaveButton.addEventListener("click", fSaveButtonClicked, true);
		var ds_CancelButton = document.createElement('button');
		ds_CancelButton.style.marginLeft = '6px';
		ds_CancelButton.addEventListener("click", fCancelButtonClicked, true);
		ds_CancelButton.appendChild(document.createTextNode("Cancel"));
		ds_ButtonsP.appendChild(ds_SaveButton);
		ds_ButtonsP.appendChild(ds_CancelButton);
		divSet.appendChild(ds_ButtonsP);
	}

	function fCloseSettingsDiv() {
		var divBlackout = document.getElementById('divBlackout');
		var divSet = document.getElementById('gm_divSet');
		var winTopPos = divSet.getAttribute('winTopPos') - 0;  // snapback code
		divSet.parentNode.removeChild(divSet);
		divBlackout.parentNode.removeChild(divBlackout);
		window.removeEventListener('resize', fSetLeftPos, true);
		document.documentElement.scrollTop = winTopPos;  // snapback code
	}

	function fCancelButtonClicked() {
		var resp = confirm('Any changes will be lost. Close Settings?');
		if (resp) {
			fCloseSettingsDiv();
		}
	}

	function fSelectAllText() {
		this.select();
	}

	// Insert element after an existing element.
	function insertAfter(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement.nextSibling);
	}

	// Insert element ahead of an existing element.
	function insertAheadOf(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement);
	}
