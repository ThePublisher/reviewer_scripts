﻿// ==UserScript==
// @name           GCR Bookmark Processor v2
// @description    Automates Opening Caches From Bookmark Lists
// @namespace      http://www.geocaching.com/admin
// @version        02.02
// @icon           http://i.imgur.com/kIEMexQ.png
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gc_Bookmark_Processor.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gc_Bookmark_Processor.user.js
// @include        http*://*.geocaching.com/bookmarks/view.aspx?*
// @include        http*://*.geocaching.com/bookmarks/bulk.aspx?ListID=*
// @grant          GM_addStyle
// @grant          GM_getValue
// @grant          GM_openInTab
// @grant          GM_registerMenuCommand
// @grant          GM_setClipboard
// @grant          GM_setValue
// ==/UserScript==

/*
Function:
 Opens bookmark list cache pages in review or cache page mode. Can
 position the page, and customize the number of logs. When opening
 in review mode, pages can be held/unheld or locked/unlocked.

NOTE:
 The following configuration must be set to use on local files,
 using about:config
	greasemonkey.fileIsGreaseable = true
*/

	var GroupSize = 10 		// Number of pages to open in a group
	var TimeDelay = 75		// Miliseconds to wait before opening next page
	var NoteEditImg =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAANCAYAAACZ3F9%2FAA" +
		"AABGdBTUEAALGPC%2FxhBQAAABl0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjUuMU7nOP" +
		"kAAAD2SURBVChTjZK7CsJAEEUnlYgviBJIIKWNqaxEJHbWBn9GsAlB0ir4Jels7LSxtIh%" +
		"2FYqGF150Nq4l56MKwyWTO3juT1Ugsz%2FPAe9EKw5Acx9H425p0WbeIj0kpg8AzF5zniO" +
		"MYlm4B%2Bx3gL6EOqAQZajF0A0ybgOEYG6LEoVJUCun3qSaKH0t02h1cL2cYXQNRFGXBb7" +
		"sSQgAcCLcVYdCmD1SmOBN2FASf0KybWahoOEUQ239bVONP9%2BQ2uKcA95PYhVLfrslpV4" +
		"JcsJ33krGLnhT0E%2BRTbV2XsDsZSZW0m1KrRZdA5Uqtpv9h2XNOkW8HJ%2F8JNdAXexeD" +
		"QlcKybYAAAAASUVORK5CYII%3D";

	// Unique ID for cross-script local memory storage - review notes.
	const JSON_RQN_KEY = '517ba8f0-86f7-46f7-8f0d-d476265f420c';

	// Unique ID for positioning open browser pages in review mode.
	const REV_PAGE_POS_KEY = 'a3148ed3-e28f-4105-a8ab-dcbacbd3b457';

	GM_addStyle(".Warning, .OldWarning, .Validation, .red {color: #ff0000 !important; }	" );
	GM_addStyle(".rqnText {background-color: yellow; margin-left: 8px; }");
	GM_addStyle("a.noul {text-decoration:none }");

	GM_registerMenuCommand('Copy Cache List to Clipboard (Bookmark List Order)', fCopyListToClipboard1);
	GM_registerMenuCommand('Copy Cache List to Clipboard (Waypoint ID Order)', fCopyListToClipboard2);

	// Add links to reposition all open review pages.
	var pCrumbs =  document.getElementById("ctl00_Breadcrumbs");
	if (!pCrumbs) {
		var NavSection = document.getElementById('Navigation');
		if (NavSection) {
			var pCrumbs = document.createElement('div');
			insertAfter(pCrumbs, NavSection);
		}
	} else {
		var pCrumbs = pCrumbs.parentNode;
	}

	if (pCrumbs) {
		chkBoxs = document.getElementsByName("BID");
		if (chkBoxs.length == 0) {
			return;
		} else {
			// Index the rows.
			for (i = 0; i <= chkBoxs.length-1; i++) {
				var zRow = chkBoxs[i].parentNode.parentNode.cells[1];
				insertAheadOf(document.createTextNode(i-0+1), zRow.firstChild);
			}
		}

		var spanPosToLable = document.createElement('span');
		spanPosToLable.style.marginLeft = '30px';
		spanPosToLable.style.marginRight = '12px';
		spanPosToLable.appendChild(document.createTextNode('Position to:'));
		pCrumbs.appendChild(spanPosToLable);

		var spanPosTo = document.createElement('span');
		spanPosTo.style.marginRight = '12px';
		var aPosTo = document.createElement('a');
		aPosTo.href = 'javascript:void(0);';
		spanPosTo.appendChild(aPosTo);

		var spanClone = spanPosTo.cloneNode(true);
		spanClone.firstChild.setAttribute('hash', '#hd');
		spanClone.firstChild.title = 'Position open review pages to Top';
		spanClone.firstChild.appendChild(document.createTextNode('Top'));
		spanClone.firstChild.addEventListener('click', fPositionRevPages, true);
		pCrumbs.appendChild(spanClone);

		var spanClone = spanPosTo.cloneNode(true);
		spanClone.firstChild.setAttribute('hash', '#ctl00_ContentBody_CacheDetails_ShortDesc');
		spanClone.firstChild.title = 'Position all open review pages to Description';
		spanClone.firstChild.appendChild(document.createTextNode('Description'));
		spanClone.firstChild.addEventListener('click', fPositionRevPages, true);
		pCrumbs.appendChild(spanClone);

		var spanClone = spanPosTo.cloneNode(true);
		spanClone.firstChild.setAttribute('hash', '#map_canvas');
		spanClone.firstChild.title = 'Position all open review pages to Map';
		spanClone.firstChild.appendChild(document.createTextNode('Map'));
		spanClone.firstChild.addEventListener('click', fPositionRevPages, true);
		pCrumbs.appendChild(spanClone);

		var spanClone = spanPosTo.cloneNode(true);
		spanClone.firstChild.setAttribute('hash', '#ctl00_ContentBody_Waypoints');
		spanClone.firstChild.title = 'Position all open review pages to Waypoints';
		spanClone.firstChild.appendChild(document.createTextNode('Waypoints'));
		spanClone.firstChild.addEventListener('click', fPositionRevPages, true);
		pCrumbs.appendChild(spanClone);

		var spanClone = spanPosTo.cloneNode(true);
		spanClone.firstChild.setAttribute('hash', '#refreshBtn');
		spanClone.firstChild.title = 'Position all open review pages to Nearby Caches';
		spanClone.firstChild.appendChild(document.createTextNode('Nearby Caches'));
		spanClone.firstChild.addEventListener('click', fPositionRevPages, true);
		pCrumbs.appendChild(spanClone);

		var spanClone = spanPosTo.cloneNode(true);
		spanClone.firstChild.setAttribute('hash', '#log_table');
		spanClone.firstChild.title = 'Position all open review pages to Logs';
		spanClone.firstChild.appendChild(document.createTextNode('Logs'));
		spanClone.firstChild.addEventListener('click', fPositionRevPages, true);
		pCrumbs.appendChild(spanClone);

		var spanClone = spanPosTo.cloneNode(true);
		spanClone.firstChild.setAttribute('hash', '~publish~');
		spanClone.firstChild.title = 'Publish all open review pages';
		spanClone.firstChild.appendChild(document.createTextNode('Publish'));
		spanClone.firstChild.addEventListener('click', fPositionRevPages, true);
		pCrumbs.appendChild(spanClone);
	}

	// Fix checkmark all/none to not include control checkmark.
	var aCheckAll = document.evaluate("//a[@href = 'javascript:checkAll(this);']",
					document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
	if (aCheckAll) {
		aCheckAll.href = 'javascript:void(0);';
		aCheckAll.setAttribute('chkStatus', false);
		aCheckAll.addEventListener('click', fCheckAll, true);
	}

	// Change GC links to open
	// Get list of rows with cache links.
	var xPathSearch = "//tr[contains(@id, '_dataRow') and not(contains(@id, '_dataRow2'))]";
	var allCacheLinks = document.evaluate(
			xPathSearch, document, null,
			XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,
			null);

	// Get review queue notes.
	var jsObj = new Object;
	var jsonString = localStorage.getItem(JSON_RQN_KEY);
	if (jsonString) {
		jsObj = JSON.parse(jsonString);
	}

	// Convert first link on row to open reviewer page and set second link to open in new tab.
	var maxLinks = allCacheLinks.snapshotLength;
	for (i = 0; i < maxLinks; i++) {
		var xRow = allCacheLinks.snapshotItem(i);
		xRow.getElementsByTagName('A')[1].target = '_blank';
		var xLink = xRow.getElementsByTagName('A')[0];
		xLink.classList.add('review_link');
		var xHref = xLink.href;
		xHref = xHref.replace(/seek\/cache_details/g, 'admin/review');
		xLink.href = xHref + '&nc=12';
		xLink.target = "_blank";
		xLink.title = "Open Review Page";

		// Add any review queue note.
		var wptId = xLink.firstChild.data;
		if (xRow.cells.length >= 6) {
			if (jsObj.hasOwnProperty(wptId)) {
				var rqnValue = jsObj[wptId]['text'];
				var noteSpan = document.createElement('span');
				noteSpan.classList.add('rqnText');
				noteSpan.appendChild(document.createTextNode(rqnValue));
				xRow.cells[4].appendChild(noteSpan);
			}

			// Add icon to edit review queue note.
			lnkNoteEdit = document.createElement("a");
			lnkNoteEdit.id = 'lnkNoteEdit' + i.toString();
			lnkNoteEdit.name = 'lnkNoteEdit' + i.toString();
			lnkNoteEdit.setAttribute('wptId', wptId);
			lnkNoteEdit.setAttribute('numid', i);
			lnkNoteEdit.href = 'javascript:void(0)';
			lnkNoteEdit.title = 'Add/Edit a Review Queue Note';
			lnkNoteEdit.addEventListener("click", fNoteEditClicked, true);
			imgNoteEdit = document.createElement("img");
			imgNoteEdit.src = NoteEditImg;
			imgNoteEdit.border = '0';
			imgNoteEdit.style.marginLeft = '6px';
			imgNoteEdit.style.marginBottom = '-2px';
			lnkNoteEdit.appendChild(imgNoteEdit);
			xRow.cells[5].appendChild(lnkNoteEdit);
		}
	}
	delete jsObj;

	// Generate list of cache links.
	var xPathSearch = "//a[contains(@href, 'seek\/cache_details.aspx')]";
	var allCacheLinks = document.evaluate(
			xPathSearch, document, null,
			XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,
			null);
	var maxLinks = allCacheLinks.snapshotLength;

	// Create span to hold new controls.
	var kzSpan = document.createElement('div');
	kzSpan.id = 'kzSpan';
	kzSpan.style.marginBottom = '5px';
	kzSpan.style.backgroundColor = 'rgb(245, 247, 200)';

	var selHold = document.createElement("select");
	selHold.id = "selHold";
	selHold.title = 'Automated action to take.';
	selHold.style.marginRight = '12px';

	optHold = document.createElement("option");
	optHold.value = -1;
	optHold.setAttribute("style", "font-weight: bold; text-align: center; font-style: italic;");
	optHold.appendChild(document.createTextNode("-Action to Perform-"));
	selHold.appendChild(optHold);

	optHold = document.createElement("option");
	optHold.value = "open-rev";
	optHold.appendChild(document.createTextNode("Open Review Page"));
	optHold.selected = true;
	selHold.appendChild(optHold);

	optHold = document.createElement("option");
	optHold.value = "open-norm";
	optHold.appendChild(document.createTextNode("Open Cache Page"));
	selHold.appendChild(optHold);

	optHold = document.createElement("option");
	optHold.value = "logbook";
	optHold.appendChild(document.createTextNode("Open Logbook"));
	selHold.appendChild(optHold);

	optHold = document.createElement("option");
	optHold.value = "hold";
	optHold.appendChild(document.createTextNode("Hold"));
	selHold.appendChild(optHold);

	optHold = document.createElement("option");
	optHold.value = "remhold";
	optHold.appendChild(document.createTextNode("Unhold"));
	selHold.appendChild(optHold);

	optHold = document.createElement("option");
	optHold.value = "watch";
	optHold.appendChild(document.createTextNode("Watch"));
	selHold.appendChild(optHold);

	optHold = document.createElement("option");
	optHold.value = "remwatch";
	optHold.appendChild(document.createTextNode("Unwatch"));
	selHold.appendChild(optHold);

	optHold = document.createElement("option");
	optHold.value = "lock";
	optHold.appendChild(document.createTextNode("Lock"));
	selHold.appendChild(optHold);

	optHold = document.createElement("option");
	optHold.value = "unlock";
	optHold.appendChild(document.createTextNode("Unlock"));
	selHold.appendChild(optHold);

	optHold = document.createElement("option");
	optHold.value = "postlog";
	optHold.appendChild(document.createTextNode("Post Log"));
	selHold.appendChild(optHold);

	optHold = document.createElement("option");
	optHold.value = "oakill";
	optHold.appendChild(document.createTextNode("Owner Action - Kill"));
	selHold.appendChild(optHold);

	optHold = document.createElement("option");
	optHold.value = "kzkill";
	optHold.appendChild(document.createTextNode("Kill Zone - Kill"));
	selHold.appendChild(optHold);

	kzSpan.appendChild(selHold);

	// Add controls to span.
	var aNextGroup = document.createElement('a');
	aNextGroup.href = 'javascript:void(0)';
	aNextGroup.id = 'aNextGroup';
	aNextGroup.title = 'Launch next group in tabs';
	aNextGroup.appendChild(document.createTextNode('Open Group'));
	kzSpan.appendChild(aNextGroup);
	kzSpan.appendChild(document.createTextNode(' starting with '));
	inputNextIndex = document.createElement('input');
	inputNextIndex.type = 'text';
	inputNextIndex.name = 'inputNextIndex';
	inputNextIndex.id = 'inputNextIndex';
	inputNextIndex.size = 4;
	inputNextIndex.maxlength = 4;
	inputNextIndex.value = GM_getValue("UrlIndex", 1);
	kzSpan.appendChild(inputNextIndex);

	var aReset = document.createElement('a');
	aReset.href = 'javascript:void(0)';
	aReset.id = 'aReset';
	aReset.title = 'Reset starting index';
	aReset.style.marginLeft = '14px';
	aReset.appendChild(document.createTextNode('Reset'));
	kzSpan.appendChild(aReset);
	kzSpan.appendChild(document.createTextNode(' '));
	kzSpan.appendChild(document.createTextNode(' \u00A0 Caches: ' + maxLinks + ' displayed'));
	kzSpan.appendChild(document.createElement('br'));

	var pageUrl = document.location.href + '';
	if (pageUrl.match('geocaching.com/bookmarks/')) {
		kzSpan.style.paddingLeft = '8px';
		aNextGroup.setAttribute('class', 'miniwhite');
		inputNextIndex.style.marginTop = '3px';
		inputNextIndex.style.height = '14px';
	}

	// Add span to page.
	var allTables = document.getElementsByTagName('TABLE');
	var firstTable = allTables[0];
	insertAheadOf(kzSpan, firstTable)

	// Add event listener.
	aNextGroup.addEventListener("click", fLoadNextGroup, true);
	aReset.addEventListener("click", fReset, true);

	// Monitor for storage change.
	window.addEventListener('storage', fStorageChanged, false);

	// If paging active, duplicate paging control to the top of the screen.
	var TableList = document.evaluate(
		".//table[(contains(descendant::*, 'Total Records:'))]",
		document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
	var maxTables = TableList.snapshotLength;
	if (maxTables) {
		var thisTable = TableList.snapshotItem(maxTables - 1);
		var tableClone = thisTable.cloneNode(true);
		var lstOwner = document.getElementById('ctl00_ContentBody_ListInfo_uxListOwner');
		lstOwner.parentNode.appendChild(tableClone);
	}

//****************************************************************************************//
//                                                                                        //
//                         Functions                                                      //
//                                                                                        //
//****************************************************************************************//

	// Replacement function for check all/none.
	function fCheckAll() {
		var chkStatus = !eval(this.getAttribute('chkStatus', false));
		this.setAttribute('chkStatus', chkStatus);
		var boxes = document.getElementsByName('BID');
		for (var i in boxes) {
			boxes[i].checked = chkStatus;
		}
	}

	// Copy list of displayed caches to the clipboard.
	function fCopyListToClipboard1() { fCopyListToClipboard(false); }
	function fCopyListToClipboard2() { fCopyListToClipboard(true); }
	function fCopyListToClipboard(bWptSort) {
		var allRevLinks = document.getElementsByClassName('review_link');
		var cList = [];
		var ic = allRevLinks.length;
		for (var i = 0; i < ic; i++) {
			cList.push(allRevLinks[i].firstChild.data);
		}
		if (bWptSort) { cList.sort(); }
		cListX = cList.join('\n');
		GM_setClipboard(cListX);
		alert(`${ic} caches copied to the clipboard.`);
	}

	// Change browser page position for all open review pages.
	function fPositionRevPages() {
		if (this.getAttribute('hash') == '~publish~') {
			var rslt = confirm('Publish all open Review pages?');
			if (!rslt) { return; }
		}
		window.localStorage.setItem(REV_PAGE_POS_KEY, '');
		window.localStorage.setItem(REV_PAGE_POS_KEY, this.getAttribute('hash'));
	}

	// Open next group of cache pages.
	function fLoadNextGroup() {
		// Launch cache page function.
		var fLaunchCachePage = function() {
			var idx = GM_getValue("UrlIndex", 1)

			// Calculate index to load group in reverse order.
			var c = startIndex + (endIndex - idx) - 1;
			var thisLink = allCacheLinks.snapshotItem(c);

			// Modify cache URL to open at logs anchor.
			var CacheUrl = thisLink.href;
		    var cGuid = UrlParm('guid', true, CacheUrl);

			if (selHold.value == 'open-rev') {
				CacheUrl = '/admin/review.aspx?guid=' + cGuid + '&nc=12';
			} else if (selHold.value == 'open-norm') {
				CacheUrl = '/seek/cache_details.aspx?guid=' + cGuid;
			} else if (selHold.selectedIndex >= 4 && selHold.selectedIndex <=9) {
				CacheUrl = '/admin/review.aspx?guid=' + cGuid + '&action=' + selHold.value;
			} else if (selHold.value == 'postlog') {
				CacheUrl = '/seek/log.aspx?wid=' + cGuid;
			} else if (selHold.value == 'logbook') {
				CacheUrl = '/seek/cache_logbook.aspx?guid=' + cGuid + '#tabs-1';
			} else if (selHold.value == 'oakill') {
				CacheUrl = '/seek/cache_logbook.aspx?guid=' + cGuid + '&killoawarn=true';
			} else if (selHold.value == 'kzkill') {
				CacheUrl = '/seek/cache_logbook.aspx?guid=' + cGuid + '&killkzwarn=true';
			}

			// Open cache in new tab.
			GM_openInTab(CacheUrl);

			// Increment counters.
			idx++;
			inputNextIndex.value = idx;
			GM_setValue("UrlIndex", idx);
		}

		// Get first checked.
		var firstChecked = maxLinks;
		var xPathSearch = "//input[(@type='checkbox' and @name='BID') ]";
		var allCheckboxes = document.evaluate(
				xPathSearch, document, null,
				XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,
				null);
		var ac = allCheckboxes.snapshotLength;
		if (ac > 0) {
			for (var i = 0; i < ac; i++) {
				if (allCheckboxes.snapshotItem(i).checked) {
					firstChecked = i + 1;
					break;
				}
			}
		}

		if (selHold.value == '-1') {
			alert('No action selected');
		} else {
			// Get starting index and range.
			var startIndex = inputNextIndex.value - 0;
			GM_setValue("UrlIndex", startIndex);

			// Calculate range to open.
			var endIndex = startIndex + GroupSize - 1;
			endIndex = Math.min(endIndex, maxLinks, firstChecked);
			var thisGroupSize = endIndex - startIndex;

			// Invoke function for each cache in range.
			var qTimes = 0;
			for (i = startIndex; i <= endIndex; i++) {
				var TimeOutID = window.setTimeout(fLaunchCachePage, TimeDelay * qTimes);
				qTimes++;
			}
		}
	}

	// Reset start index back to 1.
	function fReset() {
		GM_setValue("UrlIndex", 1);
		inputNextIndex.value = 1;
	}

	// Logs selector changed.
	function fLogsChanged() {
		GM_setValue("NumLogs", selLogs.value);
	}

	// Note Edit.
	function fNoteEditClicked() {
		// Get any current text.
		var wptid = this.getAttribute('wptid');
		var jsRqnObj = new Object;
		var jsonRqnString = window.localStorage.getItem(JSON_RQN_KEY);
		if (jsonRqnString) {
			jsRqnObj = JSON.parse(jsonRqnString);
			if (jsRqnObj.hasOwnProperty(wptid)) {
				var rqnt = jsRqnObj[wptid]['text'];
			} else {
				rqnt = '';
			}
		}

		// Display note for edit.
		var rtnval = prompt('Enter your Review Queue Note for ' + wptid + ' below. ' +
				'Press ENTER to save. Press CANCEL to abort.', rqnt);

		// If OK clicked, update note.
		if (rtnval != null) {
			var note = rtnval.trim();
			// ShowRmvRQNote(wptid, note);
			if (note) {
				jsRqnObj[wptid] = {'text': note};
			} else {
				delete jsRqnObj[wptid];
			}

			// Save to local storage for access by other scripts.
			var jsonRqnString = JSON.stringify(jsRqnObj);
			window.localStorage.setItem(JSON_RQN_KEY, jsonRqnString);
		}
		delete jsRqnObj;
		fStorageChanged('update');
	}

	// Processing storage change.
	function fStorageChanged(e) {
		if (e.key == JSON_RQN_KEY || e == 'update') {
			// Remove existing notes.
			var allNotes = document.getElementsByClassName('rqnText');
			var ic = allNotes.length;
			for (var i = ic-1; i >= 0; i--) {
				allNotes[i].parentNode.removeChild(allNotes[i]);
			}

			// Put updated data into JSON object.
			var jsObj = new Object;
			if (e.newValue) {
				jsObj = JSON.parse(e.newValue);
			} else {
				var jsonString = localStorage.getItem(JSON_RQN_KEY);
				jsObj = JSON.parse(jsonString);
			}

			// Generate list of waypoint id links.
			var allRevLinks = document.getElementsByClassName('review_link');
			var ic = allRevLinks.length;
			for (var i = ic-1; i >= 0; i--) {
				var wptId = allRevLinks[i].firstChild.data;

				// Check if this waypoint id has a note.
				if (jsObj.hasOwnProperty(wptId)) {
					// Find row, and add note to cache name cell.
					var xRow = allRevLinks[i].parentNode;
					while (xRow.nodeName.toUpperCase() != 'TR') {
						xRow = xRow.parentNode;
					}
					var rqnValue = jsObj[wptId]['text'];
					var noteSpan = document.createElement('span');
					noteSpan.classList.add('rqnText');
					noteSpan.appendChild(document.createTextNode(rqnValue));
					xRow.cells[4].appendChild(noteSpan);
				}
			}

			// Delete JSON object.
			delete jsObj;
		}
	}

	// Insert element after an existing element.
	function insertAfter(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement.nextSibling);
	}

	// Insert element aheadd of an existing element.
	function insertAheadOf(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement);
	}

	// Returns a URL parameter.
	//  ParmName - Parameter name to look for.
	//  IgnoreCase - (optional) *false, true. Ignore parmeter name case.
	//  UrlString - (optional) String to search. If omitted, document URL is used.
	function UrlParm(ParmName, IgnoreCase, UrlString) {
		var RegRslt, sc = '', RtnVal = '';
		if (IgnoreCase) {sc = 'i'}
		if(UrlString) {
			var PageUrl = UrlString;
		} else {
			PageUrl = document.location + '';
		}
		var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
		var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
		RegRslt = RegEx1.exec(ParmString);
		if (RegRslt) {RtnVal = RegRslt[2]}
		return RtnVal;
	}
