﻿// ==UserScript==
// @name           GCR GMail Review Links v2
// @description    GMail Review Links
// @namespace      http://www.geocaching.com/admin
// @version        02.01
// @icon           http://i.imgur.com/iEntr3d.png
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Gmail_Review_Links.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Gmail_Review_Links.user.js
// @include        http://mail.google.com/*
// @include        https://mail.google.com/*
// @grant          GM_getValue
// @grant          GM_openInTab
// @grant          GM_setValue
// @grant          GM_registerMenuCommand
// ==/UserScript==

/*

Function:
 Adds Reviewer Page Links To Logs Viewed In GMail. Renames cache
 page links to "Cache Page".

*/

	var oldHash = '';
	var TimeOutID = 0;
	var TimeOutID2 = 0;
	var rnd = Math.random();

	// Menu options to set optional values.
	GM_registerMenuCommand('Turn Cache Page Auto-Open On/Off', fToggleAutoOpen);
	GM_registerMenuCommand('Set Number of Logs for Cache Page', fSetNumOfLogs);
	GM_registerMenuCommand('Set Number of Nearby Caches on Review Page', fSetNumOfCaches);

	// Get optional values.
	var Logs_to_Show = (GM_getValue('LogsToShow', '20'));
	var Caches_To_Show = (GM_getValue('CachesToShow', '12'));
	TimeOutID = window.setInterval(chkHash, 4000);

//****************************************************************************************//
//                                                                                        //
//                         Functions                                                      //
//                                                                                        //
//****************************************************************************************//

	function chkHash() {
		if (location.hash != oldHash) {
			oldHash = location.hash;
			if (oldHash != '#inbox') {
				chklinks();
			}
		}
	}

	// Locate "info" links, convert to original format, and create review link.
	function chklinks(){
		var CacheUrl;
		var changed = false;
		if (TimeOutID2) {
			clearInterval(TimeOutID2);
			TimeOutID2 = 0;
		}

		// Generate list of coord.info links for caches.
		xPathSearch = "//a[starts-with(@href, 'http://coord.info/GC')]";
		LinkList = document.evaluate(
			xPathSearch,
			document,
			null,
			XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
			null);

		// If at least one cache page link.
		ic = LinkList.snapshotLength;
		if (ic > 0) {
			changed = true;

			// Change URL for each cache page link.
			for (var i = 0; i < ic; i++) {
				// Get link's URL.
				var ThisLink = LinkList.snapshotItem(i);
				var CacheUrl = ThisLink.href;

				// Function convert coord.info URL to standard cache page URL.
				var orgCacheLink = fInfo2Ref(CacheUrl, 'C');

				// Change link to original format.
				ThisLink.href = orgCacheLink;
				ThisLink.target = "_blank";
			}
		}

		// Generate list of links for caches.
		xPathSearch = "//a[starts-with(@href, 'http://www.geocaching.com/seek/cache_details.aspx')]";
		LinkList = document.evaluate(
			xPathSearch,
			document,
			null,
			XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
			null);

		// If at least one cache page link.
		ic = LinkList.snapshotLength;
		if (ic > 0) {
			changed = true;

			// Add review page link after every cache page link.
			for (var i = 0; i < ic; i++) {
				// Get link's URL.
				var ThisLink = LinkList.snapshotItem(i);
				if (!ThisLink.getAttribute('edited', false)) {
					CacheUrl = ThisLink.href;

					// Change text to 'Cache Page'. Old text may be broken into multiple parts.
					while(ThisLink.hasChildNodes()){
						ThisLink.removeChild(ThisLink.lastChild);
					}
					ThisLink.style.fontVariant = 'small-caps';
					ThisLink.style.fontWeight = 'bolder';
					ThisLink.appendChild(document.createTextNode('Cache Page'));

					// Add number of logs to show to the URL.
					ThisLink.href = CacheUrl + '&log=n&numlogs=' + Logs_to_Show;
					ThisLink.setAttribute('edited', true);

					// Generate review page URL.
					var RevUrl = CacheUrl.replace(/seek\/cache_details\./, 'admin/review.');
					RevUrl += '&nc=' + Caches_To_Show;

					// Create new link.
					var newLink = document.createElement("a");
					newLink.href = RevUrl;
					newLink.target = '_blank';
					newLink.style.marginLeft = '10px';
					newLink.onclick = 'return top.js.OpenExtLink(window,event,this)';

					// Insert link into document.
					ThisLink.parentNode.insertBefore(newLink, ThisLink.nextSibling);

					// Add link text.
					newLink.style.fontVariant = 'small-caps';
					newLink.style.fontWeight = 'bolder';
					var newText = document.createTextNode('Review Page');
					newLink.appendChild(newText);

					// Add space after new link.
					var lkSpacer2 = document.createTextNode(' ');
					newLink.parentNode.insertBefore(lkSpacer2, newLink.nextSibling);
				}
			}

			// If auto-open cache page is turned on, open in a new tab.
			if (GM_getValue('AutoOpen', 'Off') == 'On') {
				if (CacheUrl) {
					GM_openInTab(CacheUrl + '&log=n&numlogs=20');
				}
			}
		}

		if (!changed) {
			TimeOutID2 = window.setInterval(chklinks, 250);
		}
	}

	// Converts INFO cache page URL to old style Cache page or Review Page URL (Pass "C" or "R").
	// http://coord.info/GC2AEJX
	// http://www.geocaching.com/seek/cache_details.aspx?wp=GC2AEJX
	// http://www.geocaching.com/admin/review.aspx?wp=GC2AEJX
	function fInfo2Ref(infoUrl, pagetype) {
		var RtnVar = "";
		var i = infoUrl.toUpperCase().lastIndexOf("/GC");
		if (i >= 0) {
			switch(pagetype.toUpperCase())
			{
			case 'C':
				RtnVar = "http://www.geocaching.com/seek/cache_details.aspx?wp=" + infoUrl.substr(i + 1);
				break;
			case 'R':
				RtnVar = "http://www.geocaching.com/admin/review.aspx?wp=" + infoUrl.substr(i + 1);
				break;
			}
		}

		return RtnVar;
	}
	// Test for positive numeric-only data.
	function isPositiveNumeric(value) {
		if (value == null || !value.toString().match(/^\d*\.?\d*$/)) {
			return false;
		} else {
			return true;
		}
	}

	// Toggle Auto-open value.
	function fToggleAutoOpen() {
		var getVar = (GM_getValue('AutoOpen', 'Off'));
		var xRtnVar = confirm('Click OK to automatically open the cache page when \n' +
				'an email generated from that cache is displayed.\n\n' +
				'Click CANCEL to turn off this feature.\n\n' +
				'Currently this feature is turned ' + getVar + '.');

		if (xRtnVar) {
			autoOpen = 'On';
		} else {
			autoOpen = 'Off';
		}

		GM_setValue('AutoOpen', autoOpen);
		alert('Auto Open of cache pages has been turned ' + autoOpen + '.');
	}

	// Set number of logs for cache page.
	function fSetNumOfLogs() {
		var getVar = (GM_getValue('LogsToShow', '20'));
		var newNum = prompt('Number of logs to initially display on cache page?', getVar);
		if (newNum != null) {
			newNum = newNum.trim();
			if (newNum && isPositiveNumeric(newNum)) {
				newNum = parseInt(newNum,10).toString();
				Logs_To_Show = newNum;
				GM_setValue('LogsToShow', newNum);
				alert('Logs to display has been set to ' + newNum + '.\n' +
						'You may need to refresh the page to see the change.');
			} else {
				alert('Invalid entry. Please use integers only');
				fSetNumOfLogs();
			}
		} else {
			alert('Update canceled');
		}
	}

	// Set number of nearby caches for review page.
	function fSetNumOfCaches() {
		var getVar = (GM_getValue('CachesToShow', '12'));
		var newNum = prompt('Number of nearby caches to initially display on Review page?', getVar);
		if (newNum != null) {
			newNum = newNum.trim();
			if (newNum && isPositiveNumeric(newNum)) {
				newNum = parseInt(newNum,10).toString();
				Caches_To_Show = newNum;
				GM_setValue('CachesToShow', newNum);
				alert('Caches to display has been set to ' + newNum + '.\n' +
						'You may need to refresh the page to see the change.');
			} else {
				alert('Invalid entry. Please use integers only');
				fSetNumOfCaches();
			}
		} else {
			alert('Update canceled');
		}
	}
