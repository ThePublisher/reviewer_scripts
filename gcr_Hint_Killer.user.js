﻿// ==UserScript==
// @name           GCR Hint Killer v2
// @description    Quick Delete of Hints
// @namespace      http://www.geocaching.com/admin
// @version        02.01
// @icon           http://i.imgur.com/s4O08IN.png
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Hint_Killer.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Hint_Killer.user.js
// @include        http*://*.geocaching.com/admin/review.aspx*
// @include        http*://*.geocaching.com/hide/report.aspx*
// @grant          GM_addStyle
// ==/UserScript==

/*

Function:
 Quick delete of worthless hints.
 Quick decryption of hints that should not be encoded.
 Quick enable/disable of unpublished pages.
 Quick enable may also include unwatching the cache.

NOTE (Important!):
 You must change your system's configuration in order to allow the script to close a tab.
 Type about:config in the URL locator.
 Change dom.allow_scripts_to_close_windows from false to true.

*/

	var Copy2ClipboardSrc =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAA" +
		"yVBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" +
		"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUdt1UofBZ" +
		"o%2FBepvBgp%2FBhqPFkqfFnq%2FFprPFsrvJtrvJysfFzsfJ9t%2FJ%2Bt%2FOJvfSJvv" +
		"SWxfWshQDAwMDO4Pfq6urr6%2Bvs7Ozt7e3u7u7v7%2B%2Fw8PDx8fH09PT29vb6%2Bvr%" +
		"2FxgD%2F0TP%2F3Gb%2F6JmpHrZvAAAAH3RSTlMAAQIDBAUGBwgJCwwPEBITFRYaOj4%2F" +
		"QEFCQ0VMTU9R4GGmRgAAAKhJREFUGBkFwUtOAlEQAMDqnsdPSETcGJcuXOr9r8IBXKkgCR" +
		"kIzGurQkQOL%2Fjp09SrWuSH32fY2U%2B3PuTn0%2BowjuPodbU%2BRm%2BpvIFiUdlCcQ" +
		"RbmdFC0bf4O2RGJOh1OlVvt4hM0GuzqX7pu8wGrnXmsfr37NZArweqqg9DAzVdoVVEA2FJ" +
		"VQWtIOICi6DV%2Fp20hJ5EzmeL%2BXoIqqrO51b3qPtllQH9yz80T0%2BF6QnzjAAAAABJ" +
		"RU5ErkJggg%3D%3D";

	// Constants
	const TYPE_UNKNOWN  = '0';
	const TYPE_PHYSICAL = '1';
	const TYPE_VIRTUAL  = '2';

	// Set global variables for flashing icons when clicked.
	var t2 = [], i2 = [];

	// Get current domain.
	var domain = location.hostname;

	// Set style class for clickable icons.
	GM_addStyle(".click-icon {vertical-align: text-bottom; cursor: pointer; margin-left:8px }");
	GM_addStyle(".noclick-icon {vertical-align: text-bottom; margin-right:2px }");

	// Fade/Flash Clicked Icons. Can handle any number of icons simultaneously.
	var t2 = [], i2 = [];  // Set at global level.

	function fFadeClickedIcon(imgToFade) {
		imgToFade.style.opacity = 0.2;
		t2.unshift(0);
		i2.unshift(imgToFade);
		t2[0] = window.setTimeout(
			function() {
				window.clearTimeout(t2.pop());
				i2.pop().style.opacity = 1;
			}
		, 650);
	}

	// Review Page.
	if (location.href.match(/^https?:\/\/.*?\.geocaching.com\/admin\/.*/)) {
		var snapback = sessionStorage.getItem('snapback-hintkiller');
		sessionStorage.removeItem('snapback-hintkiller');
		if (snapback > 0) {
			document.documentElement.scrollTop = snapback;
		}

		// Get cache data.
		var dataDiv = document.getElementsByClassName('CacheData')[0];

		// Set cache status variables.
		var cachetypeid = dataDiv.getAttribute('data-cachetypeid');
		var cacheguid   = dataDiv.getAttribute('data-cacheguid');
		var iamwatching = (dataDiv.getAttribute('data-iamwatching') == 'true');
		var ispublished = (dataDiv.getAttribute('data-ispublished') == 'true');
		var isarchived  = (dataDiv.getAttribute('data-isarchived')  == 'true');
		var isdisabled  = (dataDiv.getAttribute('data-isdisabled')  == 'true');
		var isactive    = (!isarchived && !isdisabled);

		// Find hint heading.
		var e_CacheDetails_Hints = document.getElementById("ctl00_ContentBody_CacheDetails_Hints");

		// If hint was entered.
		if (e_CacheDetails_Hints.firstChild.data != 'No Hints Available') {
			var hintHeading = e_CacheDetails_Hints.previousSibling.previousSibling;
			var hintCtrl = document.createElement('span');
			hintCtrl.id = 'hintCtrl';
			hintHeading.appendChild(hintCtrl);
			hintCtrl.style.fontSize = 'x-small';
			hintCtrl.style.fontWeight = 'bold';
			hintCtrl.appendChild(document.createTextNode(' ['));

			var a_hintKiller = document.createElement('a');
			hintCtrl.appendChild(a_hintKiller);
			a_hintKiller.appendChild(document.createTextNode('remove'));
			hintCtrl.appendChild(document.createTextNode('] ['));

			var a_hintFixer = document.createElement('a');
			hintCtrl.appendChild(a_hintFixer);
			a_hintFixer.appendChild(document.createTextNode('decrypt'));
			hintCtrl.appendChild(document.createTextNode(']'));

			a_hintKiller.href = 'javascript:void(0)';
			a_hintKiller.title = 'Remove hint';
			a_hintKiller.addEventListener('click', OpenEditorKill, true);

			a_hintFixer.href = 'javascript:void(0)';
			a_hintFixer.title = 'Fix hint';
			a_hintFixer.addEventListener('click', OpenEditorFix, true);
		}

		// Get status.
		e_CacheDetails_Status = document.getElementById("ctl00_ContentBody_CacheDetails_Status");

		// If unpublished and inactive.
		if (e_CacheDetails_Status) {
			if (!ispublished && isdisabled) {
				var aToActive = document.createElement('a');
				aToActive.appendChild(document.createTextNode('\u2192Active'));
				aToActive.href = 'javascript:void(0)';
				aToActive.addEventListener('click', OpenEditorActive, false);
				e_CacheDetails_Status.appendChild(document.createTextNode(' '));
				e_CacheDetails_Status.appendChild(aToActive);

				if (iamwatching) {
					var aToUnwatch = document.createElement('a');
					aToUnwatch.appendChild(document.createTextNode('& Unwatch'));
					aToUnwatch.href = 'javascript:void(0)';
					aToUnwatch.addEventListener('click', ftoActiveAndUnwatch, false);
					e_CacheDetails_Status.appendChild(document.createTextNode(' '));
					e_CacheDetails_Status.appendChild(aToUnwatch);
				}
			}

			// If unpublished and active.
			if (!ispublished && isactive) {
				var aToDisable = document.createElement('a');
				aToDisable.appendChild(document.createTextNode('\u2192Disable'));
				aToDisable.href = 'javascript:void(0)';
				aToDisable.addEventListener('click', OpenEditorDisable, false);
				e_CacheDetails_Status.appendChild(document.createTextNode(' '));
				e_CacheDetails_Status.appendChild(aToDisable);
			}
		}

		// If not Traditional, Event, CITO, Earthcache, or Mega-Event...
		if (['2','6','13','137','453'].indexOf(cachetypeid) == -1) {
			var coordType = document.getElementById('ctl00_ContentBody_CacheDetails_coordType');

			if (coordType) {
				var coordTypeText = coordType.firstChild.data.trim();
				// Following is temporary, until DIV data elements are available.
				var coordTypeID = TYPE_UNKNOWN;
				if (coordTypeText == 'Physical') {
					coordTypeID = TYPE_PHYSICAL;
					curCoordTypeImg = document.createElement('img');
					curCoordTypeImg.classList.add('noclick-icon');
					curCoordTypeImg.src = '/images/WptTypes/sm/2.gif';
					insertAheadOf(curCoordTypeImg, coordType);
				}
				if (coordTypeText == 'Virtual') {
					coordTypeID = TYPE_VIRTUAL;
					curCoordTypeImg = document.createElement('img');
					curCoordTypeImg.classList.add('noclick-icon');
					curCoordTypeImg.src = '/images/WptTypes/sm/4.gif';
					insertAheadOf(curCoordTypeImg, coordType);
				}
				// --------

				// Create controls.
				var spanUpdateCoordType = document.createElement('span');
				spanUpdateCoordType.id = 'spanUpdateCoordType';
				spanUpdateCoordType.appendChild(document.createTextNode('\u2192'));

				if (coordTypeID == TYPE_VIRTUAL || coordTypeID == TYPE_UNKNOWN) {
					var imgToPhysical = document.createElement('img');
					imgToPhysical.id = 'imgToPhysical';
					imgToPhysical.src = '/images/WptTypes/sm/2.gif';
					imgToPhysical.title = 'Click to Change type to Physical';
					imgToPhysical.classList.add('click-icon');
					imgToPhysical.setAttribute('CoordType', TYPE_PHYSICAL);
					imgToPhysical.addEventListener('click', fUpdateCoordTypeClicked, true);
					spanUpdateCoordType.appendChild(imgToPhysical);
				}

				if (coordTypeID == TYPE_PHYSICAL || coordTypeID == TYPE_UNKNOWN) {
					var imgToVirtual = document.createElement('img');
					imgToVirtual.id = 'imgToVirtual';
					imgToVirtual.src = '/images/WptTypes/sm/4.gif';
					imgToVirtual.title = 'Click to Change type to Virtual';
					imgToVirtual.classList.add('click-icon');
					imgToVirtual.setAttribute('CoordType', TYPE_VIRTUAL);
					imgToVirtual.addEventListener('click', fUpdateCoordTypeClicked, true);
					spanUpdateCoordType.appendChild(imgToVirtual);
				}
				insertAfter(spanUpdateCoordType, coordType.nextSibling);
			}
		}
	// Edit page and close.
	} else {
		if (sessionStorage.getItem('autoclose') == 'true') {
			window.opener.location.reload();
			window.close();
			return;
		}

		var e_btnSubmit = document.getElementById("ctl00_ContentBody_btnSubmit");
		var newCoordType = UrlParm('coordtype', true);
		if (newCoordType) {
			var rbCoords = document.getElementsByName('ctl00$ContentBody$PostedCoordinateTypeRadioButtons');
			if (rbCoords.length) {
				var ic = rbCoords.length;
				for (var i = 0; i < ic; i++) {
					if (rbCoords[i].getAttribute('value') == newCoordType) {
						rbCoords[i].checked = true;
					}
				}
				sessionStorage.setItem('autoclose', 'true');
				e_btnSubmit.click();
			}
		}

		if (UrlParm('killhint', true) == 'y') {
			var e_gsHints = document.getElementById("ctl00_ContentBody_tbHints");
			if (e_gsHints.value.length > 0) {
				e_gsHints.value = '';
				sessionStorage.setItem('autoclose', 'true');
				e_btnSubmit.click();
			}
		}

		if (UrlParm('fixhint', true) == 'y') {
			var e_gsHints = document.getElementById("ctl00_ContentBody_tbHints");
			if (!e_gsHints.value.match(/^\[.*\]$/)) {
				e_gsHints.value = '[' + e_gsHints.value + ']';
				sessionStorage.setItem('autoclose', 'true');
				e_btnSubmit.click();
			}
		}

		if (UrlParm('setactive', true) == 'y') {
			var e_chkIsActive = document.getElementById("ctl00_ContentBody_chkIsActive");
			if (e_chkIsActive) {
				if (!e_chkIsActive.checked) {
					e_chkIsActive.checked = true;
					sessionStorage.setItem('autoclose', 'true');
					e_btnSubmit.click();
				}
			} else {
				window.close();
			}
		}

		if (UrlParm('setdisable', true) == 'y') {
			var e_chkIsActive = document.getElementById("ctl00_ContentBody_chkIsActive");
			if (e_chkIsActive) {
				if (e_chkIsActive.checked) {
					e_chkIsActive.checked = false;
					sessionStorage.setItem('autoclose', 'true');
					e_btnSubmit.click();
				}
			} else {
				window.close();
			}
		}

		// Add paste-in box.
		var coordSelect = document.getElementsByName('ctl00$ContentBody$LatLong')[0];

		var txtaraPasteIn = document.createElement('textarea');
		txtaraPasteIn.id = 'txtaraPasteIn';
		txtaraPasteIn.style.width = '253px';
		txtaraPasteIn.style.height = '40px';
		txtaraPasteIn.style.marginTop = '7px';
		insertAfter(txtaraPasteIn, coordSelect);
		insertAfter(document.createElement('br'), coordSelect);

		var imgPasteIn = document.createElement('img');
		imgPasteIn.id = 'imgPasteIn';
		imgPasteIn.src = Copy2ClipboardSrc;
		imgPasteIn.style.marginBottom = '10px';
		imgPasteIn.classList.add('click-icon');
		insertAfter(imgPasteIn, txtaraPasteIn);
		imgPasteIn.addEventListener('mousedown', fCoordPasteIn, true);
	}

	// Set to active, and stop watching.
	function ftoActiveAndUnwatch() {
		OpenEditorActive();
		var e_ddAction = document.getElementById('ctl00_ContentBody_ddAction');
		var e_btnGo = document.getElementById('ctl00_ContentBody_btnGo');
		for (i = 0; i < e_ddAction.length; i++) {
			if (e_ddAction.options[i].value == 'DoNotWatch') {
				e_ddAction.selectedIndex = i;
					e_btnGo.click();
				break;
			}
		}
	}

	// Coordinates Paste-in Clicked.
	function fCoordPasteIn() {
		fFadeClickedIcon(this);
		var cs = document.getElementById('txtaraPasteIn').value;
		var nums = new Array();
		var j = -1;
		var dig = false;
		for (var i = 0; i < cs.length; i++) {
			c = cs.substr(i,1);
			if (c.match(/\d/)) {
				if (!dig) {
					j++;
					nums[j]='';
					dig = true;
				}
				nums[j] += c;
			} else {
				dig = false;
			}
		}

		var coorTab = document.getElementsByName("ctl00$ContentBody$LatLong")[0];
		var WptEntryType = coorTab.value;
		var LatDegs = document.getElementById("ctl00_ContentBody_LatLong__inputLatDegs");
		var LonDegs = document.getElementById("ctl00_ContentBody_LatLong__inputLongDegs");

		// Type 0: Decimal Degrees (DegDec)
		if (WptEntryType == 0) {
			if (nums.length < 4) {
				alert('Not enough numeric data elements.');
			} else {
				LatDegs.value = nums[0] + '.' + nums[1];
				LonDegs.value = nums[2] + '.' + nums[3];
			}
		// Type 1: Degrees and minutes (MinDec)
		} else if (WptEntryType == 1) {
			if (nums.length < 6) {
				alert('Not enough numeric data elements.');
			} else {
				var LatMins = document.getElementById("ctl00_ContentBody_LatLong__inputLatMins");
				var LonMins = document.getElementById("ctl00_ContentBody_LatLong__inputLongMins");
				LatDegs.value = nums[0];
				LatMins.value = nums[1] + '.' + nums[2];
				LonDegs.value = nums[3];
				LonMins.value = nums[4] + '.' + nums[5];
			}
		// Type 2: Degrees, minutes, seconds (DMS)
		} else if (WptEntryType == 2) {
			if (nums.length < 8) {
				alert('Not enough numeric data elements.');
			} else {
				var LatMins = document.getElementById("ctl00_ContentBody_LatLong__inputLatMins");
				var LatSecs = document.getElementById("ctl00_ContentBody_LatLong__inputLatSecs");
				var LonMins = document.getElementById("ctl00_ContentBody_LatLong__inputLongMins");
				var LonSecs = document.getElementById("ctl00_ContentBody_LatLong__inputLongSecs");
				LatDegs.value = nums[0];
				LatMins.value = nums[1];
				LatSecs.value = nums[2] + '.' + nums[3];
				LonDegs.value = nums[4];
				LonMins.value = nums[5];
				LonSecs.value = nums[6] + '.' + nums[7];
			}
		}
	}

//****************************************************************************************//
//                                                                                        //
//                         Functions                                                      //
//                                                                                        //
//****************************************************************************************//

	// Open editor on click (update coordinate type).
	function fUpdateCoordTypeClicked() {
		var coordtype = this.getAttribute('CoordType');
		fFadeClickedIcon(this);
		window.open('http://' + domain + '/hide/report.aspx?guid=' +
				cacheguid + '&coordtype=' + coordtype);
		sessionStorage.setItem('snapback-hintkiller', document.documentElement.scrollTop);
	}

	// Open editor on click (kill).
	function OpenEditorKill() {
		window.open('http://' + domain + '/hide/report.aspx?guid=' + cacheguid + '&killhint=y');
		sessionStorage.setItem('snapback-hintkiller', document.documentElement.scrollTop);
	}

	// Open editor on click (fix).
	function OpenEditorFix() {
		window.open('http://' + domain + '/hide/report.aspx?guid=' + cacheguid + '&fixhint=y');
		sessionStorage.setItem('snapback-hintkiller', document.documentElement.scrollTop);
	}

	// Open editor on click (active).
	function OpenEditorActive() {
		window.open('http://' + domain + '/hide/report.aspx?guid=' + cacheguid + '&setactive=y');
		sessionStorage.setItem('snapback-hintkiller', document.documentElement.scrollTop);
	}

	// Open editor on click (disable).
	function OpenEditorDisable() {
		window.open('http://' + domain + '/hide/report.aspx?guid=' + cacheguid + '&setdisable=y');
		sessionStorage.setItem('snapback-hintkiller', document.documentElement.scrollTop);
	}

	// Fade Clicked Icons (Needs global arrays t2 and i2). Can handle any number of icons simultaneously.
	function fFadeClickedIcon(imgToFade) {
		imgToFade.style.opacity = 0.2;
		t2.unshift(0);
		i2.unshift(imgToFade);
		t2[0] = window.setTimeout(
			function() {
				window.clearTimeout(t2.pop());
				i2.pop().style.opacity = 1;
			}
		, 650);
	}

	function fRefreshPage() {
		location.reload();
	}

	// Returns a URL parameter.
	//  ParmName - Parameter name to look for.
	//  IgnoreCase - (optional) *false, true. Ignore parmeter name case.
	//  UrlString - (optional) String to search. If omitted, document URL is used.
	function UrlParm(ParmName, IgnoreCase, UrlString) {
		var RegRslt, sc = '', RtnVal = '';
		if (IgnoreCase) {sc = 'i'}
		if(UrlString) {
			var PageUrl = UrlString;
		} else {
			PageUrl = document.location + '';
		}
		var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
		var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
		RegRslt = RegEx1.exec(ParmString);
		if (RegRslt) {RtnVal = RegRslt[2]}
		return RtnVal;
	}

	// Insert element after an existing element.
	function insertAfter(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement.nextSibling);
	}

	// Insert element aheadd of an existing element.
	function insertAheadOf(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement);
	}
