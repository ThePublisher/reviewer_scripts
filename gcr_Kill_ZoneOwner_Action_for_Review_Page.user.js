// ==UserScript==
// @name           GCR Kill Zone/Owner Action for Review Page v2
// @description    Adds Kill Zone and Owner Action Needed links to Review Page
// @namespace      http://www.geocaching.com/admin
// @version        02.01
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Kill_ZoneOwner_Action_for_Review_Page.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Kill_ZoneOwner_Action_for_Review_Page.user.js
// @include        http*://*.geocaching.com/admin/review.aspx?*
// @grant          GM_openInTab
// @grant          GM_addStyle
// @grant          GM_log
// ==/UserScript==

/*

Function:
 Automates month-end clean-up functions.

*/

	// Images.
	var imgsrcOanWarn =
		"data:image/png,%89PNG%0D%0A%1A%0A%00%00%00%0DIHDR%00%00%00%1" +
		"0%00%00%00%10%04%03%00%00%00%ED%DD%E2R%00%00%000PLTE%1C%1C%1" +
		"1%86%86%0EFFEmmy%A2%A2%0A%C7%C7%02%C6%C6%23%C8%C8%B2%DE%DE%1" +
		"0%DE%DE%C1%DE%DE%CE%FF%00%FF%F7%F7%00%FF%FF%00%E7%E7%08%F0%F" +
		"0%B0%D9%D0%09%95%00%00%00%0CtRNS%FF%FF%FF%FF%FF%FF%FF%FF%FF%" +
		"FF%FF%00%12%DF%CE%CE%00%00%00%87IDATx%DAc%D8%BD%7B%F76)%20%C" +
		"1%00%C43%9AW%83%19%BBr%AFX%81%19%3B%7B%EFi%81%19%CB%AE%C59%A" +
		"F%061V%1C%609%5C%0Dd%EC%CAM%60%3Ch%05b%F460%9E%D3%022%96%DF%" +
		"3D%C8rWx5%C3%EEuw%0F%F2%DD%7D%0Cd%E4%DC%BD%91%7B%F7b5%C3%AE%" +
		"DC%BB%D7T%EE%DE%D0b%D8v%F6%EEE%C6%BBw%85%19%B6%DC%BD%7B%A3%E" +
		"3%EE%DD%24%86%ED%A1%AE%AE!!!%E6%0C%BB%8B%94%40%A0%1A%00g%FCK" +
		"pi%10%0B%13%00%00%00%00IEND%AEB%60%82";

	var imgsrcOanKill =
		"data:image/png,%89PNG%0D%0A%1A%0A%00%00%00%0DIHDR%00%00%00%1" +
		"0%00%00%00%10%04%03%00%00%00%ED%DD%E2R%00%00%000PLTE%00%00%0" +
		"0BBBRRRkkk%8C%8C%8C%94%94%94%9C%9C%A5%A5%A5%A5%B5%B5%B5%BD%B" +
		"D%BD%DE%DE%DE%FF%00%FF%FF%F7%EF%FF%FF%FF%FF%FF%FF%FF%FF%FFi%" +
		"9FN%B7%00%00%00%0CtRNS%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%00%1" +
		"2%DF%CE%CE%00%00%00yIDATx%DA%1D%CE1%0A%C30%0C%05%D0%3F%94%80" +
		"%25z%A0%0E%3DJ%C1%04%BA%E4%20%9D%DD%B5%93%A6%80E%0D%DAu%89t%" +
		"C9%60%C8%5D%EA%E8Oo%F8%F0%3F%1C%3E%02%875v%B7%CE%B8%7CN%D4%0" +
		"CK%02Pa%B8%D1%D1%85G%CB%D6%A65%B0%A8%3E%03%5D%BFk%20%A7%92%0" +
		"2%BF%FB%FB%16h%DBu%0FLBy%C0%A8%3Cf%12%06U%E8%0B%8B%C0%B2%03%" +
		"3E%8D%09%3Eo%98%FF%01%FD%00%3D%DD%89%40%B0%E9%00%00%00%00IEN" +
		"D%AEB%60%82";

	var imgsrcKzWarn =
		"data:image/png,%89PNG%0D%0A%1A%0A%00%00%00%0DIHDR%00%00%00%1" +
		"0%00%00%00%10%04%03%00%00%00%ED%DD%E2R%00%00%000PLTE%1C%1C%1" +
		"1%86%86%0EFFEmmy%A2%A2%0A%C7%C7%02%C6%C6%23%C8%C8%B2%DE%DE%1" +
		"0%DE%DE%C1%DE%DE%CE%FF%00%FF%F7%F7%00%FF%FF%00%E7%E7%08%F0%F" +
		"0%B0%D9%D0%09%95%00%00%00%0CtRNS%FF%FF%FF%FF%FF%FF%FF%FF%FF%" +
		"FF%FF%00%12%DF%CE%CE%00%00%00%87IDATx%DAc%D8%BD%7B%F76)%20%C" +
		"1%00%C43%9AW%83%19%BBr%AFX%81%19%3B%7B%EFi%81%19%CB%AE%C59%A" +
		"F%061V%1C%609%5C%0Dd%EC%CAM%60%3Ch%05b%F460%9E%D3%022%96%DF%" +
		"3D%C8rWx5%C3%EEuw%0F%F2%DD%7D%0Cd%E4%DC%BD%91%7B%F7b5%C3%AE%" +
		"DC%BB%D7T%EE%DE%D0b%D8v%F6%EEE%C6%BBw%85%19%B6%DC%BD%7B%A3%E" +
		"3%EE%DD%24%86%ED%A1%AE%AE!!!%E6%0C%BB%8B%94%40%A0%1A%00g%FCK" +
		"pi%10%0B%13%00%00%00%00IEND%AEB%60%82";

	var imgsrcKzKill =
		"data:image/png,%89PNG%0D%0A%1A%0A%00%00%00%0DIHDR%00%00%00%1" +
		"0%00%00%00%10%04%03%00%00%00%ED%DD%E2R%00%00%000PLTE%00%00%0" +
		"0BBBRRRkkk%8C%8C%8C%94%94%94%9C%9C%A5%A5%A5%A5%B5%B5%B5%BD%B" +
		"D%BD%DE%DE%DE%FF%00%FF%FF%F7%EF%FF%FF%FF%FF%FF%FF%FF%FF%FFi%" +
		"9FN%B7%00%00%00%0CtRNS%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%00%1" +
		"2%DF%CE%CE%00%00%00yIDATx%DA%1D%CE1%0A%C30%0C%05%D0%3F%94%80" +
		"%25z%A0%0E%3DJ%C1%04%BA%E4%20%9D%DD%B5%93%A6%80E%0D%DAu%89t%" +
		"C9%60%C8%5D%EA%E8Oo%F8%F0%3F%1C%3E%02%875v%B7%CE%B8%7CN%D4%0" +
		"CK%02Pa%B8%D1%D1%85G%CB%D6%A65%B0%A8%3E%03%5D%BFk%20%A7%92%0" +
		"2%BF%FB%FB%16h%DBu%0FLBy%C0%A8%3Cf%12%06U%E8%0B%8B%C0%B2%03%" +
		"3E%8D%09%3Eo%98%FF%01%FD%00%3D%DD%89%40%B0%E9%00%00%00%00IEN" +
		"D%AEB%60%82";

	// Text checking function.
	String.prototype.startsWith = function(str) {
		return (this.indexOf(str) == 0);
	}

	String.prototype.contains = function(str) {
		return (this.indexOf(str) >= 0);
	}

	String.prototype.endsWith = function(str) {
		return (this.lastIndexOf(str) == this.length() - str.length());
	}

	// Get published status for this cache. If not published, exit script.
	var cacheStatus = document.getElementById("ctl00_ContentBody_CacheDetails_Status");
	if (cacheStatus) {
		var cacheStatusText = cacheStatus.firstChild.data;
		if (cacheStatusText.contains('Not Published')) {
			return;
		}
	}

	// Get cache data element.
	var CacheData = document.getElementById('ctl00_ContentBody_CacheDataControl1_CacheData');
	if (!CacheData) {
		CacheData = document.getElementsByClassName('CacheData')[0];
	}

	if (CacheData) {
		// Get GUID for this cache.
		var CacheGuid = CacheData.getAttribute('data-cacheguid');
	} else {
		GM_log('Unable to locate CacheData element. Script ending.');
		return;
	}

	// Add styles for links.
	GM_addStyle(".OAN_Background { background-color: rgb(252, 250, 164); " +
			"margin-right: 10px; font-variant: small-caps; font-weight: bolder; } ");
	GM_addStyle(".KZ_Background { background-color: rgb(229, 246, 255); " +
			"margin-right: 10px; font-variant: small-caps; font-weight: bolder; } ");
	GM_addStyle(".LogDate {	font-weight: bold !important } ");

	// Create links.
	var oakzDiv = document.createElement('div');
	oakzDiv.id = 'oakzDiv';

	var sep01 = document.createElement('br');
	oakzDiv.appendChild(sep01);

	if (document.getElementById('log_table')) {
		document.getElementById('log_table').removeAttribute('id');
	}

	sep01.id = 'log_table';

	var spanOanWarn = document.createElement('span');
	spanOanWarn.id = 'spanOanWarn';
	oakzDiv.appendChild(spanOanWarn);

	var lnkOanWarn = document.createElement("A");
	lnkOanWarn.title = 'Disable a Problem Cache';
	lnkOanWarn.classList.add('OAN_Background');
	lnkOanWarn.href = '/bookmarks/mark.aspx?guid=' + CacheGuid + '&WptTypeID=2&oan=y';
	spanOanWarn.appendChild(lnkOanWarn);

	var imgOanWarn = document.createElement("IMG");
	imgOanWarn.border = '0';
	imgOanWarn.align = 'absmiddle';
	imgOanWarn.src = imgsrcOanWarn;
	lnkOanWarn.appendChild(imgOanWarn);
	lnkOanWarn.appendChild(document.createTextNode(' Warn Problem Cache'));
	lnkOanWarn.addEventListener('click', fWarn_Oan_clicked, false);

	var lnkOanKill = document.createElement("A");
	lnkOanKill.id = 'lnkOanKill';
	lnkOanKill.title = 'Archive a Problem Cache';
	lnkOanKill.classList.add('OAN_Background');
	lnkOanKill.href = '/seek/log.aspx?wid=' + CacheGuid + '&LogType=5&oan=y';
	spanOanWarn.appendChild(lnkOanKill);

	var imgOanKill = document.createElement("IMG");
	imgOanKill.border = '0';
	imgOanKill.align = 'absmiddle';
	imgOanKill.src = imgsrcOanKill;
	lnkOanKill.appendChild(imgOanKill);
	lnkOanKill.appendChild(document.createTextNode(' Kill Problem Cache'));

	var spanKzWarn = document.createElement('span');
	spanKzWarn.style.marginLeft = '20px';
	spanKzWarn.id = 'spanKzWarn';
	oakzDiv.appendChild(spanKzWarn);

	var lnkKzWarn = document.createElement("A");
	lnkKzWarn.title = 'Warn a Long-Disabled Cache';
	lnkKzWarn.classList.add('KZ_Background');
	lnkKzWarn.href = '/bookmarks/mark.aspx?guid=' + CacheGuid + '&WptTypeID=2&kz=y';
	spanKzWarn.appendChild(lnkKzWarn);

	var imgKzWarn = document.createElement("IMG");
	imgKzWarn.border = '0';
	imgKzWarn.align = 'absmiddle';
	imgKzWarn.src = imgsrcKzWarn;
	lnkKzWarn.appendChild(imgKzWarn);
	lnkKzWarn.appendChild(document.createTextNode(' Warn Disabled Cache'));
	lnkKzWarn.addEventListener('click', fWarn_Kz_clicked, false);

	var lnkKzKill = document.createElement("A");
	lnkKzKill.id = 'lnkKzKill';
	lnkKzKill.title = 'Archive Long-Disabled Cache';
	lnkKzKill.classList.add('KZ_Background');
	lnkKzKill.href = '/seek/log.aspx?wid=' + CacheGuid + '&LogType=5&kz=y';
	spanKzWarn.appendChild(lnkKzKill);

	var imgKzKill = document.createElement("IMG");
	imgKzKill.border = '0';
	imgKzKill.align = 'absmiddle';
	imgKzKill.src = imgsrcKzKill;
	lnkKzKill.appendChild(imgKzKill);
	lnkKzKill.appendChild(document.createTextNode(' Kill Disabled Cache'));

	// Add links to page.
	var ncTbl = document.getElementById("ncTbl");
	if (ncTbl) {
		insertAfter(oakzDiv, ncTbl);
	}

//****************************************************************************************//
//                                                                                        //
//                         Functions                                                      //
//                                                                                        //
//****************************************************************************************//

	// Bookmark this cache, in a separate tab.
	function fWarn_Oan_clicked() {
		GM_openInTab(curDomain() + '/seek/log.aspx?wid=' + CacheGuid + '&LogType=22&oan=y')
	}

	// Bookmark this cache, in a separate tab.
	function fWarn_Kz_clicked() {
		GM_openInTab(curDomain() + '/seek/log.aspx?wid=' + CacheGuid + '&LogType=68&kz=y')
	}

	// Returns the current domain, including http or https protocol, without a trailing '/';
	function curDomain() {
		return location.protocol + '//' + document.domain;
	}

	// Returns a URL parameter.
	//  ParmName - Parameter name to look for.
	//  IgnoreCase - (optional) *false, true. Ignore parmeter name case.
	//  UrlString - (optional) String to search. If omitted, document URL is used.
	function UrlParm(ParmName, IgnoreCase, UrlString) {
		var RegRslt, sc = '', RtnVal = '';
		if (IgnoreCase) {sc = 'i'}
		if(UrlString) {
			var PageUrl = UrlString;
		} else {
			PageUrl = document.location + '';
		}

		var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
		var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
		RegRslt = RegEx1.exec(ParmString);
		if (RegRslt) {RtnVal = RegRslt[2]}
		return RtnVal;
	}

	// Insert element after an existing element.
	function insertAfter(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement.nextSibling);
	}

	// Insert element ahead of an existing element.
	function insertAheadOf(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement);
	}

	// Remove element and everything below it.
	function removeNode(element) {
		element.parentNode.removeChild(element);
	}
