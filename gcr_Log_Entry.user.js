﻿/*
Geocaching Log Entry Enhancement v2

Greasemonkey user script: see http://greasemonkey.mozdev.org

Changes log defaults, based on log type. Auto-submits handicap notes.

Log type codes:
	 2 = Found It
	 3 = Didn't Find It
	 4 = Write Note
	 5 = Archive
	 7 = Needs Archive
	 9 = Will Attend
	10 = Attended
	11 = Webcam Photo Taken
	12 = Unarchive
	18 = Reviewer Note (unpublished)
	22 = Disable
	23 = Enable
	24 = Publish
	25 = Retract Listing
	45 = Needs Maintenance
	47 = Update Coordinates
	68 = Reviewer Note (published)
	74 = Announcement
	76 = Submitted for Review

Auto-close:
	Use URL parameter close=y to close the tab on redisplay. Requires the browser
	setting dom.allow_scripts_to_close_windows to be set to True.
	See http://tinyurl.com/5vom4r7 for instructions.

Change Log:
v02.01 2017-08-17 - SigneInAs fix for new header release

* v02.0 2017-08-02 - Version reset to 2.0 and name change to add v2.

* (v02.35) 2015-11-18 Bug fix.

* (v02.34) 2015-09-13 Fixed log deletion, when requested from review page.

* (v02.32) 2013-06-17 Added icon for 'restore previous log', and removed that options
	from the GM menu. Log text now gets saved whenever a log page unloads, regardless
	of whether the Submit button was clicked. This means you can recall your text if
	you accidently close a tab, close the browser, click a link or refresh the page.

* (v02.31) 2013-06-04 Fixed focus problem with Update Coordinates logs, so the submit
	button is activated. Added new auto-log type, for Traditional caches that are too
	close to a multi or puzzle stage. Added %dlat% and %dlon% replacement variables,
	which are the decimal-degree coordinates of the cache being posted to. This is to
	allow the creation of "all nearby caches" links within the logs. Added "Copy to
	Other Tabs" link, which copies the current text to any other open log entry pages.
	Will no longer replace any existing log text when changing log type to Publish or
	Retract. Removed obsolete options for No Home Coordinates.Added export/import
	option to make it easier to set up all templates at once for new reviewers.

* (v02.30) 2011-09-28 Added %wptno% log replacement value, for the numeric waypoint id.

* (v02.20) 2011-02-15 Added ability to delete logs and close tabs.

* (v02.11) 2010-11-10 Update for site changes.

* (v02.10) 2010-05-18 Added multiple language/version support. Enclose various language
	versions in <lang id="xxx"></lang> tags, and user can select the language from an
	array of buttons next to the log type selector.
	Direction to nearby cache now returns an empty string if the distance is 0 feet,
	instead of "Here";

* (v02.01) 2010-04-05 Bug fix (text for Retract Listing). Added auto update
	notification.

* (v02.00) 2010-01-26 Added option to always set Cannot Delete for Review Notes.

* (v01.91) 2010-01-13 Update for site changes.

* (v01.90) 2009-12-10 Added Owner and Proximity Direction replacement
	variables.

* (v01.80) 2009-11-03 Added template system for Proximity, Vacation,
	Home Coordinate, Handicap, Update Coordinates logs. Must be
	using the Review Page script v6.6 or later to use.

* (v01.70) 2009-06-26	Added ability to pass default text in a parameter.

* (v01.60) 2008-07-24 Fix to accommodate site changes.

* (v01.50) 2008-06-06 Change for new Review Note type.

* (v01.40) 2007-10-31 Change to work with multiple subdomains.

* (v01.30) Reworked auto-submit code. Added code for auto-submitting Publish logs.

* (v01.20) Update for Greasemonkey 0.6.4.

* (v01.10) Handicap notification log now automatically submits itself.

* (v01.10) Changed text of the Handicap notification log to reflect new PQ attributes filter.

* (v01.10) Cleaned up URL parameter parsing.


// ==UserScript==
// @name           GC Log Entry v2
// @description    Defaults for log entry.
// @namespace      http://www.geocaching.com/admin
// @version        02.01
// @scriptGuid     c3748a27-1ae8-45c3-9931-50a024c7fc8e
// @include        http*://*.geocaching.com/seek/log.aspx*
// @include        http*://*.geocaching.com/track/log.aspx*
// @icon           http://i.imgur.com/1GZXUf0.png
// @grant          GM_info
// @grant          GM_getValue
// @grant          GM_openInTab
// @grant          GM_setValue
// @grant          GM_xmlhttpRequest
// @grant          GM_addStyle
// @grant          GM_registerMenuCommand
// @unusedAPIs     -----------------------
// @grantXX        GM_listValues
// @grantXX        GM_deleteValue
// @grantXX        GM_getResourceText
// @grantXX        GM_getResourceURL
// @grantXX        GM_log
// @grantXX        GM_setClipboard
// ==/UserScript==

*/


	//// --Version Checking-- //
	//fCheckScriptVersion();
	//// --Version Checking-- //


	var imgsrc_LogRestore =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAA" +
		"AXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAH" +
		"UwAADqYAAAOpgAABdwnLpRPAAAABp0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjUuMTAw" +
		"9HKhAAAEn0lEQVRIS5WVe0xbVRzHy3AOnS5TNKAmc%2F7jFrNoMvYAYjTsD3VxMDFDxkNY" +
		"xmOabRjJ5lTA6JgowmBZWMJgA8bLMhjEDTISHsIc74cFRkvp%2Bi5Q6IOWvmgpfD3nYrsK" +
		"heBJvjn3nnPu73N%2B33vP73qwXFp2bRuw5MGyLy6xALBYHh6sTWTeg%2FYeLJbOssTyJA" +
		"N6q4Wl0c2xRBMGVmdOIpnZYKMA6bSW0aRWj8dyNQYFMjwYEeF%2BPx8V7YPIq%2B9DVk0H" +
		"TuXVICjlOgKTC8hONtgyq1uZ4I62hCUYTfOY0pjAV6jRx5Ogvn8cdQ95yLjzFwNIudVM%2" +
		"BqKNQVwBdpsNBpsZ0zorJEojxmRKcARytHBEaBx8jNy7vUwW7ZwxfHW9YWOQDPaTDOx2O%" +
		"2BwLNpgXFqE1mDChVGFMrEQ%2Fbwqdj2QIKXh6Ta1p2KWKZsaipl4FAuJvIyCumvQ1OBhX" +
		"hf2xlfCLLsPeyFJGFHB75swq0fE1ARcrlwG02e0W2IhNCyQLm90GvXUek3ojZCo9FDNaJ%" +
		"2BA32T44RIEU8NYXV9xD0sqbXABWBkBFrbISiI5AVHpil1a3LmBXQo57wPmiRheLapYtcl" +
		"V8NfypXSeqcPjqZpRLT%2BEn7ttO0Xs6vvNkpntAUt4fzgwWyEs2UYuIJKpHy3bRjMi4ym" +
		"TBofSncIMbhwtdu52i9x%2FlbsaO2J%2FdAxJyKp5YBDvMxJa7o%2Fk4VrQNiyS4Q1aLHY" +
		"HnPJHXF4MzjW84de7eQca6Vz%2F%2F1T0gIqPEaZF%2FYjEOXz6A8%2Ffex9FCL8Yq%2F3" +
		"8VQKzyS%2FBEdnsEYtm%2BiCp%2FCeGl2xF68xl8UvgsfKJ%2BcQ8ISbvBAIYUrYgpew1s" +
		"Xgrk84MIvbF1zW8%2BrHgbEut2Ir37ECqESThe4oOXI9awKCilEFdaz%2BL0nT0Y0NRCbO" +
		"mGwjrEQFZKQuYGDGy0aLNxX3MR9epUsKVfI%2F733fAOv7Q6A%2BLdmyH5O3Ct5yRE5m7w" +
		"za3gGGoxZKj7j%2FrmylE9k4Qc%2BbvIkh1wnoESZRSucsLxY0MMXvxsBYAE9z1auEWT1R" +
		"HG7JhvamGCu6p3royc2tO4LA90BnU9ZLWqZKQ2B6HkYQFeWAmgR9uRQW5HJMmgEwJzG7Pz" +
		"nrlSVM18iWy5%2F6rANIO8iQ9QOZ0AtjgZJypfx4NRrnsAhTjeQQJ7F7pUlejQF6BlNgfN" +
		"s9nupc1C3dR3uDYSibBiX%2FSJ%2F0QvX74eoMj5FUWXvYLM7iNo0mauWzmjSn2Q2RyDFn" +
		"49U8PWBdAi5Sh2UrUCaQ0f42zdHgagnTMxmiXSGC2YMZowM6cD%2BXVCbzRDqTUsA8bXyc" +
		"ABoOV6f2wF9hG9dyEBwflbmWuH9kbfIiWbKIqUblLC%2FaLL8c7xIljm7egXKLD9WLrbg7" +
		"bJOzQVPJEcOp0OarUaSqUSCoUCvdw2iMViCIVCRvRaKpUyc3QNXUufMRqNECnV8P70B%2F" +
		"cn%2Bbngb5kFAoUSPIkCI0IJOHwh%2Bkf56BkeReffw%2BjijKB3mIsB7jiGBWKMimXgyy" +
		"YhmJiGeFoDuUYPrw%2B%2FcQ94PuR7UHkFpxKl%2FC9tOUKeI4EdcvzV%2FgFNxeyVyQie" +
		"YAAAAABJRU5ErkJggg%3D%3D";

	var imgsrc_TabDup =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAA" +
		"BGdBTUEAAK%2FINwWK6QAAABp0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjUuMTAw9HKh" +
		"AAAFNklEQVRIS7WTe0xTdxTHD0QGMkPEF4oF5xbdxuZ0j%2BgkbWEtfZfHpbRUAZGqwHQx" +
		"m7AZFKOICnMGVAoVSB0w3cDR6FS2hGzL%2FnDGMIlWh0oQBEQeFpE3fdB79rvXYKIXk23Z" +
		"bvLJ73dvk%2FPJ%2BZ5TD0SE%2F%2FVhBKxkG9FMsZXc0wlphI2ETYRkQhLz%2FhKAwScW" +
		"krw6Ic7TANHeAJr5AFGLABTBT3la928LNpDiCQS9B4AWYsIPhtPh%2ByNokHmeAmoOEQT%" +
		"2BCwHTRQIsJaQT8kFP0HruAylY9CXR9M%2Bu46gtpdBTMbcRVAvmgPKfdJAKMyDF45ulu4" +
		"LtulORmHoxAVMvEM4nouHMejzwx2dYgen4HX5O3uPRSxHYDDKez1RML47oEzb%2FWR5bPK" +
		"3rapVotGWicWI7HnekY5EzjcXoSscjYymYbaNwz0AMFro3otgkQhAGVoCIByDmsXNlZ%2F" +
		"v0wgx2CgOcpSxCzB1Ixs3tIkxpF2LyXT4mtzAI2DOxORST74ThprZwNLSF4fLdK4gg%2BB" +
		"CIlhDBkmkEaWR4DBsh5NX9C51b7kgwomEFyhpW4srK1zAgexEdsGuxe2FWsHvBp4vpNSfe" +
		"w6im91H0%2B9u4OGM5gmDJOZC%2BAiBjmE7ArKKBXctDwuoQDL2yDEN%2BWYQ84wIEje9t" +
		"oHylEDVbDFFzQyFs9r61eWvpDy6%2BgT6JPBr4vHKQBMHLstfBT%2FYmCyei5VryY6IfeK" +
		"TChWBjAPqZfXFmyUz03OpLe2v9P5ynCQKInwUQR1ZSGaD137SU9tIGjXrweelBopUQHLEK" +
		"RJIIiFSoQU3gCOJUWhCIBeBPzV%2FnRfn%2B5h3tV%2B%2Bt9v%2FVK3J29Vuid%2BZFSC" +
		"SwWhUKoXIBBEpC1niFBZ1ZFbpm2bvC1SAShkOE8CMQi8SgUChZOAKlUhWglqvP6zS6O%2F" +
		"FRukYGPUFHaa%2FHUZpmhUyZo5YqQSWRR0aro69RcRqrXqO7oYvVNsfGxjZTFHVXpVJflc" +
		"sVYdMKZDJF%2BenT32K%2FrR%2B7urqws7MT2zvasbOjA1taWjAj8wsnny8Q63S6uxaLBX" +
		"%2Bq%2BxFramqwqqoKy8rKsLi4GHNzczEqKrpFKpVxO4iM1FxsaLhKOnv2oWmafKCxqKiE" +
		"5vNFhrS0j22XLl3Curo6rK6uxoqKCjSZTFhQUICFhYUYH6%2FvlcvVPpyIYmL0P1y%2BfI" +
		"Wt7nK50OFwoN1ux4mJCZbCwiJaIJAmpqRs6auvr8faWgtWVlZiaWkpHj16FPPy8lliY3Xd" +
		"FJUwgyNYv37zYGPjtWkF4%2BPjJIavMSkpdWTbth3uvXsPYXZ2LmZl7cOdO%2FdgZuZuzM" +
		"jIwh07sjAtbTu9YUP6PY4gJ%2Bcr1%2BjoKEfAFB8aGmLncu9eO7a2tuKtW7exqekWoQlv" +
		"3vwTrdYbeP26lcVqtWJOzpdujiA%2Fv8hJHo5gbGwMGTEDcx8ZGSbCQXz8eAAHBh5hf78N" +
		"Hz7sw56eHuzt7WWXIy%2FvmIsjOHzY5HQ47C8UDA8P4%2BDgICk6QIr2k6IPsa%2FvSeEH" +
		"Dx7g%2Ffv32S7b2loxP7%2BYKygoMDuZoT4%2F5KkO%2FongyJFSruDYsSrSgYMV0LQbJy" +
		"ddyERmtzNbNM5G9LzEZrM900V3dze2t7eRjTNzBSZTtYsRuN2TpDCzolOFR57J%2FdGjfr" +
		"TZmHh6SDzdJJ4uEk8ndpA%2FJUNbWwsajacmOTM4eND8%2BOTJ85Nm8zmn2XzWWV7%2BhL" +
		"IyC0tpqcV54kQtOWvJ%2Bb3TZGI44ywpqXEWF1c7jcYnkPvkgQPmIY5g6sN%2Fff4FFxwq" +
		"FbDK5UAAAAAASUVORK5CYII%3D";

	var imgsrc_SubmitAll =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAA" +
		"AXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAH" +
		"UwAADqYAAAOpgAABdwnLpRPAAAABp0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjUuMTAw" +
		"9HKhAAADTElEQVRIS7WVWUwTURSG23Q6c4maGJeHxugLYoJ7AkKolUUUqpSCuIJFUFK2dt" +
		"pilZRFMGpkca0WFAFlMRE0mkCCoIJQCy0YWkopxVdjfLG%2BGX3QMMcZcKG2pcSGm5yne%" +
		"2Bb%2Fzn%2FuvWfYAMBa1PW%2FgOhLaFmInMuer7gZ7f8FZLcGy890hVSGyjCvEL8Asrat" +
		"pM6aTKm7tnuF%2BAdo30LW2g7ALauIOt21tYJf6N4u%2FwCPN5M1tmTQjSfCzTEhVfB8U0" +
		"XEP5B5AUItWlrYHXGyclDUftWUNHrFlGipNu23VJmElgrjXjpiPzCA2%2BMi0I7vg%2Btj" +
		"sVRBd3BluPrvmXgERJ7H2EU9AmmNWfKp2ZFPNTly4P5kFjROZkK9PR3q7Klwd%2BII1NpS" +
		"oMaW9AsghBvWPXDVEkWpujf8gbgB%2BEU450J%2FfF2LQ0a1TpHQPJUPTY7sGUDDZAYNkE" +
		"DdRCrcmTg8A9DZxHBrPAG01ngaEAvXxqKhysynVD2BlfxCjO0GKH0Ro2lxyKmH7xTQOiWH" +
		"ZkcePHBIacApaLBnwD37cRpwjK48hek7VI0K4OJICJQPb4QS43rQDK2Ds0M8IHtXO8MU2F" +
		"oXgPjmSt5da%2BaXWXESWqZkNCB3pnrtWCJcHA4DjSEQlP2rIL9vCeT1Ich7jSC%2FH4Fs" +
		"gA49AvkbBDkvCWekBtvFPEAXgOa5gGSEfwN01hQ4Z9wC8tfLIbeXFvMiSBoQMKEYRHQe8T" +
		"mqaFbcDXBZn%2FiIact1yz5Q63mzgv3E9ImnhPlQI14t1nFVcyOtDX%2BsHEKgNCJQmRgn" +
		"hDOqGBPMHR0uDkr6wvVnDWv%2BWJZ2E%2B93l2FiDLFwT%2FMm7QlOFowgOP2WaQ%2FhjC" +
		"7G%2BP%2FmuQAyO%2FABpoeM3ZxXxMcQKSdovkEmeYaTZ8wBQA4iZ3QpttNTriugE29n7C" +
		"qNBHWwkSv1NcJPdOAknf8ppsyzuNsZSJ7ipYxlhRF935bF4fkCiG5zD8wn7gaIq8Z2qEfR" +
		"tMKEvq0IYiFfgIXsu7SIE8Di0NfMoDajH5EXOGsWIuArx%2B0lx1VgEbSLr%2Bmd%2BFFf" +
		"Hy9k3%2BOwS67hZuTqCWNCPX1B%2FVxex%2FXuciwmqQG7zAtlY%2F4wfP1wuP6Iu9wihr" +
		"RY8RMWsG1Bj8nBYAAAAABJRU5ErkJggg%3D%3D";



	// -------------------------------------------------------------------------

	var imgBlockQuote =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AABGdBTUEAAK%2FINwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZT" +
		"wAAAEvSURBVDjLY%2Fj%2F%2Fz8DJZiBagZEtO8QAuKlQPwTiP%2FjwbuAWAWbARtXHrz1" +
		"%2F%2Fefv%2F%2FxgS0n74MMuQ3EbHADgBweIP7z99%2B%2F%2Fx%2B%2B%2Ffv%2F8tO%" +
		"2F%2F88%2B%2Fvv%2F5P2%2F%2Fw%2Ff%2Fft%2F782%2F%2F7df%2Ff1%2F5xXE8OoFx0" +
		"GGmCEbIJcz9QBY8gVQ47MP%2F%2F4%2FBmp%2B8Pbf%2F7tQzddf%2FP1%2F9RnEgM5VZ0" +
		"EGeGM14ClQ86N3UM2v%2F%2F2%2F9RKi%2BQpQ88UnuA2AewHk%2FPtAW%2B%2B8%2Fvv%" +
		"2FJlDzted%2F%2F18Gar7wBGTAH7ABtYtOgAywxBqIIEOQAcg1Fx7%2FBRuMFoicuKLxDy" +
		"zK5u64Cjfo%2FecfYD5Q%2FDLWaMSGgQrvPH%2F3FabxOxDXEp0SgYp7Z267AtL4BYgLSU" +
		"rKQA1KQHwPiFPolxcGzAAA94sPIr7iagsAAAAASUVORK5CYII%3D";

	var imgTextLink =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1%2BjfqAA" +
		"AABGdBTUEAAK%2FINwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZT" +
		"wAAADpSURBVCjPY%2FjPgB8y0EmBHXdWaeu7ef9rHuaY50jU3J33v%2FVdVqkdN1SBEZtP" +
		"18T%2FL%2F7f%2FX%2Fwf%2BO96kM3f9z9f%2BT%2FxP8%2BXUZsYAWGfsUfrr6L2Ob9J%" +
		"2FX%2FpP%2BV%2F1P%2Fe%2F%2BJ2LbiYfEHQz%2BICV1N3yen%2B3PZf977%2F9z%2FQ%" +
		"2F%2FX%2Frf%2F7M81Ob3pu1EXWIFuZvr7aSVBOx1%2Fuf0PBEK3%2F46%2FgnZOK0l%2F" +
		"r5sJVqCp6Xu99%2F2qt%2Bv%2BT%2F9f%2BL8CSK77v%2Bpt73vf65qaYAVqzPYGXvdTvm" +
		"R%2Fz%2F4ZHhfunP0p%2B3vKF6%2F79gZqzPQLSYoUAABKPQ%2BkpVV%2FigAAAABJRU5E" +
		"rkJggg%3D%3D";

	var imgTextBold =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1%2BjfqAA" +
		"AABGdBTUEAAK%2FINwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZT" +
		"wAAADCSURBVCjPY%2FjPgB8yUEtBeUL5%2BZL%2FBe%2Bz61PXJ7yPnB8sgGFCcX3m%2F6" +
		"z9IFbE%2FJD%2FXucxFOTWp%2F5PBivwr%2Ff77%2FgfQ0F6ffz%2FaKACXwG3%2B27%2F" +
		"LeZjKEioj%2FwffN%2Bn3vW8y3%2Bz%2FVh8EVEf%2FN8LLGEy3%2BK%2F2nl5ATQF%2Fv" +
		"W%2B%2Fx3BCrQF1P7r%2FhcvQFPgVg%2B0GWq0zH%2FN%2FwL1aAps6x3%2B64M9J12g8p" +
		"%2F%2FPZcCigKbBJP1uvvV9sv3S%2FYL7%2Bft51SgelzghgBKWvx6E5D1XwAAAABJRU5E" +
		"rkJggg%3D%3D";

	var imgTextItalic =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1%2BjfqAA" +
		"AABGdBTUEAAK%2FINwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZT" +
		"wAAABxSURBVCjPY%2FjPgB8yUFtBdkPqh4T%2FkR%2BCD%2BA0Ie5B5P%2FABJwmxBiE%2" +
		"F%2Ff%2FgMeKkAlB%2F90W4FHg88Dzv20ATgVeBq7%2FbT7g8YXjBJf%2FRgvwKLB4YPFf" +
		"KwCnAjMH0%2F8a%2F3EGlEmD7gG1A%2FIHJDfQOC4wIQALYP87Y6unEgAAAABJRU5ErkJg" +
		"gg%3D%3D";

	var imgTextStrikethrough =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1%2BjfqAA" +
		"AABGdBTUEAAK%2FINwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZT" +
		"wAAACfSURBVCjPY%2FjPgB8yUFNBiWDBzOy01PKEmZG7sSrIe5dVDqIjygP%2FY1GQm5b2" +
		"P7kDwvbAZkK6S8L%2F6P8hM32N%2FzPYu2C1InJ36P%2FA%2Fx7%2Fbc%2BYoSooLy3%2F" +
		"D4Px%2F23%2BSyC5G8kEf0EIbZSmfdfov9wZDCvc0uzLYWyZ%2F2J3MRTYppn%2F14eaIv" +
		"KOvxxDgUma7ju1M%2FLlkmnC5bwdNIoL7BAAWzr8P9A5d4gAAAAASUVORK5CYII%3D";

	var imgTextColor =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AABGdBTUEAAK%2FINwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZT" +
		"wAAAMOSURBVDjLVZNNaBxlAIafb%2Bab2Z3N7Oxv%2FnYTEyv2LzQJpKBgrQqNUKmY4kUI" +
		"XqUHT70p9iB48CKIiN5E0It6KFiwiv9FpAVpKUggNc3mZ7vpJpv9n93ZnZ35PNRI%2B8B7" +
		"e9%2Fn9gqlFAeIVUfPeN3zh0R0eVpYM1OanhvTCEY0f3tU79%2BctnpfHM73fuQhxIHAWH" +
		"nmkOGXPjgZyS09l5hnNv4YOdMhoQmigzqGt4nhfeub1fpnVsl%2Fe%2BhMv%2Fq%2FQKy%" +
		"2BMe0EO5dfso%2FOvzB8grgV4HGXJC7jwAQ2oxxDuC36xZ%2BRhe%2Bv6iutZf2iqklReN" +
		"e0tPSHZ2Nz84ujR7ht3iJKjcexiOIQI8SiixxcR7QtRORFlK7O9t0rlyy4KBEj5%2BYisV" +
		"eez85wy9zGIUeGDDYhDhYOITYuoh2BvTJ68y7B0GnCym8XGq%2BKL2U0MrE8Z2SRVhqdPm" +
		"lCsvgk8RlCkgAivRbUNKj1YPMeeu4wcnjRql7%2F%2BjVpyvxsPjbK3whi5LEAB0WWgBRg" +
		"qwAaFah04X4V7puwdwddz%2BFXjJMSbXI8aSTYCgU2oKMwEdgCEoDhug%2FG5SjsmFDUoV" +
		"%2BDXJ7BnpiUVCNBaJqEXfDVfwG6CjoKnF4crZGCVvNBug0IPXzPZOCnAunfk8W6ro7H2g" +
		"K3A02gGoDeA1MDGx2nkYG6C24bvDaMSzq7ZfxBsiC7O%2BaNDaWOn0oLfl0HMwDlQRCAHY" +
		"UkEGvFkLsp2G9Bo0n41AiNG6sMBvY1yZr6%2FJsV%2F%2FXZZ3WZaEp2t6DvgWFA1QRHQb" +
		"wjSDeTUGvCiSPU1ovU%2FtypQPIrTV0yrrl3vE%2B%2F%2B8XlaCIgq8H%2BBtSLUN2C2i" +
		"bsl8ArR%2BHYGE0rwvbvRTr96HsL6od1CUDDf%2BenK92JwT%2B982cWEswvRmiug6qAr0" +
		"E4AV4uoFXosnV1g8bN5kcp7E8eOZOYKtmUqm%2FZiDdfPhV3Zp6IM5k0SIUBstwmXKvCX5" +
		"UdM6y9n2b34wV1IXxEcEBU3J4dprU0zODpjFBTIyoIxgjXxlB%2FPIl1eUmdLjzc%2Fxce" +
		"OVXddrB6BQAAAABJRU5ErkJggg%3D%3D";

	var imgTextFont =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AABGdBTUEAAK%2FINwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZT" +
		"wAAAHJSURBVDjLY%2Fj%2F%2Fz8DJZiBZgY4tN9wcO6%2B0erZd2uKc%2BfNfoeWGxMcW2" +
		"7Msiq%2B3GWUdIZXL%2FokI14D7JqvB%2Bcsf3Rv4p6X%2F%2Ft3Pf%2Ffvf35%2F8Ilj3" +
		"471V3bph9zmougC6xrr8mETbu7q3jl40%2FFKx5%2BLVzy8Ltd%2BeUZBvGnOYjygk3llf" +
		"KCZY%2B%2Bu3fcWutcd21B07on%2F61yz88kKgwsCi8qJc%2B%2B9yhu2p37ppnnQ4C4oW" +
		"blo%2F9WOReXEjTANOsCs1PD9VVZ8%2B9%2FN0k7m6Yfe5LLOPFMR%2BWyh%2F9dqq5eUv" +
		"c6xIbXALOs8zEZc%2B9%2FC%2Bq%2BddEw%2FrSfXuRxLfP0swuqgAYEt934pOq2nxenAU" +
		"bJZ0TjJt9%2BVbn80X%2Bv5huXrbLOb7LMOLfVterqjcYVj%2F%2BHtd38qey4TxqrAQax" +
		"pxntSy7PBvnVPO0MSmCZJ5%2FZWL7g%2Fv%2Bozlv%2Flex2K2EYoB9zigsYPS6lSx7%2B" +
		"j%2Bi59UYn6JgtTIGK635hdY%2FD9dnT7vxP6L%2F9X9F%2Bb4icxTYmFAMsMs6ti%2B2%" +
		"2F9S9hwu3%2FAc3X32oHHOlVdtoroGS%2FR0vb9%2FAip8ILrwLrrv33rbn63zD02F5Zy2" +
		"2GtM8LdDMAACVPr6ZjGHxnAAAAAElFTkSuQmCC";

	// -------------------------------------------------------------------------

	
	
	//  Get currently signed-on geocaching.com profile.
	// var SignedInAs = document.getElementsByClassName('SignedInProfileLink')[0];
	// SignedInAs = SignedInAs.firstChild.data.trim();
	// SignedInAs = SignedInAs.replace(/<\&amp;>/g, '&');
	//  Get currently signed-on geocaching.com profile.
	var SignedInX = document.getElementsByClassName('user-name')[0];
	if (SignedInX) {
		var SignedInAs =  SignedInX.firstChild.data.trim();
		SignedInAs = SignedInAs.replace(/<\&amp;>/g, '&');
	} else { return; }

	var dateCtrl = document.getElementById("uxDateVisited");

	var encryptChk = document.getElementById('ctl00_ContentBody_LogBookPanel1_chkEncrypt');
	var cannotDeleteChk = document.getElementById("ctl00_ContentBody_LogBookPanel1_chkCannotDelete");



	//  Check session storage to see if auto-close was requested.
	if (sessionStorage.getItem('autoclose') == 'true') {
		if (sessionStorage.getItem('dbcu') == 'true') {
			window.opener.close();
		}
		window.close();
		return;
	}

	//  If auto-close requested, set session storage value.
	if (UrlParm('close') == 'y') {
		//  Set session storage value to close invoking tab on redisplay.
		sessionStorage.setItem('autoclose', 'true');
		if (UrlParm('dbcu') == 'y') {
			//  Set session storage value to close tab on redisplay.
			sessionStorage.setItem('dbcu', 'true');
		}
	}

	//  Set value for text copy localstorage key.
	var lsCopyKey = '812f9b8e-e8fc-40ba-9af9-c2c2f87b2ae2';

	// Enhancement switches.
	// Set value to true or false.
	var HCA_AutoSubmit = true;	// Auto-submit handicap notices.

	// Set general auto submit value.
	if (UrlParm('auto') == 'y') {
		var AutoSubmit = true;
	}

	//  Array for language versions.
	var aLangText = new Array();

	// Set text, if passed.
	var dftText = decodeURIComponent(UrlParm('text'));

	// Locate textarea element.
	var e_LogInfo = document.getElementById("ctl00_ContentBody_LogBookPanel1_uxLogInfo");
	if (! e_LogInfo) {
		var txtAreas = document.getElementsByTagName('textarea');
		if (txtAreas.length == 1) {
			e_LogInfo = txtAreas[0];
		}
	}

	// If no text entry, then check if requested to delete this log.
	if (! e_LogInfo) {
		if (UrlParm('delete') == 'y') {
			if (!document.getElementById("ctl00_ContentBody_LogBookPanel1_lbEtc")) {
				var delLink = document.getElementById("ctl00_ContentBody_LogBookPanel1_lnkbtnDelete");
				if (delLink) {
					delLink.click();
					// location.assign(delLink.href + ";void(0)");
					
				}
			} else {
				alert('Log is already archived.');
			}
		}
		return;
	}

	// Only apply following changes if signed on as a reviewer.
	if (cannotDeleteChk != null) {

		var e_LogBookPanel1_LogButton = document.getElementById("ctl00_ContentBody_LogBookPanel1_btnSubmitLog");


		GM_registerMenuCommand('Set Log Entry Script Options', fSetOptions);

		GM_registerMenuCommand('Option: Always make Reviewer Notes UnDeletable', fOptRevNoteUndel);
		GM_registerMenuCommand('Template: Import All Templates', fImportTemplates);
		GM_registerMenuCommand('Template: Export All Templates', fExportTemplates);

		GM_registerMenuCommand('Template: View/Edit Proximity Template and Log Type', fEditProxTmpl);
		GM_registerMenuCommand('Template: Save Proximity Template and Log Type...', fSaveProxTmpl);

		GM_registerMenuCommand('Template: View/Edit Litmus-1 Template and Log Type', fEditLit1Tmpl);
		GM_registerMenuCommand('Template: Save Litmus-1 Template and Log Type...', fSaveLit1Tmpl);

		GM_registerMenuCommand('Template: View/Edit Vacation Template and Log Type', fEditVacaTmpl);
		GM_registerMenuCommand('Template: Save Vacation Template and Log Type...', fSaveVacaTmpl);
		GM_registerMenuCommand('Template: View/Edit Handicap Template and Log Type', fEditHndiTmpl);
		GM_registerMenuCommand('Template: Save Handicap Template and Log Type...', fSaveHndiTmpl);
		GM_registerMenuCommand('Template: View/Edit Update Coordinates Template and Log Type', fEditUpcoTmpl);
		GM_registerMenuCommand('Template: Save Update Coordinates Template and Log Type...', fSaveUpcoTmpl);

		//  Add event listener to log type dropdown.
		e_LogType = document.getElementById("ctl00_ContentBody_LogBookPanel1_ddLogType");
		e_LogType.addEventListener("change", TypeChange, true);


		// Generate array of valid log types.
		var OkLogTypes = new Array();
		for (var i in e_LogType.options) {
			if (e_LogType.options[i].value != '-1') {
				OkLogTypes.push(e_LogType.options[i].value);
			}
		}


		//  Get current note text.
		var NoteText = e_LogInfo.value;
		if (!NoteText) {
			NoteText = dftText;
			e_LogInfo.value = NoteText;
		}

		//  Get LogType.
		var LogType = UrlParm('LogType');



		//  If Archive note, default 'Cannot Delete' box to checked.
		// if (LogType == '5') {
			cannotDeleteChk.checked=true;
		// }


		//  If Review Note, set Cannot Delete, if option selected.
		if (LogType == '18' || LogType == '68') {
			if (GM_getValue('OptRevNoteUndel', 'Off') == 'On') {
				cannotDeleteChk.checked=true;
			}
		}


		//  If Disable note, default 'Cannot Delete' box to checked.
		if (LogType == '22') {
			cannotDeleteChk.checked=true;
		}

		//  If Unarchive note, set default text to 'Unarchive cache.'
		if (LogType == '12') {
			if (NoteText == "") {
				e_LogInfo.value='Unarchiving cache.';
				if (cannotDeleteChk) { cannotDeleteChk.checked = true; }
				e_LogInfo.focus();
				e_LogBookPanel1_LogButton.focus();
			}
		}

		//  If Enabling note, set default text to 'Enabling cache.'
		if (LogType == '23') {
			if (NoteText == "") {
				e_LogInfo.value='Enabling cache.';
				e_LogInfo.focus();
				e_LogBookPanel1_LogButton.focus();
			}
		}

		//  If Publish note, set default text to 'Published'
		if (LogType == '24') {
			if (NoteText == "") {
				e_LogInfo.value='Published';
				cannotDeleteChk.checked=true;
			}
		}

		//  If Update Coordinates,
		if (LogType == '47') {
			if (NoteText == "") {
				var upcoNote = GM_getValue('UpcoNote', 'Updating coordinates.');
				var owner    = decodeURIComponent(UrlParm('owner'));
				var title    = decodeURIComponent(UrlParm('title'));
				var wptid    = decodeURIComponent(UrlParm('WP'));
				var wptno    = WptToID(wptid);
				var coords   = decodeURIComponent(UrlParm('coords'));
				var dlatlon  = dm2dd(coords);
				if (title.length) {
					upcoNote = upcoNote.replace(/%owner%/gi, owner);
					upcoNote = upcoNote.replace(/%title%/gi, title);
					upcoNote = upcoNote.replace(/%wptid%/gi, wptid);
					upcoNote = upcoNote.replace(/%wptno%/gi, wptno);
					upcoNote = upcoNote.replace(/%coords%/gi, coords);
					upcoNote = upcoNote.replace(/%dlat%/gi, dlatlon[0]);
					upcoNote = upcoNote.replace(/%dlon%/gi, dlatlon[1]);

					var rtnLang = fParseLang(upcoNote);
					e_LogBookPanel1_LogButton.disabled = false;
					if (rtnLang[0] == 0) {
						e_LogInfo.value = upcoNote;
						e_LogInfo.focus();
					} else {
						if (rtnLang[1] >= 0) {
							e_LogInfo.value = aLangText[rtnLang[1]].trim();
							e_LogInfo.focus();
						}
					}

					cannotDeleteChk.checked=true;
				}

			}
		}


		//  If Proximity Note, get passed data and template.
		if (LogType == 'prox') {
			var owner    = decodeURIComponent(UrlParm('owner'));
			var title    = decodeURIComponent(UrlParm('title'));
			var wptid    = decodeURIComponent(UrlParm('wptid'));
			var wptno    = WptToID(wptid);
			var coords   = decodeURIComponent(UrlParm('coords'));
			var dlatlon  = dm2dd(coords);
			var nctitle  = decodeURIComponent(UrlParm('nctitle'));
			var ncwptid  = decodeURIComponent(UrlParm('ncwptid'));
			var ncdistft = decodeURIComponent(UrlParm('ncdistft'));
			var ncdistm  = decodeURIComponent(UrlParm('ncdistm'));
			var ncdir    = decodeURIComponent(UrlParm('ncdir'));
			if (ncdir == 'Here') { ncdir = ''; }
			var proxNote = GM_getValue('ProxNote', 'No Template Available. ' +
					'Enter text here and use Tools/Greasemonkey menu to save.');

			proxNote = proxNote.replace(/%owner%/gi,    owner);
			proxNote = proxNote.replace(/%title%/gi,    title);
			proxNote = proxNote.replace(/%wptid%/gi,    wptid);
			proxNote = proxNote.replace(/%wptno%/gi,    wptno);
			proxNote = proxNote.replace(/%coords%/gi,   coords);
			proxNote = proxNote.replace(/%dlat%/gi,     dlatlon[0]);
			proxNote = proxNote.replace(/%dlon%/gi,     dlatlon[1]);
			proxNote = proxNote.replace(/%nctitle%/gi,  nctitle);
			proxNote = proxNote.replace(/%ncwptid%/gi,  ncwptid);
			proxNote = proxNote.replace(/%ncdistft%/gi, ncdistft);
			proxNote = proxNote.replace(/%ncdistm%/gi,  ncdistm);
			proxNote = proxNote.replace(/%ncdir%/gi,    ncdir);
			proxNote = proxNote.replace(/%dlat%/gi,     dlatlon[0]);
			proxNote = proxNote.replace(/%dlon%/gi,     dlatlon[1]);

			var rtnLang = fParseLang(proxNote);
			e_LogBookPanel1_LogButton.disabled = false;
			if (rtnLang[0] == 0) {
				e_LogInfo.value = proxNote;
			} else {
				if (rtnLang[1] >= 0) {
					e_LogInfo.value = aLangText[rtnLang[1]].trim();
				}
			}

			fSetLogType(GM_getValue('ProxType', '18'));
			cannotDeleteChk.checked=true;
			e_LogBookPanel1_LogButton.focus();

		}


		//  If Litmus-1 Note, get passed data and template.
		if (LogType == 'lit1') {
			var owner       = decodeURIComponent(UrlParm('owner'));
			var title       = decodeURIComponent(UrlParm('title'));
			var wptid       = decodeURIComponent(UrlParm('wptid'));
			var wptno       = WptToID(wptid);
			var coords      = decodeURIComponent(UrlParm('coords'));
			var dlatlon     = dm2dd(coords);
			var nctitle     = decodeURIComponent(UrlParm('nctitle'));
			var ncwptid     = decodeURIComponent(UrlParm('ncwptid'));
			var nctype      = decodeURIComponent(UrlParm('nctype'));
			var ncdistft    = decodeURIComponent(UrlParm('ncdistft'));
			var ncdistftfuz = decodeURIComponent(UrlParm('ncdistftfuz'));
			var ncdistm     = decodeURIComponent(UrlParm('ncdistm'));
			var ncdistmfuz  = decodeURIComponent(UrlParm('ncdistmfuz'));
			var ncdir       = decodeURIComponent(UrlParm('ncdir'));
			if (ncdir == 'Here') { ncdir = ''; }
			var ncdirx      = fExpandDir(ncdir);
			var proxNote = GM_getValue('Lit1Note', 'No Template Available. ' +
					'Enter text here and use Tools/Greasemonkey menu to save.');

			proxNote = proxNote.replace(/%owner%/gi,       owner);
			proxNote = proxNote.replace(/%title%/gi,       title);
			proxNote = proxNote.replace(/%wptid%/gi,       wptid);
			proxNote = proxNote.replace(/%wptno%/gi,       wptno);
			proxNote = proxNote.replace(/%coords%/gi,      coords);
			proxNote = proxNote.replace(/%dlat%/gi,        dlatlon[0]);
			proxNote = proxNote.replace(/%dlon%/gi,        dlatlon[1]);
			proxNote = proxNote.replace(/%nctitle%/gi,     nctitle);
			proxNote = proxNote.replace(/%ncwptid%/gi,     ncwptid);
			proxNote = proxNote.replace(/%nctype%/gi,      nctype);
			proxNote = proxNote.replace(/%ncdistft%/gi,    ncdistft);
			proxNote = proxNote.replace(/%ncdistftfuz%/gi, ncdistftfuz);
			proxNote = proxNote.replace(/%ncdistm%/gi,     ncdistm);
			proxNote = proxNote.replace(/%ncdistmfuz%/gi,  ncdistmfuz);
			proxNote = proxNote.replace(/%ncdir%/gi,       ncdir);
			proxNote = proxNote.replace(/%ncdirx%/gi,      ncdirx);

			var rtnLang = fParseLang(proxNote);
			e_LogBookPanel1_LogButton.disabled = false;
			if (rtnLang[0] == 0) {
				e_LogInfo.value = proxNote;
			} else {
				if (rtnLang[1] >= 0) {
					e_LogInfo.value = aLangText[rtnLang[1]].trim();
				}
			}

			fSetLogType(GM_getValue('Lit1Type', '18'));
			cannotDeleteChk.checked=true;
			e_LogBookPanel1_LogButton.focus();

		}


		//  If Vacation Note, get passed data and template.
		if (LogType == 'vaca') {
			var owner    = decodeURIComponent(UrlParm('owner'));
			var title    = decodeURIComponent(UrlParm('title'));
			var wptid    = decodeURIComponent(UrlParm('wptid'));
			var wptno    = WptToID(wptid);
			var coords   = decodeURIComponent(UrlParm('coords'));
			var dlatlon  = dm2dd(coords);
			var vdistmi  = decodeURIComponent(UrlParm('vdistmi'));
			var vdistkm  = decodeURIComponent(UrlParm('vdistkm'));
			var vacaNote = GM_getValue('VacaNote', 'No Template Available. ' +
					'Enter text here and use Tools/Greasemonkey menu to save.');

			vacaNote = vacaNote.replace(/%owner%/gi,   owner);
			vacaNote = vacaNote.replace(/%title%/gi,   title);
			vacaNote = vacaNote.replace(/%wptid%/gi,   wptid);
			vacaNote = vacaNote.replace(/%wptno%/gi,   wptno);
			vacaNote = vacaNote.replace(/%coords%/gi,  coords);
			vacaNote = vacaNote.replace(/%dlat%/gi,    dlatlon[0]);
			vacaNote = vacaNote.replace(/%dlon%/gi,    dlatlon[1]);
			vacaNote = vacaNote.replace(/%vdistmi%/gi, vdistmi);
			vacaNote = vacaNote.replace(/%vdistkm%/gi, vdistkm);

			var rtnLang = fParseLang(vacaNote);
			e_LogBookPanel1_LogButton.disabled = false;
			if (rtnLang[0] == 0) {
				e_LogInfo.value = vacaNote;
			} else {
				if (rtnLang[1] >= 0) {
					e_LogInfo.value = aLangText[rtnLang[1]].trim();
				}
			}

			fSetLogType(GM_getValue('VacaType', '18'));
			cannotDeleteChk.checked=true;
			e_LogBookPanel1_LogButton.focus();

		}


		//  If Handicap Note, get passed data and template.
		if (LogType == 'hndi') {
			var owner    = decodeURIComponent(UrlParm('owner'));
			var title    = decodeURIComponent(UrlParm('title'));
			var wptid    = decodeURIComponent(UrlParm('wptid'));
			var wptno    = WptToID(wptid);
			var coords   = decodeURIComponent(UrlParm('coords'));
			var dlatlon  = dm2dd(coords);
			var hndiNote = GM_getValue('HndiNote', 'No Template Available. ' +
					'Enter text here and use Tools/Greasemonkey menu to save.');

			hndiNote = hndiNote.replace(/%owner%/gi,  owner);
			hndiNote = hndiNote.replace(/%title%/gi,  title);
			hndiNote = hndiNote.replace(/%wptid%/gi,  wptid);
			hndiNote = hndiNote.replace(/%wptno%/gi,  wptno);
			hndiNote = hndiNote.replace(/%coords%/gi, coords);
			hndiNote = hndiNote.replace(/%dlat%/gi,   dlatlon[0]);
			hndiNote = hndiNote.replace(/%dlon%/gi,   dlatlon[1]);

			var rtnLang = fParseLang(hndiNote);
			e_LogBookPanel1_LogButton.disabled = false;
			if (rtnLang[0] == 0) {
				e_LogInfo.value = hndiNote;
			} else {
				if (rtnLang[1] >= 0) {
					e_LogInfo.value = aLangText[rtnLang[1]].trim();
				}
				//  Don't auto-submit if language tags used.
				AutoSubmit = false;
			}

			fSetLogType(GM_getValue('HndiType', '18'));
			cannotDeleteChk.checked=true;
			e_LogBookPanel1_LogButton.focus();
		}


		//  Find absolute screen position of month control, to position new controls.
		var dateCtrlDD = dateCtrl.parentNode;

		//  Add div to screen.
		GM_addStyle(".quickbuttonxl {border:solid 1px rgb(68,142,53); margin-right:12px; " +
				"border-radius:5px; -moz-border-radius:7px; background-color:rgb(239,239,239); " +
				"padding:3px; cursor:pointer; }");

		divCtrls = document.createElement('div');
		divCtrls.id = 'gm_divCtrls';
		divCtrls.setAttribute('style', 'float:right; width: 135px; height:32px; margin-top:-26px; ');
		dateCtrlDD.appendChild(divCtrls);

		var imgLogRestore = document.createElement('img');
		imgLogRestore.src = imgsrc_LogRestore;
		imgLogRestore.id = 'imgLogRestore';
		imgLogRestore.setAttribute('action', 'restore');
		imgLogRestore.title = 'Restore last log text.';
		imgLogRestore.classList.add('quickbuttonxl');
		divCtrls.appendChild(imgLogRestore);

		var imgTabDup = document.createElement('img');
		imgTabDup.src = imgsrc_TabDup;
		imgTabDup.id = 'imgTabDup';
		imgTabDup.setAttribute('action', 'copy');
		imgTabDup.title = 'Copy Log Text and Attributes to Open Log Tabs.';
		imgTabDup.classList.add('quickbuttonxl');
		divCtrls.appendChild(imgTabDup);

		var imgSubmitAll = document.createElement('img');
		imgSubmitAll.src = imgsrc_SubmitAll;
		imgSubmitAll.id = 'imgSubmitAll';
		imgSubmitAll.setAttribute('action', 'submit');
		imgSubmitAll.title = 'Submit All Open Log Tabs.';
		imgSubmitAll.classList.add('quickbuttonxl');
		divCtrls.appendChild(imgSubmitAll);


		imgTabDup.addEventListener('click', fCopy2Storage, false);
		imgSubmitAll.addEventListener('click', fCopy2Storage, false);
		imgLogRestore.addEventListener('click', fLogRestore, false);
		window.addEventListener('storage', fStorageChanged, false);

	}



	//  Add event to save text if leaving log entry page.
	window.addEventListener("beforeunload", fBeforeExit, true);

	//  Before exit from page.
	function fBeforeExit() {
		if (e_LogInfo.value.trim().length > 0) {
			GM_setValue('LastLogText', e_LogInfo.value);
		}
	}





	// Temporary fix to always check Cannot Delete.
	if (cannotDeleteChk) {
		window.addEventListener("load", fCheckCannotDelete, false);
	}
	function fCheckCannotDelete() {
		cannotDeleteChk.checked=true;
	}



	if (AutoSubmit) {
		fSubmitLog();
	}






// ---------------------------------- Functions ---------------------------------- //


	//  Autosubmit a log, if requested.
	function fSubmitLog() {
	//  Function to click the log submit button.
		e_LogBookPanel1_LogButton.disabled = false;
		var ClickLogSubmitButton = function() {
			if (e_LogBookPanel1_LogButton) {
				e_LogBookPanel1_LogButton.click();
			}
		}
		TimeOutID = window.setTimeout(ClickLogSubmitButton, 250);
	}



	//  Parse into individual languages, create buttons, and return number
	//  of languages and index to default language (or -1 if none).
	function fParseLang(note) {
		var sp = document.createElement('span');
		sp.innerHTML = note;
		var j = sp.getElementsByTagName('lang').length - 1;
		var dftIdx = -1;
		if (j >= 0) {
			//  Add span to page to contain buttons.
			spanLangButtons = document.createElement('span');
			spanLangButtons.style.marginRight = '15px';
			insertAheadOf(spanLangButtons, e_LogBookPanel1_LogButton);
			for (i = 0; i<=j; i++) {
				var langNode = sp.getElementsByTagName('lang')[i];
				var langName = langNode.id;
				aLangText[i] = langNode.textContent;
				aLangText[i] = aLangText[i].replace(new RegExp("^\\n*", 'gi'),"");
				//  Create buttons.
				var langButton = document.createElement('input');
				langButton.type = 'button';
				langButton.value = langName;
				langButton.id = 'lButton_' + i.toString();
				langButton.style.marginLeft = '4px';
				langButton.setAttribute('index', i);
				spanLangButtons.appendChild(langButton);
				langButton.addEventListener('click', fLangClicked, false);
				//  If first default, store index.
				var isDft = eval(langNode.getAttribute('default', 'false'));
				if (isDft && (dftIdx < 0)) {
					dftIdx = i;
					langButton.style.fontWeight = "bold";
				}
				//  If append, prepend "right-arrow" symbol to name, and set attribute.
				var isAppend = eval(langNode.getAttribute('append', 'false'));
				if (isAppend) {
					langButton.value = '\u21D2' + langButton.value;
					langButton.setAttribute('append', true);
				}
				//  If auto-submit, append "enter" symbol to name, and set attribute.
				var isAutoSubmit = eval(langNode.getAttribute('autosubmit', 'false'));
				if (isAutoSubmit) {
					langButton.value += '\u21B5';
					langButton.setAttribute('autosubmit', true);
				}
			}
		}
		//  Return number of languages, and the default index.
		return [j + 1, dftIdx];
	}


	//  Process language button clicked.
	function fLangClicked() {
		var idx = this.getAttribute('index',0) - 0;
		var isAppend = eval(this.getAttribute('append', false));
		var isAutoSubmit = eval(this.getAttribute('autosubmit', false));
		if (!isAppend) {
			e_LogInfo.value = fRebracket(aLangText[idx].trim());
		} else {
			//  Strip trailing newlines.
			var xTxt = e_LogInfo.value.replace(/\n+$/, '');
			//  Append to existing text.
			e_LogInfo.value = xTxt + '\n\n' + fRebracket(aLangText[idx].trim());
			e_LogInfo.scrollTop = e_LogInfo.scrollHeight;
		}
		if (isAutoSubmit) {
			fSubmitLog();
		} else {
			e_LogBookPanel1_LogButton.disabled = false;
		}
	}


	//  Retrieve log text.
	function fLogRestore() {
		fFlashButton(this);
		var lastLog = GM_getValue('LastLogText', "");
		if (!lastLog) {
			alert('No text saved.');
		} else {
			if (e_LogInfo.value.trim().length > 0) {
				var Resp = confirm('Last log text will be appended to any text\n' +
						'currently in the text area.\n\nContinue?');
				if (Resp) {
					e_LogInfo.value += lastLog;
				} else {
					alert('Action canceled.');
				}
			} else {
				e_LogInfo.value = lastLog;
			}
		}
	}


	//  Copy log attributes to storage. The random number is included to ensure the
	//  storage string is different each time it is stored, to trigger the change event.
	function fCopy2Storage() {
		fFlashButton(this);
		var jsObj = new Object;
		if (this.getAttribute('action') == 'copy') {
			if (e_LogInfo.value.trim().length > 0) {
				jsObj['logtext'] = e_LogInfo.value;
				jsObj['logtype'] = e_LogType.value;
				jsObj['logdate']= dateCtrl.value;
				jsObj['encrypt'] = encryptChk.checked;
				jsObj['nodelete']= cannotDeleteChk.checked;
				jsObj['submit']  = 'false';
				jsObj['random']  = Math.random();
				var jsonString = JSON.stringify(jsObj);
				localStorage.setItem(lsCopyKey, jsonString);
			} else {
				jsonString = localStorage.getItem(lsCopyKey);
				if (jsonString) {
					jsObj = JSON.parse(jsonString);
					e_LogInfo.value         = jsObj['logtext'];
					e_LogType.value         = jsObj['logtype'];
					dateCtrl.value           = jsObj['logdate'];
					encryptChk.checked      = jsObj['encrypt'];
					cannotDeleteChk.checked = jsObj['nodelete'];
					e_LogInfo.focus();
					jsObj['submit'] = 'false';
					jsObj['random'] = Math.random();
					var jsonString = JSON.stringify(jsObj);
					localStorage.setItem(lsCopyKey, jsonString);
				}
			}
		} else if (this.getAttribute('action') == 'submit') {
			jsObj['submit'] = 'true';
			jsObj['random'] = Math.random();
			var jsonString = JSON.stringify(jsObj);
			sessionStorage.setItem('autoclose', 'true');
			localStorage.setItem(lsCopyKey, jsonString);
			fSubmitLog();
		}
	}


	//  Update log to text in storage.
	function fStorageChanged(e) {
		var jsObj = new Object;
		if (e.key == lsCopyKey) {
			var jsonString = e.newValue;
			jsObj = JSON.parse(jsonString);
			if ((jsObj['submit']) == 'true') {
				sessionStorage.setItem('autoclose', 'true');
				fSubmitLog();
			} else {
				e_LogInfo.value         = jsObj['logtext'];
				e_LogType.value         = jsObj['logtype'];
				dateCtrl.value           = jsObj['logdate'];
				encryptChk.checked      = jsObj['encrypt'];
				cannotDeleteChk.checked = jsObj['nodelete'];
				e_LogInfo.click();
			}
		}
	}


	//  Change button background to white, then fade to gray.
	function fFlashButton(obj) {
		var obj;
		var rgb = 256;
		var si = window.setInterval(fFade2Gray, 40);
		//  Function to fade-in blackout div.
		function fFade2Gray() {
			rgb -= 1;
			obj.style.backgroundColor = 'rgb('+rgb+','+rgb+','+rgb+')';
			if (rgb <= 239) {
				window.clearInterval(si);
			}
		}
	}


	//  Replace all left brackets with entity code, except for those enclosing the "lang" tag.
	//  This allows brackets to be seen as text, rather than child nodes.
	function fUnbracket(work) {
		var work = work.replace(/</g, '&lt;');
		work = work.replace(new RegExp("&lt;lang ", 'gi'),"<lang ");
		work = work.replace(new RegExp("&lt;/lang\\s*>", 'gi'),"</lang>");
		return work;
	}


	//  Change any left bracket entities back to actual brackets.
	function fRebracket(work) {
		var work = work.replace(/&lt;/g, '<');
		return work;
	}


	//  Set the Always Cannot Delete option on/off.
	function fOptRevNoteUndel() {
		var OptRevNoteUndel = GM_getValue('OptRevNoteUndel', 'On');
		var Resp = confirm(
				'Click OK to always automatically set the Cannot Delete\n' +
				'option for a log.\n\n' +
				'Click CANCEL to turn this option Off.\n\n' +
				'This option is currently turned ' + OptRevNoteUndel + '.');
		if (Resp) {
			OptRevNoteUndel = 'On';
		} else {
			OptRevNoteUndel = 'Off';
		}
		GM_setValue('OptRevNoteUndel', OptRevNoteUndel);
		alert('Auto Cannot Delete has been turned ' + OptRevNoteUndel + '.');
	}



	//  Edit Proximity Note Template.
	function fEditProxTmpl() {
		var proxNote = GM_getValue('ProxNote', '');
		var proxType = GM_getValue('ProxType', '-1');
		e_LogInfo.value = fRebracket(proxNote);
		if (proxType != '-1') {
			if (OkLogTypes.indexOf(proxType) != -1) {
				e_LogType.value = proxType;
			} else {
				alert('Unable to set log type to saved value');
			}
		}
	}

	//  Save Proximity Note Template.
	function fSaveProxTmpl() {
		var Resp = confirm('This will save the current text and log type as the proximity\n' +
				'note template.\n\n' +
				'Click OK to save.\n' +
				'Click CANCEL to abort the save.');
		if (Resp) {
			proxNote = fUnbracket(e_LogInfo.value);
			GM_setValue('ProxNote', proxNote);
			GM_setValue('ProxType', e_LogType.value);
			alert('Template has been saved.');

		} else {
			alert('Save aborted.');
		}
	}


	//  Edit Litmus-1 Note Template.
	function fEditLit1Tmpl() {
		var lit1Note = GM_getValue('Lit1Note', '');
		var lit1Type = GM_getValue('Lit1Type', '-1');
		e_LogInfo.value = fRebracket(lit1Note);
		if (lit1Type != '-1') {
			if (OkLogTypes.indexOf(lit1Type) != -1) {
				e_LogType.value = lit1Type;
			} else {
				alert('Unable to set log type to saved value');
			}
		}
	}

	//  Save Litmus-1 Note Template.
	function fSaveLit1Tmpl() {
		var Resp = confirm('This will save the current text and log type as the Litmus-1\n' +
				'note template.\n\n' +
				'Click OK to save.\n' +
				'Click CANCEL to abort the save.');
		if (Resp) {
			lit1Note = fUnbracket(e_LogInfo.value);
			GM_setValue('Lit1Note', lit1Note);
			GM_setValue('Lit1Type', e_LogType.value);
			alert('Template has been saved.');

		} else {
			alert('Save aborted.');
		}
	}

	//  Edit Vacation Template.
	function fEditVacaTmpl() {
		var vacaNote = GM_getValue('VacaNote', "");
		var vacaType = GM_getValue('VacaType', "-1");
		e_LogInfo.value = fRebracket(vacaNote);
		if (vacaType != '-1') {
			if (OkLogTypes.indexOf(vacaType) != -1) {
				e_LogType.value = vacaType;
			} else {
				alert('Unable to set log type to saved value');
			}
		}
	}


	//  Save Vacation Template.
	function fSaveVacaTmpl() {
		var Resp = confirm('This will save the current text and log type as the vacation \n' +
				'note template.\n\n' +
				'Click OK to save.\n' +
				'Click CANCEL to abort the save.');
		if (Resp) {
			vacaNote = fUnbracket(e_LogInfo.value);
			GM_setValue('VacaNote', vacaNote);
			GM_setValue('VacaType', e_LogType.value);
			alert('Template has been saved.');

		} else {
			alert('Save aborted.');
		}
	}


	//  Edit Handicap Template.
	function fEditHndiTmpl() {
		var hndiNote = GM_getValue('HndiNote', "");
		var hndiType = GM_getValue('HndiType', "-1");
		e_LogInfo.value = fRebracket(hndiNote);
		if (hndiType != '-1') {
			if (OkLogTypes.indexOf(hndiType) != -1) {
				e_LogType.value = hndiType;
			} else {
				alert('Unable to set log type to saved value');
			}
		}
	}


	//  Save Handicap Template.
	function fSaveHndiTmpl() {
		var Resp = confirm('This will save the current text and log type as the handicap \n' +
				'note template.\n\n' +
				'Click OK to save.\n' +
				'Click CANCEL to abort the save.');
		if (Resp) {
			hndiNote = fUnbracket(e_LogInfo.value);
			GM_setValue('HndiNote', hndiNote);
			GM_setValue('HndiType', e_LogType.value);
			alert('Template has been saved.');

		} else {
			alert('Save aborted.');
		}
	}


	//  Edit Updating Coordinates Template.
	function fEditUpcoTmpl() {
		var upcoNote = GM_getValue('UpcoNote', "Updating coordinates.");
		e_LogInfo.value = fRebracket(upcoNote);
	}


	//  Save Updating Coordinates Template.
	function fSaveUpcoTmpl() {
		var Resp = confirm('This will save the current text as the update coordinates \n' +
				'note template.\n\n' +
				'Click OK to save.\n' +
				'Click CANCEL to abort the save.');
		if (Resp) {
			upcoNote = fUnbracket(e_LogInfo.value);
			GM_setValue('UpcoNote', upcoNote);
			alert('Template has been saved.');

		} else {
			alert('Save aborted.');
		}
	}


	//  Set log type. Default to Review Note if log type not available.
	function fSetLogType(logType) {
		if (OkLogTypes.indexOf(logType) != -1) {
			e_LogType.value = logType;
		} else {
			if (OkLogTypes.indexOf('18') != -1) {
				e_LogType.value = '18';
			} else {
				e_LogType.value = '68';
			}
		}
	}


	//  Process log type change.
	function TypeChange() {
		newType = this.value - 0;
			var LogIsBlank = (e_LogInfo.value.trim().length == 0);
		switch(newType) {
			case 24:
				if (LogIsBlank) { e_LogInfo.value = 'Published'; }
				if (cannotDeleteChk) { cannotDeleteChk.checked = true; }
				e_LogInfo.focus();
				e_LogBookPanel1_LogButton.focus();
				break;
			case 25:
				if (LogIsBlank) { e_LogInfo.value = 'Listing retracted.'; }
				if (cannotDeleteChk) { cannotDeleteChk.checked = true; }
				e_LogInfo.focus();
				e_LogBookPanel1_LogButton.focus();
				break;
			case 18: case 68:
				if (GM_getValue('OptRevNoteUndel', 'Off') == 'On') {
					if (cannotDeleteChk) { cannotDeleteChk.checked = true; }
				}
				e_LogInfo.focus();
				e_LogBookPanel1_LogButton.focus();
				break;
		}
	}



	//  Export All Templates.
	function fExportTemplates() {
		var jsObj = new Object;
		jsObj['UpcoNote'] = {
			'template': GM_getValue('UpcoNote',''),
			'logtype' : '47',
			'logtypei': 'UpcoType'};
		jsObj['ProxNote'] = {
			'template': GM_getValue('ProxNote',''),
			'logtype' : GM_getValue('ProxType',''),
			'logtypei': 'ProxType'};
		jsObj['Lit1Note'] = {
			'template': GM_getValue('Lit1Note',''),
			'logtype' : GM_getValue('Lit1Type',''),
			'logtypei': 'Lit1Type'};
		jsObj['VacaNote'] = {
			'template': GM_getValue('VacaNote',''),
			'logtype' : GM_getValue('VacaType',''),
			'logtypei': 'VacaType'};
		jsObj['HndiNote'] = {
			'template': GM_getValue('HndiNote',''),
			'logtype' : GM_getValue('HndiType',''),
			'logtypei': 'HndiType'};

		var tstr = JSON.stringify(jsObj,null,4);
		e_LogInfo.value = tstr;
	}


	//  Import Templates.
	function fImportTemplates() {
		if (!e_LogInfo.value.trim()) {
			alert('Text box is empty. Paste the entire Template Import\n' +
					'Text into the text box, and invoke the option again.');
			return;
		}
		var Resp = confirm('This will replace existing templates with\n' +
				'the Import Text currently in the text box.\n\n' +
				'Are you SURE you want to continue?');
		if (Resp) {
			try {
				var jsObj = JSON.parse(e_LogInfo.value);
			}
			catch(err) {
				alert('Error: ' + err.description + '\n\n' +
				'Unable to parse the Import Text. It may be corrupted,\n' +
				'or not copied in full.\n\n' +
				'Import process aborted. Existing Templates are unchanged.');
				return;
			}
			var totimp = 0;
			for (var i in jsObj) {
				if (jsObj.hasOwnProperty(i)) {
					GM_setValue(i, jsObj[i]['template']);
					GM_setValue(jsObj[i]['logtypei'], jsObj[i]['logtype']);
					++totimp;
				}
			}
			alert(totimp + ' template(s) imported.');
		} else {
			alert('Import canceled');
		}
	}


	// Expand directions.
	function fExpandDir(dir) {
		if (dir.length == 2) {
			dir = dir.substr(0,1) + '-' + dir.substr(1,1);
		}
		dir = dir.replace(/N/g,'north');
		dir = dir.replace(/S/g,'south');
		dir = dir.replace(/E/g,'east');
		dir = dir.replace(/W/g,'west');
		return dir;
	}


	//  Find true left (index 0) and top (index 1) position of any object.
	//  Returned: rtnvar[0] = left; rtnvar[1] = top;
	function fFindPos(obj) {
		var curleft = curtop = 0;
		if (obj.offsetParent) {
			do {
				curleft += obj.offsetLeft;
				curtop += obj.offsetTop;
			} while (obj = obj.offsetParent);
			return [curleft,curtop];
		}
	}


	//  Escape a string for regex operations.
	function escStr(str) {
		return (str+'').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
	}


	//  Returns a URL parameter.
	//    ParmName - Parameter name to look for.
	//    IgnoreCase - (optional) *false, true. Ignore parmeter name case.
	//    UrlString - (optional) String to search. If omitted, document URL is used.
	function UrlParm(ParmName, IgnoreCase, UrlString) {
		var RegRslt, sc = '', RtnVal = '';
		if (IgnoreCase) {sc = 'i'}
		if(UrlString) {
			var PageUrl = UrlString;
		} else {
			PageUrl = document.location + '';
		}
		var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
		var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
		RegRslt = RegEx1.exec(ParmString);
		if (RegRslt) {RtnVal = RegRslt[2]}
		return RtnVal;
	}


	//	Insert element after an existing element.
	function insertAfter(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement.nextSibling);
	}

	//  Insert element ahead of an existing element.
	function insertAheadOf(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement);
	}



	//  Convert Waypoint to ID number.
	function WptToID(Wpt) {
		var BASE_GC = "0123456789ABCDEFGHJKMNPQRTVWXYZ";
		var BASE_31 = "0123456789ABCDEFGHIJKLMNOPQRSTU";
		var WptWork, Wpt31 = '', idNum;
		// Strip off leading 'GC'.
		WptWork = Wpt.replace(/^GC/,'');
		// If old-style base-16 value.
		if ((WptWork.length <= 4) && (WptWork.search(/^(\d|[A-F]).*/)) >= 0) {
			idNum = parseInt(WptWork, 16);
		// If new-style base-31 value.
		} else {
			for (var i = 0; i < WptWork.length; i++) {
				Wpt31 += BASE_31.substr(BASE_GC.indexOf(WptWork.substr(i, 1)), 1);
			}
			idNum = parseInt(Wpt31, 31) - 411120;
		}
		return idNum;
	}


	//  Convert single coordinate decimal-minutes string to latitude & logitude decimal degree values.
	function dm2dd(inStr) {
		var RegEx1 = new RegExp('^([N|S])(\\d{1,2}).*?(\\d{1,2}[.]\\d{1,3})\\s+' +
				'([E|W])(\\d{1,3}).*?(\\d{1,2}[.]\\d{1,3})', 'ig');
		var v = RegEx1.exec(inStr);
		var lat = ((v[2] - 0) + (v[3] / 60)).toFixed(6);
		var lon = ((v[5] - 0) + (v[6] / 60)).toFixed(6);
		if (v[1] == 'S') { lat *= -1; }
		if (v[4] == 'W') { lon *= -1; }
		return [lat.toString(), lon.toString()];
	}































	// ---------------------------------- Setting Functions --------------------------------- //




	function fSetOptions() {
		if (!fCreateSettingsDiv('Log Entry Script Options')) { return; };
		var divSet = document.getElementById('gm_divSet');
		divSet.setAttribute('winTopPos', document.documentElement.scrollTop);  // snapback code

		//  Generate one unique DOM name for all setting controls being created.
		settingName = 'sc_' + Math.floor(Math.random()*100000001).toString();

		GM_addStyle('legend.gmsettings { background: cornsilk; border: solid 2px slategray; border-radius: 8px; padding: 6px; !important;}');
		GM_addStyle('fieldset.gmsettings { background: lightyellow; border-radius: 10px; padding: 5px; border: solid 2px green; margin-bottom: 30px; !important; }' );


		//****************************************************************************************//
		//                                                                                        //
		//                             Start of Control Creation                                  //
		//                                                                                        //
		//	fCreateSetting(vID, vType, vLabel, vTitle, vDftVal, vSize, vSelVal, vSelTxt)          //
		//                                                                                        //
		//		vID = Identifier for control, and for saving setting data.                        //
		//		vType = checkbox (input + type.checkbox), text (input), textarea, select          //
		//		vLabel = Text for control label. Above textareas, or to the right of all others.  //
		//		vTitle = Title text for input control and label, or vLabel if not specified.      //
		//		vDftVal = May be text, true/false, or matches a vSelVal array element.            //
		//		vSize = Length of input (text) box, or height of textarea (e.g., '20px').         //
		//		vSelVal = Array of select option values.                                          //
		//		vSelTxt = Array of select option text. If omitted, SelVal values are used.        //
		//                                                                                        //
		//****************************************************************************************//


/*
		var vDftVal = GM_getValue('ExampleText_' + SignedInAs, 100);
		var vLabel = 'Example label text.';
		var txtDistWarn = fCreateSetting('ExampleText', 'text', vLabel, '', vDftVal, '4em');

		vDftVal = GM_getValue('ExampleTextArea_' + SignedInAs, 100);
		vLabel = 'Example label text.';
		var txtDistWarn = fCreateSetting('ExampleTextArea', 'textarea', vLabel, '', vDftVal, '30px');

		vDftVal = GM_getValue('ExampleCheckbox_' + SignedInAs, false);
		vLabel = 'Example label text.';
		var cbReverseSeq = fCreateSetting('ExampleCheckbox', 'checkbox', vLabel, '', vDftVal);

		vDftVal = GM_getValue('ExampleSelect_' + SignedInAs, 'Top');
		var vSelVal = [1, 2, 3];
		var vSelTxt = ['One', 'Two', 'Three'];
		var selOpenUcCacheAt = fCreateSetting('ExampleSelect', 'select', vLabel, '', vDftVal, '',
				vSelVal, vSelText);
*/



		var repvar1 = 
				"REPLACEMENT VARIABLES" +
				"\n" + 
				"\n %owner% = Cache owner's account name" +
				"\n %title% = Name of the cache." +
				"\n %wptid% = Waypoint ID." +
				"\n %coords% = Coordinates of the cache in HDD° MM.mmm HDDD° MM.mmm format." +
				"\n %vdistmi% = Distance between home coordinates and cache in miles, rounded to nearest 5 mile increment" +
				"\n %vdistkm% = Distance between home coordinates and cache in kilometers, rounded to nearest 5 km increment" +
				"\n %ncdir% = Compass direction (N,S,E,W,NE,NW, etc.) the nearby cache is from the cache being reviewed." +
				"\n %nctitle% = Name of the cache the owner's cache is too near." +
				"\n %ncwptid% = Waypoint ID of the cache the owner's cache is too near." +
				"\n %ncdistft% = Distance between the two caches in feet, rounded to the nearest 5 foot increment." +
				"\n %ncdistm% = Distance between the two caches in meters (not rounded)."
				"\n %contact% = Contact text, as you have specified above."
		;

		
		

		//  Log Settings: Contact info.
		var vSettingID = 'ContactText';
		// vDftVal = GM_getValue(vSettingID + '_' + SignedInAs, 22);
		vDftVal = GM_getValue(vSettingID, '');
		vLabel = 'Enter your contact info.\nTo insert the contact info into the subsequent templates, use %contact% as replacement variable.\n';
		fCreateSetting(vSettingID, 'textarea', vLabel, '', vDftVal, '200px');




		//  Log Settings: Posted Near Posted.
		var vSettingID = 'ProxType';
		// vDftVal = GM_getValue(vSettingID + '_' + SignedInAs, 22);
		vDftVal = GM_getValue(vSettingID, 22);
		var vSelVal = [4, 5, 22, 18];
		var vSelText = ['Write Note', 'Archive', 'Temporarily Disable Listing', 'Post Reviewer Note'];
		var vLabel = 'Select Log Type';
		fCreateSetting(vSettingID, 'select', vLabel, '', vDftVal, '',
				vSelVal, vSelText);

		var vSettingID = 'ProxNote';
 		// vDftVal = GM_getValue(vSettingID + '_' + SignedInAs, '');
 		vDftVal = GM_getValue(vSettingID, '');
		vLabel = 'Log Text';
		fCreateSetting(vSettingID, 'textarea', vLabel, repvar1, vDftVal, '200px');

		fGroupSettings('Proximity Log: Posted coordinates too near existing Posted coordinates', ['ProxType', 'ProxNote']);

		fAddEditControls('txtaraProxNote');



		//  Log Settings: Posted Near Wpt.
		var vSettingID = 'Lit1Type';
		vDftVal = GM_getValue(vSettingID, 22);
		var vSelVal = [4, 5, 22, 18];
		var vSelText = ['Write Note', 'Archive', 'Temporarily Disable Listing', 'Post Reviewer Note'];
		var vLabel = 'Select Log Type';
		fCreateSetting(vSettingID, 'select', vLabel, '', vDftVal, '',
				vSelVal, vSelText);

		var vSettingID = 'Lit1Note';
 		vDftVal = GM_getValue(vSettingID, '');
		vLabel = 'Log Text';
		fCreateSetting(vSettingID, 'textarea', vLabel, repvar1, vDftVal, '200px');

		fGroupSettings('Proximity Log: Posted coordinates too near existing Waypoint coordinates', ['Lit1Type', 'Lit1Note']);

		fAddEditControls('txtaraLit1Note');



		//  Log Settings: Wpt Near Posted.
		var vSettingID = 'Lit2Type';
		vDftVal = GM_getValue(vSettingID, 22);
		var vSelVal = [4, 5, 22, 18];
		var vSelText = ['Write Note', 'Archive', 'Temporarily Disable Listing', 'Post Reviewer Note'];
		var vLabel = 'Select Log Type';
		fCreateSetting(vSettingID, 'select', vLabel, '', vDftVal, '',
				vSelVal, vSelText);

		var vSettingID = 'Lit2Note';
 		vDftVal = GM_getValue(vSettingID, '');
		vLabel = 'Log Text';
		fCreateSetting(vSettingID, 'textarea', vLabel, repvar1, vDftVal, '200px');

		fGroupSettings('Proximity Log: Waypoint coordinates too near existing Posted coordinates', ['Lit2Type', 'Lit2Note']);

		fAddEditControls('txtaraLit2Note');




		//  Log Settings: Wpt Near Wpt.
		var vSettingID = 'Lit3Type';
		vDftVal = GM_getValue(vSettingID, 22);
		var vSelVal = [4, 5, 22, 18];
		var vSelText = ['Write Note', 'Archive', 'Temporarily Disable Listing', 'Post Reviewer Note'];
		var vLabel = 'Select Log Type';
		fCreateSetting(vSettingID, 'select', vLabel, '', vDftVal, '',
				vSelVal, vSelText);

		var vSettingID = 'Lit3Note';
 		vDftVal = GM_getValue(vSettingID, '');
		vLabel = 'Log Text';
		fCreateSetting(vSettingID, 'textarea', vLabel, repvar1, vDftVal, '200px');

		fGroupSettings('Proximity Log: Waypoint coordinates too near existing Waypoint coordinates', ['Lit3Type', 'Lit3Note']);

		fAddEditControls('txtaraLit3Note');








		//****************************************************************************************//
		//                                                                                        //
		//                              End of Control Creation                                   //
		//                                                                                        //
		//****************************************************************************************//


		fOpenSettingsDiv();
	}


	//  Cancle button clicked.
	function fCancelButtonClicked() {
		var resp = confirm('Any changes will be lost. Close Settings?');
		if (resp) {
			fCloseSettingsDiv();
		}
	}


	//  Save button clicked. Store all settings.
	function fSaveButtonClicked() {
		//  Get array of setting controls.
		var scs = document.getElementsByName(settingName);
		//  Process each control.
		var scl = scs.length;
		for (var i = 0; i < scl; i++) {
			var sc = scs[i];
			var rawid = sc.getAttribute('rawid', '');
			var ctrltype = sc.getAttribute('ctrltype', '');
			if (ctrltype == 'checkbox') {
				GM_setValue(rawid, sc.checked);
			} else {
				GM_setValue(rawid, sc.value);
			}
		}
		fCloseSettingsDiv();
	}


	//  Create Div for settings GUI.
	function fCreateSettingsDiv(sTitle) {

		//  If div already exists, reposition browser, and show alert.
		var divSet = document.getElementById("gm_divSet");
		if (divSet) {
			var YOffsetVal = parseInt(divSet.getAttribute('YOffsetVal', '0'));
			window.scrollTo(window.pageXOffset, YOffsetVal);
			alert('Edit Setting interface already on screen.');
			return false;
		}

		//  Set styles for titles and elements.
		GM_addStyle('.SettingTitle {font-size: medium; font-style: italic; font-weight: bold; ' +
				'text-align: center; margin-bottom: 12px; !important; } ' );
		GM_addStyle('.SettingVersionTitle {font-size: small; font-style: italic; font-weight: bold; ' +
				'text-align: center; margin-bottom: 12px; !important; } ' );
		GM_addStyle('.SettingElement {text-align: left; margin-left: 6px; ' +
				'margin-right: 6px; margin-bottom: 12px; !important; } ' );
		GM_addStyle('.SettingButtons {text-align: right; margin-left: 6px; ' +
				'margin-right: 6px; margin-bottom: 6px; !important; } ' );
		GM_addStyle('.SettingLabel {font-weight: bold; margin-left: 6px !important; line-height:120% !important; } ' ); +

		//  Create blackout div.
		document.body.setAttribute('style', 'height:100%;');
		var divBlackout = document.createElement('div');
		divBlackout.id = 'divBlackout';
		divBlackout.setAttribute('style', 'z-index: 900; background-color: rgb(0, 0, 0); ' +
				'visibility: hidden; opacity: 0; position: fixed; left: 0px; top: 0px; '+
				'height: 100%; width: 100%; display: block;');

		//  Create div.
		divSet = document.createElement('div');
		divSet.id = 'gm_divSet';
		divSet.setAttribute('style', 'position: absolute; z-index: 1000; visibility: hidden; ' +
				'padding: 5px; outline: 6px ridge blue; background: #FFFFCC;');
		popwidth = parseInt(window.innerWidth * .7);
		divSet.style.width = popwidth + 'px';

		//  Create heading.
		var ds_Heading = document.createElement('div');
		ds_Heading.setAttribute('class', 'SettingTitle');
		ds_Heading.appendChild(document.createTextNode(sTitle));
		divSet.appendChild(ds_Heading);

		var ds_Version = document.createElement('div');
		ds_Version.setAttribute('class', 'SettingVersionTitle');
		ds_Version.appendChild(document.createTextNode('Script Version ' + GM_info.script.version));
		divSet.appendChild(ds_Version);

		//  Add div to page.
		var toppos =  parseInt(window.pageYOffset +  60);
		var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
		divSet.style.top = toppos + 'px';
		divSet.style.left = leftpos + 'px';
		divSet.setAttribute('YOffsetVal', window.pageYOffset);

		//  Add blackout and setting divs.
		document.body.appendChild(divBlackout);
		document.body.appendChild(divSet);
		window.addEventListener('resize', fSetLeftPos, true);

		return true;
	}




	/*
	vID = GetSetting ID, and type + vID is control ID.
	vType = checkbox (input + type.checkbox), text (input), textarea, select
	vLabel = Text for control label.
	vTitle = Title text for input control and label, or vLabel if not specified.
	vDftVal = May be text, true/false, or matches a vSelVal array element.
	vSize = Length of input (text) box, or height of textarea, in pixels.
	vSelVal = Array of select option values.
	vSelTxt = Array of select option text.
	*/
	//  Create settings from passed data.
 	function fCreateSetting(vID, vType, vLabel, vTitle, vDftVal, vSize, vSelVal, vSelTxt) {
		var divSet = document.getElementById('gm_divSet');
		var kParagraph = document.createElement('p');
		kParagraph.id = 'p' + vID;
		kParagraph.setAttribute('class', 'SettingElement');
	
		switch (vType) {
			case 'checkbox':
				var kElem = document.createElement('input');
				kElem.id = 'cb' + vID;
				kElem.type = 'checkbox';
				kElem. checked = vDftVal;
				break;
			case 'text':
				if (!vSize) { vSize = '150px'; }
				var kElem = document.createElement('input');
				kElem.id = 'txt' + vID;
				kElem.style.width = vSize;
				kElem.value = vDftVal;
				kElem.addEventListener('focus', fSelectAllText, true);
				break;
			 case 'textarea':
				if (!vSize) { vSize = '80px'; }
				var kElem = document.createElement('textarea');
				kElem.id = 'txtara' + vID;
				kElem.style.width = '100%';
				kElem.style.height = vSize;
				kElem.value = vDftVal;
				break;
			case 'select':
				var kElem = document.createElement('select');
				kElem.id = 'sel' + vID;
				if (vSelVal) {
					if (vSelVal.constructor == Array) {
						for (var i in vSelVal) {
							var kOption = document.createElement('option');
							kOption.value = vSelVal[i];
							kOption.selected = (vSelVal[i] == vDftVal);
							if ((vSelTxt.constructor == Array) && (vSelTxt.length >= i-1)) {
								var kTxtNode = vSelTxt[i];
							} else {
								var kTxtNode = vSelVal[i];
							}
							kOption.appendChild(document.createTextNode(kTxtNode));
							kElem.appendChild(kOption);
						}
					}
				}
				break;
				
		}

		var kLabel = document.createElement('label');
		kLabel.setAttribute('class', 'SettingLabel');
		kLabel.setAttribute('for', kElem.id);
		kLabel.appendChild(document.createTextNode(vLabel));
		if (!vTitle) { vTitle = vLabel; }
		kElem.title = vTitle;
		kLabel.title = kElem.title;
		kElem.name = settingName;
		kElem.setAttribute('ctrltype', vType);
		kElem.setAttribute('rawid', vID);

		kLabel.innerHTML = kLabel.innerHTML.replace(/\n/g,'<br>');
		kLabel.innerHTML = kLabel.innerHTML.replace(/\t/g,'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');

		if (vType == 'textarea') {
			kParagraph.appendChild(kLabel);
			kParagraph.appendChild(document.createElement('br'));
			kParagraph.appendChild(kElem);
		} else {
			kParagraph.appendChild(kElem);
			kParagraph.appendChild(kLabel);
		}
		divSet.appendChild(kParagraph);

		return kElem;
	}


	//  Routine for creating setting groupings.
	//  Use after the settings group has been established.
	//  Pass the heading text, and an array of setting IDs.
	//  Example:
	//  [Create SettingName1 here]
	//  [Create SettingName2 here]
	//  [Create SettingName3 here]
	//  fGroupSettings('Group Heading Text', ['SettingName1', 'SettingName2', 'SettingName3']);
	function fGroupSettings(legendText, aID) {
		var divSet = document.getElementById('gm_divSet');
		var fldset = document.createElement('fieldset');
		fldset.classList.add('gmsettings');
		divSet.appendChild(fldset);
		var fldsetlgd = document.createElement('legend');
		fldsetlgd.classList.add('gmsettings');
		fldset.appendChild(fldsetlgd);
		fldsetlgd.appendChild(document.createTextNode(legendText));
		var ic = aID.length;
		for (var i = 0; i < ic; i++) {
			fldset.appendChild(document.getElementById('p' + aID[i]));
		}
	}


	//  Resize/reposition on window resizing.
	function fSetLeftPos() {
		var divSet = document.getElementById('gm_divSet');
		if (divSet) {
			var popwidth = parseInt(window.innerWidth * .7);
			divSet.style.width = popwidth + 'px';
			var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
			divSet.style.left = leftpos + 'px';
		}
	}


	function fOpenSettingsDiv() {
		//  Create buttons.
		fCreateSaveCancleButtons();

		//  Add blackout and setting divs.
		var divBlackout = document.getElementById('divBlackout');
		var divSet = document.getElementById('gm_divSet');
		divSet.style.visibility = 'visible';
		divBlackout.style.visibility = 'visible';
		var op = 0;
		var si = window.setInterval(fShowBlackout, 40);
		//  Function to fade-in blackout div.
		function fShowBlackout() {
			op = op + .05;
			divBlackout.style.opacity = op;
			if (op >= .75) {
				window.clearInterval(si);
			}
		}
	}


	//  Create Save/Cancel Buttons.
	function fCreateSaveCancleButtons() {
		var divSet = document.getElementById('gm_divSet');
		var ds_ButtonsP = document.createElement('div');
		ds_ButtonsP.setAttribute('class', 'SettingButtons');
		var ds_SaveButton = document.createElement('button');
		ds_SaveButton = document.createElement('button');
		ds_SaveButton.appendChild(document.createTextNode("Save"));
		ds_SaveButton.addEventListener("click", fSaveButtonClicked, true);
		var ds_CancelButton = document.createElement('button');
		ds_CancelButton.style.marginLeft = '6px';
		ds_CancelButton.addEventListener("click", fCancelButtonClicked, true);
		ds_CancelButton.appendChild(document.createTextNode("Cancel"));
		ds_ButtonsP.appendChild(ds_SaveButton);
		ds_ButtonsP.appendChild(ds_CancelButton);
		divSet.appendChild(ds_ButtonsP);
	}


	function fCloseSettingsDiv() {
		var divBlackout = document.getElementById('divBlackout');
		var divSet = document.getElementById('gm_divSet');
		var winTopPos = divSet.getAttribute('winTopPos') - 0;  // snapback code
		divSet.parentNode.removeChild(divSet);
		divBlackout.parentNode.removeChild(divBlackout);
		window.removeEventListener('resize', fSetLeftPos, true);
		document.documentElement.scrollTop = winTopPos;  // snapback code
	}



	function fCancelButtonClicked() {
		var resp = confirm('Any changes will be lost. Close Settings?');
		if (resp) {
			fCloseSettingsDiv();
		}
	}


	function fSelectAllText() {
		this.select();
	}







	GM_addStyle('.EditIcons {vertical-align: text-bottom; margin-bottom: 2px; !important; } ' );

	function fAddEditControls(ctrl2add) {
		//  Create span to hold new controls.
		var spanCtrls = document.createElement("span");
		spanCtrls.name = 'spanCtrls';
		spanCtrls.style.whiteSpace = 'nowrap';
		spanCtrls.style.fontSize = '20px';
		spanCtrls.setAttribute('TextAreaID', ctrl2add);
		ctrl2add = document.getElementById(ctrl2add);
		insertAheadOf(spanCtrls, ctrl2add);


		//  Create block-quote controls and add to span.
		var lnkQuoteB = document.createElement("a");
		var lnkQuoteBImg = document.createElement("img");
		lnkQuoteBImg.src = imgBlockQuote;
		lnkQuoteBImg.classList.add('EditIcons');
		lnkQuoteB.appendChild(lnkQuoteBImg);
		lnkQuoteB.title = 'Forum-style Block Quoting';
		lnkQuoteB.setAttribute('editType', 'quote');
		lnkQuoteB.style.marginLeft = '3px';
		lnkQuoteB.href = 'javascript:void(0)';
		lnkQuoteB.addEventListener("click", fEditText, true);
		spanCtrls.appendChild(lnkQuoteB);


		//  Create link control and add to span.
		var lnkTextLink = document.createElement("a");
		var lnkTextLinkImg = document.createElement("img");
		lnkTextLinkImg.src = imgTextLink;
		lnkTextLinkImg.classList.add('EditIcons');
		lnkTextLink.appendChild(lnkTextLinkImg);
		lnkTextLink.title = 'Insert hyperlink';
		lnkTextLink.style.marginLeft = '3px';
		lnkTextLink.href = 'javascript:void(0)';
		lnkTextLink.addEventListener("click", fTextLink, true);
		spanCtrls.appendChild(lnkTextLink);

		//  Create bold control and add to span.
		var lnkTextBold = document.createElement("a");
		var lnkTextBoldImg = document.createElement("img");
		lnkTextBoldImg.src = imgTextBold;
		lnkTextBoldImg.classList.add('EditIcons');
		lnkTextBold.appendChild(lnkTextBoldImg);
		lnkTextBold.title = 'Bold';
		lnkTextBold.setAttribute('editType', 'b');
		lnkTextBold.style.marginLeft = '3px';
		lnkTextBold.href = 'javascript:void(0)';
		lnkTextBold.addEventListener("click", fEditText, true);
		spanCtrls.appendChild(lnkTextBold);

		//  Create italic control and add to span.
		var lnkTextItalic = document.createElement("a");
		var lnkTextItalicImg = document.createElement("img");
		lnkTextItalicImg.src = imgTextItalic;
		lnkTextItalicImg.classList.add('EditIcons');
		lnkTextItalic.appendChild(lnkTextItalicImg);
		lnkTextItalic.title = 'Italic';
		lnkTextItalic.setAttribute('editType', 'i');
		lnkTextItalic.style.marginLeft = '3px';
		lnkTextItalic.href = 'javascript:void(0)';
		lnkTextItalic.addEventListener("click", fEditText, true);
		spanCtrls.appendChild(lnkTextItalic);

		//  Create Strikethrough control and add to span.
		var lnkTextStrikethrough = document.createElement("a");
		var lnkTextStrikethroughImg = document.createElement("img");
		lnkTextStrikethroughImg.src = imgTextStrikethrough;
		lnkTextStrikethroughImg.classList.add('EditIcons');
		lnkTextStrikethrough.appendChild(lnkTextStrikethroughImg);
		lnkTextStrikethrough.title = 'Strikethrough';
		lnkTextStrikethrough.setAttribute('editType', 's');
		lnkTextStrikethrough.style.marginLeft = '3px';
		lnkTextStrikethrough.href = 'javascript:void(0)';
		lnkTextStrikethrough.addEventListener("click", fEditText, true);
		spanCtrls.appendChild(lnkTextStrikethrough);


		//  Create Font selector and add to span.
		var fontSelect = document.createElement("select");
		fontSelect.name = 'fontSelect';
		fontSelect.style.borderStyle = 'solid';
		fontSelect.style.borderWidth = 'thin';
		fontSelect.style.padding = "1px";
		fontSelect.style.borderColor = 'rgb(165, 172, 178)';
		fontSelect.style.marginBottom = '3px';
		fontSelect.style.marginLeft = '6px';
		fontSelect.title = 'Select text font';
		spanCtrls.appendChild(fontSelect);

		//  Add options to Font selector.
		var lastFont = GM_getValue("EditTextFont", 'Andale Mono');
		var aFonts = ['Andale Mono', 'Arial', 'Arial Black', 'Book Antiqua', 'Century Gothic',
				'Comic Sans Ms', 'Courier New', 'Georgia', 'Impact', 'Lucida Console', 'Tahoma',
				'Times New Roman', 'Trebuchet Ms'];
		for (var c = 0; c < aFonts.length; c++) {
			var cOption = document.createElement("option");
			cOption.value = aFonts[c];
			cOption.selected = (aFonts[c] == lastFont);
			fontSelect.appendChild(cOption);
			var tFont = aFonts[c];
			cOption.appendChild(document.createTextNode(tFont));
			cOption.style.fontFamily = tFont;
			cOption.style.fontSize = 'small';
		}
		fontSelect.style.fontFamily = lastFont;
		fontSelect.style.fontSize = 'small';


		//  Create Font control and add to span.
		var lnkTextFont = document.createElement("a");
		var lnkTextFontImg = document.createElement("img");
		lnkTextFontImg.src = imgTextFont;
		lnkTextFontImg.classList.add('EditIcons');
		lnkTextFont.name = 'textFont';
		lnkTextFont.appendChild(lnkTextFontImg);
		lnkTextFont.title = 'Apply selected font';
		lnkTextFont.setAttribute('editType', lastFont);
		lnkTextFont.style.marginLeft = '6px';
		lnkTextFont.href = 'javascript:void(0)';
		lnkTextFont.addEventListener("click", fEditTextFont, true);
		spanCtrls.appendChild(lnkTextFont);


		//  Create Color selector and add to span.
		var colorSelect = document.createElement("select");
		colorSelect.style.borderStyle = 'solid';
		colorSelect.style.borderWidth = 'thin';
		colorSelect.style.padding = "1px";
		colorSelect.style.borderColor = 'rgb(165, 172, 178)';
		colorSelect.style.marginBottom = '3px';
		colorSelect.style.marginLeft = '10px';
		colorSelect.style.fontSize = 'small';
		colorSelect.title = 'Select text color';
		spanCtrls.appendChild(colorSelect);

		//  Add options to Color selector.
		var aColors = ['beige', 'black', 'blue', 'brown', 'gold', 'green', 'limegreen', 'maroon',
				'navy', 'orange', 'pink', 'purple', 'red', 'teal', 'violet', 'white', 'yellow'];
		for (var c = 0; c < aColors.length; c++) {
			var cOption = document.createElement("option");
			cOption.value = aColors[c];
			cOption.selected = (aColors[c] == 'black');
			colorSelect.appendChild(cOption);
			var tColor = aColors[c].substr(0,1).toUpperCase() + aColors[c].substr(1);
			cOption.appendChild(document.createTextNode(tColor));
			cOption.style.color = tColor;
			cOption.style.fontSize = 'small';
			cOption.style.backgroundColor = 'Lavender';
		}

		//  Create Color control and add to span.
		var lnkTextColor = document.createElement("a");
		var lnkTextColorImg = document.createElement("img");
		lnkTextColorImg.src = imgTextColor;
		lnkTextColorImg.classList.add('EditIcons');
		lnkTextColor.appendChild(lnkTextColorImg);
		lnkTextColor.name = 'colorPicker';
		lnkTextColor.title = 'Apply selected color';
		lnkTextColor.style.marginLeft = '6px';
		lnkTextColor.href = 'javascript:void(0)';
		lnkTextColor.addEventListener("click", fEditText, true);
		spanCtrls.appendChild(lnkTextColor);

	}


	//  Insert hyperlink.
	function fTextLink() {

		var TextAreaID = this.parentNode.getAttribute('TextAreaID');
		var e_LogTextArea = document.getElementById(TextAreaID);

		var tlRtnVar = prompt('Enter the complete URL for the hyperlink.', 'http://');
		if (tlRtnVar == null) {
			alert('Link insertion canceled.');
			return;
		}
		//  [url=tlRtnVar][/url]
		var opnBBCode = '[url=' + tlRtnVar.trim() + ']';
		var clsBBCode = '[/url]';

		//  Autotrim selection;
		fAutoTrim(e_LogTextArea);

		//  Reset work fields.
		var nTxt = '';
		var xTxt = '';

		//  Save original text value and scroll position.
		var oTxt = e_LogTextArea.value;
		var txtTop = e_LogTextArea.scrollTop;

		//  If portion of the text selected, save that text.
		var selStart = e_LogTextArea.selectionStart;
		var selEnd = e_LogTextArea.selectionEnd;
		if (selStart < selEnd) {
			xTxt = oTxt.substring(selStart, selEnd);
		}
		//  Get text on each side of the selection/carat;
		var txt1 = oTxt.substr(0, selStart);
		var txt2 = oTxt.substr(selEnd);
		//  Create new text and insert into text area.
		nTxt = txt1 + opnBBCode + xTxt + clsBBCode + txt2;
		e_LogTextArea.value = nTxt;
		//  Position carot, based on if any text was selected.
		var newCaretPos = selStart + opnBBCode.length;
		if (xTxt.length) { newCaretPos += xTxt.length + clsBBCode.length; }
		e_LogTextArea.focus();
		e_LogTextArea.selectionStart = newCaretPos;
		e_LogTextArea.selectionEnd = newCaretPos;
		e_LogTextArea.scrollTop = txtTop;
	}



	//  Apply text edits (block-quotes, bold, italic, strikethrough, and color).
	function fEditText() {

		var TextAreaID = this.parentNode.getAttribute('TextAreaID');
		var e_LogTextArea = document.getElementById(TextAreaID);

		if (this.name == 'colorPicker') {
			var newColor = this.previousSibling.value;
			var opnBBCode = '[' + newColor + ']';
			var clsBBCode = '[/' + newColor + ']';
		} else {
			var editType = this.getAttribute('editType');
			var opnBBCode = '[' + editType + ']';
			var clsBBCode = '[/' + editType + ']';
		}

		//  Autotrim selection;
		fAutoTrim(e_LogTextArea);

		//  Reset work fields.
		var nTxt = '';
		var xTxt = '';

		//  Save original text value and scroll position.
		var oTxt = e_LogTextArea.value;
		var txtTop = e_LogTextArea.scrollTop;

		//  If portion of the text selected, save starting and ending points.
		var selStart = e_LogTextArea.selectionStart;
		var selEnd = e_LogTextArea.selectionEnd;

		//  If block quote, use enhances mode, if turned on.
		if (editType == 'quote') {
			if (selStart == selEnd) {
				e_LogTextArea.select();
				selStart = e_LogTextArea.selectionStart;
				selEnd = e_LogTextArea.selectionEnd;
			}
		}

		//  Get selected text.
		if (selStart < selEnd) {
			xTxt = oTxt.substring(selStart, selEnd);
		}

		//  Get text on each side of the selection/carat;
		var txt1 = oTxt.substr(0, selStart);
		var txt2 = oTxt.substr(selEnd);

		//  Create new text and insert into text area.
		nTxt = txt1 + opnBBCode + xTxt + clsBBCode + txt2;
		e_LogTextArea.value = nTxt;

		//  Position carot, based on if any text was selected.
		var newCaretPos = selStart + opnBBCode.length;
		if (xTxt.length) { newCaretPos += xTxt.length + clsBBCode.length; }
		e_LogTextArea.focus();
		e_LogTextArea.selectionStart = newCaretPos;
		e_LogTextArea.selectionEnd = newCaretPos;
		e_LogTextArea.scrollTop = txtTop;
	}



	//  Apply text edits (fonts).
	function fEditTextFont() {

		var TextAreaID = this.parentNode.getAttribute('TextAreaID');
		var e_LogTextArea = document.getElementById(TextAreaID);

		// var newFont = this.getAttribute('editType');
		if (this.name = 'textFont') {;
			var newFont = this.previousSibling.value;
			var opnBBCode = '[font=' + newFont + ']';
			var clsBBCode = '[font=Verdana]';
		}

		//  Autotrim selection;
		fAutoTrim(e_LogTextArea);

		//  Reset work fields.
		var nTxt = '';
		var xTxt = '';

		//  Save original text value and scroll position.
		var oTxt = e_LogTextArea.value;
		var txtTop = e_LogTextArea.scrollTop;

		//  If portion of the text selected, save that text.
		var selStart = e_LogTextArea.selectionStart;
		var selEnd = e_LogTextArea.selectionEnd;
		if (selStart < selEnd) {
			xTxt = oTxt.substring(selStart, selEnd);
		}
		//  Get text on each side of the selection/carat;
		var txt1 = oTxt.substr(0, selStart);
		var txt2 = oTxt.substr(selEnd);
		//  Create new text and insert into text area.
		nTxt = txt1 + opnBBCode + xTxt + clsBBCode + txt2;
		e_LogTextArea.value = nTxt;
		//  Position carot, based on if any text was selected.
		var newCaretPos = selStart + opnBBCode.length;
		if (xTxt.length) { newCaretPos += xTxt.length + clsBBCode.length; }
		e_LogTextArea.focus();
		e_LogTextArea.selectionStart = newCaretPos;
		e_LogTextArea.selectionEnd = newCaretPos;
		e_LogTextArea.scrollTop = txtTop;
	}



	//  Apply Auto Trim to selected text.
	function fAutoTrim(curLogTextArea) {
		var atStart = curLogTextArea.selectionStart;
		var atEnd = curLogTextArea.selectionEnd;
		if (atEnd > atStart) {
			var atTxt = curLogTextArea.value;
			var atLastChar = atTxt.substr(atEnd-1, 1);
			while (atLastChar.match(/\s/)) {
				atEnd--;
				var atLastChar = atTxt.substr(atEnd-1, 1);
			}
			curLogTextArea.selectionEnd = atEnd;
		}
	}













