﻿// ==UserScript==
// @name           GCR Owner Action Needed v2
// @description    Owner Action Needed Automation
// @namespace      http://www.geocaching.com/admin
// @version        02.01
// @icon           http://i.imgur.com/dCZKt69.png
// @require        md5.js
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Owner_Action_Needed.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Owner_Action_Needed.user.js
// @include        http*://*.geocaching.com/seek/log.aspx*
// @include        http*://*.geocaching.com/bookmarks/mark.aspx*
// @include        http*://*.geocaching.com/seek/cache_details.aspx*
// @grant          GM_getValue
// @grant          GM_openInTab
// @grant          GM_setValue
// @grant          GM_addStyle
// @grant          GM_registerMenuCommand
// ==/UserScript==

/*

Function:
 Automates issuing Owner Action Needed warnings.

*/

	GM_registerMenuCommand('Edit "Owner Action Needed" Script Settings', fEditSettings);

	// NOTICE: Do not edit the text below. This is only default text.
	// Instead, use 'Edit Settings' from the User Script Commands menu.
	var dftRevNote =
			"The cache appears to be in need of owner intervention. " +
			"I'm temporarily disabling it, to give the owner an opportunity to " +
			"check on the cache, and take whatever action is necessary. " +
			"Please respond to this situation in a timely " +
			"manner (i.e., within %D% days) to prevent the " +
			"cache from being archived for non-responsiveness.";

	var dftArcNote =
			"No response from owner. " +
			"If you wish to repair/replace the cache sometime in the future, " +
			"just contact us (by email), and assuming it meets the current " +
			"guidelines, we'll be happy to unarchive it.";

	var OAN_BookMark = GM_getValue("OAN_BookMark", '');

	var RevNote = GM_getValue("OAN_RevNote", dftRevNote);

	var ArcNote = GM_getValue("OAN_ArcNote", dftArcNote);

	var OAN_Days_to_Respond = parseInt(GM_getValue("OAN_Days_to_Respond", "30"));

	var OAN_Auto_Close_Tabs = GM_getValue("OAN_Auto_Close_Tabs", true);

	// Get current domain.
	var domain = thisDomain();

	// Get page url.
	var PageUrl = document.location + "";

	var CacheGUID = '';

	if (PageUrl.match(/http:\/\/.*?\.geocaching\.com\/seek\/cache_details\.aspx/i)) {
		// Exit script if not signed on as a reviewer.
		var revLink = document.evaluate(
				"//a[contains(@href, '/admin/review.aspx?guid=')]",
				document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null
				).singleNodeValue;
		if (!revLink) { return; }

		// Images.
		var imgsrcWarn =
				"data:image/png,%89PNG%0D%0A%1A%0A%00%00%00%0DIHDR%00%00%00%1" +
				"0%00%00%00%10%04%03%00%00%00%ED%DD%E2R%00%00%000PLTE%1C%1C%1" +
				"1%86%86%0EFFEmmy%A2%A2%0A%C7%C7%02%C6%C6%23%C8%C8%B2%DE%DE%1" +
				"0%DE%DE%C1%DE%DE%CE%FF%00%FF%F7%F7%00%FF%FF%00%E7%E7%08%F0%F" +
				"0%B0%D9%D0%09%95%00%00%00%0CtRNS%FF%FF%FF%FF%FF%FF%FF%FF%FF%" +
				"FF%FF%00%12%DF%CE%CE%00%00%00%87IDATx%DAc%D8%BD%7B%F76)%20%C" +
				"1%00%C43%9AW%83%19%BBr%AFX%81%19%3B%7B%EFi%81%19%CB%AE%C59%A" +
				"F%061V%1C%609%5C%0Dd%EC%CAM%60%3Ch%05b%F460%9E%D3%022%96%DF%" +
				"3D%C8rWx5%C3%EEuw%0F%F2%DD%7D%0Cd%E4%DC%BD%91%7B%F7b5%C3%AE%" +
				"DC%BB%D7T%EE%DE%D0b%D8v%F6%EEE%C6%BBw%85%19%B6%DC%BD%7B%A3%E" +
				"3%EE%DD%24%86%ED%A1%AE%AE!!!%E6%0C%BB%8B%94%40%A0%1A%00g%FCK" +
				"pi%10%0B%13%00%00%00%00IEND%AEB%60%82";

		var imgsrcKill =
				"data:image/png,%89PNG%0D%0A%1A%0A%00%00%00%0DIHDR%00%00%00%1" +
				"0%00%00%00%10%04%03%00%00%00%ED%DD%E2R%00%00%000PLTE%00%00%0" +
				"0BBBRRRkkk%8C%8C%8C%94%94%94%9C%9C%A5%A5%A5%A5%B5%B5%B5%BD%B" +
				"D%BD%DE%DE%DE%FF%00%FF%FF%F7%EF%FF%FF%FF%FF%FF%FF%FF%FF%FFi%" +
				"9FN%B7%00%00%00%0CtRNS%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%00%1" +
				"2%DF%CE%CE%00%00%00yIDATx%DA%1D%CE1%0A%C30%0C%05%D0%3F%94%80" +
				"%25z%A0%0E%3DJ%C1%04%BA%E4%20%9D%DD%B5%93%A6%80E%0D%DAu%89t%" +
				"C9%60%C8%5D%EA%E8Oo%F8%F0%3F%1C%3E%02%875v%B7%CE%B8%7CN%D4%0" +
				"CK%02Pa%B8%D1%D1%85G%CB%D6%A65%B0%A8%3E%03%5D%BFk%20%A7%92%0" +
				"2%BF%FB%FB%16h%DBu%0FLBy%C0%A8%3Cf%12%06U%E8%0B%8B%C0%B2%03%" +
				"3E%8D%09%3Eo%98%FF%01%FD%00%3D%DD%89%40%B0%E9%00%00%00%00IEN" +
				"D%AEB%60%82";

		GM_addStyle(".OAN_Background { background-color: rgb(252, 250, 164); " +
				"margin-right: 10px; font-variant: small-caps; font-weight: bolder; }");

		// Find Navagation table.
		var xPathSearch = "//img[contains(@src, '/images/icons/16/bookmark_list.png')]";
		var ImgList = document.evaluate(
			xPathSearch,
			document,
			null,
			XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
			null);
		var ic = ImgList.snapshotLength;
		if (ic > 0) {

			// Move up the DOM until you find the link. Get cache GUID.
			var tblNav = ImgList.snapshotItem(0).parentNode;
			while (tblNav.nodeName != 'A') {
				tblNav = tblNav.parentNode;
			}

			CacheGUID = UrlParm('guid', true, tblNav.href);

			// Create Div for Owner Action & Kill Zone, if it doesn't yet exist.
			if (!document.getElementById('oakzDiv')) {
				var oakzDiv = document.createElement('div');
				oakzDiv.id = 'oakzDiv';
				var divBreadcrumbs = document.getElementById("ctl00_divBreadcrumbs");
				insertAfter(oakzDiv, divBreadcrumbs);
			} else {
				oakzDiv = document.getElementById('oakzDiv');
			}

			// Add links to div.
			var spanWarn = document.createElement('span');
			spanWarn.id = 'spanWarn';

			// Insert ahead of any other children.
			oakzDiv.insertBefore(spanWarn, oakzDiv.firstChild);
			var lnkWarn = document.createElement("A");
			lnkWarn.name = 'lnkWarn_oan';
			lnkWarn.title = 'Disable a Problem Cache';
			lnkWarn.setAttribute('class', 'OAN_Background');
			lnkWarn.href = 'http://' + domain + '/bookmarks/mark.aspx?guid=' +
					CacheGUID + '&WptTypeID=2&oan=y';
			spanWarn.appendChild(lnkWarn);
			var imgWarn = document.createElement("IMG");
			imgWarn.border = '0';
			imgWarn.align = 'absmiddle';
			imgWarn.src = imgsrcWarn;
			lnkWarn.appendChild(imgWarn);
			lnkWarn.appendChild(document.createTextNode(' Warn Problem Cache'));
			lnkWarn.addEventListener('click', Warn_clicked, false);

			var lnkKill = document.createElement("A");
			lnkKill.title = 'Archive a Problem Cache';
			lnkKill.setAttribute('class', 'OAN_Background');
			lnkKill.href = 'http://' + domain + '/seek/log.aspx?wid=' +
					CacheGUID + '&LogType=5&oan=y';
			spanWarn.appendChild(lnkKill);
			var imgKill = document.createElement("IMG");
			imgKill.border = '0';
			imgKill.align = 'absmiddle';
			imgKill.src = imgsrcKill;
			lnkKill.appendChild(imgKill);
			lnkKill.appendChild(document.createTextNode(' Kill Problem Cache'));

			// Add duplicate links at start of logs.
			var oakzDiv2 = document.getElementById('oakzDiv2');
			if (!oakzDiv2) {
				var oakzDiv2 = oakzDiv.cloneNode(false);
				oakzDiv2.id = 'oakzDiv2';
				var logsTable = document.evaluate(
				"//table[contains(@class, 'LogsTable')]",
				document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null
				).singleNodeValue;
				if (logsTable) {
					insertAheadOf(oakzDiv2, logsTable);
				}
			}

			var spanWarn2 = spanWarn.cloneNode(true);
			oakzDiv2.insertBefore(spanWarn2, oakzDiv2.firstChild);
			var lnkWarn2 = document.getElementsByName('lnkWarn_oan')[1];
			lnkWarn2.addEventListener('click', Warn_clicked, false);
		}
	} else {
		// If not in launched from a Owner Action Warning link, exit.
		if (UrlParm('oan') != 'y') { return; }

		// If log published (check for Edit button), then close window.
		if (PageUrl.search(/seek\/log\.aspx/gi)>=0) {
			if (document.getElementById("ctl00_ContentBody_LogBookPanel1_lnkBtnEdit") != null) {
				if (OAN_Auto_Close_Tabs) {
					window.close();
				}
				return;
			}
		}

		// Only apply changes if signed on as a reviewer.
		if (document.getElementById("ctl00_ContentBody_LogBookPanel1_chkCannotDelete") != null) {
			// Get LogType.
			var LogType = UrlParm('LogType');

			// Function to click the log submit button.
			ClickLogSubmitButton = function() {
				var e_LogBookPanel1_LogButton = document.getElementById("ctl00_ContentBody_LogBookPanel1_btnSubmitLog");
				e_LogBookPanel1_LogButton.click();
			}

			// Get current note text, if any.
			var e_LogBookPanel1_tbLogInfo = document.getElementById("ctl00_ContentBody_LogBookPanel1_uxLogInfo");

			var NoteText = e_LogBookPanel1_tbLogInfo.value;

			// Default 'Cannot Delete' box to checked.
			var e_chkCannotDelete = document.getElementById("ctl00_ContentBody_LogBookPanel1_chkCannotDelete");
			if (e_chkCannotDelete) {
				e_chkCannotDelete.checked=true;
			}

			var subButtons = document.getElementsByName("ctl00$ContentBody$LogBookPanel1$btnSubmitLog");
			for each (subButton in subButtons) {
				subButton.disabled = false;
			}

			// If Archive note.
			if (LogType == '5') {
				if (NoteText == "") {
					e_LogBookPanel1_tbLogInfo.focus;
					e_LogBookPanel1_tbLogInfo.value = ArcNote.replace(/%D%/g, OAN_Days_to_Respond);

					// Start timer function to click Submit button.
					TimeOutID = window.setTimeout(ClickLogSubmitButton, 250);
				}
			}

			// If Disable note.
			if (LogType == '22') {
				if (NoteText == "") {
					var wid = UrlParm('wid', true);
					var oanTag = '\n\n{#' + calcMD5('OAN' + wid) + '}';
					e_LogBookPanel1_tbLogInfo.focus;
					e_LogBookPanel1_tbLogInfo.value = RevNote.replace(/%D%/g, OAN_Days_to_Respond) +
							oanTag;

					// Start timer function to click Submit button.
					TimeOutID = window.setTimeout(ClickLogSubmitButton, 250);
				}
			}
		}

		// If adding to a bookmark, default to the warning bookmark.
		if (PageUrl.search(/bookmarks\/mark\.aspx/gi)>=0) {
			// If bookmark already submitted, close window.
			if (!document.getElementById("ctl00_ContentBody_Bookmark_btnCreate")) {
				if (OAN_Auto_Close_Tabs) {
					window.close();
				}
				return;
			}

			// Function to click the bookmark submit button.
			ClickBookmarkSubmitButton = function() {
				var e_Bookmark_btnSubmit = document.getElementById("ctl00_ContentBody_Bookmark_btnCreate");
				if (e_Bookmark_btnSubmit) {
					e_Bookmark_btnSubmit.click();
				}
			}

			var e_BookmarkList = document.getElementById("ctl00_ContentBody_Bookmark_ddBookmarkList");
			if (e_BookmarkList) {
				e_BookmarkName = document.getElementById("ctl00_ContentBody_Bookmark_tbName");
				if (e_BookmarkName) {
					// Append current date to cache name.
					var IncDays = OAN_Days_to_Respond;
					var d = new Date();
					d.setDate(d.getDate() + IncDays)
					var curr_date = d.getDate(d) + '';
					if (curr_date.length < 2) {curr_date = '0' + curr_date}
					var curr_month = d.getMonth(d) + 1;	// Month is zero indexed.
					curr_month += '';
					if (curr_month.length < 2) {curr_month = '0' + curr_month}
					var curr_year = d.getFullYear(d) + '';
					var NewDate = curr_year  + "-" + curr_month + "-" + curr_date;

					// Insert date before cache name.
					e_BookmarkName.value = NewDate + ' . . . ' + e_BookmarkName.value;
				}

				// Select bookmark, and submit.
				for each (op in e_BookmarkList.options) {
					if (op.value == OAN_BookMark) {
						op.selected = true;
						// Start timer function to click Submit button.
						TimeOutID = window.setTimeout(ClickBookmarkSubmitButton, 250);
						break;
					}
				}
			}
		}
	}

//****************************************************************************************//
//                                                                                        //
//                         Functions                                                      //
//                                                                                        //
//****************************************************************************************//

	// Bookmark this cache, in a separate tab.
	function Warn_clicked() {
		GM_openInTab('http://' + domain + '/seek/log.aspx?wid=' +
				CacheGUID + '&LogType=22&oan=y')
	}

	// Remove element and everything below it.
	function removeNode(element) {
		element.parentNode.removeChild(element);
	}

	// Show Edit Settings box.
	function fEditSettings() {
		// If not on bookmark page.
		var ddBookmarkList = document.getElementById("ctl00_ContentBody_Bookmark_ddBookmarkList");
		if (!ddBookmarkList) {
			alert("You need to be on a bookmark entry page\n" +
					"to edit this script's settings.\n\n" +
					"Use the 'bookmark listing' link from the cache page.");
			return;
		}

		// If div already exists, reposition browser, and show alert.
		var divSet = document.getElementById("gm_divSet");
		if (divSet) {
			var YOffsetVal = parseInt(divSet.getAttribute('YOffsetVal', '0'));
			window.scrollTo(window.pageXOffset, YOffsetVal);
			alert('Edit Setting interface already on screen.');
			return;
		}

		// Create div.
		divSet = document.createElement('div');
		divSet.id = 'gm_divSet';
		divSet.setAttribute('style', 'position: absolute; ' +
				'padding: 5px; outline: 6px ridge blue; background: #FFFFCC;');
		var popwidth = parseInt(window.innerWidth * .5);
		divSet.style.width = popwidth + 'px';

		// Create heading.
		var ds_Heading = document.createElement('span');
		ds_Heading.style.fontSize = 'medium';
		ds_Heading.style.fontStyle = 'italic';
		ds_Heading.style.fontWeight = 'bold';
		ds_Heading.appendChild(document.createTextNode('Owner Action Needed - Script Settings'));
		divSet.appendChild(ds_Heading);

		// Create duration selector.
		var ds_DaysP = document.createElement('p');
		ds_DaysP.style.textAlign = 'left';
		ds_DaysP.style.marginLeft = '6px';
		ds_DaysP.appendChild(document.createTextNode("Warning Duration in Days: "));
		var ds_DaysSel = document.createElement("select");
		ds_DaysSel.id = 'ds_DaysSel';
		for (var i = 1; i <= 90; i++) {
			var ds_DaysOpt = document.createElement("option");
			ds_DaysOpt.value = i;
			ds_DaysOpt.id = 'ds_DaysOpt' + i;
			ds_DaysOpt.text = i.toString();
			if (i == OAN_Days_to_Respond) { ds_DaysOpt.selected = true; }
			ds_DaysSel.appendChild(ds_DaysOpt);
		}

		ds_DaysP.appendChild(ds_DaysSel);
		ds_DaysP.appendChild(document.createTextNode(" Use %D% to insert value into text."));
		divSet.appendChild(ds_DaysP);

		// Create warning text area.
		var ds_WarnTextAreaP = document.createElement("p");
		ds_WarnTextAreaP.style.textAlign = 'left';
		ds_WarnTextAreaP.style.marginLeft = '6px';
		ds_WarnTextAreaP.appendChild(document.createTextNode("Enter warning text for Disable log:"));
		ds_WarnTextAreaP.appendChild(document.createElement('br'));
		var ds_WarnTextArea = document.createElement("textarea");
		ds_WarnTextArea.id = 'ds_WarnTextArea';
		ds_WarnTextArea.style.width = popwidth - 16 + 'px';
		ds_WarnTextArea.style.height = '80px';
		ds_WarnTextAreaP.appendChild(ds_WarnTextArea);
		divSet.appendChild(ds_WarnTextAreaP);
		ds_WarnTextArea.value = RevNote;

		// Create archive text area.
		var ds_ArchiveTextAreaP = document.createElement("p");
		ds_ArchiveTextAreaP.style.textAlign = 'left';
		ds_ArchiveTextAreaP.style.marginLeft = '6px';
		ds_ArchiveTextAreaP.appendChild(document.createTextNode("Enter Archive log text:"));
		ds_ArchiveTextAreaP.appendChild(document.createElement('br'));
		var ds_ArchiveTextArea = ds_WarnTextArea.cloneNode(true);
		ds_ArchiveTextArea.id = 'ds_ArchiveTextArea';
		ds_ArchiveTextAreaP.appendChild(ds_ArchiveTextArea);
		divSet.appendChild(ds_ArchiveTextAreaP);
		ds_ArchiveTextArea.value = ArcNote;

		// Add bookmark selector to page.
		var ds_BookmarkListP = document.createElement("p");
		ds_BookmarkListP.style.textAlign = 'left';
		ds_BookmarkListP.style.marginLeft = '6px';
		ds_BookmarkListP.appendChild(document.createTextNode("Select Bookmark List to use:"));
		ds_BookmarkListP.appendChild(document.createElement('br'));
		var ds_BookmarkList = ddBookmarkList.cloneNode(true);
		ds_BookmarkList.id = "ds_BookmarkList";
		ds_BookmarkList.name = "ds_BookmarkList";
		ds_BookmarkList.setAttribute('style', '');
		for each (op in ds_BookmarkList.options) {
			if (op.value == OAN_BookMark) { op.selected = true; }
		}

		ds_BookmarkListP.appendChild(ds_BookmarkList);
		divSet.appendChild(ds_BookmarkListP);

		// Create auto-close checkbox.
		var ds_AutoCloseP = document.createElement("p");
		ds_AutoCloseP.style.textAlign = 'left';
		ds_AutoCloseP.style.marginLeft = '6px';
		var ds_AutoCloseLabel = document.createElement("label");
		ds_AutoCloseLabel.style.fontWeight = 'normal';
		ds_AutoCloseLabel.setAttribute('for', 'ds_AutoCloseCB_OAN');
		ds_AutoCloseLabel.appendChild(document.createTextNode("Auto Close Tabs on Completion:"));
		ds_AutoCloseP.appendChild(ds_AutoCloseLabel);

		var ds_AutoCloseCB = document.createElement("input");
		ds_AutoCloseCB.id = "ds_AutoCloseCB_OAN";
		ds_AutoCloseCB.name = "ds_AutoCloseCB";
		ds_AutoCloseCB.type = "Checkbox";
		ds_AutoCloseCB.style.marginLeft = '6px';
		ds_AutoCloseCB.checked = OAN_Auto_Close_Tabs;
		ds_AutoCloseP.appendChild(ds_AutoCloseCB);

		var ds_AutoCloseInfo = document.createElement("a");
		ds_AutoCloseInfo.style.fontSize = 'x-small';
		ds_AutoCloseInfo.style.marginLeft = '9px';
		ds_AutoCloseInfo.href = 'https://sites.google.com/site/allowscriptstoclosetabs/';
		ds_AutoCloseInfo.title = 'Click to view instructions in a new tab.';
		ds_AutoCloseInfo.target = '_blank';
		ds_AutoCloseInfo.appendChild(document.createTextNode("(Requires configuration change)"));
		ds_AutoCloseP.appendChild(ds_AutoCloseInfo);

		divSet.appendChild(ds_AutoCloseP);

		// Create buttons.
		divSet.appendChild(document.createElement('p'));
		var ds_ButtonsP = document.createElement('p');
		ds_ButtonsP.style.textAlign = 'right';
		var ds_SaveButton = document.createElement('button');
		ds_SaveButton = document.createElement('button');
		ds_SaveButton.appendChild(document.createTextNode("Save"));
		ds_SaveButton.addEventListener("click", fSaveButtonClicked, true);
		var ds_CancelButton = document.createElement('button');
		ds_CancelButton.style.marginLeft = '6px';
		ds_CancelButton.addEventListener("click", fCancelButtonClicked, true);
		ds_CancelButton.appendChild(document.createTextNode("Cancel"));
		ds_ButtonsP.appendChild(ds_SaveButton);
		ds_ButtonsP.appendChild(ds_CancelButton);
		divSet.appendChild(ds_ButtonsP);

		// Add div to page.
		var toppos =  parseInt(window.pageYOffset +  100);
		var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
		divSet.style.top = toppos + 'px';
		divSet.style.left = leftpos + 'px';
		divSet.setAttribute('YOffsetVal', window.pageYOffset);
		document.body.appendChild(divSet);

		// Save values.
		function fSaveButtonClicked() {
			OAN_Days_to_Respond = ds_DaysSel.options[ds_DaysSel.selectedIndex].value;
			GM_setValue("OAN_Days_to_Respond", OAN_Days_to_Respond);
			RevNote = ds_WarnTextArea.value;
			GM_setValue("OAN_RevNote", RevNote);
			ArcNote = ds_ArchiveTextArea.value;
			GM_setValue("OAN_ArcNote", ArcNote);
			OAN_BookMark = ds_BookmarkList.options[ds_BookmarkList.selectedIndex].value;
			GM_setValue("OAN_BookMark", OAN_BookMark);
			OAN_Auto_Close_Tabs = ds_AutoCloseCB.checked;
			GM_setValue("OAN_Auto_Close_Tabs", OAN_Auto_Close_Tabs);
			alert('Settings have been saved.');
			removeNode(divSet);
		}

		// Cancel requeste.
		function fCancelButtonClicked() {
			var resp = confirm('Cancel requested. You will lose any changes.\n\n' +
					'Press OK to exit without saving changes. Otherwise, press Cancel.');
			if (resp) {
				removeNode(divSet);
			}
		}
	}

	// Returns a URL parameter.
	//  ParmName - Parameter name to look for.
	//  IgnoreCase - (optional) *false, true. Ignore parmeter name case.
	//  UrlString - (optional) String to search. If omitted, document URL is used.
	function UrlParm(ParmName, IgnoreCase, UrlString) {
		var RegRslt, sc = '', RtnVal = '';
		if (IgnoreCase) {sc = 'i'}
		if(UrlString) {
			var PageUrl = UrlString;
		} else {
			PageUrl = document.location + '';
		}

		var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
		var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
		RegRslt = RegEx1.exec(ParmString);
		if (RegRslt) {RtnVal = RegRslt[2]}

		return RtnVal;
	}

	// Returns raw current domain ('www.geocaching.com' for example).
	function thisDomain() {
		var wDomain = document.location.href + '';
		var RegEx1 = new RegExp('^.*?:\/\/(.*?\..*?)($|\/)');
		RegRslt = RegEx1.exec(wDomain);
		wDomain = RegRslt[1];

		return wDomain;
	}

	// Insert element after an existing element.
	function insertAfter(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement.nextSibling);
	}

	// Insert element aheadd of an existing element.
	function insertAheadOf(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement);
	}
