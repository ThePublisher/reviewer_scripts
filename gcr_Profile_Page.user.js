// ==UserScript==
// @name		   GCR Profile Page v2
// @description	   Reviewer Enhancements For Profile Page.
// @namespace      http://www.geocaching.com/admin
// @version    	   02.01
// @icon           http://i.imgur.com/z2OLuP3.png
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Profile_Page.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Profile_Page.user.js
// @include        http*://*.geocaching.com/profile/*
// @grant          GM_xmlhttpRequest
// ==/UserScript==

/*

Function:
	Adds links to view all hide and finds.

*/

	var ImgSrcOwned =
		"data:image/gif,GIF89a%0F%00%0B%00%B3%00%00%00%00%00%08%08%08%0C%" +
		"10%0C%25%2B%1E%2BB%22Ls4%60g%5Br~m%FF%00%FF%84%84%88%A4%C2%91%C1" +
		"%C0%C1%E2%E4%E9%EC%EF%F1%F3%F7%FB%FF%FF%FF!%F9%04%01%00%00%08%00" +
		"%2C%00%00%00%00%0F%00%0B%00%40%04Y%10I%C9Z%028%03%0A%9E%7F%C7b%1" +
		"8%0D31%D7Q%18j%B1%3C%CCF1%C7p%10x%BD%C8%0D%86%0B%01%85%E31r%9C4%" +
		"80Di%C2d%D0%08%8Aht%20s%26%A0%85l%01%BA%08Hz%02Ca%20%200%86%84%C" +
		"3%17%B08%24%16%84%04%91dB%C4%18%9FK%C2)%89%00%00%3B";

	var ImgSrcFound =
		"data:image/gif,GIF89a%0D%00%0B%00%B3%00%00%00%00%00%BD%AD%00%C6%" +
		"AD%00%FF%00%FF%FF%E7%00%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%F" +
		"F%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF%FF!%F9%0" +
		"4%01%00%00%03%00%2C%00%00%00%00%0D%00%0B%00%40%04%2FpH%09j%9D%13" +
		"%90%CD%09%C8%9B%A0%01%C2%F6Q%1Dw%0E%9A%A7%9A%18k%AD%18%10p%01m%1" +
		"3%A2W%E6%A8%94%8Ar%F3T6%C0L%91%90%8C%00%00%3B";

	// Enhancement switches.
	// Set value to 0 (zero) to turn off, or 1 (one) to turn on.
	var Switch_OwnerNotes = 0;	// Add Owner Notes

	// Declare global variables.
	var UserGuid;
	var e_UserGuid;
	var e_MemberName;
	var CacheNoteTb;
	var UserName;

	// Get User GUID and Name.
	var e_HomeLink = document.getElementById("ctl00_ContentBody_ProfilePanel1_lblMemberName");
	UserName = e_HomeLink.firstChild.data;
	UserName = UserName.replace(/<\&amp;>/g, '&');

	var emailLink = document.getElementById("ctl00_ContentBody_ProfilePanel1_lnkEmailUser");
	if (emailLink) {
		UserGuid = UrlParm('guid', true, e_HomeLink.href);
	}

	// Find user name. Add All Finds and All Hides links.
	e_UserProfile = document.getElementById("ctl00_ContentBody_lblUserProfile");
	if (e_UserProfile) {
		var LinkImgFinds = document.createElement('img');
		LinkImgFinds.src = 'http://www.geocaching.com/images/icons/icon_smile.gif';
		LinkImgFinds.height = 16;
		LinkImgFinds.width = 16;
		LinkImgFinds.border = 0;
		LinkImgFinds.style.marginLeft = '12px';
		LinkImgFinds.title = 'Show all caches found by ' + UserName;
		var LinkFinds = document.createElement('a');
		LinkFinds.href = 'http://www.geocaching.com/seek/nearest.aspx?ul=' + encodeURIComponent(UserName);
		LinkFinds.appendChild(LinkImgFinds);

		var LinkImgOwned = document.createElement('img');
		LinkImgOwned.src = 'http://www.geocaching.com/images/WptTypes/sm/2.gif';
		LinkImgOwned.height = 16;
		LinkImgOwned.width = 16;
		LinkImgOwned.border = 0;
		LinkImgOwned.style.marginLeft = '8px';
		LinkImgOwned.title = 'Show all caches owned by ' + UserName;
		var LinkOwned = document.createElement('a');
		LinkOwned.href = 'http://www.geocaching.com/seek/nearest.aspx?u=' + encodeURIComponent(UserName);
		LinkOwned.appendChild(LinkImgOwned);

		e_UserProfile.appendChild(LinkFinds);
		e_UserProfile.appendChild(LinkOwned);
	}

//****************************************************************************************//
//                                                                                        //
//                         Functions                                                      //
//                                                                                        //
//****************************************************************************************//

	// Refresh Cache Note Data.
	function RefreshCacheNoteData() {
		GM_xmlhttpRequest( {
			method:"GET",
			url:cnReqUrl,
			onload:function(result) {
				NewCacheNote = result.responseText;
				gmRCN = document.getElementById('cnTabCol1');
				// Update cache note text.
				if ((gmRCN != null) && (NewCacheNote.substring(0,1) == "_")) {
					UpdateCacheNoteTable(gmRCN, NewCacheNote.substring(1));
				// Create new cache note on page.
				} else if ((gmRCN == null) && (NewCacheNote.substring(0,1) == "_")) {
					CreateCacheNoteTable(NewCacheNote.substring(1));
				// Remove cache note from page.
				} else if ((gmRCN != null) && (NewCacheNote == "%")) {
					DestroyCacheNoteTable();
				}
			}
		});
	}

	// Create Cache Note Table.
	function CreateCacheNoteTable(ccntText) {
		CacheNoteTb = document.createElement("table");
		CacheNoteTb.id = 'CacheNoteTb';
		CacheNoteTb.style.border = 'solid 2px rgb(241, 241, 241)';
		CacheNoteTb.style.textAlign = 'left';
		CacheNoteTb.style.marginTop = '6';
		CacheNoteTb.style.marginBottom = '6';
		CacheNoteTb.style.marginRight = '4';
		CacheNoteTb.style.MozBorderRadius = '15px';
		CacheNoteTb.style.fontSize = 'x-small';
		CacheNoteTb.style.backgroundColor = 'rgb(255, 255, 160)';
		var cnTabRow1 = CacheNoteTb.insertRow(0);
		var cnTabCol1 = cnTabRow1.insertCell(0);
		cnTabCol1.id = 'cnTabCol1';
		cnTabCol1.innerHTML = '<font color = red><b>Note:' +
				'</b></font> ' + ccntText;
		EditNoteLink.parentNode.insertBefore(CacheNoteTb, EditNoteLink.nextSibling);
	}

	// Update Cache Note Table.
	function UpdateCacheNoteTable(ucntDomElem, ucntText) {
		ucntDomElem.innerHTML = '<font color = red><b>Note:' +
				'</b></font> ' + ccntText;
	}

	// Destroy Cache Note Table.
	function DestroyCacheNoteTable(ccntText) {
		var e_CacheNoteTable = document.getElementById('CacheNoteTb');
		if (e_CacheNoteTable) {
		    e_CacheNoteTable.parentNode.removeChild(e_CacheNoteTable);
		}
	}

	// Returns a URL parameter.
	//  ParmName - Parameter name to look for.
	//  IgnoreCase - (optional) *false, true. Ignore parmeter name case.
	//  UrlString - (optional) String to search. If omitted, document URL is used.
	function UrlParm(ParmName, IgnoreCase, UrlString) {
		var RegRslt, sc = '', RtnVal = '';
		if (IgnoreCase) {sc = 'i'}
		if(UrlString) {
			var PageUrl = UrlString;
		} else {
			PageUrl = document.location + '';
		}

		var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
		var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
		RegRslt = RegEx1.exec(ParmString);
		if (RegRslt) {RtnVal = RegRslt[2]}

		return RtnVal;
	}
