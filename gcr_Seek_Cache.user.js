// ==UserScript==
// @name           GCR Seek Cache v2
// @description    Seek Results Page Enhancements for Reviewers
// @namespace      http://www.geocaching.com/admin
// @version        02.01
// @icon           http://i.imgur.com/a94PYHJ.png
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Seek_Cache.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Seek_Cache.user.js
// @include        http*://*.geocaching.com/seek/nearest.aspx?*
// @grant          GM_addStyle
// ==/UserScript==

/*

Function:
 Seek Results Page Enhancements for Reviewers.

*/

	// Set style for review links.
	GM_addStyle(".gcrseekrp {font-size: 110%; font-variant: small-caps; font-weight: bold !important}");

	// Regular expression patterns.
	var RegEx1 = new RegExp('\\b(GC[0-9A-HJKMNP-RTV-Z]{2,6})\\b', 'g');
	var rslt = '<span class="gcrseekrp"><a href="/admin/review.aspx?wp=$1&nc=12" ' +
			'target="_blank">$1</a></span>';

	// Get list of text strings with 'small' class. Replace GC codes with link code.
	var txts = document.getElementsByClassName('small');
	var ic = txts.length;
	for (var i = 0; i < ic; i++) {
		txts[i].innerHTML = txts[i].innerHTML.replace(RegEx1, rslt);
	}
