﻿// ==UserScript==
// @name           GCR Timed Publishing 1 Review Page v2
// @description    Timed Publishing 1 - Review Page
// @namespace      http://www.geocaching.com/admin
// @version        02.01
// @icon           http://i.imgur.com/GP6D2vX.png
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Timed_Publishing_1_Review.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Timed_Publishing_1_Review.user.js
// @include        http*://*.geocaching.com/admin/review.aspx?*
// @grant          GM_info
// @grant          GM_getValue
// @grant          GM_openInTab
// @grant          GM_setValue
// @grant          GM_xmlhttpRequest
// @grant          GM_addStyle
// @grant          GM_listValues
// @grant          GM_registerMenuCommand
// @grant          GM_setClipboard
// ==/UserScript==

/*

Function:
 Timed Publishing 1 - Review Page
 */

	// Constants:
	// Local Storage key for JSON tracking data.
	const TIMED_PUBLISHING_TRACKING_KEY = 'c8a7a86e-ced2-4667-b01b-cb4698a7e06a';

	// Key for user last date.
	const JSON_DATES_LS_KEY 	        = '2118dd9a-2785-47b6-bc84-4ef54b330358';

	// Key for prefs across scripts.
	const JSON_TIMED_PUBLISH_PREFS_KEY 	= '9646db26-9d83-4743-b589-1de0d67bd352';

	// Key for Google Time Zone API.
	const TZ_API_KEY                    = 'AIzaSyB8c4ilf8Ac3K4-Y-9dhsksx5EnXOECIeI';

	var srcImgClock =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AABGdBTUEAAK%2FINwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZT" +
		"wAAAMESURBVDjLXZNrSFNxGMYPgQQRfYv6EgR9kCgKohtFgRAVQUHQh24GQReqhViWlVYb" +
		"ZJlZmZmombfVpJXTdHa3reM8uszmWpqnmQuX5drmLsdjenR7ev9DR3Xgd3h43%2Bd5%2Fp" +
		"w%2FHA4AN9zITSPUhJ14R0xn87%2Bh2ZzJvZVInJpzAQOXQOQMt%2B%2F5rvhMCLXv9Vjr" +
		"t1rSXitmwj%2BJua1%2BOx%2B2HfGNdGf6yW8l5sUKPNVcRsiaPDA22Ahv6%2F7Ae%2F0a" +
		"KdviQ0G7B%2Fc6f8Zg%2Bgbfh079Mjno0MhS58lflOsgEjh3BXc%2BbM%2F0DzbvDwj314" +
		"znt%2Fbjof0HdPw3FBq6kP%2BoCxVNfdDZvqPsrQmf6zdFRtyPJgbrFoqUTeS%2BFnPrek" +
		"pmiC2lS%2BQcUx%2Bqrf0wmFzodYfgC0nwhoYh9oegfdmLsmYXHj7JhV23erS7ZNYHyibG" +
		"LiLtXsO19BoHSiwu6Ok09gwFg%2Fgy8BO%2FSTOkKFBk7EWh2YkLeh5Hy4Ws2B2w157iDv" +
		"Opxw4UPRPRTSfL41FIsow7ZeXwUFF4dBQ1L96A%2FxLEFf1HMC%2FLxAt25PH%2BVN0HXH" +
		"1gh2dEwdBoBGO0OKvW4L7hCdIvavBSsMIRVHCi0ArmZZl4wbYrz%2FyHSq1Ql9vQLylUEo" +
		"E7GMal3OuxMG%2F7CO848N6n4HheK5iXZeIFmy88Nu%2B8aYJG24G3ziB%2B0Ee7wwqeml" +
		"vQ5w9hcAJwyUDtpwBOFLeBeVkmXpB0qlK9RV2HlLsCsvUivHRhQwoQjhCkA1TgJX1OK0JV" +
		"zIN5WSZesPZ44XKia%2BP5BqSS4aq%2BBzZXABLdhyQrsJPOqv4MVcEbMA%2Fzsky8gLHy" +
		"YO7hI9laecOZWuzLfYXU2zzSblmQerMZqjwTknOeY9dlIw5kVcrMG%2F8XpoQgCEkOhwNN" +
		"Jn5i7bFSrFDpsCrFEIPpLacr0WxpibYIQpS86%2F8pMBqNswnJ6XSivqHBv3R3pmbxzgwz" +
		"4Z%2BEaTXtwqIogrzjxIJ4QVVV1UyihxgjFv3%2FK09Bu%2FlEkBgg5rLZH%2BfT5dvfn7" +
		"iFAAAAAElFTkSuQmCC";

	var srcImg30Min =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAANCAYAAABCZ%2FVdAA" +
		"AAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgA" +
		"AHUwAADqYAAAOpgAABdwnLpRPAAAABp0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjUuMT" +
		"Aw9HKhAAAAmUlEQVQ4T8VTAQqAQAjrE72id%2FSTvtObq8UN1thxQkFClDqn6Jqmn2y9%2" +
		"Bh7h0XEqmNL4W2uEN4zE9BFzTIkYoL2Rg1SJ6CdMmZwrWloFm9FH2DE3dJZE6pbyJELOOR" +
		"iLk%2FcOpUX4dpIUG67Gj4dVcHIWE4N1PcyP5fmeUpToc6WoDEcDdtfTU4rKMCrFGavHTH" +
		"%2BvynJ4yFeAE1ONTAJ0L31XAAAAAElFTkSuQmCC";

	var srcImgBookmark =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AABGdBTUEAAK%2FINwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZT" +
		"wAAAIASURBVDjLpVPPaxNREJ6Vt01caH4oWk1T0ZKlGIo9RG%2BBUsEK4kEP%2FQ8qPXnp" +
		"qRdPBf8A8Wahhx7FQ0GF9FJ6UksqwfTSBDGyB5HkkphC9tfb7jfbtyQQTx142byZ75v5Zn" +
		"ZWC4KALmICPy%2B2DkvKIX2f%2FPOz83LxCL7nrz%2BWPNcll49DrhM9v7xdO9JW330DuX" +
		"rrqkFSgig5iR2Cfv3t3gNxOnv5BwU%2BeZ5HuON5%2FPMPJZKJ%2ByKQfpW0S7TxdC6WJa" +
		"Wkyvff1LDaFRAeLZj05MHsiPTS6hua0PUqtwC5sHq9zv9RYWl%2Bnu5cETcnJ1M0M5WlWq" +
		"3GsX6%2FT%2BVymRzHDluZiGYAAsw0TQahV8uyyGq1qFgskm0bHIO%2F1%2Bsx1rFtchJh" +
		"ArwEyIQ1Gg2WD2A6nWawHQJVDIWgIJfLhQowTIeE9D0mKAU8qPC0220afsWFQoH93W6X7y" +
		"CDJ%2BDEBeBmsxnPIJVKxWQVUwry%2BXyUwBlKMKwA8jqdDhOVCqVAzQDVvXAXhOdGBFgy" +
		"mYwrGoZBmUyGjxCCdF0fSahaFdgoTHRxfTveMCXvWfkuE3Y%2Bf40qhgT%2FnMitupzApd" +
		"vT18bu%2BYeDQwY9Xl4aG9%2Fd%2FURiMBhQq%2FdvZMeVghtT17lSZW9%2FrAKsvPa%2F" +
		"r9Fc2dw%2BPe0%2FxI6kM9mT5vtXy%2BNw2kU%2F5zOGRpvuMIu0YAAAAABJRU5ErkJggg" +
		"%3D%3D";

	var srcMinusIcon =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAvRJREFUeNp8VF1IU2EY" +
		"frazOXRrFanHv23iQmf%2BhGKKpck0RAL1yoJw3ZT9kN0U3irYZV2K913kQM3pTUEQpFg2" +
		"ndQsaMxOOmTTUNP9%2F3S29X6nIhPqg4dzzve9z%2FP%2BfkfW3d0NmUwGuVwuPQk5hHIA" +
		"PEGLnytA%2BJpOpz8RtglIpVJgTwX%2BLBWh3mAwnOvp6blSUlJSJYqidKBQKCAIwoeJiY" +
		"kxj8fzmrYWCXF2xplMJuaVkds7Ozt7LRbLfdHp5LfGx7E5OoqdqSkEBQEatZpvvXzpAkXK" +
		"ud1uRvYQklxZWRkLpamrq6vXbDZfXRsagnJ%2BHgbyasjPR2F2NjSBAPxzc9hyLKPK0nua" +
		"4ziRRLaZiDyZTPJ6vb65ra3VIgwNIo%2BM83ge4XAYOzs7EiKRCHjay97dxfrwMFpaWiyF" +
		"hYXNjMsZjcaGvr7rA2GHIzdr4S0yVSrskmEsFvsLoVAISqUSic0tRI5qoaut5e32xWUFqR" +
		"TodLoK95MxFGs08Hq9UuG%2BJxKS5whFkqD33wUtKCoClhzIv3WzgnGZwLFgMISo0wkxJw" +
		"eNs7Pw%2BXzY398H5Qq1Wg0NCWdkZEgCr2pqkCbbaDQGxlWQMqsDROpr1O%2BHy%2BWSvL" +
		"FwD5OltlE0MbKVOMRlAkGOU0BZUYmYYwkesxn%2FW1kkxmwBORMIMu%2B%2B1dXPLk1dPf" +
		"aoWHoqYgHL9R%2BIUlpZtWewtrbuYlwmsGK1Wm3ZTS2IV1bDTwZHaKw1ZHwYQUorRY6ONz" +
		"ZhZsZmY1wuNzc3tLf3TQnIMs%2FfuXfqy8cVbG94oGbhSoESkbBKCDecRfmDR5iennxqt7" +
		"%2Bx0QC%2BZwLsYnidzndyqo28Y2DQhJOl8HBKLHs34FYoIbZ2oODabZTeuAur9fH05OSY" +
		"jUb6OWmKsurqaulmBQKBzHg83m40lrb19w9crKtrNB4snsOxIIyMPHwmCO6XKpXqhVarjU" +
		"o3%2BICANL40NDr6biBOMeHEL%2F4uYZ0IdmrpBmsvCUi%2FgB8CDACXUnTearO2hAAAAA" +
		"BJRU5ErkJggg%3D%3D";

	var srcPlusIcon =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
		"AAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAx9JREFUeNpMlF9IU1Ec" +
		"x7%2B797rpBraY7aY2jVk6G2qQFGImtoqUVCgEC%2BdjT1FQRA9JPfXUo9pbQRDuwWqKVG" +
		"BUJEWUltioTLe5obktncnm3J%2Fu3e13rmUe%2BN7zu%2Becz%2Fece37nXE17ezs0Gg04" +
		"jlNr0g5SJQCRlI%2BNEiNFFEX5RloiIZvNgtUC%2Fhcd6WBpaWl9R0fHOavVWiVJktohCA" +
		"J8Pp9ncHBwIBgMvqWmD6Q06%2BNtNhublcEnWltbu5xO55W36XHRFXajN3QPg0sjmI7NQt" +
		"ALotNx9hitlJ%2BZmWFwkCTzFRUVbCmH29raupqamrovenswwXugmDmYxAIYC7YjlpvA%2" +
		"B%2FgkXobf4HxNdw3P8xKZLDETTpZlsaSkpMHhOOq8NHsdGZMEo9GIUCiEB7t6VYXDYbVt" +
		"3ZjGZf9NNDY2OouLixsYywzsZ86c7hyaf4JVfRwK2UajUaRSqc3NYfHKyooaL2l%2F4Wlk" +
		"FC0tzZ2MFehRZLFY7He%2Fu5BXqMfCwoI6MJ1OY3l5GclkEoFgQN11VnaKIj7RJx4prLMz" +
		"lhkY4%2FE1eFJfYc4WYdwxikgkgrW1NdUgJycHE47naiaYDowdg4f%2FSsYpMFagVLHPgE" +
		"wp%2B5Vchdfr3cgvDWbwP%2FCf0hyBkoHBYCwziPO8gEq%2BHB%2BTX1A336IuVSNr8Lnh" +
		"tQrte1UPhVc2zkQuj0qUU8QxgzibfXF21ju931ADOZZWz1%2FGlIG0TdqclcWsjUmb4VCj" +
		"r8bcXGCasczgs8vlcjeLJ2FJFUGb4KHo6ahqs5sGLJbzZOSuC9grWXHcdBzDw243YzlaRs" +
		"jv944NDT1%2B1F91B3t%2BWqDzKZA1GRT77BC%2FV6hxrh%2BwRa3otfdjZGT40dycf4yx" +
		"vNlsZin6MTU1yVGmuBunbtnE3wXQLtLt8QShCwBHlUZ07ezGtaoeuFz3hx4%2BHHDTkX5G" +
		"GyFpqqur1RzHYrE8yv2JsrJyx4ULV1tqa%2BvKtlw0TEy88%2FX13X7q88280Ol0o%2Fn5" +
		"%2BUn1Bm8xQCKRQCaTsdD7IWJ2k0x%2F%2BSgpQMB7rVY7bzAYQAbqL%2BCPAAMAVUOTme" +
		"%2B7FysAAAAASUVORK5CYII%3D";

	// Exit script if cache is already published.
	var bPublished = true;
	var selAction = document.getElementById('ctl00_ContentBody_ddAction');
	for (var i in selAction.options) {
		if (selAction.options[i].value == 'Approve') { bPublished = false; }
	}
	if (bPublished) { return; };

	// Get currently signed-on geocaching.com profile.
	var SignedInAs = document.getElementById("ctl00_LoginUrl");
	if (!SignedInAs) { SignedInAs = document.getElementById('ctl00_LogoutUrl'); }
	var SignedInAs = SignedInAs.parentNode.childNodes[1].firstChild.data.trim();
	SignedInAs = SignedInAs.replace(/<\&amp;>/g, '&');

	// Load preferences from local storage.
	var tpPrefs = new Object;
	fLoadPrefs();

	// Create styles.
	GM_addStyle(".off30min {border:solid 1px rgb(68,142,53); border-radius:5px; " +
			"-moz-border-radius:5px; background-color:rgb(170,170,170); padding:1px; }");
	GM_addStyle(".on30min {border:solid 1px rgb(68,142,53); border-radius:5px; " +
			"-moz-border-radius:5px; background-color:rgb(255, 255, 0); padding:1px; }");
	GM_addStyle(".click-icon {vertical-align: text-bottom; cursor: pointer; }");

	GM_addStyle("tr.outline_box:hover { outline: 5px ridge yellow !important; }");

	// Array of abbreviated month names.
	var arMths = new Array('January','February','March','April','May','June',
			'July','August','September','October','November','December');

	// Array of abbreviated day names.
	var arDays = new Array('Sun','Mon','Tue','Wed','Thu','Fri','Sat');

	// Get current year, month, and day.
	var d = new Date();
	var curYear = d.getFullYear(d);
	var curMonth = d.getMonth(d) + 1;
	var curDate = d.getDate(d);
	d.setDate(d.getDate() + 1);
	var dftYear = d.getFullYear(d);
	var dftMonth = d.getMonth(d) + 1;
	var dftDate = d.getDate(d);

	// Collect cache info.
	var CacheData     = document.getElementsByClassName('CacheData')[0];
	var cacheid       = CacheData.getAttribute('data-cacheid');
	var gccode        = CacheData.getAttribute('data-gccode');
	var cacheguid     = CacheData.getAttribute('data-cacheguid');
	var cachename     = CacheData.getAttribute('data-cachename');
	var ownerguid     = CacheData.getAttribute('data-ownerguid');
	var ownerusername = CacheData.getAttribute('data-ownerusername');
	var cachetypeid   = CacheData.getAttribute('data-cachetypeid');
	var latitude      = Number(CacheData.getAttribute('data-latitude'));
	var longitude     = Number(CacheData.getAttribute('data-longitude'));

	// Create controls
	var CtrlWidthSetting = GM_getValue('CtrlWidth_' + SignedInAs, 42);

	tpDT = document.createElement('dt');
	var imgClock = document.createElement('img');
	imgClock.src = srcImgClock;
	imgClock.title = 'Open Timed Publishing Settings';
	tpDT.appendChild(imgClock);

	tpDD = document.createElement('dd');
	selTpMth = document.createElement('select');
	selTpMth.style.width = CtrlWidthSetting + 'px';
	selTpMth.id = 'selTpMth';
	selTpMth.name = 'selTpCtrl';
	selTpMth.title = 'Select Month';
	tpDD.appendChild(selTpMth);

	selTpDay = document.createElement('select');
	selTpDay.style.width = CtrlWidthSetting + 'px';
	selTpDay.style.marginLeft = '6px';
	selTpDay.id = 'selTpDay';
	selTpDay.name = 'selTpCtrl';
	selTpDay.title = 'Select Day';
	tpDD.appendChild(selTpDay);

	selTpHour = document.createElement('select');
	selTpHour.style.width = CtrlWidthSetting + 'px';
	selTpHour.style.marginLeft = '6px';
	selTpHour.id = 'selTpHour';
	selTpHour.name = 'selTpCtrl';
	selTpHour.title = 'Select Hour';
	tpDD.appendChild(selTpHour);

	var img30Min = document.createElement('img');
	img30Min.src = srcImg30Min;
	img30Min.id = "img30Min";
	img30Min.title = 'Add 30 minutes.';
	img30Min.style.marginLeft = '6px';
	img30Min.classList.add('click-icon');
	img30Min.classList.add('off30min');
	tpDD.appendChild(img30Min);

	var imgBookmark = document.createElement('img');
	imgBookmark.src = srcImgBookmark;
	imgBookmark.id = 'imgBookmark';
	imgBookmark.title = 'Add to Timed Publishing\nBookmark List';
	imgBookmark.style.marginLeft = '10px';
	imgBookmark.classList.add('click-icon');
	tpDD.appendChild(imgBookmark);

	var lnkTimedPending = document.createElement('a');
	lnkTimedPending.id = 'lnkTimedPending';
	lnkTimedPending.title = 'Pending Timed Caches';
	lnkTimedPending.style.marginLeft = '10px';
	lnkTimedPending.style.fontWeight = 'bold';
	lnkTimedPending.style.color = 'green';
	lnkTimedPending.href = 'javascript:void(0)';
	lnkTimedPending.appendChild(document.createTextNode('0'));
	tpDD.appendChild(lnkTimedPending);
	lnkTimedPending.addEventListener('click', fTimedPendingClicked, false);

	// Load controls.
	fLoadMonths();
	fLoadDays();
	fLoadHours();

	// Add controls to page.
	var locSeq = GM_getValue('ControlLocSeq_' + SignedInAs, '2');
	var locNode = locSeq - 1;
	var widBody = document.getElementsByClassName('WidgetBody')[1];
	var widBodyDl = widBody.firstChild;
	while (widBodyDl.nodeName != 'DL') { widBodyDl = widBodyDl.nextSibling; }
	var dts = widBodyDl.getElementsByTagName('dt');
	if (locNode >= (dts.length - 1)) { locNode = (dts.length - 1); }
	insertAheadOf(tpDT, dts[locNode]);
	insertAfter(tpDD, tpDT);
	selTpMth.addEventListener('change', fLoadDays, false);
	imgClock.addEventListener('click', fSetOptions, false);
	imgBookmark.addEventListener('click', fBookmarkClicked, false);
	img30Min.addEventListener('click', fToggleMin, false);

	fUpdatePendingCounter();
	SetUserDateTime();
	window.addEventListener('storage', fStorageChanged, false);

	// Add dropdown width controls
	if (!GM_getValue('HideWidthControls_' + SignedInAs, false)) {
		var divHomeLink = document.getElementById('ctl00_HDHomeLink');
		if (divHomeLink) {
			divHomeLink = divHomeLink.parentNode;
			var spanSizeCtrl = document.createElement('span');
			spanSizeCtrl.style.marginLeft = '16px';
			divHomeLink.appendChild(spanSizeCtrl);

			var imgSizeCtrlMinus = document.createElement('img');
			imgSizeCtrlMinus.src = srcMinusIcon;
			imgSizeCtrlMinus.setAttribute('increment', -1);
			imgSizeCtrlMinus.classList.add('click-icon');
			imgSizeCtrlMinus.title = 'Decrease width of Timed Publishing controls.';
			spanSizeCtrl.appendChild(imgSizeCtrlMinus);

			var imgSizeCtrlPlus = document.createElement('img');
			imgSizeCtrlPlus.style.marginLeft = '7px';
			imgSizeCtrlPlus.src = srcPlusIcon;
			imgSizeCtrlPlus.setAttribute('increment', 1);
			imgSizeCtrlPlus.classList.add('click-icon');
			imgSizeCtrlPlus.title = 'Increase width of Timed Publishing controls.';
			spanSizeCtrl.appendChild(imgSizeCtrlPlus);

			imgSizeCtrlMinus.addEventListener('click', fAdjustCtrlSize, false);
			imgSizeCtrlPlus.addEventListener('click', fAdjustCtrlSize, false);
		}
	}

	GM_registerMenuCommand('Set Timed Publishing Options', fSetOptions);
	GM_registerMenuCommand('Dump Timed Publishing Local Storage', fDumpJSON);

//****************************************************************************************//
//                                                                                        //
//                         Functions                                                      //
//                                                                                        //
//****************************************************************************************//

	// If Local Storage changes.
	function fStorageChanged(e) {
		if (e.key == JSON_DATES_LS_KEY) {
			SetUserDateTime();
		} else if (e.key == TIMED_PUBLISHING_TRACKING_KEY) {
			var olddata = e.oldValue;
			var newdata = e.newValue;
			// alert(fIsNewlyAdded(gccode, olddata, newdata));
			if (fIsNewlyAdded(gccode, olddata, newdata)) {
				fHoldCache();
			}
			fUpdatePendingCounter();
		}
	}

	// Load prefs from local storage.
	function fLoadPrefs() {
		var rtnvar = localStorage.getItem(JSON_TIMED_PUBLISH_PREFS_KEY);
		if (rtnvar) {
			tpPrefs = JSON.parse(rtnvar);
		}
	}

	// Load months to selector.
	function fLoadMonths() {

		var dftMthNames = GM_getValue('MonthNames_' + SignedInAs, '');
		if (dftMthNames) {
			var arMonths = dftMthNames.split(',');
		} else {
			arMonths = arMths;
		}

		for (i = curMonth; i<=(curMonth + 4); i++) {
			var xOption = document.createElement('option');
			var j = (i - 1) % 12 + 1;
			xOption.value = pad(j, 2);
			xOption.setAttribute('year', curYear + Math.floor((i - 1) / 12));
			xOption.appendChild(document.createTextNode(pad(j, 2) + ' (' + arMonths[j-1].trim() + ')'));
			selTpMth.appendChild(xOption);
			if (j == curMonth) { xOption.selected = true; }
		}
	}

	// Load days, based on year and month selected.
	function fLoadDays() {

		var dftDayNames = GM_getValue('DayNames_' + SignedInAs, '');
		if (dftDayNames) {
			var arDayNames = dftDayNames.split(',');
		} else {
			arDayNames = arDays;
		}

		// Get selected year and month.
		var selMonth = selTpMth.value - 0;
		var selYear = selTpMth.options[selTpMth.selectedIndex].getAttribute('year') -0;
		// Remove any existing days.
		while (selTpDay.childNodes.length) {
			removeNode(selTpDay.lastChild);
		}
		// Determine number of days in the selected month.
		var eom = fDaysInMonth(selYear, selMonth);
		// Determine starting day of the week.
		var d = new Date();
		d.setFullYear(selYear, selMonth - 1, 1);
		var idx = d.getDay();
		// Add days to the selector.
		for (i = 1; i<=eom; i++) {
			xOption = document.createElement('option');
			xOption.value = pad(i, 2);
			xOption.appendChild(document.createTextNode(pad(i, 2) + ' (' + arDayNames[idx].trim() + ')'));
			idx = (idx + 1) % 7;
			selTpDay.appendChild(xOption);
			// If showing current month, select current day.
			if (selYear == curYear && selMonth == curMonth) {
				if (i == curDate) {xOption.selected = true;}
			// Otherwise, set to the 1st.
			} else {
				if (i == 1) {xOption.selected = true;}
			}
		}
	}

	// Load hours.
	function fLoadHours() {
		for (i = 0; i<=23; i++) {
			xOption = document.createElement('option');
			xOption.value = pad(i, 2);
			switch(i) {
				case  0: var hour_ap = '12m'; break;
				case 12: hour_ap = '12n'; break;
				default: hour_ap = (i < 13) ? (i % 12) + 'am' : (i % 12) + 'pm';
			}
			xOption.appendChild(document.createTextNode(pad(i, 2) + ' (' + hour_ap + ')'));
			selTpHour.appendChild(xOption);
			if (i == 7) { xOption.selected = true; }
		}
	}

	// Turn 30 minute indicator on/off.
	function fToggleMin() {
		this.classList.toggle('off30min');
		this.classList.toggle('on30min');
	}

	// Set date and time to match last values by this user.
	function SetUserDateTime() {
		var jsonDates = new Object;
		var jsonString = localStorage.getItem(JSON_DATES_LS_KEY);
		if (jsonString) {
			jsonDates = JSON.parse(jsonString);
			if (jsonDates.hasOwnProperty(ownerguid)) {
				if (jsonDates[ownerguid]['datestamp'] >= curTimeStamp()) {
					var selTpMth = document.getElementById("selTpMth");
					var selTpDay = document.getElementById("selTpDay");
					var selTpHour = document.getElementById("selTpHour");
					var img30Min = document.getElementById("img30Min");

					selTpMth.value = jsonDates[ownerguid]['month'];
					fLoadDays();
					selTpDay.value = jsonDates[ownerguid]['day'];
					selTpHour.value = jsonDates[ownerguid]['hour'];
					var minVal = jsonDates[ownerguid]['minute'];
					img30Min.classList.remove(minVal == '30' ? 'off30min' : 'on30min');
					img30Min.classList.add(minVal == '30' ? 'on30min' : 'off30min');
				} else {
					delete jsonDates[ownerguid];
					jsonString = JSON.stringify(jsonDates);
					localStorage.setItem(JSON_DATES_LS_KEY, jsonString);
				}
			}
		}
	}

	// Change width of dropdown controls when icons clicked.
	function fAdjustCtrlSize() {
		var inc = this.getAttribute('increment') - 0;
		var curSetting = GM_getValue('CtrlWidth_' + SignedInAs, 42) - 0;
		var newSetting = curSetting + inc;
		if ((newSetting >= 20) && (newSetting <= 60)) {
			GM_setValue('CtrlWidth_' + SignedInAs, newSetting);
			var ctrls = document.getElementsByName('selTpCtrl');
			var ic = ctrls.length;
			for (var i = 0; i < ic; i++) {
				ctrls[i].style.width = newSetting.toString() + 'px';
			}
		}
	}

	// Delete expired entries from json database.
	function fCleanUpJson() {
		var jsonDates = new Object;
		var jsonString = localStorage.getItem(JSON_DATES_LS_KEY);
		if (jsonString) {
			jsonDates = JSON.parse(jsonString);
			var cts = curTimeStamp();
			for (var i in jsonDates) {
				if (jsonDates.hasOwnProperty(i)) {
					if (jsonDates[i]['datestamp'] < cts) {
						delete jsonDates[i];
					}
				}
			}
			var jsonString = JSON.stringify(jsonDates);
			localStorage.setItem(JSON_DATES_LS_KEY, jsonString);
		}
	}

	// Return date stamp in yyyymmdd format.
	function curTimeStamp() {
		var rtnvar = pad(curYear,4) + pad(curMonth,2) + pad(curDate,2);
		return rtnvar;
	}

	// Open bookmark creation page.
	function fBookmarkClicked() {
		// Make sure settings are in local storage.
		fPrefs2LocalStorage(false);

		// Error if cache is already in the database.
		if (fGetTimedStatus(gccode)) {
			alert('Cache ' + gccode + ' is already in\n' +
					'the Timed Publishing database.');
			return;
		}

		// Get date/time settings.
		var selTpMth = document.getElementById("selTpMth");
		var selTpDay = document.getElementById("selTpDay");
		var selTpHour = document.getElementById("selTpHour");
		var img30Min = document.getElementById("img30Min");

		var tpYear = selTpMth.options[selTpMth.selectedIndex].getAttribute('year');
		var tpMth = selTpMth.value;
		var tpDay = selTpDay.value;
		var tpHour = selTpHour.value;
		var tpMin = (img30Min.classList.contains('on30min')) ? '30' : '00';
		var tpTime = [tpYear, tpMth, tpDay, tpHour, tpMin].toString();

		// Save users date and time info as later default values.
		var jsonDates = new Object;
		var jsonString = localStorage.getItem(JSON_DATES_LS_KEY);
		if (jsonString) {
			jsonDates = JSON.parse(jsonString);
		}

		jsonDates[ownerguid] = {
			'ownerusername' : ownerusername,
			'year'          : tpYear,
			'month'         : tpMth,
			'day'           : tpDay,
			'hour'          : tpHour,
			'minute'        : tpMin,
			'datestamp'     : tpYear + tpMth + tpDay
		};

		jsonString = JSON.stringify(jsonDates);
		localStorage.setItem(JSON_DATES_LS_KEY, jsonString);

		// Get UTC epoch time value.
		var tpEpoch = Date.UTC(tpYear, tpMth-1, tpDay, tpHour, tpMin, 0)/1000;

		// Get timezone info for future time at remote location.
		// 	Specs: https://developers.google.com/maps/documentation/timezone/

		GM_xmlhttpRequest({
			method: "get",
			url: 'https://maps.googleapis.com/maps/api/timezone/json?' +
					`location=${latitude},${longitude}&timestamp=${tpEpoch}&sensor=false`,
			onload: function(response) {
				var jsObject = JSON.parse(response.responseText);
				if (jsObject.hasOwnProperty('status')) {
					if (jsObject['status'] == 'OK') {
						// Convert to cache's local epoch time value.
						var tpDstOffset = jsObject['dstOffset'];
						var tpRawOffset = jsObject['rawOffset'];
						var newEpoch = tpEpoch - tpRawOffset;

						// Get timezone data for the cache's location, at the requested
						// publishing date and time. A second call with the adjusted time is
						// necessary, for correct timing on DST change-over days, and retrieves
						// time zone ID and Name in localized language.
						var tpLanguage = GM_getValue('Language_' + SignedInAs, 'en');
						GM_xmlhttpRequest({
							method: "get",
							url: 'https://maps.googleapis.com/maps/api/timezone/json?' +
									`key=${TZ_API_KEY}&language=${tpLanguage}` +
									`&location=${latitude},${longitude}&timestamp=${newEpoch}&sensor=false`,
							onload: function(response) {
								jsObject = JSON.parse(response.responseText);
								if (jsObject.hasOwnProperty('status')) {
									if (jsObject['status'] == 'OK') {
										var tpTimeZoneId   = jsObject['timeZoneId'];
										var tpTimeZoneName = jsObject['timeZoneName'];
										var tpDstOffset    = jsObject['dstOffset'];
										var tpRawOffset    = jsObject['rawOffset'];
										var tpUctOffset    = (tpRawOffset + tpDstOffset) / 3600;
										tpTimeZoneId = encodeURIComponent(window.btoa(tpTimeZoneId));
										tpTimeZoneName = encodeURIComponent(window.btoa(tpTimeZoneName));

										var bmUrl = document.getElementById("ctl00_ContentBody_lnkBookmark").href;
										bmUrl += `&tpdt=${tpTime}&tpoffset=${tpUctOffset}&tptzid=${tpTimeZoneId}` +
												`&tptzname=${tpTimeZoneName}&refid=${cacheid}&wpid=${gccode}`;
										bmUrl = unescape(encodeURI(bmUrl));
										GM_openInTab(bmUrl, true);
									} else {
										alert('Time zone retrieval failed. Reason: ' + jsObject['status']);
									}
								} else {
									alert('Time zone retrieval failed.');
								}
							}
						});
					} else {
						alert('Time zone retrieval failed. Reason: ' + jsObject['status']);
					}
				} else {
					alert('Time zone retrieval failed.');
				}
			}
		});
	}

	// Update number of pending caches.
	function fUpdatePendingCounter() {
		var lnkTimedPending = document.getElementById('lnkTimedPending');
		var count = fTimedPendingCount();
		lnkTimedPending.firstChild.data = count;
		lnkTimedPending.style.color = (count != '0') ? 'red' : 'green';
	}

	// Return number of pending caches.
	function fTimedPendingCount() {
		var jsonString = localStorage.getItem(TIMED_PUBLISHING_TRACKING_KEY);
		if (jsonString) {
			var timePubDb = JSON.parse(jsonString);
		} else {
			return 0;
		}

		var counter = 0;
		for (var i in timePubDb) {
			if (timePubDb.hasOwnProperty(i)) {
				counter += timePubDb[i]['caches'].length;
			}
		}
		return counter;
	}

	// Check if cache was just added. If so, put on hold.
	function fIsNewlyAdded(gccode, olddata, newdata) {
		if (!olddata) { olddata = '{}'; }
		var oldStat = fGetTimedStatus(gccode, olddata);
		var newStat = fGetTimedStatus(gccode, newdata);
		if (oldStat == false && newStat == true) {
			return true;
		} else {
			return false;
		}
	}

	// Return True if cache already in database.
	function fGetTimedStatus(gccode, jsonString) {
		var ts = false;
		if (!jsonString) {
			jsonString = localStorage.getItem(TIMED_PUBLISHING_TRACKING_KEY);
		}
		if (jsonString) {
			var timePubDb = JSON.parse(jsonString);

			s1:
			for (var i in timePubDb) {
				if (timePubDb.hasOwnProperty(i)) {
					for (var j in timePubDb[i]['caches']) {
						if (timePubDb[i]['caches'][j]['wpid'] == gccode) {
							ts = true;
							break s1;
						}
					}
				}
			}
		}
		return ts;
	}

	// Put cache on hold.
	function fHoldCache() {
		var e_ddAction = document.getElementById('ctl00_ContentBody_ddAction');
		var e_btnGo = document.getElementById('ctl00_ContentBody_btnGo');
		for (i = 0; i < e_ddAction.length; i++) {
			if (e_ddAction.options[i].value == 'Hold') {
				e_ddAction.selectedIndex = i;
					e_btnGo.click();
				break;
			}
		}
	}

	// Open bookmark page.
	function fTimedPendingClicked() {
		// Clean up old user date defaults.
		fCleanUpJson();

		// Get timed publishing bookmark url. Error if not found.
		var bmPageUrl = GM_getValue('BookmarkPageUrl_' + SignedInAs, '');
		if (!bmPageUrl) {
			alert('Bookmark URL not specified in settings.\n\n' +
					'Click the clock icon to open preferences settings.')
			return;
		}

		// Propagate preferences to other scripts.
		fPrefs2LocalStorage(false);

		// Extract bookmark url guid, and build new url.
		var bmGuid = UrlParm('guid', true, bmPageUrl);
		bmPageUrl = '../bookmarks/view.aspx?guid=' + bmGuid;

		// Open page in new tab.
		GM_openInTab(bmPageUrl, false);
	}

	// Save prefs to local storage.
	function fPrefs2LocalStorage(bForced) {
		var bSaveIt = false;
		if (!bForced) {
			bSaveIt = (!localStorage.getItem(JSON_TIMED_PUBLISH_PREFS_KEY));
		} else {
			bSaveIt = true;
		}
		if (bSaveIt) {
			var jsonObj = new Object;

			// Fix for Firefox 35 security change  //
			// var prefList = GM_listValues();
			var prefList = cloneInto(GM_listValues(), window);
			// Fix for Firefox 35 security change  //

			var ic = prefList.length;
			for (var i = 0; i < ic; i++) {
				if (prefList[i] != 'LastVrCheck') {
					var xval = GM_getValue(prefList[i], '');
					jsonObj[prefList[i]] = xval;
				}
			}
			var jsonString = JSON.stringify(jsonObj);
			localStorage.setItem(JSON_TIMED_PUBLISH_PREFS_KEY, jsonString);
		}
	}

	// Dump local storage to console.
	function fDumpJSON() {
		var dumpstring = '';
		var jsonString = localStorage.getItem(JSON_TIMED_PUBLISH_PREFS_KEY);
		var jsonObj = JSON.parse(jsonString);
		dumpstring += 'JSON_TIMED_PUBLISH_PREFS_KEY\n' + JSON.stringify(jsonObj,null,4);

		jsonString = localStorage.getItem(TIMED_PUBLISHING_TRACKING_KEY);
		jsonObj = JSON.parse(jsonString);
		dumpstring += '\n\nTIMED_PUBLISHING_TRACKING_KEY\n' + JSON.stringify(jsonObj,null,4);

		jsonString = localStorage.getItem(JSON_DATES_LS_KEY);
		jsonObj = JSON.parse(jsonString);
		dumpstring += '\n\nJSON_DATES_LS_KEY\n' + JSON.stringify(jsonObj,null,4);

		GM_setClipboard(dumpstring);
		alert('Storage data has been copied to your clipboard. ' + dumpstring.length + ' characters.');
	}

	// Return number of days in a month.
	function fDaysInMonth(year, month) {
		var dd = new Date(year, month, 0);
		return dd.getDate();
	}

	// Pad a number with leading zeros.
	function pad(number, length) {
		var str = "" + number;
		while(str.length < length) {
			str = '0' + str;
		}
		return str;
	}

	// Returns a URL parameter.
	//  ParmName - Parameter name to look for.
	//  IgnoreCase - (optional) *false, true. Ignore parmeter name case.
	//  UrlString - (optional) String to search. If omitted, document URL is used.
	function UrlParm(ParmName, IgnoreCase, UrlString) {
		var RegRslt, sc = '', RtnVal = '';
		if (IgnoreCase) {sc = 'i'}
		if(UrlString) {
			var PageUrl = UrlString;
		} else {
			PageUrl = document.location + '';
		}
		var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
		var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
		RegRslt = RegEx1.exec(ParmString);
		if (RegRslt) {RtnVal = RegRslt[2]}
		return RtnVal;
	}

	// Remove element and everything below it.
	function removeNode(element) {
		element.parentNode.removeChild(element);
	}

	// Return protocol and domain, for building URLs for the current subdomain.
	function fDomain() {
		return location.protocol + '//' + document.domain;
	}

	function toUnicode(theString) {
		var unicodeString = '';
		for (var i=0; i < theString.length; i++) {
			var theUnicode = theString.charCodeAt(i).toString(16).toUpperCase();
			while (theUnicode.length < 4) {
				theUnicode = '0' + theUnicode;
			}
			theUnicode = '\\u' + theUnicode;
			unicodeString += theUnicode;
		}
	  return unicodeString;
	}

	function fSetOptions() {
		if (!fCreateSettingsDiv('Timed Publishing Script Options')) { return; };
		var divSet = document.getElementById('gm_divSet');
		divSet.setAttribute('winTopPos', document.documentElement.scrollTop);  // snapback code

		//****************************************************************************************//
		//                                                                                        //
		//                       Start of Optional Info Div Code                                  //
		//                                                                                        //
		//****************************************************************************************//

		var ds_InfoDiv = document.getElementById('ds_InfoDiv');
		ds_InfoDiv.style.textAlign = 'right';
		var linkInfoDiv = document.createElement('a');
		linkInfoDiv.href = 'https://sites.google.com/site/timedpublishing/set-up-timed-publishing';
		linkInfoDiv.target = '_blank';
		linkInfoDiv.title = 'Open documentation in a new tab.';
		linkInfoDiv.appendChild(document.createTextNode('Timed Publishing Documentation'));
		ds_InfoDiv.appendChild(linkInfoDiv);

		//****************************************************************************************//
		//                                                                                        //
		//                        End of Optional Info Div Code                                   //
		//                                                                                        //
		//****************************************************************************************//

		// Generate one unique DOM name for all setting controls being created.
		settingName = 'sc_' + Math.floor(Math.random()*100000001).toString();

		//****************************************************************************************//
		//                                                                                        //
		//                           Start of Control Creation                                    //
		//                                                                                        //
		// fCreateSetting(vID, vType, vLabel, vTitle, vDftVal, vSize, vSelVal, vSelTxt)           //
		//                                                                                        //
		// 	vID = Identifier for control, and for saving setting data.                            //
		// 	vType = checkbox (input + type.checkbox), text (input), textarea, select              //
		// 	vLabel = Text for control label. Above textareas, or to the right of all others.      //
		// 	vTitle = Title text for input control and label, or vLabel if not specified.          //
		// 	vDftVal = May be text, true/false, or matches a vSelVal array element.                //
		// 	vSize = Length of input (text) box, or height of textarea (i.e., '20px').             //
		// 	vSelVal = Array of select option values.                                              //
		// 	vSelTxt = Array of select option text. If omitted, SelVal values are used.            //
		//                                                                                        //
		//****************************************************************************************//

		GM_addStyle('legend.gmsettings { background: cornsilk; border: solid 2px slategray; border-radius: 8px; padding: 6px; }');
		GM_addStyle('fieldset.gmsettings { background: lightyellow; border-radius: 10px; padding: 5px; border: solid 4px green} ' );

		// =============================== Review Page ================================= //
		var vDftVal = GM_getValue('AutoHold_' + SignedInAs, true);
		var vLabel = 'Auto Hold cache page after adding to bookmark.';
		var cbAutoHold = fCreateSetting('AutoHold', 'checkbox', vLabel, '', vDftVal);

		var vDftVal = GM_getValue('ControlLocSeq_' + SignedInAs, '2');
		var vLabel = 'Location sequence to display the Timed Publishing controls.';
		var vSelVal = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11'];
		var selControlLocSeq = fCreateSetting('ControlLocSeq', 'select', vLabel, '', vDftVal, '', vSelVal, '');

		var vDftVal = GM_getValue('Language_' + SignedInAs, 'en');
		var vLabel = 'Localization for Time Zone Name (for inclusion in the log text).';
		var vSelVal = ["ar","eu","eu","bn","bg","ca","zh-CN","zh-TW","hr","cs","da","nl","en",
				"en-AU","en-GB","fa","fil","fi","fr","gl","de","el","gu","iw","hi","hu","id",
				"it","ja","kn","ko","lv","lt","ml","mr","no","pl","pt","pt-BR","pt-PT","ro",
				"ru","sr","sk","sl","es","sv","tl","ta","te","th","tr","uk","vi"];
		var vSelText = ["Arabic","Basque","Basque","Bengali","Bulgarian","Catalan",
				"Chinese (Simplified)","Chinese (Traditional)","Croatian","Czech","Danish","Dutch",
				"English","English (Australian)","English (Great Britain)","Farsi","Filipino",
				"Finnish","French","Galician","German","Greek","Gujarati","Hebrew","Hindi",
				"Hungarian","Indonesian","Italian","Japanese","Kannada","Korean","Latvian",
				"Lithuanian","Malayalam","Marathi","Norwegian","Polish","Portuguese",
				"Portuguese (Brazil)","Portuguese (Portugal)","Romanian","Russian","Serbian",
				"Slovak","Slovenian","Spanish","Swedish","Tagalog","Tamil","Telugu","Thai","Turkish",
				"Ukrainian","Vietnamese"];
		var selLanguage = fCreateSetting('Language', 'select', vLabel, '', vDftVal, '', vSelVal, vSelText);

		var vDftVal = GM_getValue('HideWidthControls_' + SignedInAs, false);
		var vLabel = 'Hide the drop-down controls width adjustment buttons.';
		var cbAutoHold = fCreateSetting('HideWidthControls', 'checkbox', vLabel, '', vDftVal);

		fGroupSettings('Review Page Settings', ['AutoHold', 'ControlLocSeq', 'Language', 'HideWidthControls']);
		// ============================================================================= //

		// =============================== Bookmark Creation Page ====================== //
		vDftVal = GM_getValue('BookmarkName_' + SignedInAs, '');
		vLabel = 'Exact name of default Bookmark for Timed Publishing.';
		var txtBookmarkName = fCreateSetting('BookmarkName', 'text', vLabel, '', vDftVal);

		var vDftVal = GM_getValue('CreateRQN_' + SignedInAs, true);
		var vLabel = 'Create Review Queue Notes indicating Timed Publishing status.';
		var cbAutoHold = fCreateSetting('CreateRQN', 'checkbox', vLabel, '', vDftVal);

		var vDftVal = GM_getValue('UseGenericClock_' + SignedInAs, false);
		var vLabel = 'Use a generic clock icon in Review Queue Notes instead of the real-time clock icon.';
		var cbUseGenericClock = fCreateSetting('UseGenericClock', 'checkbox', vLabel, '', vDftVal);

		fGroupSettings('Bookmark Creation Page Settings', ['BookmarkName', 'CreateRQN', 'UseGenericClock']);
		// ============================================================================= //

		// =============================== Bookmark Page =============================== //
		vDftVal = GM_getValue('BookmarkPageUrl_' + SignedInAs, '');
		vLabel = 'Full URL for the default Bookmark page for Timed Publishing.';
		var txtBookmarkPageUrl = fCreateSetting('BookmarkPageUrl', 'text', vLabel, '', vDftVal, '500px');

		fGroupSettings('Bookmark Page Settings', ['BookmarkPageUrl']);
		// ============================================================================= //

		// =============================== Submission Page ============================= //
		var dftDayNames = 'Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday';
		vDftVal = GM_getValue('DayNames_' + SignedInAs, dftDayNames);
		vLabel = 'Day of the Week Names, for Localization, separated by commas, beginning with Sunday.';
		var txtDayNames = fCreateSetting('DayNames', 'text', vLabel, '', vDftVal, '500px');

		var dftMthNames = 'January, February, March, April, May, June, July, ' +
				'August, September, October, November, December';
		vDftVal = GM_getValue('MonthNames_' + SignedInAs, dftMthNames);
		vLabel = 'Month Names, for Localization, separated by commas.';
		var txtMonthNames = fCreateSetting('MonthNames', 'text', vLabel, '', vDftVal, '500px');

		vDftVal = GM_getValue('LogText_' + SignedInAs, '');
		vLabel = '';
		var txtaraLogText = fCreateSetting('LogText', 'textarea', vLabel, '', vDftVal, '150px');

		var infoLogText = document.createElement('span');
		infoLogText.innerHTML =
			'<p><b>Available Replacement Variables:</b></p>' +
			'<p> &bullet; <b>%monthname%</b> - The month name, as specified in the list above.</p>' +
			'<p> &bullet; <b>%monthnum%</b> - The month number, without a leading zero.</p>' +
			'<p> &bullet; <b>%dayname%</b> - The day of the week name, as specified in the list above.</p>' +
			'<p> &bullet; <b>%daynum%</b> - The numeric day of the month, without a leading zero.</p>' +
			'<p> &bullet; <b>%h12time%</b> - The scheduled time in 12-hour am/pm format. ' +
			'Midnight and noon are presented as 12m and 12n. Zero minutes are excluded (7pm, 7:30pm).</p>' +
			'<p> &bullet; <b>%h24time%</b> - The scheduled time in 24-hour (military) time, ' +
			'without a leading zero (i.e., 7:30).</p>' +
			'<p> &bullet; <b>%h24ztime%</b> - The scheduled time in 24-hour (military) time, ' +
			'with a leading zero (i.e., 07:30).</p>' +
			'<p> &bullet; <b>%timezone%</b> - Time Zone name, such as Eastern Standard Time.</p>' +
			'<p><b>Text for the Timed Publishing Log:</b></p>';
		txtaraLogText.parentNode.insertBefore(infoLogText, txtaraLogText);

		var vDftVal = GM_getValue('AutoSubmit_' + SignedInAs, false);
		var vLabel = 'Auto Submit for Timed Publishing.';
		var cbAutoSubmit = fCreateSetting('AutoSubmit', 'checkbox', vLabel, '', vDftVal);

		fGroupSettings('Timed Publishing Submission', ['DayNames', 'MonthNames', 'LogText', 'AutoSubmit']);
		// ============================================================================= //

		//****************************************************************************************//
		//                                                                                        //
		//                            End of Control Creation                                     //
		//                                                                                        //
		//****************************************************************************************//


		fOpenSettingsDiv();
	}

	// Cancle button clicked.
	function fCancelButtonClicked() {
		var resp = confirm('Any changes will be lost. Close Settings?');
		if (resp) {
			fCloseSettingsDiv();
		}
	}

	// Save button clicked. Store all settings.
	function fSaveButtonClicked() {
		// Get array of setting controls.
		var scs = document.getElementsByName(settingName);
		// Process each control.
		var scl = scs.length;
		for (var i = 0; i < scl; i++) {
			var sc = scs[i];
			var rawid = sc.getAttribute('rawid', '');
			var ctrltype = sc.getAttribute('ctrltype', '');
			if (ctrltype == 'checkbox') {
				GM_setValue(rawid + '_' + SignedInAs, sc.checked);
			} else {
				GM_setValue(rawid + '_' + SignedInAs, sc.value);
			}
		}
		fCloseSettingsDiv();

		// ======================================================================================== //
		fPrefs2LocalStorage(true); // <<<=========== Custom code to save settings to local storage
		// ======================================================================================== //

	}

	// Create Div for settings GUI.
	function fCreateSettingsDiv(sTitle) {

		// If div already exists, reposition browser, and show alert.
		var divSet = document.getElementById("gm_divSet");
		if (divSet) {
			var YOffsetVal = parseInt(divSet.getAttribute('YOffsetVal', '0'));
			window.scrollTo(window.pageXOffset, YOffsetVal);
			alert('Edit Setting interface already on screen.');
			return false;
		}

		// Set styles for titles and elements.
		GM_addStyle('.SettingTitle {font-size: medium; font-style: italic; font-weight: bold; ' +
				'text-align: center; margin-bottom: 12px; !important; } ' );
		GM_addStyle('.SettingVersionTitle {font-size: small; font-style: italic; font-weight: bold; ' +
				'text-align: center; margin-bottom: 12px; !important; } ' );
		GM_addStyle('.SettingElement {text-align: left; margin-left: 6px; ' +
				'margin-right: 6px; margin-bottom: 12px; !important; } ' );
		GM_addStyle('.SettingButtons {text-align: right; margin-left: 6px; ' +
				'margin-right: 6px; margin-bottom: 6px; !important; } ' );
		GM_addStyle('.SettingLabel {font-weight: bold; margin-left: 6px !important;} ' ); +

		// Create blackout div.
		document.body.setAttribute('style', 'height:100%;');
		var divBlackout = document.createElement('div');
		divBlackout.id = 'divBlackout';
		divBlackout.setAttribute('style', 'z-index: 900; background-color: rgb(0, 0, 0); ' +
				'visibility: hidden; opacity: 0; position: fixed; left: 0px; top: 0px; '+
				'height: 100%; width: 100%; display: block;');

		// Create div.
		divSet = document.createElement('div');
		divSet.id = 'gm_divSet';
		divSet.setAttribute('style', 'position: absolute; z-index: 1000; visibility: hidden; ' +
				'padding: 5px; outline: 6px ridge blue; background: #FFFFCC;');
		popwidth = parseInt(window.innerWidth * .7);
		divSet.style.width = popwidth + 'px';

		// Create heading.
		var ds_Heading = document.createElement('div');
		ds_Heading.setAttribute('class', 'SettingTitle');
		ds_Heading.appendChild(document.createTextNode(sTitle));
		divSet.appendChild(ds_Heading);

		var ds_Version = document.createElement('div');
		ds_Version.setAttribute('class', 'SettingVersionTitle');
		ds_Version.appendChild(document.createTextNode('Script Version ' + GM_info.script.version));
		divSet.appendChild(ds_Version);

		// Add info div to page.
		var ds_InfoDiv = document.createElement('div');
		ds_InfoDiv.id = 'ds_InfoDiv';
		divSet.appendChild(ds_InfoDiv);

		// Add div to page.
		var toppos =  parseInt(window.pageYOffset +  60);
		var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
		divSet.style.top = toppos + 'px';
		divSet.style.left = leftpos + 'px';
		divSet.setAttribute('YOffsetVal', window.pageYOffset);

		// Add blackout and setting divs.
		document.body.appendChild(divBlackout);
		document.body.appendChild(divSet);
		window.addEventListener('resize', fSetLeftPos, true);

		return true;
	}

	/*
	vID = GetSetting ID, and type + vID is control ID.
	vType = checkbox (input + type.checkbox), text (input), textarea, select
	vLabel = Text for control label.
	vTitle = Title text for input control and label, or vLabel if not specified.
	vDftVal = May be text, true/false, or matches a vSelVal array element.
	vSize = Length of input (text) box, or height of textarea, in pixels.
	vSelVal = Array of select option values.
	vSelTxt = Array of select option text.
	*/
	// Create settings from passed data.
 	function fCreateSetting(vID, vType, vLabel, vTitle, vDftVal, vSize, vSelVal, vSelTxt) {
		var divSet = document.getElementById('gm_divSet');
		var kParagraph = document.createElement('p');
		kParagraph.id = 'p' + vID;
		kParagraph.setAttribute('class', 'SettingElement');
		switch (vType) {
			case 'checkbox':
				var kElem = document.createElement('input');
				kElem.id = 'cb' + vID;
				kElem.type = 'checkbox';
				kElem. checked = vDftVal;
				break;
			case 'text':
				if (!vSize) { vSize = '150px'; }
				var kElem = document.createElement('input');
				kElem.id = 'txt' + vID;
				kElem.style.width = vSize;
				kElem.value = vDftVal;
				kElem.addEventListener('focus', fSelectAllText, true);
				break;
			 case 'textarea':
				if (!vSize) { vSize = '80px'; }
				var kElem = document.createElement('textarea');
				kElem.id = 'txtara' + vID;
				kElem.style.width = '100%';
				kElem.style.height = vSize;
				kElem.value = vDftVal;
				break;
			case 'select':
				var kElem = document.createElement('select');
				kElem.id = 'sel' + vID;
				if (vSelVal) {
					if (vSelVal.constructor == Array) {
						for (var i in vSelVal) {
							var kOption = document.createElement('option');
							kOption.value = vSelVal[i];
							kOption.selected = (vSelVal[i] == vDftVal);
							if ((vSelTxt.constructor == Array) && (vSelTxt.length >= i-1)) {
								var kTxtNode = vSelTxt[i];
							} else {
								var kTxtNode = vSelVal[i];
							}
							kOption.appendChild(document.createTextNode(kTxtNode));
							kElem.appendChild(kOption);
						}
					}
				}
				break;
		}

		var kLabel = document.createElement('label');
		kLabel.setAttribute('class', 'SettingLabel');
		kLabel.setAttribute('for', kElem.id);
		kLabel.appendChild(document.createTextNode(vLabel));
		if (!vTitle) { vTitle = vLabel; }
		kElem.title = vTitle;
		kLabel.title = kElem.title;
		kElem.name = settingName;
		kElem.setAttribute('ctrltype', vType);
		kElem.setAttribute('rawid', vID);

		if (vType == 'textarea') {
			kParagraph.appendChild(kLabel);
			kParagraph.appendChild(document.createElement('br'));
			kParagraph.appendChild(kElem);
		} else {
			kParagraph.appendChild(kElem);
			kParagraph.appendChild(kLabel);
		}
		divSet.appendChild(kParagraph);

		return kElem;
	}

	// Routine for creating setting groupings.
	// Use after the settings group has been established.
	// Pass the heading text, and an array of setting IDs.
	// Example:
	// [Create SettingName1 here]
	// [Create SettingName2 here]
	// [Create SettingName3 here]
	// fGroupSettings('Group Heading Text', ['SettingName1', 'SettingName2', 'SettingName3']);
	function fGroupSettings(legendText, aID) {
		var divSet = document.getElementById('gm_divSet');
		var fldset = document.createElement('fieldset');
		fldset.classList.add('gmsettings');
		divSet.appendChild(fldset);
		var fldsetlgd = document.createElement('legend');
		fldsetlgd.classList.add('gmsettings');
		fldset.appendChild(fldsetlgd);
		fldsetlgd.appendChild(document.createTextNode(legendText));
		var ic = aID.length;
		for (var i = 0; i < ic; i++) {
			fldset.appendChild(document.getElementById('p' + aID[i]));
		}
	}

	// Resize/reposition on window resizing.
	function fSetLeftPos() {
		var divSet = document.getElementById('gm_divSet');
		if (divSet) {
			var popwidth = parseInt(window.innerWidth * .7);
			divSet.style.width = popwidth + 'px';
			var leftpos = parseInt((window.innerWidth / 2) - (popwidth / 2));
			divSet.style.left = leftpos + 'px';
		}
	}

	function fOpenSettingsDiv() {
		// Create buttons.
		fCreateSaveCancleButtons();

		// Add blackout and setting divs.
		var divBlackout = document.getElementById('divBlackout');
		var divSet = document.getElementById('gm_divSet');
		divSet.style.visibility = 'visible';
		divBlackout.style.visibility = 'visible';
		var op = 0;
		var si = window.setInterval(fShowBlackout, 40);
		// Function to fade-in blackout div.
		function fShowBlackout() {
			op = op + .05;
			divBlackout.style.opacity = op;
			if (op >= .75) {
				window.clearInterval(si);
			}
		}
	}

	// Create Save/Cancel Buttons.
	function fCreateSaveCancleButtons() {
		var divSet = document.getElementById('gm_divSet');
		var ds_ButtonsP = document.createElement('div');
		ds_ButtonsP.setAttribute('class', 'SettingButtons');
		var ds_SaveButton = document.createElement('button');
		ds_SaveButton = document.createElement('button');
		ds_SaveButton.appendChild(document.createTextNode("Save"));
		ds_SaveButton.addEventListener("click", fSaveButtonClicked, true);
		var ds_CancelButton = document.createElement('button');
		ds_CancelButton.style.marginLeft = '6px';
		ds_CancelButton.addEventListener("click", fCancelButtonClicked, true);
		ds_CancelButton.appendChild(document.createTextNode("Cancel"));
		ds_ButtonsP.appendChild(ds_SaveButton);
		ds_ButtonsP.appendChild(ds_CancelButton);
		divSet.appendChild(ds_ButtonsP);
	}

	function fCloseSettingsDiv() {
		var divBlackout = document.getElementById('divBlackout');
		var divSet = document.getElementById('gm_divSet');
		var winTopPos = divSet.getAttribute('winTopPos') - 0;  // snapback code
		divSet.parentNode.removeChild(divSet);
		divBlackout.parentNode.removeChild(divBlackout);
		window.removeEventListener('resize', fSetLeftPos, true);
		document.documentElement.scrollTop = winTopPos;  // snapback code
	}

	function fCancelButtonClicked() {
		var resp = confirm('Any changes will be lost. Close Settings?');
		if (resp) {
			fCloseSettingsDiv();
		}
	}

	function fSelectAllText() {
		this.select();
	}

	// Insert element after an existing element.
	function insertAfter(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement.nextSibling);
	}

	// Insert element ahead of an existing element.
	function insertAheadOf(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement);
	}

