﻿// ==UserScript==
// @name           GCR Timed Publishing 2 Create Bookmark v2
// @description    Timed Publishing 2 - Create Bookmark Page
// @version        02.01
// @scriptGuid     33656f20-6a13-4cd1-b25b-91f50fd2bbdc
// @namespace      http://www.geocaching.com/admin
// @grantZZZ       GM_info
// @grantZZZ       GM_getValue
// @grantZZZ       GM_openInTab
// @grantZZZ       GM_setValue
// @grantZZZ       GM_xmlhttpRequest
// @grantZZZ       GM_addStyle
// @grantZZZ       GM_listValues
// @grantZZZ       GM_registerMenuCommand
// @grantZZZ       GM_setClipboard
// @grantZZZ       GM_deleteValue
// @grantZZZ       GM_getResourceText
// @grantZZZ       GM_getResourceURL
// @grantZZZ       GM_log
// @include        http*://*.geocaching.com/bookmarks/mark.aspx?guid=*
// @icon           http://i.imgur.com/GP6D2vX.png
// ==/UserScript==
/* 

Release log:
v02.01 2017-08-17 - SignedInAs fix for new header release

* v02.0 2017-08-02 - Version reset to 2.0 and name change to add v2.

v01.15 2016-06-30 - Fix for site change.

v01.14 2016-06-29 - Fix for site change.

v01.13 2015-03-31 - Fix for site change.

v01.12 2015-02-01 - Fix for Firefox 35 security change.

v01.11 2014-11-09 - Bug fix.

v01.10 2014-11-08 - Added optional Review Queue Note generation when a cache is
		successfully added to the bookmark.

v01.00 2014-04-04 - Initial release.

 */

	//  Local Storage keys:
	//  Local Storage key for JSON tracking data.
	const TIMED_PUBLISHING_TRACKING_KEY = 'c8a7a86e-ced2-4667-b01b-cb4698a7e06a';
	//	Key for user last date.
	const JSON_DATES_LS_KEY 	        = '2118dd9a-2785-47b6-bc84-4ef54b330358';
	//	Key for prefs across scripts.
	const JSON_TIMED_PUBLISH_PREFS_KEY 	= '9646db26-9d83-4743-b589-1de0d67bd352';
	//  Key for Review Queue Notes.
	const JSON_RQN_KEY                  = '517ba8f0-86f7-46f7-8f0d-d476265f420c';
	

	//  Get currently signed-on geocaching.com profile.
	var SignedInX = document.getElementsByClassName('user-name')[0];
	if (SignedInX) {
        var SignedInAs =  SignedInX.firstChild.data.trim();
		SignedInAs = SignedInAs.replace(/<\&amp;>/g, '&');
	} else { return; }

	
	//  Successfully added bookmark?
	var wasSubmitted = sessionStorage.getItem('TimedSubmitted');
	var success = document.getElementsByClassName('Success')[0];
	sessionStorage.removeItem('TimedSubmitted');
	if (wasSubmitted && !success) {
		return;
	}
	
	//  Get cache name, and maximum allowed characters.
	var bmName = document.getElementById('ctl00_ContentBody_Bookmark_tbName');

	
	//  Get passed date/time data.
	var tpdt = (UrlParm('tpdt', true));
	if (!tpdt) { return; }
	var tptzid = UrlParm('tptzid', true);
	tptzid = window.atob(decodeURIComponent(tptzid));
	var tptzname = UrlParm('tptzname', true);
	tptzname = window.atob(decodeURIComponent(tptzname));
	var tpoffset = UrlParm('tpoffset', true);
	var wpid = UrlParm('wpid', true);
	var refid = UrlParm('refid', true);
	

	if (success) { 
		if (fGetPrefs('CreateRQN', true)) {
			var dtSegs = tpdt.split(',');
			if (fGetPrefs('UseGenericClock', true)) {
				var timeIcon = '\u23F0';
			} else {
				timeIcon = 56656 + (parseInt(dtSegs[3]) - 1)%12 + (dtSegs[4] == '30' ? 12:0);
				timeIcon = '\uD83D' + String.fromCharCode(timeIcon);
			}
			var dtStamp = dtSegs[0] + '-' + dtSegs[1] + '-' + dtSegs[2] + ' ' + 
					dtSegs[3] + ':' + dtSegs[4];
			fEditRqnText(wpid, timeIcon + ' ' + dtStamp + ' ' + tptzname, 500);
		}
		fProcessSuccess(tptzid,tptzname,tpdt,tpoffset,wpid,refid); 
		return;
	}
	if (!bmName) { return; }
	
	
	var bmMaxLen = bmName.getAttribute('maxlength') - 0;
	var bmComments = document.getElementById('ctl00_ContentBody_Bookmark_tbComments');
	var cacheTitle = bmName.value;
	bmComments.value = cacheTitle;
	var newTitle = tptzid + '|' + tpdt + '|' + tpoffset + '|' + tptzname + '|' + wpid;
	if (newTitle.length > bmMaxLen) { newTitle = newTitle.substr(0, bmMaxLen); }
	bmName.value = newTitle;
	sessionStorage.setItem('tp-cacheTitle', cacheTitle);


	//  Get bookmark name.
	var jsonPrefsString = localStorage.getItem(JSON_TIMED_PUBLISH_PREFS_KEY);
	var BookmarkName = '';
	if (jsonPrefsString) {
		var jsonPrefObj = new Object;
		jsonPrefObj = JSON.parse(jsonPrefsString);
		BookmarkName = jsonPrefObj['BookmarkName_' + SignedInAs].trim();
	}
	if (!BookmarkName) {
		alert('Preferences have not been set.\n\n' +
				'Click the Clock icon on the Review Page\n' +
				'to enter the preferences.')
		return;
	}

	
	//  Set bookmark selector.
	var bmList = document.getElementById('ctl00_ContentBody_Bookmark_ddBookmarkList');
	var ic = bmList.options.length;
	var selId = 0;
	for (var i = 0; i < ic; i++) {
		if (bmList.options[i].firstChild.data.trim().toUpperCase() == BookmarkName.toUpperCase()) {
			selId = bmList.options[i].value;
			break;
		}
	}
	if (selId != 0) {
		bmList.value = selId;
		var e_btnSubmit = document.getElementById("ctl00_ContentBody_Bookmark_btnCreate");
		sessionStorage.setItem('TimedSubmitted', true);
		e_btnSubmit.click();
	} else {
		alert('Unable to locate a bookmark named "' + BookmarkName + '".\n' +
				'Please recheck your Timed Publishing settings.');
	}

	
	
	
	//  If bookmark entry successfully added.
	function fProcessSuccess(tptzid,tptzname,tpdt,tpoffset,wpid,refid) {

		var jsonString = localStorage.getItem(TIMED_PUBLISHING_TRACKING_KEY);


		if (jsonString) {
			timePubDb = JSON.parse(jsonString);
		} else {
			timePubDb = new Object;
		}
		
		var cacheTitle = sessionStorage.getItem('tp-cacheTitle');
		
		var tzKey = tptzid + '-' + tpdt;
		if (!timePubDb.hasOwnProperty(tzKey)) {
			timePubDb[tzKey] = {
				'tptzid'   : tptzid,
				'tptzname' : tptzname,
				'tpdt'     : tpdt,
				'tpoffset' : tpoffset,
				'caches'   : []
			}
		}

		timePubDb[tzKey]['caches'].push({ 'refid':refid, 'wpid':wpid, 'cacheTitle':cacheTitle });

		// jsonString = JSON.stringify(timePubDb);
		jsonString = JSON.stringify(timePubDb,null,4);
		localStorage.setItem(TIMED_PUBLISHING_TRACKING_KEY, jsonString);

		sessionStorage.removeItem('TimedSubmitted');
		window.close();
		
	}
	

	


	//  Return a specific preference setting value, appendSig is t/f value for appending profile name.
	function fGetPrefs(pref, appendSig) {
		jsonString = localStorage.getItem(JSON_TIMED_PUBLISH_PREFS_KEY);
		if (jsonString) {
			var timePubPrefs = JSON.parse(jsonString);
			var jsonKey = pref;
			if (appendSig) { jsonKey = jsonKey + '_' + SignedInAs; }
			var rtnvar = timePubPrefs[jsonKey];
		}
		return rtnvar;
	}
	
	
	
	
	//  Retrieve RQNotes by Waypoint ID.
	function fGetRqnText(WptId) {
		var jsObj = new Object;
		var jsonString = localStorage.getItem(JSON_RQN_KEY);
		if (jsonString) {
			jsObj = JSON.parse(jsonString);
			if (jsObj.hasOwnProperty(WptId)) { 
				var rtnVal = jsObj[WptId]; 
			}
		}
		delete jsObj;
		return rtnVal;
	}
	
	//  Add/Edit/Delete RQNotes.
	//  Pass waypoint ID, and text to add/update, or blank to delete.
	//  If MaxRqnEntries passed, it will limit the total number entries, deleting
	//  oldest entries first.
	function fEditRqnText(WptId, NoteText, MaxRqnEntries) {
		var WptId, MaxRqnEntries;
		var NoteText = NoteText.trim();
		
		var jsObj = new Object;
		var jsonString = localStorage.getItem(JSON_RQN_KEY);
		if (jsonString) {
			jsObj = JSON.parse(jsonString);
			if (jsObj.hasOwnProperty(WptId)) { delete jsObj[WptId]; }
		}
		if (NoteText) { 
			jsObj[WptId] = {'text': NoteText};
		}
		if (MaxRqnEntries) { 
			var keyList = Object.keys(jsObj);
			if (keyList.length > MaxRqnEntries) {
				var lim = keyList.length - MaxRqnEntries;
				for (var i = 0; i < lim; i++) {
					delete jsObj[keyList[i]];
				}
			}
		}
		jsonString = JSON.stringify(jsObj);
		localStorage.setItem(JSON_RQN_KEY, jsonString);
		delete jsObj;
	}
	



	
	//  Returns a URL parameter.
	//    ParmName - Parameter name to look for.
	//    IgnoreCase - (optional) *false, true. Ignore parmeter name case.
	//    UrlString - (optional) String to search. If omitted, document URL is used.
	function UrlParm(ParmName, IgnoreCase, UrlString) {
		var RegRslt, sc = '', RtnVal = '';
		if (IgnoreCase) {sc = 'i'}
		if(UrlString) {
			var PageUrl = UrlString;
		} else {
			PageUrl = document.location + '';
		}
		// PageUrl = unescape(JSON.parse('"' + PageUrl + '"'));
		PageUrl = unescape(JSON.parse('"' + PageUrl + '"'));
		var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
		var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
		RegRslt = RegEx1.exec(ParmString);
		if (RegRslt) {RtnVal = RegRslt[2]}
		return RtnVal;
	}
	