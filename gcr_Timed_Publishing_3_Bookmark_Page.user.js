﻿// ==UserScript==
// @name           GCR Timed Publishing 3 Bookmark Page v2
// @description    Timed Publishing 3 - Bookmark Page
// @version        02.02
// @scriptGuid     8b4a9560-750c-4655-8f24-eb5a39147b87
// @namespace      http://www.geocaching.com/admin
// @grant          GM_addStyle
// @grant          GM_getValue
// @grant          GM_setValue
// @grantZZZ       GM_info
// @grantZZZ       GM_openInTab
// @grantZZZ       GM_xmlhttpRequest
// @grantZZZ       GM_listValues
// @grantZZZ       GM_registerMenuCommand
// @grantZZZ       GM_setClipboard
// @grantZZZ       GM_deleteValue
// @grantZZZ       GM_getResourceText
// @grantZZZ       GM_getResourceURL
// @grantZZZ       GM_log
// @include        http*://*.geocaching.com/bookmarks/view.aspx?guid=*
// @include        http*://*.geocaching.com/bookmarks/bulk.aspx?ListID=*
// @icon           http://i.imgur.com/GP6D2vX.png
// ==/UserScript==
/* 

Release log:
* v02.02 2017-08-22 - Updating SignedInAs fix to work on bookmark page with new header and Bulk Publish page

* v02.01 2017-08-17 - SignedInAs fix for new header release

* v02.0 2017-08-02 - Version reset to 2.0 and name change to add v2.

* v01.15 2016-06-30 - Fix for site change.

* v01.14 2016-06-29 - Addition of Recall Settings button, for manual submission. Fix for site change.

* v01.13 2015-03-31 - Fix for site change.

* v01.12 2015-02-01 - Fix for Firefox 35 security change.

* v01.11 2014-11-09 - Bug fix.

* v01.10 2014-11-08 - Update release number.

* v01.00 2014-04-04 - Initial release.

 */

	//  Local Storage keys:
	//  Local Storage key for JSON tracking data.
	const TIMED_PUBLISHING_TRACKING_KEY = 'c8a7a86e-ced2-4667-b01b-cb4698a7e06a';
	//	Key for user last date.
	const JSON_DATES_LS_KEY 	        = '2118dd9a-2785-47b6-bc84-4ef54b330358';
	//	Key for prefs across scripts.
	const JSON_TIMED_PUBLISH_PREFS_KEY 	= '9646db26-9d83-4743-b589-1de0d67bd352';

	
	//  Get currently signed-on geocaching.com profile.
	var SignedInX = document.getElementsByClassName('user-name')[0];
	if (SignedInX) {
		var SignedInAs =  SignedInX.firstChild.data.trim();
	} else {
		SignedInX = document.getElementById('ctl00_LoginUrl');
		if (!SignedInX) { SignedInX = document.getElementById('ctl00_LogoutUrl'); }	
		SignedInAs = SignedInX.parentNode.childNodes[1].firstChild.data.trim();
	}

	if (SignedInAs) {
		SignedInAs = SignedInAs.replace(/<\&amp;>/g, '&');
	} else { return; }

	
	
	//  Set up Restore Last Settings button.
	var pubNow = document.getElementById('ctl00_ContentBody_MassCachePublish_PublishNow');
	if (pubNow) {
		var submitButton = document.getElementById('ctl00_ContentBody_MassCachePublish_LogButton');
		submitButton.addEventListener('click', fSaveLastSettings, false);
		var vLastSettings = GM_getValue('LastSettings', '');
		if (vLastSettings) {
			ls = vLastSettings.split('\u00B6')
			var btnRecall = document.createElement('input');
			btnRecall.type = 'button';
			btnRecall.style.marginLeft = '25px';
			btnRecall.value = 'Recall Last Settings';
			btnRecall.setAttribute('last-settings', vLastSettings);
			btnRecall.title = 
`Date: ${ls[0]}
Time: ${ls[1]}
UTC Offset: ${ls[2]}
Log Text: ${ls[3]}`;
			pubNow.parentNode.appendChild(btnRecall);
			btnRecall.addEventListener('click', fRestoreLastSettings, false);
		}
	}

	
	//  Restore last settings.
	function fRestoreLastSettings() {
		var splitter = '\u00B6';
		var x = this.getAttribute('last-settings');
		var ls = x.split(splitter);    //  date, time, UTC-offset, msg-text
		var dateentry = document.getElementById('ctl00_ContentBody_MassCachePublish_DateTimePublished');
		dateentry.value = ls[0];
		var timeentry = document.getElementById('ctl00_ContentBody_MassCachePublish_HourToPublish');
		timeentry.value = ls[1];
		var e_TimeZonesDDL = document.getElementById("ctl00_ContentBody_MassCachePublish_TimeZonesDDL");
		e_TimeZonesDDL.value = ls[2];
		var e_tbLogInfo = document.getElementById("ctl00_ContentBody_MassCachePublish_tbLogInfo");
		e_tbLogInfo.value = ls[3];
	}
		
	
	//  Save last settings.
	function fSaveLastSettings() {
		var dateentry = document.getElementById('ctl00_ContentBody_MassCachePublish_DateTimePublished');
		var timeentry = document.getElementById('ctl00_ContentBody_MassCachePublish_HourToPublish');
		var e_TimeZonesDDL = document.getElementById("ctl00_ContentBody_MassCachePublish_TimeZonesDDL");
		var e_tbLogInfo = document.getElementById("ctl00_ContentBody_MassCachePublish_tbLogInfo");
		var ls = new Array();
		ls[0] = dateentry.value;
		ls[1] = timeentry.value;
		ls[2] = e_TimeZonesDDL.value;	
		ls[3] = e_tbLogInfo.value;
		var lsx = ls.join('\u00B6');
		GM_setValue('LastSettings', lsx);
	}
	
	
	
	//  If not Timed Publishing bookmark, then exit script.
	var leaveScript = true;
	var tpBookmarkUrl = fGetPrefs('BookmarkPageUrl', true);
	var tpBookmarkGuid = UrlParm('guid', true, tpBookmarkUrl);
	var currentGuid = UrlParm('guid', true);
	if (currentGuid && (currentGuid == tpBookmarkGuid)) {
		leaveScript = false;
	} else {
		var ssListID = sessionStorage.getItem('TZ_ListID');
		var chkListID = UrlParm('ListID',true);
		var leaveScript = true;
		if ((chkListID) && (ssListID) && (chkListID == ssListID)) {
			leaveScript = false;
		}
	}
	if (leaveScript) { return; }

	
	
	var srcToggleCollapse = 
			"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
			"AAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAWJJREFUeNqkkzFOwzAU" +
			"QH9ixy0lJVRVydaxOxtzkbgBKGLkAuUASFUlJDgFCwyFA4DEwAW65w4pBZW0SZrESbA%2F" +
			"qkcTqV%2F61rfk9%2Fxjx0ZVVbBLUDlc3c4dSulElJ5I9x8mEDnlnI8fbo5%2BUMB5Pjke" +
			"WCPv7AB6h0RLfy4Ld%2FoejmZ%2BLqfXphyjKPIuTvehY1dCxrUp15wPW8ioT1itVm7XMX" +
			"FBneg6BBklWK9DKMuytkAevGSUIIpiKIoCcxt5nss2IY4TrIviT04IBdu2kVGCzWaDHcg8" +
			"ufzQ7v5yP4A0TZFRAs4rbCtJUuj3h%2Fp7p3MwTYKMEhgGFXAm2srg%2Ba6lFSwWTSEwkF" +
			"ECSvcgy0qwLAZhGGsFjDVgyygBY21xOGw7rRWSUYJGox0slqbb67BacPCVIyNr%2FBObzf" +
			"b08TWG79AUrVnalGue3hJkVAeEWOOZDzDzl7Ufk2TwAnZ9zr8CDAAh1rhAOsCiXQAAAABJ" +
			"RU5ErkJggg%3D%3D";	
	
	var srcToggleExpand = 
			"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAA" +
			"AAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAapJREFUeNqkUztPAkEQ" +
			"3rvbOx4eIiF4FRQUNFY2xBoSSzvMxdI%2FAJ2NCSEx0cr4C2y0QCtjoYmJ%2FgFqKSzOGk" +
			"SDx%2FPe7s7JchcSNGGSudmdme%2B72ZldzvM8tIpg%2Bjk86SYxxg2yVIkqf2A6RJu2bd" +
			"cvjze%2FgcC2rcZ2Qayqu%2BsosyEsRX%2F0HaX5pFdbbYtuazz9jkYjdb%2B8hlKyR8js" +
			"pUpzKqU4YNgRBoOBkk7ykBCUvdor2PuLrZA%2FnRQAwwiGQx25rrtAQP3%2BEcN%2B2vhZ" +
			"DPtHGCPHcUBnYlkW%2BKlo2juJ%2BSSCgJEsyywGBNPpFCqgunPwstC4ytEbW9%2BeFZBh" +
			"GIBhBLbtQVmTiYFyuRJL1rRnsPl8eT533EU8LwCGEXAcJmCTlGWim9M4Sy6qECa%2B%2BW" +
			"h7vSgh4ADDCDCOIdN0kShKSNfHgb%2FFwAZ9khQJxbDvTJDmSLMtk2y2CFYUuYW%2BUAwj" +
			"iEQSnV6fVzIpKZR0dy7%2BjtMK3%2BVPCzB0DTcxGk00rx7G6EvnSWkiU9poqkEfzbl%2B" +
			"nACGVSAIYr3VRqjV7v%2F7MVEMDGDV5%2FwjwABZz8%2BTuF15TgAAAABJRU5ErkJggg%3" +
			"D%3D";	
	
	var srcWarnMissing =
			"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAMAAABhq6zVAAAA" +
			"GXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAORQTFRF+8kX8sIb+sgS+8" +
			"kW9cAKrm8G7Nqx+NJC/vrt/NdF6ZsG97gO8rsK1YwE368nu4Qe17iE+/jzwpRF9M5IrGwA" +
			"1ZoB8aAH/vXV8ujV05ID/uui+bAMwIob38aczZcVxZY/98ke9bIN+scR5LIY+MUI9sEN3p" +
			"oF+b0R2Kcn+vbt2ZMF+em0+8gA/KgI3KMB89Vy3bxy/qkI+MUL66YIzaFI+vfwu4Yt4JgG" +
			"7Mha/NQ23LZa77gG0IkD8rgM/eWH//zz/vvw+8sM/awK+8YW/L8T/bAL/MMU/LcP/LwR/b" +
			"MNISEh////av0XKwAAAEx0Uk5T////////////////////////////////////////////" +
			"////////////////////////////////////////////////////////AJ6N5MwAAACKSU" +
			"RBVHjaLM1FEgMxEEPRDjMzMzMz2eOxo77/feJFtHhVWn1i5mrkM788X8zE71C/6yPvtl2y" +
			"J+eXEpBy4AnT7Oa6LmCJr+j8cBwHsIx6lIwZYwCLWdK4o7UGLLpA1933v1qTghWlFGAZro" +
			"n3UyEAIU71CfGhdY8uNsVEqmyjzI1sIH9MZ5h/AgwARU4ePSeNokAAAAAASUVORK5CYII=";

	var srcTrashcan = 
			"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAYAAABWdVznAAAA" + 
			"BGdBTUEAALGPC/xhBQAAABp0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjUuMTFH80I3AA" +
			"AAYUlEQVQoU81QQQ7AIAgbL1/2P/5UqaCRbJMdR2KI0lpaAXCsdYnMhxOQNOSFBEOQtj/E" +
			"dUTnfCjHBaH6Peaq6jKlihEIvhPGeg+dYF9pVfgh4S3dMJ09FNFm02Z8xLbrVGidPIi+sz" +
			"atwQAAAABJRU5ErkJggg==";		

	
	// Styles for output div.
	GM_addStyle(".tpdivstyle { border: 2px solid slateblue; " +
			"margin-top: 10px; padding: 6px 18px 6px 18px; " +
			"margin-bottom: 10px; padding: 6px 18px 6px 18px; " +
			"border-radius: 15px; background-color: rgb(244, 244, 244); display:table; }");
	
	GM_addStyle('.dropshadow_se { box-shadow:4px 4px 6px 2px rgba(80, 80, 80, .5)};');			

	GM_addStyle(".tpheader { color:DarkRed; font-size:110%; font-weight: bold; }" );
	
	GM_addStyle(".tpheaderul:hover { text-decoration:underline; cursor:pointer}" );
	
	GM_addStyle(".tpdetail { color:MidnightBlue; font-size:smaller; }" );

	GM_addStyle(".warn { margin-left:34px; margin-right:4px; vertical-align:text-bottom; }");
	
	GM_addStyle(".nowarn { margin-left:50px; }");

	GM_addStyle(".trash { margin-right:8px; vertical-align:text-bottom; }" );
	GM_addStyle(".trashpointer:hover { cursor:pointer; }" );
	
	GM_addStyle(".strikethru { text-decoration:line-through; }" );

	
	
	
	

	// ==================================================================================== //
	//  If bookmark list displayed.
	// if (document.getElementById('ctl00_ContentBody_ListInfo_MassPublish')) {
	if (document.getElementById('ctl00_ContentBody_ListInfo_btnMassPublish')) {

		var lnkEdit = document.getElementById('ctl00_ContentBody_lbHeading');
		if (lnkEdit) {
			lnkEdit = lnkEdit.childNodes[1];
			if (lnkEdit.nodeName == 'A') {
				var tzListID = UrlParm('ListID', true, lnkEdit.href);
				sessionStorage.setItem('TZ_ListID', tzListID);
			}
		}	
	
		sessionStorage.removeItem('timekey');
		var cacheArr = fGetCachesList();
		fCreateList(true);
		window.addEventListener('storage', fStorageChanged, false);
		
	}
	// ==================================================================================== //
	
	
	
	// ==================================================================================== //
	//  If entry for timed publishing data for selected caches.
	if (document.getElementById('ctl00_ContentBody_MassCachePublish_PublishNow')) {
	
		fSubmitSelectedCaches();
	}
	// ==================================================================================== //
	
	
	
	






	// ==================================================================================== //
	//                                                                                      //
	//                                 First Level Functions                                //
	//                                                                                      //
	// ==================================================================================== //


	
	//  Create list display. Pass true or false, for expanded or collapsed mode. 
	function fCreateList(expanded) {	

		//	If div already exists, remove.
		var divNFP = document.getElementById('divNFP');
		if (divNFP) {
			removeNode(divNFP);
		}
		
		//  Get insertion point.
		var ctl00_ContentBody_lbHeading = document.getElementById('ctl00_ContentBody_lbHeading');
		var hp = ctl00_ContentBody_lbHeading.parentNode;
		
		//  Create output div.
		divNFP = document.createElement('div');
		divNFP.id = 'divNFP';
		divNFP.classList.add('tpdivstyle');
		divNFP.classList.add('dropshadow_se');

		insertAfter(divNFP, hp)

		var idx = 0;
		jsonString = localStorage.getItem(TIMED_PUBLISHING_TRACKING_KEY);
		if (jsonString) {
			var cleanupdone = false;
			var timePubDb = JSON.parse(jsonString);
			for (var i in timePubDb) {
				if (timePubDb.hasOwnProperty(i)) {
					if (timePubDb[i]['caches'].length) {
						var lnkExpand = document.createElement('a');
						lnkExpand.setAttribute('idx', idx);
						if (!expanded) { lnkExpand.classList.add('expand'); }
						lnkExpand.href = 'javascript:void(0)';
						lnkExpand.style.marginRight = '8px';
						var imgExpand = document.createElement('img');
						imgExpand.src = (expanded) ? srcToggleCollapse : srcToggleExpand;
						imgExpand.style.marginTop = '4px';
						imgExpand.style.verticalAlign = 'text-bottom';					
						lnkExpand.appendChild(imgExpand);
						var tptzname = timePubDb[i]['tptzname'];
						var tpdt = timePubDb[i]['tpdt'];
						var tpoffset = timePubDb[i]['tpoffset'];
						var tpdtSeg = tpdt.split(',');
						var tzLine = tptzname + ', ' + tpdtSeg[0] + '-' + tpdtSeg[1] + '-' + tpdtSeg[2] + 
								 ' ' + tpdtSeg[3] + ':' + tpdtSeg[4] + ' (UTC ' + tpoffset + ') ';
						var tzSpan = document.createElement('span');
						tzSpan.title = 'Submit this group of caches for Timed Publishing.';
						tzSpan.classList.add('tpheader');
						tzSpan.classList.add('tpheaderul');
						tzSpan.setAttribute('timekey', timePubDb[i]['tptzid'] + 
								'-' + timePubDb[i]['tpdt']);
						tzSpan['refids'] = [];
						divNFP.appendChild(lnkExpand);
						var tzTxNode = document.createTextNode(tzLine);
						tzSpan.appendChild(tzTxNode);
						divNFP.appendChild(tzSpan);
						divNFP.appendChild(document.createElement('br'));
						tzSpan.addEventListener('click', fHeaderClicked, false);
						lnkExpand.addEventListener('click', fToggleExpand, false);
					
					
						var detHide = 'detHide' + idx;
						var detShow = 'detShow' + idx;
						GM_addStyle("." + detHide + " { display:none; }" );
						GM_addStyle("." + detShow + " { display:visible; }" );
					
						for (var j in timePubDb[i]['caches']) {
							var wpid = timePubDb[i]['caches'][j]['wpid'];
							var refid = timePubDb[i]['caches'][j]['refid'];
							var cacheTitle = timePubDb[i]['caches'][j]['cacheTitle'];
							var tzRow = document.createElement('row');
							var tzCell = document.createElement('cell');
							tzRow.appendChild(tzCell);
							
							tzSpan['refids'].push(refid);
							
							if (cacheArr.indexOf(wpid) < 0) {
								tzWarn = document.createElement('img');
								tzWarn.src = srcWarnMissing;
								tzWarn.classList.add('warn');
								tzWarn.title ='This cache not found in bookmark list.\n' +
										'You may need to increase the list size,\n' +
										'or refresh the bookmark list page.';
								tzCell.appendChild(tzWarn);
							} else {
								tzNoWarn = document.createElement('span');
								tzNoWarn.classList.add('nowarn');
								tzCell.appendChild(tzNoWarn);
							}
							
							tzTrash = document.createElement('img');
							tzTrash.src = srcTrashcan;
							tzTrash.classList.add('trash');
							tzTrash.classList.add('trashpointer');
							tzTrash.setAttribute('wpid', wpid);
							tzTrash.setAttribute('cacheTitle', cacheTitle);
							tzTrash.title = 'Click to remove from list.\n' +
									'This cannot be reversed.';
							tzTrash.addEventListener('click', fTrashClicked, false);
							tzCell.appendChild(tzTrash);
							
							
							tzRow.classList.add('tpdetail');
							tzRow.classList.add( (expanded) ? detShow : detHide);
							tzRow.classList.add('idx' + idx);
							tzRow.setAttribute('idx', idx);
							
							var tzRevLink = document.createElement('a');
							tzRevLink.href = '../admin/review.aspx?wp=' + wpid;
							tzRevLink.target = '_blank';
							tzRevLink.title = 'Open Review Page in a new tab.';
							tzRevLink.appendChild(document.createTextNode(wpid));
							
							tzLine = ' - ' + cacheTitle;
							tzTxNode = document.createTextNode(tzLine);
							tzCell.appendChild(tzRevLink);
							tzCell.appendChild(tzTxNode);
							tzCell.appendChild(document.createElement('br'));						
							divNFP.appendChild(tzRow);
						}
						idx++;
						
					//  If no caches, then clean up.
					} else {
						delete timePubDb[i];
						cleanupdone = true;
					}
				}
			}
			
			//  If any key entries removed, then update storage.
			if (cleanupdone) {
				jsonString = JSON.stringify(timePubDb);
				localStorage.setItem(TIMED_PUBLISHING_TRACKING_KEY, jsonString);
			}
			
		}
		
		
		
		//  If nothing to display, remove div.
		// if (idx == 0) { removeNode(divNFP); }
		if (idx == 0) {
			divNFP.appendChild(document.createTextNode(
					'No Timed Publishing Entries Are Pending Submission'));
		}
		
	}

	
	//  Expand or collapse a list of caches.
	function fToggleExpand() {
		var idx = this.getAttribute('idx');
		var detail = 'detail' + idx;
		this.classList.toggle('expand');
		if (this.classList.contains('expand')) {
			this.firstChild.src = srcToggleExpand;
		} else {
			this.firstChild.src = srcToggleCollapse;
		}
		var ns = document.getElementsByClassName('idx' + idx);
		var ic = ns.length;
		for (var i = 0; i < ic; i++) {
			ns[i].classList.toggle('detHide' + idx);
			ns[i].classList.toggle('detShow' + idx);
		}
	}

	
	//  Time/date header clicked. Select checkboxes and click bulk publishing button.
	function fHeaderClicked() {
		var refids = [];
		refids = this['refids'];
		var cbl = document.getElementsByName('BID');
		var ic = cbl.length;
		for (var i = 0; i < ic; i++) {
			cbl[i].checked = (refids.indexOf(cbl[i].getAttribute('value')) >= 0);
		}
		//  Save key value to session storage.
		sessionStorage.setItem('timekey', this.getAttribute('timekey'));
		//  Click submission button.
		// document.getElementById('ctl00_ContentBody_ListInfo_MassPublish').click();
		document.getElementById('ctl00_ContentBody_ListInfo_btnMassPublish').click();
	}
	

	//  If a trashcan icon clicked, confirm and remove.
	function fTrashClicked() {
		if (!this.parentNode.classList.contains('strikethru')) {
			var resp = confirm('Are you sure you want to remove\n' +
					this.getAttribute('wpid') + ' - ' + this.getAttribute('cacheTitle') + '\n' +
					'from the list? This is not reversable.');
			if (resp) {
				this.classList.remove('trashpointer');
				this.parentNode.classList.add('strikethru');
				this.style.opacity = '.25';
				var wpid = this.getAttribute('wpid');
				fRemoveCache(wpid);
			}
		}
	}

	
	//  Removes a cache based on passed waypoint ID.
	function fRemoveCache(wpid) {
		jsonString = localStorage.getItem(TIMED_PUBLISHING_TRACKING_KEY);
		if (jsonString) {
			var timePubDb = JSON.parse(jsonString);
			
			s1: 
			for (var i in timePubDb) {
				if (timePubDb.hasOwnProperty(i)) {
					for (var j in timePubDb[i]['caches']) {
						if (timePubDb[i]['caches'][j]['wpid'] == wpid) {
							timePubDb[i]['caches'].splice(j,1);
							break s1;
						}
					}
				}
			}
			var jsonString = JSON.stringify(timePubDb);
			localStorage.setItem(TIMED_PUBLISHING_TRACKING_KEY, jsonString);
		}
	}

	
	






	// ==================================================================================== //
	//                                                                                      //
	//                                Second Level Functions                                //
	//                                                                                      //
	// ==================================================================================== //

	
	
	function fSubmitSelectedCaches() {
		
		var arMonths = new Array('January','February','March','April','May','June',
				'July','August','September','October','November','December');
		
		
		//  Get time key value.
		var timekey = sessionStorage.getItem('timekey');
		
		var jsonString = localStorage.getItem(TIMED_PUBLISHING_TRACKING_KEY);
		if (jsonString) {
			var timePubDb = JSON.parse(jsonString);
			var tptzname  = timePubDb[timekey]['tptzname'];
			var tpdt      = timePubDb[timekey]['tpdt'];
			var tpoffset  = timePubDb[timekey]['tpoffset'];
			
			//  Split out date and time elements.
			var dtar       = tpdt.split(',');
			var sav_year   = dtar[0];
			var sav_mon    = dtar[1] - 1;
			var sav_day    = dtar[2];
			var sav_hour   = dtar[3];
			var sav_min    = dtar[4];
			var datestring = sav_day + ' ' + arMonths[sav_mon] + ' ' + sav_year;
			
			//  Set publish date.
			var dateentry = document.getElementById('ctl00_ContentBody_MassCachePublish_DateTimePublished');
			dateentry.value = datestring;
			
			//  Set publish time.
			var timeentry = document.getElementById('ctl00_ContentBody_MassCachePublish_HourToPublish');
			timeentry.value = sav_hour + sav_min;

			//  Set UTC offset.
			var e_TimeZonesDDL = document.getElementById("ctl00_ContentBody_MassCachePublish_TimeZonesDDL");
			if (e_TimeZonesDDL.value != tpoffset) {
				e_TimeZonesDDL.value = tpoffset;
			}
		}
		
		jsonString = localStorage.getItem(JSON_TIMED_PUBLISH_PREFS_KEY);
		if (jsonString) {
			var timePubPrefs = JSON.parse(jsonString);
			var logText      = timePubPrefs['LogText_' + SignedInAs];
			var dayNames     = timePubPrefs['DayNames_' + SignedInAs];
			var monthNames   = timePubPrefs['MonthNames_' + SignedInAs];
			var dayNamesAr   = dayNames.split(',');
			var monthNamesAr = monthNames.split(',');
			var autoSubmit   = timePubPrefs['AutoSubmit_' + SignedInAs];
			
			//  Create replacement values.
			var monthname = monthNamesAr[sav_mon].trim();
			var monthnum  = sav_mon + 1;
			var weekday   = dotw(sav_year, sav_mon, sav_day, true);
			var dayname   = dayNamesAr[weekday].trim();
			var daynum    = sav_day - 0;
			var h12time   = toAmPm(sav_hour, sav_min);
			var h24ztime  = pad(sav_hour, 2) + ':' + sav_min;
			var h24time   = (sav_hour - 0) + ':' + sav_min;
			var timezone  = tptzname;

			//  Perform text replacemnt.
			logText = logText.replace(/%monthname%/gi, monthname);
			logText = logText.replace(/%monthnum%/gi,  monthnum);
			logText = logText.replace(/%dayname%/gi,   dayname);
			logText = logText.replace(/%daynum%/gi,    daynum);
			logText = logText.replace(/%h12time%/gi,   h12time);
			logText = logText.replace(/%h24time%/gi,   h24time);
			logText = logText.replace(/%h24ztime%/gi,  h24ztime);
			logText = logText.replace(/%timezone%/gi,  timezone);
			
			//  Put final text into log entry textarea. 
			var e_tbLogInfo = document.getElementById("ctl00_ContentBody_MassCachePublish_tbLogInfo");
			e_tbLogInfo.value = logText

		}

		
		//  Auto Submit, if selected in preferences.
		if (autoSubmit) {
			var submitButton = document.getElementById('ctl00_ContentBody_MassCachePublish_LogButton');
			if (submitButton) { submitButton.click(); }
		}
			
	}


	//  Convert 24 hour to 12 hour am/pm.
	function toAmPm(hr, min) {
		var h = hr - 0;
		var m = min - 0;
		switch(h) {
			case 0:
				var ap = (m) ? 'am' : 'm';
				var h = 12;
				break;
			case 12:
				var ap = (m) ? 'pm' : 'n';
				break;
			default:
				ap = 'am';
				if (h >= 12) { ap = 'pm'; }
				if (h > 12) { h -= 12; }
		}
		var rtnval = h.toString();
		rtnval += (m) ? ':' + min + ap : ap;
		return rtnval;
	}

	
	//  Pass year, month, day, and t/f for zero indexed month and return day of the week (0=Sun, 6=Sat).
	function dotw(yy, m, d, zeroMonth) {
		if (!zeroMonth) { m-- ; };
		var d = new Date(yy, m, d);
		var rtnvar = d.getDay();
		return rtnvar;
	}


	//  Pad a number with leading zeros.
	function pad(number, length) {
		var str = "" + number;
		while(str.length < length) {
			str = '0' + str;
		}
		return str;
	}


	//  Return a specific preference setting value, appendSig is t/f value for appending profile name.
	function fGetPrefs(pref, appendSig) {
		jsonString = localStorage.getItem(JSON_TIMED_PUBLISH_PREFS_KEY);
		if (jsonString) {
			var timePubPrefs = JSON.parse(jsonString);
			var jsonKey = pref;
			if (appendSig) { jsonKey = jsonKey + '_' + SignedInAs; }
			var rtnvar = timePubPrefs[jsonKey];
		}
		return rtnvar;
	}






	// ==================================================================================== //
	//                                                                                      //
	//                                  General Functions                                   //
	//                                                                                      //
	// ==================================================================================== //
	
	
	//  Process local storage changes.
	function fStorageChanged(e) {
		if (e.key == TIMED_PUBLISHING_TRACKING_KEY) {
			fCreateList(true)
		}
	}

	
	//  Return array with list of waypoint IDs.
	function fGetCachesList() {
		var rtnAr = new Array();
		var cbl = document.getElementsByName('BID');
		var ic = cbl.length;
		for (var i = 0; i < ic; i++) {
			var wptnum = cbl[i].getAttribute('value');
			rtnAr.push(IDToWpt(wptnum));
		}
		return rtnAr;
	}	
			

	//  Convert ID number to waypoint.
	function IDToWpt(idNum) {
		var BASE_GC = "0123456789ABCDEFGHJKMNPQRTVWXYZ";
		var BASE_31 = "0123456789ABCDEFGHIJKLMNOPQRSTU";
		var WptWork, Wpt;
		idNum = idNum - 0;
		if (idNum <= 65535) {
			Wpt = idNum.toString(16);
			Wpt = 'GC' + Wpt.toUpperCase();
		} else {
			idNum = idNum + 411120;
			WptWork = idNum.toString(31);
			WptWork = WptWork.toUpperCase();
			Wpt = 'GC';
			for (var i = 0; i < WptWork.length; i++) {
				Wpt += BASE_GC.substr(BASE_31.indexOf(WptWork.substr(i, 1)), 1);
			}
		}
		return Wpt;
	}
			






	//  Returns a URL parameter.
	//    ParmName - Parameter name to look for.
	//    IgnoreCase - (optional) *false, true. Ignore parmeter name case.
	//    UrlString - (optional) String to search. If omitted, document URL is used.
	function UrlParm(ParmName, IgnoreCase, UrlString) {
		var RegRslt, sc = '', RtnVal = '';
		if (IgnoreCase) {sc = 'i'}
		if(UrlString) {
			var PageUrl = UrlString;
		} else {
			PageUrl = document.location + '';
		}
		var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
		var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
		RegRslt = RegEx1.exec(ParmString);
		if (RegRslt) {RtnVal = RegRslt[2]}
		return RtnVal;
	}

	
	//  Remove element and everything below it.
	function removeNode(element) {
		element.parentNode.removeChild(element);
	}	


	//	Insert element after an existing element.
	function insertAfter(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement.nextSibling);
	}

	//  Insert element ahead of an existing element.
	function insertAheadOf(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement);
	}
