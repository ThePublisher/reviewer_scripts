﻿// ==UserScript==
// @name           GCR Timed Publishing 4 Results Page v2
// @description    Timed Publishing 4 - Results Page
// @version        02.02
// @scriptGuid     49f15fab-c017-401c-8cca-756a398b47b3
// @namespace      http://www.geocaching.com/admin
// @grantZZZ       GM_info
// @grantZZZ       GM_getValue
// @grantZZZ       GM_openInTab
// @grantZZZ       GM_setValue
// @grantZZZ       GM_xmlhttpRequest
// @grantZZZ       GM_addStyle
// @grantZZZ       GM_listValues
// @grantZZZ       GM_registerMenuCommand
// @grantZZZ       GM_setClipboard
// @grantZZZ       GM_deleteValue
// @grantZZZ       GM_getResourceText
// @grantZZZ       GM_getResourceURL
// @grantZZZ       GM_log
// @include        http*://*geocaching.com/admin/CachePublish.aspx?IDS=*
// @icon           http://i.imgur.com/GP6D2vX.png
// ==/UserScript==
/* 

Release log:
* v02.02 2017-08-22 - Fix SignedInAs.

* v02.01 2017-08-17 - SignedInAs fix for new header release.

* v02.0 2017-08-02 - Version reset to 2.0 and name change to add v2.

* v01.15 2016-06-30 - Fix for site change.

* v01.14 2016-06-29 - Fix for site change.

* v01.13 2015-03-31 - Fix for site change.

* v01.12 2015-02-01 - Fix for Firefox 35 security change.

* v01.11 2014-11-09 - Bug fix.

* v01.10 2014-11-08 - Update release number.

* v01.00 2014-04-04 - Initial release.

 */

	//  Local Storage keys:
	//  Local Storage key for JSON tracking data.
	const TIMED_PUBLISHING_TRACKING_KEY = 'c8a7a86e-ced2-4667-b01b-cb4698a7e06a';
	//	Key for user last date.
	const JSON_DATES_LS_KEY 	        = '2118dd9a-2785-47b6-bc84-4ef54b330358';
	//	Key for prefs across scripts.
	const JSON_TIMED_PUBLISH_PREFS_KEY 	= '9646db26-9d83-4743-b589-1de0d67bd352';

	
	//  Get currently signed-on geocaching.com profile.
	var SignedInX = document.getElementsByClassName('user-name')[0];
	if (SignedInX) {
		var SignedInAs = SignedInX.firstChild.data.trim();
	} else {
		SignedInX = document.getElementById("ctl00_LogoutUrl");
		var SignedInAs =  SignedInX.parentNode.childNodes[1].firstChild.data.trim();
	}

	if (SignedInAs) {
		SignedInAs = SignedInAs.replace(/<\&amp;>/g, '&');
	} else { return; }
	

	//  Add link back to timed publishing bookmark page.
	var breadcrumbs = document.getElementById('ctl00_Breadcrumbs');
	if (breadcrumbs) {
		var bmName = fGetPrefs('BookmarkName', true)
		var tpSpan = document.createElement('span');
		var tpLink = document.createElement('a');
		tpSpan.style.marginLeft = '30px';
		tpLink.id = 'tpLink';
		tpLink.href = 'javascript:void(0)';
		tpLink.appendChild(document.createTextNode('Open Bookmark: '+ bmName));
		tpSpan.appendChild(tpLink);
		breadcrumbs.appendChild(tpSpan);
		tpLink.addEventListener('click', fTpLinkClicked, false);
	}


	//  Display break message if any errors detected.
	if (document.getElementById('ctl00_ContentBody_MassCachePublish_ValidationSummary1')) {
		alert('Error encountered with one or more caches.')
	}

	
	//  Generate list of all processed caches, and remove from database.
	var PathSearch = ".//a[starts-with(@id, " +
			"'ctl00_ContentBody_MassCachePublish_PublishedCacheList') " + 
			"and (contains(@id, 'HyperLink1'))]";
	var aCacheList = document.evaluate(PathSearch, document, null,
			XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,	null);
	var ic = aCacheList.snapshotLength;
	for (var i = 0; i < ic; i++) {
		var aCache = aCacheList.snapshotItem(i);
		var wpid = aCache.firstChild.data;
		fRemoveCache(wpid);
	}


	//  Get a preference value from local storage.
	function fGetPrefs(pref, appendSig) {
		jsonString = localStorage.getItem(JSON_TIMED_PUBLISH_PREFS_KEY);
		if (jsonString) {
			var timePubPrefs = JSON.parse(jsonString);
			var jsonKey = pref;
			if (appendSig) { jsonKey = jsonKey + '_' + SignedInAs; }
			var rtnvar = timePubPrefs[jsonKey];
		}
		return rtnvar;
	}

	
	//  Timed Publishing Bookmark link clicked.
	function fTpLinkClicked() {
		var bmGuid = UrlParm('guid', true, fGetPrefs('BookmarkPageUrl', true));
		bmPageUrl = '../bookmarks/view.aspx?guid=' + bmGuid;
		location.assign(bmPageUrl);
	}

	
	//  Removes a cache based on passed waypoint ID.
	function fRemoveCache(wpid) {
		jsonString = localStorage.getItem(TIMED_PUBLISHING_TRACKING_KEY);
		if (jsonString) {
			var timePubDb = JSON.parse(jsonString);
			
			s1: 
			for (var i in timePubDb) {
				if (timePubDb.hasOwnProperty(i)) {
					for (var j in timePubDb[i]['caches']) {
						if (timePubDb[i]['caches'][j]['wpid'] == wpid) {
							timePubDb[i]['caches'].splice(j,1);
							break s1;
						}
					}
				}
			}
			var jsonString = JSON.stringify(timePubDb);
			localStorage.setItem(TIMED_PUBLISHING_TRACKING_KEY, jsonString);
		}
	}


	//  Returns a URL parameter.
	//    ParmName - Parameter name to look for.
	//    IgnoreCase - (optional) *false, true. Ignore parmeter name case.
	//    UrlString - (optional) String to search. If omitted, document URL is used.
	function UrlParm(ParmName, IgnoreCase, UrlString) {
		var RegRslt, sc = '', RtnVal = '';
		if (IgnoreCase) {sc = 'i'}
		if(UrlString) {
			var PageUrl = UrlString;
		} else {
			PageUrl = document.location + '';
		}
		var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
		var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
		RegRslt = RegEx1.exec(ParmString);
		if (RegRslt) {RtnVal = RegRslt[2]}
		return RtnVal;
	}


