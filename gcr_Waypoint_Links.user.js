﻿// ==UserScript==
// @name           GCR Review Waypoint Links v2
// @description    Adds links to waypoint coordinates to open nearby caches and maps.
// @namespace      http://www.geocaching.com/admin
// @version        02.01
// @icon           http://i.imgur.com/a94PYHJ.png
// @updateURL      https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Waypoint_Links.user.js
// @downloadURL    https://gitlab.com/geocachinghq/reviewer_scripts/raw/master/gcr_Waypoint_Links.user.js
// @include        http*://*.geocaching.com/admin/review.aspx?*
// @grant          GM_getValue
// @grant          GM_setValue
// @grant          GM_registerMenuCommand
// ==/UserScript==

/*
Function:
 Adds links to waypoint coordinates to open nearby caches and maps.

*/

	// Distance to get nearby caches.
	var dist = 5;

	// Get currently signed-on geocaching.com profile.
	var e_LogIn = document.getElementById("ctl00_LoginUrl");
	if (e_LogIn.firstChild.data.trim() != 'Log out') { return; }
	SignedInAs = e_LogIn.parentNode.childNodes[1].firstChild.data.trim();
	SignedInAs = SignedInAs.replace(/<\&amp;>/g, '&');

	// Get current domain.
	var domain = thisDomain();

	var GoogleImg =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAA" +
		"AXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAH" +
		"UwAADqYAAAOpgAABdwnLpRPAAAAwBQTFRFAAAAAncfA3YxAHo0AHs8AHs9AHo%2BAH0%2B" +
		"Anw%2FHXAgOmUtAHlDAHpBAHxAHHhJCEqeEUiYEE%2BcBEyhAFO5AFS7AFe5Ali9Dl25FV" +
		"aoHFWkDmK8IkmOJ16fOF6dIF%2BmJ1miJGy2KGy1O3y%2BAFnBAlrEAl3GAGPIAmPMAmbI" +
		"AGbPBGTKQm%2BmVn6yAIIsAY0qAY0tAYM7AIE%2BA4I%2FAIY5AIU8AIQ9AIU%2BAIY8AI" +
		"Y9AYc%2BAIk0AIk4Aos5AIg8Aok9AYo9AI07AI84A4w8AI89DYY%2FCYk2AJQtAJU3AJY3" +
		"AJA5AZE6AJA7AJM4AJI7AJM%2FAJQ4AZY5Apc7AZQ8AJY8A5Y9AJk3AJs1AJo3A5g6A5o7" +
		"AoBAAoVBAYdCBYFDHYVKJZFVJppRRqhrSaZtUIO4V47OWZDIbpDAiisPsSYRtiARwhkGzB" +
		"AF1QEA2gID3wAAxykowz864AIA5QAA5wwK6wEA7gAA7AAE8wEA8AAD9gEA9AAE%2BQAB%2" +
		"BgIC%2FQAA%2FQAC%2FQAD%2FAMA3VNQ4EZG2KIb37AKzKY3zrA41qkq4q4C7KcM6qoA47" +
		"UA77QA8aoe8LIP8rUP8bkM9r4J8LEW8bIV9LMb97kQ56sv27BK37df77ZN77ZP8bFF%2Fs" +
		"UC%2B8MK68V87sR8hbiZmbGbhKK%2BgqnSg6%2FShqzRprvQoL%2FerL7Wqcq3v9rJuc%2" +
		"Fk3s%2BW9s6T6N2wzNviz9ru0eHe1%2Bjg2%2BHt2uLt3Oby3%2Bjx9tTV8%2BnQ8ezZ8u" +
		"ze%2BvDV4ezk6%2B796%2FLr6PH27%2FL59%2FPq%2BfHv%2B%2FTs9vv%2B%2BPv0%2Bf" +
		"v2%2F%2Fnz%2B%2Fv7%2Bvz7%2B%2Fz%2B%2B%2Fz%2F%2F%2Fj%2F%2FPv%2F%2F%2F%2" +
		"F7%2F%2F3%2B%2Fvz%2F%2Fv78%2Fv%2F9%2Ff7%2B%2Fv7%2FAAAAAAAAAAAAAAAAAAAA" +
		"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" +
		"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" +
		"AAAAAAAAAAAAAD%2F4ZgAAAQB0Uk5T%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%" +
		"2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2" +
		"F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F" +
		"%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%" +
		"2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2" +
		"F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F" +
		"%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%" +
		"2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2" +
		"F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F" +
		"%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%" +
		"2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2" +
		"F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F" +
		"%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%" +
		"2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2" +
		"F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F%2F" +
		"AFP3ByUAAAAZdEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjVJivzgAAABG0lEQVQoUw" +
		"EQAe%2F%2BAP%2FLrx4SpdHCcGx8fX55df8AzdRjFScatdG3c31%2FfXl2cQDUyCAmKSOk" +
		"1NCCe4B7eHRuANDQIiUoJGTO1IF6fX93cm0A0tKjEyoWZdPMb2poaWtxbACz1L8hFBer1K" +
		"EJR1RNRQpnAByoz8CntNOgLVlYSFBKTlwAGBErotHOqgFWVVdSUkE%2FOQAfGQ8QttO8YE" +
		"ZQUU9NQz87ABsdLGaw09K%2BYS9TTU1DPzUApr3Uu66swdTJYi5MSzw9NADQuIaEi4iHud" +
		"GyRElCPjYxAMWFjJydkYqX09BfOkA4XQcAuoOVkI%2BOlJbHxF4zWzdaBQDKmImSk5SNn9" +
		"SpAjAyBwYLAP%2FDnpqbma3GsQ4DCA0EDP97pHTsFM3pBwAAAABJRU5ErkJggg%3D%3D";

	var GoogleDownImg =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAAQCAYAAADqDXTRAAAAAX" +
		"NSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAA" +
		"DqYAAAOpgAABdwnLpRPAAAABh0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjEwcrIlkgAAA4" +
		"RJREFUSEudVG1ojWEYfrBjKx9jH8zasWO2xQgjHyEkKf74oWZtikgpheKX5KNQovZDKX6g%2" +
		"BEOKFQvREJYzGw6NNV9r7MPXOTs757znvOe8z%2BW637cNO0U7T%2B%2F9vs%2FzvM9zX%2F" +
		"d13ffzDEvf2o1167eoVFpQ%2BVW6UaD0yICKuEaprHiPiuoMpdLG809Q5YyIKWPYGOWK96r2" +
		"ES6VqdMcmNnVb5FKi5jf7G3xsAGNDnzjO87x5wjQ%2FvE7guwHAlHELHY4BzMK9MbwbFYRVO" +
		"5GmRl6owuI%2FaS9IcKBcz2o2n4NxYtrkT%2F3CdIW1KJ88y3suupH%2FYcAQujjhk40lyqC" +
		"bn0%2BdETu0IT8QQoPwsDqvV5kz7uD9yTfw38JWjft6RugYNFJTJ%2BzFz8lOuK%2BHJcO5d" +
		"noTRG0w2a67ehbTJ1%2FBl9jEkgHTO0nqoWoyEr0pnd98PpewxDEWB980zKh3Nu7UgINM4GH" +
		"z3zExCWn8Ko1RqcidycC7EXRzncvZDJuOuBhji3q0FRGeadUihBDaAYLR2tYtF1neyjfTfFI" +
		"xyaZhlgwmsCsGXEZCtnFJfMGkwEzgcacsVCZCxpsRMuiHnTEzj9Ny3%2B7gCwU7byNgqpamw" +
		"Esii2shLIkVSzMQLg8wdyH8JXyxvE%2BLwtKldxzIh8M1k9%2B0LyOSiYhcaO0uhEFa65TV6" +
		"IFIjajIPMpwFJosEwyFSLyMLBoK1o8s6FcM%2B4PaKsrK8EwYI0ebX%2F7rX%2BsPR46FCp2" +
		"IWLlzjaootOIxikrgzNsTakqrZvsHrV1wMsK87FsvK%2BCNnDDVOa0aHPr74T6%2FdB5eX8B" +
		"CrDOyHDm6usH1oZ5Hey71AJRan9NA%2BWOgAXMyhGWETxs1ihfWIvcsisomVeHYzUvWNQGfM" +
		"WToAornMgHmjj%2Bk%2BXw4fZY79799zqOuqigZ9ZjZGadx12eScMRk1QpPnPKFOLFFyC38B" +
		"AuXm6yj1RzDkEnbWpLcoaDBx0gl8v5rliRtEbyF8cnfOpMYFrpCYyacAnHa1rRRboJiu%2Bn" +
		"Sd6v%2BUyo7ApcqXtog7YUjuGNtMOXDMq8WcuWwXK7oSdPJqXks%2BznrggLROwLXxv23EV%" +
		"2BSQ2yZ15AdvljTFhej7FljXCvvIEtRy7zTnau25eFbqiF1S3JoDLD%2FNqt%2Fzt4Fe9bif" +
		"x%2FLWLaZ8a5qAncVpIPtfTmKlTUrR2yyb5U7F2%2Bwi%2Bq5NvL0ahP%2FgAAAABJRU5Erk" +
		"Jggg%3D%3D";

	var GoogleUpDownImg =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAAQCAYAAADqDXTRAAAAAX" +
		"NSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAA" +
		"DqYAAAOpgAABdwnLpRPAAAABh0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjEwcrIlkgAAA8" +
		"9JREFUSEudVF1oVEcUHk22G9CYND8aQ9bdRhPStGhSqZZWrEgp2Jc%2BFNJghAalEKioUJ9K" +
		"qRVUKBb2oVCwD02hviiCBjRUsaRKbXDTJLqWaIhWgzU%2F2mY3m929u3f3zud35proZqWlOe" +
		"y5O3fuzPnO950zs8i7awLvf7BTLcRiKqK8Vo3SL0RV0rNElWUmVUoXKVX4Ir%2FEVEVBWlmL" +
		"ipUnM61GCzyqRBe6MOvabmEhlrQfmW2ZhAWN%2B3jEZ4bvfyWB0bt%2FI8ZxNJpC2uGAc7BT" +
		"wHQav6%2BtharcITP5poNB6N27oTs6nvudISA%2BRb9JhC%2B%2Bn8T2jtNY82YXql%2F7DY" +
		"UbutDc%2FhP2noqg588o4pjhhjEM1CuC7hrMDzo4CMfrBZRyvbMzb40m5D%2BkcCkBvLs%2F" +
		"hPL1F3CH5Ce5MkufoF%2B9CdS88TVebtqPKcmOuNdLvVCBHaHcgBblamoyYNrvd%2F9LSwEm" +
		"8qyJpML048O3sPr1Y3iYhpHZ1hGiOkiJrETvvz2DUPgPWIKYnkG4oQTK1zGeG6y93QBli4uf" +
		"Mn0OcIIFPHjsLla89Q1uDKcZVOQeQ5SjFEb5nIZMZmwXPMF3hzr0N1Lel1pFCNf0E0CnoCAH" +
		"MOvxuIwbGhiISmgNh773u0nKd04iMrBNpnE2jCYwe0YCxuOmuWTeYjFgZ9FXsQyqZEOvAXQc" +
		"6sFAHPyra%2FlOm2LetXvOo2Z7l2EAh2ILK6EsRRVPMBEuz7L2cTykvBncqSpj%2FnU%2Fu5" +
		"nPB5ulP29ep6SSkLxR39aHmm1nqCvRoknDKMZ6CrA0GhybTIWI%2FJhYahhDgXVQnld%2BeS" +
		"pva6uR0Vm6NEfe2XcdCBh5xdgW2LpnBKr2W6QylJXJWUZTqkqfILtfR%2B4jxA4Ls21CN2IG" +
		"uHc1a1rbPjwHikgEuqoqB9DUsqjInevpmVub4HXw2fEhiFKfB3spdxJsYHaOsEzi8oBG88Yu" +
		"VDaeRN36bhwJXmNTWwivWQnlb3EznzMJPHs%2BhfXixW4T7duXu45v41QwsPYKSso6cZFn0n" +
		"LFJFWKz5qyhLj2AKj0f4kfT%2FSbIzVQQdCVH43kBcOBAy7QbNdu2ZK3RuqXwT3cG8uiof4o" +
		"liw%2Fjq%2BCwxgn3SzFj9Cl7qfDNlR5C052XzagQ%2F5i3kifhPNBWTdn82Y4Ph%2F0qlWk" +
		"lHuWZUOEnmSDiD%2Fg48NPL6K6LojyV39AefMVLH%2B7B8sa%2B%2BDbehY7D53gnexet9f9" +
		"PqiNbUP5oCaqhH3mf%2F4q3reS%2BX9Z0jZnxr2oCTxSVw216dw7aOl%2B73%2B77FuI365W" +
		"eAwV%2B6mpn%2FLFjAAAAABJRU5ErkJggg%3D%3D";

	var LockImg =
		"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8%2F9hAAAA" +
		"BGdBTUEAAK%2FINwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAA" +
		"J%2FSURBVDjLbVJBaxNBGH2bpEkTmxi1NTRKTZtoQUHEWz0Igj2I4kG9eVNQhEBO7bEHc%2B" +
		"yv8JAiHnr2B4gFqVrQRhObljQolBSTJqZJdnZmfbNr2rU68DEz33zfm%2FfejGHbNrxjaWlp" +
		"RCk1J6WcYZxkgPGTsWJZ1mIul%2FvlrTe8AIVC4Qqbl5PJ5GQsFoPP5wP36PV6qNfr2OIg0L" +
		"35%2Bfm1fwDYPMLDj%2Bl0OmOaJmq1Gjqdjr4dgUAAiUTCqSsWixvMXV5YWOjqvW%2BAxOSz" +
		"8fHxjBAC5XJ5s91up7gO6tDrUqn0QwOTXYZSsoO%2BwGDB5EwkEkGlUgGb7mSz2apHajWfz9" +
		"%2BsVqvFVCrl1P4PYExr5m16vYUjQ%2Bc0O11DtmN%2FebD95pG9UpnGzl7Y0Xz30ir8toAt" +
		"LdiWG0JIvFi76piaGG7g9plVTD%2F5YLgMCPLg%2Fg0YtMTwhznfApRBfsP6kAYJSKuN57Md" +
		"5oXTsvHy7aEEfZMutHZfIRAahWGMsHAICMeZVsD%2BHmTrG8zudyhrH%2BHJLGyz7wEgRSh9" +
		"k4nm%2BnvqPIb4xWuovV5k%2F2lMXJ9F8%2Bs6ARqIpk6QsIQtTC%2BAcGTYpBqfvgBfcJTu" +
		"KMi%2BxKfdMCZgIp6eRK8TYu2%2Bw2oA4PwDm%2B5qVK218XmNLN7xxILqKfS7pGqTWekLmu" +
		"VtV65STs8hA73RqJQQP5%2BCP3KKACamHj7FlGBDawfH00kEW0MuA8o9AmA6qMrSHqwTIAoM" +
		"08hAkHkN0ES3UYfotBGdiNFu5cr2AmgJobOPET7nhxEMuU%2Fo40soSjO7iHbbVNgnUen6pY" +
		"0%2FAOCTbC7PuV44H0f8Cetg5g9zP5aU7loDcfwGcrKyzYdvwUUAAAAASUVORK5CYII%3D";

	// Get cache data element.
	var CacheData = document.getElementById('ctl00_ContentBody_CacheDataControl1_CacheData');
	if (!CacheData) {
		CacheData = document.getElementsByClassName('CacheData')[0];
	}

	// Get decimal degree coordinates for this cache (grab from Yahoo maps link).
	var cLat = CacheData.getAttribute('data-latitude') - 0;
	var cLon = CacheData.getAttribute('data-longitude') - 0;

	// Get cache waypoint id.
	var WptID = CacheData.getAttribute('data-gccode')

	// Get Cache Title.
	var CacheName = CacheData.getAttribute('data-cachename').trim();

	// Code for cache type.
	var e_CacheDetails_imgType = document.getElementById("ctl00_ContentBody_CacheDetails_imgType");
	var CacheType1 = e_CacheDetails_imgType.title;
	if (CacheType1 == 'Earthcache') {
		CacheType1 = 'EC';
	} else if (CacheType1 == 'Mega-Event Cache') {
		CacheType1 = 'E';
	} else {
		CacheType1 = CacheType1.substring(0,1);
	}

	// Code for cache size.
	var e_CacheDetails_Container = document.getElementById("ctl00_ContentBody_CacheDetails_Container");
	var CacheSize1 = e_CacheDetails_Container.innerHTML;
	CacheSize1 = CacheSize1.substr(CacheSize1.indexOf('(') + 1, 1);

	// Get collection of waypoint icons.
	xPathSearch = "//img[starts-with(@src, location.protocol + '//" + domain + "/images/wpttypes/sm/')]";
	var CacheLinks = document.evaluate(
		xPathSearch,
		document,
		null,
		XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,
		null);
	ic = CacheLinks.snapshotLength;

	var altBorder = false;

	// Loop through waypoint coordinates and convert to links.
	for (var i = 0; i < ic; i++) {
		var xImg = CacheLinks.snapshotItem(i);
		if (xImg.parentNode.nodeName == 'TD') {
			var xRow = fUpGenToType(xImg, 'TR');
			var corCell = xRow.cells[6].childNodes[1];
			var corDecMin = corCell.firstChild.data.trim();
			xRow.cells[6].style.whiteSpace = 'nowrap';

			// First time through, add icons.
			if (i == 0) {
				var xTable = fUpGenToType(xRow, 'TABLE');

				// Get default value.
				radDefault = GM_getValue("AWRadiusDefault_" + SignedInAs, "");
				radDefault = radDefault.toLowerCase();

				// Find heading row of table.
				var hRow = xRow.parentNode.parentNode.childNodes[1].childNodes[1];
				var hCell = hRow.cells[6];

				// Create drop-down selector.
				var hDropdown = document.createElement("select");
				hDropdown.id = 'hDropdown';
				hDropdown.style.borderStyle = 'solid';
				hDropdown.style.borderWidth = 'thin';
				hDropdown.style.borderColor = 'rgb(7, 42, 102)';
				hDropdown.style.marginLeft = '12px';
				hDropdown.title = 'Radius distance';
				var hOption = document.createElement("option");
				hOption.value = '0,ft';
				hOption.style.fontStyle = 'italic';
				hCell.appendChild(hDropdown);
				hDropdown.appendChild(hOption);
				hOption.appendChild(document.createTextNode('-None-'));

				// Get user options.
				var hRadList = GM_getValue("CustomAWRadius_" + SignedInAs, '');
				var hRadList = hRadList.trim();
				if (!hRadList) { hRadList = '150,ft;528,ft'; }
				var hList1 = hRadList.split(';');
				for (var j = 0; j < hList1.length; j++) {
					var hList2 = hList1[j].split(',');
					var aOptValue = hList1[j].split(',');
					var hOption = document.createElement("option");
					aOptValue.splice(5);
					var hOptValue = aOptValue.join();
					hOption.value = hOptValue.replace(/\s/g,'').toLowerCase();
					if (hList2.length > 5) {
						var hOptText = hList2[5].trim();
					} else {
						hOptText = hList2[0].toLowerCase().trim();
						if (hList2.length > 1) {
							hOptText += ' ' + hList2[1].toLowerCase().trim();
						} else {
							hOptText += ' ft';
						}
					}
					hOption.appendChild(document.createTextNode(hOptText));
					hDropdown.appendChild(hOption);
					if (hOption.value == radDefault) {
						hOption.selected = true;
					}
				}

				// Radius default lock image.
				var hLockImg = document.createElement('img');
				hLockImg.align = 'absbottom';
				hLockImg.src = LockImg;
				hLockImg.style.marginLeft = '6px';
				hLockImg.style.marginBottom = '2px';
				hLockImg.border = 0;

				// Radius default lock link.
				hLockLnk = document.createElement('a');
				hLockLnk.name = 'gUpDn';
				hLockLnk.id = 'gUpDn';
				hLockLnk.title = 'Set current radius as default';
				hLockLnk.href = 'javascript:void(0);';
				hCell.appendChild(hLockLnk);
				hLockLnk.appendChild(hLockImg);
				hLockLnk.addEventListener("click", LockRadius, true);
			}

			// Make waypoint type image clickable.
			wptImgLnk = document.createElement('a');
			wptImgLnk.title = 'Click to toggle on/off';
			wptImgLnk.href = 'javascript:void(0);';
			xImg.border = 0;
			xImg.setAttribute('status', '1');
			xImg.parentNode.insertBefore(wptImgLnk, xImg);
			wptImgLnk.appendChild(xImg);
			wptImgLnk.addEventListener("click", ToggleStatus, true);

			// Get rid of degree symbol.
			var corDecMinX = corDecMin.replace(/\xB0/g,'');

			// Calculate decimal-degree value.
			var corParts = corDecMinX.split(' ');
			var dLat = (corParts[1] - 0) + (corParts[2] / 60);
			if (corParts[0] == 'S') { dLat *= -1 }
			dLat = Math.round(dLat * 1000000) / 1000000;
			var dLon = (corParts[4] - 0) + (corParts[5] / 60);
			dLon = Math.round(dLon * 1000000) / 1000000;
			if (corParts[3] == 'W') { dLon *= -1 }

			// Store coordinates.
			xImg.setAttribute('lat', dLat);
			xImg.setAttribute('lon', dLon);

			// Get elements for this waypoint.
			var wptPrefix = xRow.cells[3].childNodes[1].firstChild.data;
			var wptLookup = xRow.cells[4].firstChild.data;
			var wptName = xRow.cells[5].childNodes[1].firstChild.data;
			if (xRow.nextSibling.nextSibling.cells[2].hasChildNodes) {
				var wptNote = xRow.nextSibling.nextSibling.cells[2].innerHTML;
			} else {
				wptNote = '';
			}

			// Get waypoint type code.
			var wptAltText = xImg.alt;
			var wptTypeCode = '';
			if 		(wptAltText == 'Final Location') 		{ wptTypeCode = 'FL'; }
			else if (wptAltText == 'Parking Area') 			{ wptTypeCode = 'PA'; }
			else if (wptAltText == 'Question to Answer') 	{ wptTypeCode = 'QA'; }
			else if (wptAltText == 'Reference Point') 		{ wptTypeCode = 'RP'; }
			else if (wptAltText == 'Stages of a Multicache') { wptTypeCode = 'SM'; }
			else if (wptAltText == 'Trailhead') 				{ wptTypeCode = 'TH'; }

			// Store label.
			var gThisLabel = WptID + '[' + wptPrefix + '] - (' + wptLookup + ') WPT:' +
					wptTypeCode + ' ' + wptName + ' &nbsp; <br><b>' + CacheName + '</b>';
			if (wptNote) { gThisLabel += '<br>NOTE: ' + wptNote; }
			gThisLabel = gThisLabel.replace(/\|/g,'&#124;');
			gThisLabel = escape(toEntity(gThisLabel));
			xImg.setAttribute('label', gThisLabel);

			// If next row is for notes, expand note area.
			iRow = xRow.rowIndex;
			if (xTable.rows.length >= (iRow + 2)) {
				var xRowNext = xTable.rows[iRow + 1];
				if (xRowNext.cells.length <= 4) {
					xRowNext.cells[1].setAttribute('colspan', '2');
					if (altBorder) {
						xRowNext.setAttribute('class', 'AlternatingRow BorderBottom');
					} else {
						xRowNext.setAttribute('class', 'BorderBottom');
					}
					altBorder = !altBorder;
				}
			}
		}
	}

	// Add Google Map
	var ctlno = 0;
	do {
		var e_TopoMap = document.getElementById('ctl00_ContentBody_Waypoints_Waypoints_ctl' +
				pad(++ctlno, 2) + '_uxTopoMap');
		if (e_TopoMap) {
			e_TopoMap.parentNode.style.whiteSpace = 'nowrap';
			var gmapWpCoords = UrlParm('center', true, e_TopoMap.href);
			var gmapWpLookup = e_TopoMap.parentNode.parentNode.cells[4].firstChild.data.trim();
			var gmapWpLink = document.createElement('a');
			gmapWpLink.id = 'gmapWpLink' + pad(ctlno, 2);
			gmapWpLink.title = 'Show waypoint in Google Maps';
			gmapWpLink.style.marginLeft = '8px';
			gmapWpLink.href = 'http://maps.google.com/maps?q=' +
					gmapWpCoords + '+(' + gmapWpLookup + ')';
			gmapWpLink.target = '_blank';
			var gmapWpImg = document.createElement('img');
			gmapWpImg.src = GoogleImg;
			gmapWpLink.appendChild(gmapWpImg);
			insertAfter(gmapWpLink, e_TopoMap);
		}
	}
	while(e_TopoMap);

	// Remove useless hide/show link.
	var e_ShowHiddenCoordinates = document.getElementById("ctl00_ContentBody_Waypoints_uxShowHiddenCoordinates");
	if (e_ShowHiddenCoordinates) {
		if (e_ShowHiddenCoordinates.previousSibling.nodeName == '#text') {
			removeNode(e_ShowHiddenCoordinates.previousSibling);
		}
		removeNode(e_ShowHiddenCoordinates);
	}

	AddWaypointToLink(WptID, true);

	// Fix broken map links.
	var xPathSearch = "//a[contains(@href, '\.\.\/seek\/gmnearest\.aspx')]";
	var gmList = document.evaluate(
			xPathSearch,
			document,
			null,
			XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
			null);
	var ig = gmList.snapshotLength;
	if (ig > 0) {
		for (var i = 0; i < ig; i++) {
			var gmLink = gmList.snapshotItem(i);
			var oldHref = gmLink.href + '';
			var newHref = oldHref.replace(/&long=/g,'&lon=');
			if (newHref != oldHref) {
				gmLink.href = newHref;
				gmLink.target = '_blank';
			}
		}
	}

	// Add menu option to customize radius selections.
	GM_registerMenuCommand('Customize Additional Waypoint Radius Settings', CustomizeAWRadius);

//****************************************************************************************//
//                                                                                        //
//                         Functions                                                      //
//                                                                                        //
//****************************************************************************************//

	// Append waypoint id to all links to Add Waypoint page.
	function AddWaypointToLink(wp, newPage) {
		// Get list of additional waypoint links.
		var xPathSearch = "//a[contains(@href, '/wptlist.aspx?')]";
		var awList = document.evaluate(
				xPathSearch,
				document,
				null,
				XPathResult.ORDERED_NODE_SNAPSHOT_TYPE,
				null);
		var ic = awList.snapshotLength;

		// Append waypoint ID to the URL.
		if (ic > 0) {
			for (var i = 0; i < ic; i++) {
				var ThisLink = awList.snapshotItem(i);
				if (i == 0) {
					// Add GZ link to auto-create a Final waypoint at the posted coordinates.
					var gzwp = document.createElement('a');
					gzwp.id = 'gzwp';
					gzwp.appendChild(document.createTextNode('GZ'));
					gzwp.setAttribute('url', ThisLink.href + '&makegz=y');
					gzwp.href = 'javascript:void(0)';
					gzwp.style.marginLeft = '8px';
					gzwp.title = 'Create hidden Final waypoint at posted coordinates.';
					gzwp.addEventListener('click', fGzWpClicked, true);
					ThisLink.parentNode.appendChild(gzwp);

					var gzRefreshPage = document.createElement('script');
					gzRefreshPage.language = "javascript";
					gzRefreshPage.type = "text/javascript";
					gzRefreshPage.id = "gzRefreshPage";
					var st =
							'function fGzRefreshPage() { \n' +
							'	location.replace(location.href) \n' +
							'}';
					gzRefreshPage.appendChild(document.createTextNode(st));
					gzwp.parentNode.appendChild(gzRefreshPage);
				}
				if (UrlParm('wp', true, ThisLink.href) == '') {
						ThisLink.href += '&wp=' + wp;
				}
				if (newPage) {	ThisLink.target = '_blank'; }
			}
		}
	}

	function fGzWpClicked() {
		var url = this.getAttribute('url');
		window.open(url);
	}

	// Lock radius function.
	function LockRadius() {
		GM_setValue("AWRadiusDefault_" + SignedInAs, hDropdown.value);
		alert('"' + hDropdown.options[hDropdown.selectedIndex].text + '" set as default');
	}


	// Toggle waypoint status.
	function ToggleStatus() {
		if (this.firstChild.getAttribute('status') == '1') {
			this.firstChild.style.opacity = 0.4;
			this.firstChild.setAttribute('status', '0');
		} else {
			this.firstChild.style.opacity = 1;
			this.firstChild.setAttribute('status', '1');
		}
	}

	// Customize AW Radius Settings
	function CustomizeAWRadius() {
		var xRadList = GM_getValue("CustomAWRadius_" + SignedInAs, '');
		var xRtnVar = prompt('Enter radius codes. Separate each code group with a semicolon.\n' +
				'You must enter distance and unit, and may default the rest.\n\n' +
				'Set to blank to restore original default values.', xRadList);
		if (xRtnVar == null) { return; }
		GM_setValue("CustomAWRadius_" + SignedInAs, xRtnVar);
		alert('You need to refresh the page before\n' +
				'any changes will take effect');
	}

	// Great Circle distance calculation.
	function GrtCir(lat1, lon1, lat2, lon2) {
		var outval = '';
		var erad = 3956.665;	// Earth radius in miles.

		// Convert to radians.
		lat1 = lat1 / 180 * Math.PI;
		lat2 = lat2 / 180 * Math.PI;
		var lonDelta = (lon2 - lon1) / 180 * Math.PI;

		// Base calculation.
	    var gcWork = Math.acos(
			(Math.sin(lat1) * Math.sin(lat2)) +
			(Math.cos(lat1) * Math.cos(lat2) * Math.cos(lonDelta))
		)

		// Convert to feet or miles for output.
		var feet = erad * 5280 * gcWork;
		feet = Math.round(feet);
		var gc2far = (feet >= 15840);
		if (feet < 528) {
			outval = feet + 'ft';
		} else {
			var miles = feet / 5280;
			outval = miles.toFixed(2) + 'mi';
		}f
		return [outval, gc2far];
	}

	// Returns a URL parameter.
	//  ParmName - Parameter name to look for.
	//  IgnoreCase - (optional) *false, true. Ignore parmeter name case.
	//  UrlString - (optional) String to search. If omitted, document URL is used.
	function UrlParm(ParmName, IgnoreCase, UrlString) {
		var RegRslt, sc = '', RtnVal = '';
		if (IgnoreCase) {sc = 'i'}
		if(UrlString) {
			var PageUrl = UrlString;
		} else {
			PageUrl = document.location + '';
		}
		var ParmString = PageUrl.substring(PageUrl.indexOf('?') + 1);
		var RegEx1 = new RegExp('(^|&)' + ParmName + '=(.*?)(&|#|$)', sc);
		RegRslt = RegEx1.exec(ParmString);
		if (RegRslt) {RtnVal = RegRslt[2]}
		return RtnVal;
	}

	// Convert text above ascii 127 to entities.
	function toEntity(aa) {
		var i = 0;
		var bb = '';
		for (i=0; i<aa.length; i++) {
			if (aa.charCodeAt(i) > 127) {
				bb += '&#' + aa.charCodeAt(i) + ';';
			} else {
				bb += aa.charAt(i);
			}
		}
		return bb;
	}

	// Pad a number with leading zeros.
	function pad(number, length) {
		var str = "" + number;
		while(str.length < length) {
			str = '0' + str;
		}
		return str;
	}

	// Returns raw current domain ('www.geocaching.com' for example).
	function thisDomain() {
		var wDomain = document.location.href + '';
		var RegEx1 = new RegExp('^.*?:\/\/(.*?\..*?)($|\/)');
		RegRslt = RegEx1.exec(wDomain);
		wDomain = RegRslt[1];
		return wDomain;
	}

	// Remove element and everything below it.
	function removeNode(element) {
		element.parentNode.removeChild(element);
	}

	// Insert element after an existing element.
	function insertAfter(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement.nextSibling);
	}

	// Insert element ahead of an existing element.
	function insertAheadOf(newElement, anchorElement) {
		anchorElement.parentNode.insertBefore(newElement, anchorElement);
	}

	// Move up the DOM tree a specific number of generations.
	function fUpGen(gNode, g) {
		var gNode;
		var g;
		for (var i = 0; i < g; i++) {
			gNode = gNode.parentNode;
			if (gNode.nodeName == 'undefined') {
				gNode = null;
				break;
			}
		}
		return gNode;
	}

	// Move up the DOM tree until a specific DOM type is reached.
	function fUpGenToType(gNode, gType) {
		var gNode;
		var gType = gType.toUpperCase();
		while (gNode.nodeName.toUpperCase() != gType) {
			gNode = gNode.parentNode;
			if (gNode.nodeName == 'undefined') {
				gNode = null;
				break;
			}
		}
		return gNode;
	}
